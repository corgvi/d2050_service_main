-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: 
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!50606 SET @OLD_INNODB_STATS_AUTO_RECALC=@@INNODB_STATS_AUTO_RECALC */;
/*!50606 SET GLOBAL INNODB_STATS_AUTO_RECALC=OFF */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `idis_data`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `idis_data` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `idis_data`;

--
-- Table structure for table `add_info`
--

DROP TABLE IF EXISTS `add_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26392 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `add_info_description_language_map`
--

DROP TABLE IF EXISTS `add_info_description_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info_description_language_map` (
  `component_id` int NOT NULL,
  `add_info_id` int NOT NULL,
  `add_info_typ` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `headline` text NOT NULL,
  `description` text NOT NULL,
  `icon` varchar(255) NOT NULL,
  PRIMARY KEY (`component_id`,`add_info_id`,`add_info_typ`,`language_iso639-1`),
  KEY `fk_add_info_description_language_map_add_info_id` (`add_info_id`),
  KEY `fk_component_description_language_map_add_ino_typ_idx` (`add_info_typ`),
  CONSTRAINT `fk_add_info_description_language_map_add_info_id` FOREIGN KEY (`add_info_id`) REFERENCES `add_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_description_language_map_add_ino_typ` FOREIGN KEY (`add_info_typ`) REFERENCES `idis_glossary`.`g_add_info_typ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_description_language_map_component_id` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `add_info_files`
--

DROP TABLE IF EXISTS `add_info_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info_files` (
  `id` int NOT NULL AUTO_INCREMENT,
  `file` longblob NOT NULL,
  `file_name` text NOT NULL,
  `checksum` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `checksum_UNIQUE` (`checksum`)
) ENGINE=InnoDB AUTO_INCREMENT=15882 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `add_info_glossary_manufacturer_map`
--

DROP TABLE IF EXISTS `add_info_glossary_manufacturer_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info_glossary_manufacturer_map` (
  `add_info_id` int NOT NULL,
  `g_manufacturer_id` int NOT NULL,
  `identifier` text NOT NULL,
  PRIMARY KEY (`add_info_id`),
  KEY `fk_add_info_glossary_manufacturer_map_manufacturer1_idx` (`g_manufacturer_id`),
  CONSTRAINT `fk_add_info_glossary_manufacturer_map_manufacturer1` FOREIGN KEY (`g_manufacturer_id`) REFERENCES `idis_glossary`.`g_manufacturer` (`id`),
  CONSTRAINT `fk_add_info_glossary_manufacturer_zuordnung_add_info1` FOREIGN KEY (`add_info_id`) REFERENCES `add_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `add_info_language_map`
--

DROP TABLE IF EXISTS `add_info_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info_language_map` (
  `add_info_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `file_id` int NOT NULL,
  `file_checksum` text NOT NULL,
  PRIMARY KEY (`add_info_id`,`language_iso639-1`),
  KEY `fk_add_info_zuordnung_add_info1_idx` (`add_info_id`),
  KEY `fk_add_info_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_add_info_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `idis_glossary`.`g_language` (`iso639-1`),
  CONSTRAINT `fk_add_info_zuordnung_add_info1` FOREIGN KEY (`add_info_id`) REFERENCES `add_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `add_info_old_new_map`
--

DROP TABLE IF EXISTS `add_info_old_new_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info_old_new_map` (
  `old_add_id` int NOT NULL,
  `new_add_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `annex_pictures`
--

DROP TABLE IF EXISTS `annex_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `annex_pictures` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand_id` int NOT NULL,
  `model_id` int NOT NULL,
  `variant_id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `is_flash` tinyint DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14722 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `brake_servo_pictures`
--

DROP TABLE IF EXISTS `brake_servo_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brake_servo_pictures` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand_id` int NOT NULL,
  `model_id` int NOT NULL,
  `variant_id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `is_flash` tinyint DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=377 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `brand_model_variant_view`
--

DROP TABLE IF EXISTS `brand_model_variant_view`;
/*!50001 DROP VIEW IF EXISTS `brand_model_variant_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `brand_model_variant_view` AS SELECT 
 1 AS `language_short`,
 1 AS `brand_id`,
 1 AS `brand_name`,
 1 AS `model_id`,
 1 AS `model_name`,
 1 AS `variant_id`,
 1 AS `variant_name`,
 1 AS `period_from_month`,
 1 AS `period_from_year`,
 1 AS `period_to_month`,
 1 AS `period_to_year`,
 1 AS `status_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `changes`
--

DROP TABLE IF EXISTS `changes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `changes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `variant_id` int NOT NULL,
  `value` text NOT NULL,
  `checked` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `changes_old`
--

DROP TABLE IF EXISTS `changes_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `changes_old` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date_time` datetime DEFAULT NULL,
  `user` text,
  `variant` int DEFAULT NULL,
  `action` text,
  `table_changed` text,
  `value` text,
  `sql_used` text,
  `checked` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49872 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component`
--

DROP TABLE IF EXISTS `component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component` (
  `id` int NOT NULL AUTO_INCREMENT,
  `g_part_id` int DEFAULT NULL,
  `g_area_id` int DEFAULT NULL,
  `g_material_id` int DEFAULT NULL,
  `material_checked` tinyint(1) DEFAULT '0',
  `g_position_id` int DEFAULT NULL,
  `g_method_id` int DEFAULT NULL,
  `g_comment_id` int DEFAULT NULL,
  `weight` int DEFAULT NULL,
  `weight_unit` varchar(2) DEFAULT NULL,
  `weight_checked` tinyint(1) DEFAULT '0',
  `derivative_id` int DEFAULT NULL,
  `page_number` int DEFAULT '1',
  `quantity` int DEFAULT NULL,
  `serial_number` varchar(45) DEFAULT NULL,
  `variant_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_component_part1_idx` (`g_part_id`),
  KEY `fk_component_area1_idx` (`g_area_id`),
  KEY `fk_component_material1_idx` (`g_material_id`),
  KEY `fk_component_position1_idx` (`g_position_id`),
  KEY `fk_component_method1_idx` (`g_method_id`),
  KEY `fk_component_comment1_idx` (`g_comment_id`),
  KEY `fk_component_derivativ1_idx` (`derivative_id`),
  KEY `fk_component_variant1_idx` (`variant_id`),
  KEY `idx_component_g_part_id` (`g_part_id`),
  CONSTRAINT `fk_component_area1` FOREIGN KEY (`g_area_id`) REFERENCES `idis_glossary`.`g_area` (`id`),
  CONSTRAINT `fk_component_comment1` FOREIGN KEY (`g_comment_id`) REFERENCES `idis_glossary`.`g_comment` (`id`),
  CONSTRAINT `fk_component_derivativ1` FOREIGN KEY (`derivative_id`) REFERENCES `derivativ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_material1` FOREIGN KEY (`g_material_id`) REFERENCES `idis_glossary`.`g_material` (`id`),
  CONSTRAINT `fk_component_method1` FOREIGN KEY (`g_method_id`) REFERENCES `idis_glossary`.`g_method` (`id`),
  CONSTRAINT `fk_component_part1` FOREIGN KEY (`g_part_id`) REFERENCES `idis_glossary`.`g_part` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_position1` FOREIGN KEY (`g_position_id`) REFERENCES `idis_glossary`.`g_position` (`id`),
  CONSTRAINT `fk_component_variant1` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=835522 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_addinfo_map`
--

DROP TABLE IF EXISTS `component_addinfo_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_addinfo_map` (
  `component_id` int NOT NULL,
  `add_info_id` int NOT NULL,
  `add_info_typ` int NOT NULL,
  PRIMARY KEY (`component_id`,`add_info_typ`,`add_info_id`),
  KEY `fk_component_addinfo_map_add_info1_idx` (`add_info_id`),
  KEY `fk_component_addinfo_map_component1_idx` (`component_id`),
  KEY `fk_component_addinfo_map_g_addinfo_typ1_idx` (`add_info_typ`),
  CONSTRAINT `fk_component_addinfo_map_add_info1` FOREIGN KEY (`add_info_id`) REFERENCES `add_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_addinfo_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_addinfo_map_g_addinfo_typ1` FOREIGN KEY (`add_info_typ`) REFERENCES `idis_glossary`.`g_add_info_typ` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_fixing_map`
--

DROP TABLE IF EXISTS `component_fixing_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_fixing_map` (
  `component_id` int NOT NULL,
  `g_fixing_id` int NOT NULL,
  `quantity` int DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_component_fixing_zuordnung_component1_idx` (`component_id`),
  KEY `fk_component_fixing_map_fixing1_idx` (`g_fixing_id`),
  CONSTRAINT `fk_component_fixing_map_fixing1` FOREIGN KEY (`g_fixing_id`) REFERENCES `idis_glossary`.`g_fixing` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_fixing_zuordnung_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=366672 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_gas_map`
--

DROP TABLE IF EXISTS `component_gas_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_gas_map` (
  `component_id` int NOT NULL,
  `operation_mode` int DEFAULT NULL,
  `number_of_tanks` text,
  PRIMARY KEY (`component_id`),
  CONSTRAINT `fk_component_gas_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_gpyro_value_map`
--

DROP TABLE IF EXISTS `component_gpyro_value_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_gpyro_value_map` (
  `component_id` int NOT NULL,
  `g_pyro_id` int NOT NULL,
  `g_pyro_column_id` int NOT NULL,
  `g_pyro_column_value_id` int DEFAULT NULL,
  PRIMARY KEY (`component_id`,`g_pyro_id`,`g_pyro_column_id`),
  KEY `fk_component_gpyro_value_map_g_pyro_list_column1_idx` (`g_pyro_column_id`),
  KEY `fk_component_gpyro_value_map_g_pyro_column_value1_idx` (`g_pyro_column_value_id`),
  KEY `fk_component_gpyro_value_map_g_pyro1_idx` (`g_pyro_id`),
  CONSTRAINT `fk_component_gpyro_value_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_gpyro_value_map_g_pyro1` FOREIGN KEY (`g_pyro_id`) REFERENCES `idis_glossary`.`g_pyro` (`id`),
  CONSTRAINT `fk_component_gpyro_value_map_g_pyro_column_value1` FOREIGN KEY (`g_pyro_column_value_id`) REFERENCES `idis_glossary`.`g_pyro_column_value` (`id`),
  CONSTRAINT `fk_component_gpyro_value_map_g_pyro_list_column1` FOREIGN KEY (`g_pyro_column_id`) REFERENCES `idis_glossary`.`g_pyro_column` (`column_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_health_safety_map`
--

DROP TABLE IF EXISTS `component_health_safety_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_health_safety_map` (
  `component_id` int NOT NULL,
  `g_health_safety_id` int NOT NULL,
  PRIMARY KEY (`component_id`,`g_health_safety_id`),
  KEY `fk_component_health_safety_map_g_health_safety1_idx` (`g_health_safety_id`),
  CONSTRAINT `fk_component_health_safety_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_health_safety_map_g_health_safety1` FOREIGN KEY (`g_health_safety_id`) REFERENCES `idis_glossary`.`g_health_safety` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_picture_map`
--

DROP TABLE IF EXISTS `component_picture_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_picture_map` (
  `component_id` int NOT NULL,
  `picture` longblob NOT NULL,
  PRIMARY KEY (`component_id`),
  CONSTRAINT `component_picture_map_component_id_fk` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_pyro_adaptor_map`
--

DROP TABLE IF EXISTS `component_pyro_adaptor_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_pyro_adaptor_map` (
  `component_id` int NOT NULL,
  `pyro_adaptor_id` int NOT NULL,
  PRIMARY KEY (`component_id`,`pyro_adaptor_id`),
  KEY `fk_component_pyro_adaptor_map_pyro_adaptor1_idx` (`pyro_adaptor_id`),
  CONSTRAINT `fk_component_pyro_adaptor_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_pyro_adaptor_map_pyro_adaptor1` FOREIGN KEY (`pyro_adaptor_id`) REFERENCES `pyro_adaptor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_recycling_map`
--

DROP TABLE IF EXISTS `component_recycling_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_recycling_map` (
  `component_id` int NOT NULL,
  `g_recycling_id` int NOT NULL,
  PRIMARY KEY (`component_id`,`g_recycling_id`),
  KEY `fk_component_recycling_map_g_recycling1_idx` (`g_recycling_id`),
  CONSTRAINT `fk_component_recycling_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_recycling_map_g_recycling1` FOREIGN KEY (`g_recycling_id`) REFERENCES `idis_glossary`.`g_recycling` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_tool_map`
--

DROP TABLE IF EXISTS `component_tool_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_tool_map` (
  `component_id` int NOT NULL,
  `g_tool_id` int NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_component_tool_map_tool1_idx` (`g_tool_id`),
  KEY `fk_component_tool_zuordnung_component1` (`component_id`),
  CONSTRAINT `fk_component_tool_map_tool1` FOREIGN KEY (`g_tool_id`) REFERENCES `idis_glossary`.`g_tool` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_tool_zuordnung_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=718037 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `components_with_add_info`
--

DROP TABLE IF EXISTS `components_with_add_info`;
/*!50001 DROP VIEW IF EXISTS `components_with_add_info`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `components_with_add_info` AS SELECT 
 1 AS `id`,
 1 AS `g_part_id`,
 1 AS `add_info_id`,
 1 AS `add_info_typ`,
 1 AS `variant_id`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `has_language`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `derivativ`
--

DROP TABLE IF EXISTS `derivativ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `derivativ` (
  `id` int NOT NULL AUTO_INCREMENT,
  `variant_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_derivativ_variant1_idx` (`variant_id`),
  CONSTRAINT `fk_derivativ_variant1` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=96397 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `derivativ_g_derivativ_map`
--

DROP TABLE IF EXISTS `derivativ_g_derivativ_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `derivativ_g_derivativ_map` (
  `derivativ_id` int NOT NULL,
  `sortorder` int NOT NULL,
  `g_derivativ_id` int NOT NULL,
  PRIMARY KEY (`derivativ_id`,`sortorder`),
  KEY `fk_derivativ_glossary_derivativ_map_derivativ1_idx` (`g_derivativ_id`),
  KEY `idx_g_derivativ_id` (`g_derivativ_id`),
  CONSTRAINT `fk_derivativ_glossary_derivativ_map_derivativ1` FOREIGN KEY (`g_derivativ_id`) REFERENCES `idis_glossary`.`g_derivativ` (`id`),
  CONSTRAINT `fk_derivativ_glossary_derivativ_zuordnung_derivativ1` FOREIGN KEY (`derivativ_id`) REFERENCES `derivativ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `derivativ_g_pyro_column_text`
--

DROP TABLE IF EXISTS `derivativ_g_pyro_column_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `derivativ_g_pyro_column_text` (
  `derivativ_id` int NOT NULL,
  `g_pyro_id` int NOT NULL,
  `g_pyro_column_id` int NOT NULL,
  `text` text,
  PRIMARY KEY (`derivativ_id`,`g_pyro_column_id`,`g_pyro_id`),
  KEY `fk_derivativ_g_pyro_column_text_g_pyro_column1_idx` (`g_pyro_column_id`),
  KEY `fk_derivativ_g_pyro_column_text_g_pyro_id_idx` (`g_pyro_id`),
  CONSTRAINT `fk_derivativ_g_pyro_column_text_derivativ1` FOREIGN KEY (`derivativ_id`) REFERENCES `derivativ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_derivativ_g_pyro_column_text_g_pyro1` FOREIGN KEY (`g_pyro_id`) REFERENCES `idis_glossary`.`g_pyro` (`id`),
  CONSTRAINT `fk_derivativ_g_pyro_column_text_g_pyro_column1` FOREIGN KEY (`g_pyro_column_id`) REFERENCES `idis_glossary`.`g_pyro_column` (`column_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `derivativ_g_pyro_value_map`
--

DROP TABLE IF EXISTS `derivativ_g_pyro_value_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `derivativ_g_pyro_value_map` (
  `derivativ_id` int NOT NULL,
  `g_pyro_id` int NOT NULL,
  `g_pyro_column_id` int NOT NULL,
  `g_pyro_column_value_id` int DEFAULT NULL,
  PRIMARY KEY (`derivativ_id`,`g_pyro_id`,`g_pyro_column_id`),
  KEY `fk_derivativ_g_pyro_value_map_g_pyro_list_column1_idx` (`g_pyro_column_id`),
  KEY `fk_derivativ_g_pyro_value_map_g_pyro_column_value1_idx` (`g_pyro_column_value_id`),
  KEY `fk_derivativ_g_pyro_value_map_g_pyro1_idx` (`g_pyro_id`),
  CONSTRAINT `fk_derivativ_g_pyro_value_map_derivativ1` FOREIGN KEY (`derivativ_id`) REFERENCES `derivativ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_derivativ_g_pyro_value_map_g_pyro1` FOREIGN KEY (`g_pyro_id`) REFERENCES `idis_glossary`.`g_pyro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_derivativ_g_pyro_value_map_g_pyro_column_value1` FOREIGN KEY (`g_pyro_column_value_id`) REFERENCES `idis_glossary`.`g_pyro_column_value` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_derivativ_g_pyro_value_map_g_pyro_list_column1` FOREIGN KEY (`g_pyro_column_id`) REFERENCES `idis_glossary`.`g_pyro_column` (`column_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `derivativ_name`
--

DROP TABLE IF EXISTS `derivativ_name`;
/*!50001 DROP VIEW IF EXISTS `derivativ_name`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `derivativ_name` AS SELECT 
 1 AS `id`,
 1 AS `language_iso639-1`,
 1 AS `derivativ_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `derivatives`
--

DROP TABLE IF EXISTS `derivatives`;
/*!50001 DROP VIEW IF EXISTS `derivatives`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `derivatives` AS SELECT 
 1 AS `id`,
 1 AS `variant_id`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `sortorder`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `dismantling_pictures`
--

DROP TABLE IF EXISTS `dismantling_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dismantling_pictures` (
  `id` int NOT NULL AUTO_INCREMENT,
  `variant_id` int NOT NULL,
  `model_id` int NOT NULL,
  `brand_id` int NOT NULL,
  `area_id` int NOT NULL,
  `page_num` int NOT NULL,
  `name` text NOT NULL,
  `is_flash` tinyint DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=425136 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `fixing_qty_view`
--

DROP TABLE IF EXISTS `fixing_qty_view`;
/*!50001 DROP VIEW IF EXISTS `fixing_qty_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `fixing_qty_view` AS SELECT 
 1 AS `id`,
 1 AS `component_id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `quantity`,
 1 AS `quantityId`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `g_fixings_component_mapping_view`
--

DROP TABLE IF EXISTS `g_fixings_component_mapping_view`;
/*!50001 DROP VIEW IF EXISTS `g_fixings_component_mapping_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_fixings_component_mapping_view` AS SELECT 
 1 AS `id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `component_id`,
 1 AS `quantity`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `g_recycling_component_mapping_view`
--

DROP TABLE IF EXISTS `g_recycling_component_mapping_view`;
/*!50001 DROP VIEW IF EXISTS `g_recycling_component_mapping_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_recycling_component_mapping_view` AS SELECT 
 1 AS `id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `component_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `g_tool_component_mapping_view`
--

DROP TABLE IF EXISTS `g_tool_component_mapping_view`;
/*!50001 DROP VIEW IF EXISTS `g_tool_component_mapping_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_tool_component_mapping_view` AS SELECT 
 1 AS `id`,
 1 AS `info`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `component_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `last_selected_variants`
--

DROP TABLE IF EXISTS `last_selected_variants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `last_selected_variants` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `variant_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_id_idx` (`user_id`),
  KEY `fk_variant_id_idx` (`variant_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `idis_users`.`user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_variant_id` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=210302 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `model`
--

DROP TABLE IF EXISTS `model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model` (
  `id` int NOT NULL AUTO_INCREMENT,
  `g_brand_id` int NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_model_brand1_idx` (`g_brand_id`),
  CONSTRAINT `fk_model_brand1` FOREIGN KEY (`g_brand_id`) REFERENCES `idis_glossary`.`g_brand` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6892 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `model_language_map`
--

DROP TABLE IF EXISTS `model_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_language_map` (
  `model_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`model_id`,`language_iso639-1`),
  KEY `fk_model_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_model_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `idis_glossary`.`g_language` (`iso639-1`),
  CONSTRAINT `fk_model_language_zuordnung_model1` FOREIGN KEY (`model_id`) REFERENCES `model` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `models`
--

DROP TABLE IF EXISTS `models`;
/*!50001 DROP VIEW IF EXISTS `models`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `models` AS SELECT 
 1 AS `model_id`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `g_brand_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `part_area_view`
--

DROP TABLE IF EXISTS `part_area_view`;
/*!50001 DROP VIEW IF EXISTS `part_area_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `part_area_view` AS SELECT 
 1 AS `variant_id`,
 1 AS `id`,
 1 AS `default_picture`,
 1 AS `enabled`,
 1 AS `caution`,
 1 AS `comment`,
 1 AS `area_id`,
 1 AS `mainarea_id`,
 1 AS `picture_enabled`,
 1 AS `picture_disabled`,
 1 AS `picture_active`,
 1 AS `language_iso639-1`,
 1 AS `areaName`,
 1 AS `partName`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `part_component_view`
--

DROP TABLE IF EXISTS `part_component_view`;
/*!50001 DROP VIEW IF EXISTS `part_component_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `part_component_view` AS SELECT 
 1 AS `id`,
 1 AS `variant_id`,
 1 AS `mainarea_id`,
 1 AS `area_id`,
 1 AS `caution`,
 1 AS `component_id`,
 1 AS `quantity`,
 1 AS `derivative_id`,
 1 AS `g_material_id`,
 1 AS `text`,
 1 AS `language_iso639-1`,
 1 AS `position_id`,
 1 AS `position_text`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `part_sno_view`
--

DROP TABLE IF EXISTS `part_sno_view`;
/*!50001 DROP VIEW IF EXISTS `part_sno_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `part_sno_view` AS SELECT 
 1 AS `id`,
 1 AS `g_area_id`,
 1 AS `serial_number`,
 1 AS `variant_id`,
 1 AS `weight`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pyro_adaptor`
--

DROP TABLE IF EXISTS `pyro_adaptor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pyro_adaptor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `g_brand_id` int NOT NULL,
  `text` text,
  PRIMARY KEY (`id`),
  KEY `fk_pyro_adaptor_brand1_idx` (`g_brand_id`),
  CONSTRAINT `fk_pyro_adaptor_brand1` FOREIGN KEY (`g_brand_id`) REFERENCES `idis_glossary`.`g_brand` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `pyro_component_view`
--

DROP TABLE IF EXISTS `pyro_component_view`;
/*!50001 DROP VIEW IF EXISTS `pyro_component_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `pyro_component_view` AS SELECT 
 1 AS `variant_id`,
 1 AS `derivative_id`,
 1 AS `component_id`,
 1 AS `part_id`,
 1 AS `pyro_id`,
 1 AS `column_index`,
 1 AS `column_value`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `supercap_pictures`
--

DROP TABLE IF EXISTS `supercap_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `supercap_pictures` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand_id` int NOT NULL,
  `model_id` int NOT NULL,
  `variant_id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `is_flash` tinyint DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1082 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `user_brand_model_variant_map`
--

DROP TABLE IF EXISTS `user_brand_model_variant_map`;
/*!50001 DROP VIEW IF EXISTS `user_brand_model_variant_map`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `user_brand_model_variant_map` AS SELECT 
 1 AS `user_id`,
 1 AS `brand_id`,
 1 AS `model_id`,
 1 AS `variant_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `variant`
--

DROP TABLE IF EXISTS `variant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant` (
  `id` int NOT NULL AUTO_INCREMENT,
  `model_id` int NOT NULL,
  `variant_status_id` int NOT NULL,
  `period_from_month` int DEFAULT NULL,
  `period_from_year` int DEFAULT NULL,
  `period_to_month` int DEFAULT NULL,
  `period_to_year` int DEFAULT NULL,
  `caravan` tinyint(1) DEFAULT NULL,
  `version` int DEFAULT NULL,
  `creation_user_id` int DEFAULT NULL,
  `creation_time` datetime DEFAULT NULL,
  `first_release_user_id` int DEFAULT NULL,
  `first_release_time` datetime DEFAULT NULL,
  `last_change_user_id` int DEFAULT NULL,
  `last_change_time` datetime DEFAULT NULL,
  `prerelease_user_id` int DEFAULT NULL,
  `prerelease_time` datetime DEFAULT NULL,
  `release_user_id` int DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  `picture` longblob,
  PRIMARY KEY (`id`),
  KEY `fk_variant_model1_idx` (`model_id`),
  KEY `fk_variant_variant_status1_idx` (`variant_status_id`),
  CONSTRAINT `fk_variant_model1` FOREIGN KEY (`model_id`) REFERENCES `model` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14682 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `variant_country_map`
--

DROP TABLE IF EXISTS `variant_country_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant_country_map` (
  `variant_id` int NOT NULL,
  `g_country_id` int NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`variant_id`,`g_country_id`),
  KEY `fk_variant_country_map_country1_idx` (`g_country_id`),
  CONSTRAINT `fk_variant_country_map_country1` FOREIGN KEY (`g_country_id`) REFERENCES `idis_glossary`.`g_country` (`id`),
  CONSTRAINT `fk_variant_country_zuordnung_variant1` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `variant_language_map`
--

DROP TABLE IF EXISTS `variant_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant_language_map` (
  `variant_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`variant_id`,`language_iso639-1`),
  KEY `fk_variant_language_zuordnung_variant1_idx` (`variant_id`),
  KEY `fk_variant_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_variant_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `idis_glossary`.`g_language` (`iso639-1`),
  CONSTRAINT `fk_variant_language_zuordnung_variant1` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `variant_picture_thumbnail`
--

DROP TABLE IF EXISTS `variant_picture_thumbnail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant_picture_thumbnail` (
  `variant_id` int NOT NULL,
  `thumbnaildata` mediumblob,
  PRIMARY KEY (`variant_id`),
  CONSTRAINT `variant_picture_thumbnail_variant_id_fkey` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `variant_status`
--

DROP TABLE IF EXISTS `variant_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant_status` (
  `id` int NOT NULL,
  `logo` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `variant_status_language_map`
--

DROP TABLE IF EXISTS `variant_status_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant_status_language_map` (
  `variant_status_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`variant_status_id`,`language_iso639-1`),
  KEY `fk_variant_status_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_variant_status_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `idis_glossary`.`g_language` (`iso639-1`),
  CONSTRAINT `fk_variant_status_language_zuordnung_variant_status1` FOREIGN KEY (`variant_status_id`) REFERENCES `variant_status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `variants`
--

DROP TABLE IF EXISTS `variants`;
/*!50001 DROP VIEW IF EXISTS `variants`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `variants` AS SELECT 
 1 AS `id`,
 1 AS `model_id`,
 1 AS `variant_status_id`,
 1 AS `period_from_month`,
 1 AS `period_from_year`,
 1 AS `period_to_month`,
 1 AS `period_to_year`,
 1 AS `caravan`,
 1 AS `version`,
 1 AS `creation_user_id`,
 1 AS `creation_time`,
 1 AS `first_release_user_id`,
 1 AS `first_release_time`,
 1 AS `last_change_user_id`,
 1 AS `last_change_time`,
 1 AS `prerelease_user_id`,
 1 AS `release_user_id`,
 1 AS `release_time`,
 1 AS `picture`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `vehicle_information`
--

DROP TABLE IF EXISTS `vehicle_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vehicle_information` (
  `variant_id` int NOT NULL,
  `kerb_mass` text NOT NULL,
  `luggage_volume` text NOT NULL,
  `axle_base` text NOT NULL,
  `variant_height` text NOT NULL,
  `variant_width` text NOT NULL,
  `variant_length` text NOT NULL,
  `model_identification` text NOT NULL,
  `size_of_tire` text NOT NULL,
  `fuel_tank_capacity` text NOT NULL,
  `transmission` text NOT NULL,
  `fuel_type` text NOT NULL,
  PRIMARY KEY (`variant_id`),
  CONSTRAINT `fk_vehicle_information_variant_id1` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `idis_glossary`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `idis_glossary` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `idis_glossary`;

--
-- Temporary view structure for view `activity_areas`
--

DROP TABLE IF EXISTS `activity_areas`;
/*!50001 DROP VIEW IF EXISTS `activity_areas`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `activity_areas` AS SELECT 
 1 AS `id`,
 1 AS `mainarea_id`,
 1 AS `picture_enabled`,
 1 AS `picture_disabled`,
 1 AS `picture_active`,
 1 AS `area_id`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `brand_view`
--

DROP TABLE IF EXISTS `brand_view`;
/*!50001 DROP VIEW IF EXISTS `brand_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `brand_view` AS SELECT 
 1 AS `id`,
 1 AS `manufacturer_id`,
 1 AS `logo`,
 1 AS `active`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `button_picture_view`
--

DROP TABLE IF EXISTS `button_picture_view`;
/*!50001 DROP VIEW IF EXISTS `button_picture_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `button_picture_view` AS SELECT 
 1 AS `area_id`,
 1 AS `id`,
 1 AS `is_material`,
 1 AS `comment`,
 1 AS `sortorder`,
 1 AS `picture_comment`,
 1 AS `picture`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!50001 DROP VIEW IF EXISTS `documents`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `documents` AS SELECT 
 1 AS `id`,
 1 AS `doctype`,
 1 AS `language_iso639-1`,
 1 AS `documentText`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `g_activity`
--

DROP TABLE IF EXISTS `g_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_activity` (
  `id` int NOT NULL,
  `sortorder` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_activity_language_map`
--

DROP TABLE IF EXISTS `g_activity_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_activity_language_map` (
  `activity_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`activity_id`,`language_iso639-1`),
  KEY `fk_g_activity_language_map_iso639_1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_g_activity_language_map_activity_id1` FOREIGN KEY (`activity_id`) REFERENCES `g_activity` (`id`),
  CONSTRAINT `fk_g_activity_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_add_info_typ`
--

DROP TABLE IF EXISTS `g_add_info_typ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_add_info_typ` (
  `id` int NOT NULL,
  `is_common_document` tinyint DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_add_info_typ_language_map`
--

DROP TABLE IF EXISTS `g_add_info_typ_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_add_info_typ_language_map` (
  `add_info_typ_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  `link` text,
  PRIMARY KEY (`add_info_typ_id`,`language_iso639-1`),
  KEY `fk_add_info_typ_language_map_language1_idx` (`language_iso639-1`),
  KEY `fk_add_info_typ_language_map_add_info_typ1_idx` (`add_info_typ_id`),
  CONSTRAINT `fk_add_info_typ_language_map_add_info_typ1` FOREIGN KEY (`add_info_typ_id`) REFERENCES `g_add_info_typ` (`id`),
  CONSTRAINT `fk_add_info_typ_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_add_info_typ_part_map`
--

DROP TABLE IF EXISTS `g_add_info_typ_part_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_add_info_typ_part_map` (
  `add_info_typ_id` int NOT NULL,
  `part_id` int NOT NULL,
  PRIMARY KEY (`add_info_typ_id`,`part_id`),
  KEY `fk_add_info_typ_part_map_add_info_typ1_idx` (`add_info_typ_id`),
  KEY `fk_add_info_typ_part_map_part1_idx` (`part_id`),
  CONSTRAINT `fk_add_info_typ_part_map_add_info_typ1` FOREIGN KEY (`add_info_typ_id`) REFERENCES `g_add_info_typ` (`id`),
  CONSTRAINT `fk_add_info_typ_part_map_part1` FOREIGN KEY (`part_id`) REFERENCES `g_part` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_area`
--

DROP TABLE IF EXISTS `g_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_area` (
  `id` int NOT NULL,
  `mainarea_id` int NOT NULL,
  `picture_enabled` varchar(50) DEFAULT NULL,
  `picture_disabled` varchar(50) DEFAULT NULL,
  `picture_active` varchar(50) DEFAULT NULL,
  `tabindex` int NOT NULL,
  `tablink` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `is_mainarea` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `fk_area_mainarea1_idx` (`mainarea_id`),
  CONSTRAINT `fk_area_mainarea1` FOREIGN KEY (`mainarea_id`) REFERENCES `g_mainarea` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_area_language_map`
--

DROP TABLE IF EXISTS `g_area_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_area_language_map` (
  `area_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`area_id`,`language_iso639-1`),
  KEY `fk_area_language_map_area1_idx` (`area_id`),
  KEY `fk_area_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_area_language_map_area1` FOREIGN KEY (`area_id`) REFERENCES `g_area` (`id`),
  CONSTRAINT `fk_area_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_brand`
--

DROP TABLE IF EXISTS `g_brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_brand` (
  `id` int NOT NULL,
  `manufacturer_id` int NOT NULL,
  `logo` blob,
  `bankrupt` tinyint DEFAULT '0',
  `bankrupt_year` int DEFAULT '0',
  `active` tinyint DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_g_brand_g_manufacturer1_idx` (`manufacturer_id`),
  CONSTRAINT `fk_g_brand_g_manufacturer1` FOREIGN KEY (`manufacturer_id`) REFERENCES `g_manufacturer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_brand_g_country_map`
--

DROP TABLE IF EXISTS `g_brand_g_country_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_brand_g_country_map` (
  `g_brand_id` int NOT NULL,
  `g_country_id` int NOT NULL,
  PRIMARY KEY (`g_brand_id`,`g_country_id`),
  KEY `g_brand_g_country_map_g_country_fk_idx` (`g_country_id`),
  CONSTRAINT `g_brand_g_country_map_g_brand_fk` FOREIGN KEY (`g_brand_id`) REFERENCES `g_brand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `g_brand_g_country_map_g_country_fk` FOREIGN KEY (`g_country_id`) REFERENCES `g_country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_brand_language_map`
--

DROP TABLE IF EXISTS `g_brand_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_brand_language_map` (
  `brand_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`brand_id`,`language_iso639-1`),
  KEY `fk_brand_language_map_brand1_idx` (`brand_id`),
  KEY `fk_brand_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_brand_language_map_brand1` FOREIGN KEY (`brand_id`) REFERENCES `g_brand` (`id`),
  CONSTRAINT `fk_brand_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_button_picture`
--

DROP TABLE IF EXISTS `g_button_picture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_button_picture` (
  `id` int NOT NULL AUTO_INCREMENT,
  `comment` text,
  `picture_enabled` blob,
  `picture_disabled` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1168 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_comment`
--

DROP TABLE IF EXISTS `g_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_comment` (
  `id` int NOT NULL,
  `sortorder` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_comment_language_map`
--

DROP TABLE IF EXISTS `g_comment_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_comment_language_map` (
  `comment_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`comment_id`,`language_iso639-1`),
  KEY `fk_comment_language_map_comment1_idx` (`comment_id`),
  KEY `fk_comment_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_comment_language_map_comment1` FOREIGN KEY (`comment_id`) REFERENCES `g_comment` (`id`),
  CONSTRAINT `fk_comment_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `g_comments`
--

DROP TABLE IF EXISTS `g_comments`;
/*!50001 DROP VIEW IF EXISTS `g_comments`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_comments` AS SELECT 
 1 AS `id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `g_common_documents`
--

DROP TABLE IF EXISTS `g_common_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_common_documents` (
  `identifier` varchar(45) NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `document` longblob NOT NULL,
  `version` text,
  `show_in_addinfo_management` tinyint DEFAULT '1',
  PRIMARY KEY (`identifier`,`language_iso639-1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `g_countries`
--

DROP TABLE IF EXISTS `g_countries`;
/*!50001 DROP VIEW IF EXISTS `g_countries`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_countries` AS SELECT 
 1 AS `country_id`,
 1 AS `flag`,
 1 AS `language_iso639-1`,
 1 AS `country_name`,
 1 AS `territory_id`,
 1 AS `territory_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `g_country`
--

DROP TABLE IF EXISTS `g_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_country` (
  `id` int NOT NULL,
  `flag` longblob NOT NULL,
  `disclaimer_text_id` int DEFAULT '-1',
  `iso3166-1` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `iso3166-1_UNIQUE` (`iso3166-1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_country_language_map`
--

DROP TABLE IF EXISTS `g_country_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_country_language_map` (
  `country_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`country_id`,`language_iso639-1`),
  KEY `fk_country_language_map_country1_idx` (`country_id`),
  KEY `fk_country_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_country_language_map_country1` FOREIGN KEY (`country_id`) REFERENCES `g_country` (`id`),
  CONSTRAINT `fk_country_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_country_territory_map`
--

DROP TABLE IF EXISTS `g_country_territory_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_country_territory_map` (
  `country_id` int NOT NULL,
  `territory_id` int NOT NULL,
  PRIMARY KEY (`country_id`,`territory_id`),
  KEY `fk_country_territory_map_country1_idx` (`country_id`),
  KEY `fk_country_territory_map_territory1_idx` (`territory_id`),
  CONSTRAINT `fk_country_territory_map_country1` FOREIGN KEY (`country_id`) REFERENCES `g_country` (`id`),
  CONSTRAINT `fk_country_territory_map_territory1` FOREIGN KEY (`territory_id`) REFERENCES `g_territory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_derivativ`
--

DROP TABLE IF EXISTS `g_derivativ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_derivativ` (
  `id` int NOT NULL,
  `derivativ_group_id` int NOT NULL,
  `sortorder` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_derivativ_derivativ_group1_idx` (`derivativ_group_id`),
  CONSTRAINT `fk_derivativ_derivativ_group1` FOREIGN KEY (`derivativ_group_id`) REFERENCES `g_derivativ_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_derivativ_group`
--

DROP TABLE IF EXISTS `g_derivativ_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_derivativ_group` (
  `id` int NOT NULL,
  `sortorder` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_derivativ_group_language_map`
--

DROP TABLE IF EXISTS `g_derivativ_group_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_derivativ_group_language_map` (
  `derivativ_group_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`derivativ_group_id`,`language_iso639-1`),
  KEY `fk_derivativ_group_language_map_derivativ_group1_idx` (`derivativ_group_id`),
  KEY `fk_derivativ_group_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_derivativ_group_language_map_derivativ_group1` FOREIGN KEY (`derivativ_group_id`) REFERENCES `g_derivativ_group` (`id`),
  CONSTRAINT `fk_derivativ_group_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `g_derivativ_groups`
--

DROP TABLE IF EXISTS `g_derivativ_groups`;
/*!50001 DROP VIEW IF EXISTS `g_derivativ_groups`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_derivativ_groups` AS SELECT 
 1 AS `id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `g_derivativ_language_map`
--

DROP TABLE IF EXISTS `g_derivativ_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_derivativ_language_map` (
  `derivativ_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`derivativ_id`,`language_iso639-1`),
  KEY `fk_derivativ_language_map_derivativ1_idx` (`derivativ_id`),
  KEY `fk_derivativ_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_derivativ_language_map_derivativ1` FOREIGN KEY (`derivativ_id`) REFERENCES `g_derivativ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_derivativ_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `g_derivatives`
--

DROP TABLE IF EXISTS `g_derivatives`;
/*!50001 DROP VIEW IF EXISTS `g_derivatives`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_derivatives` AS SELECT 
 1 AS `id`,
 1 AS `derivativ_group_id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `g_document_language_map`
--

DROP TABLE IF EXISTS `g_document_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_document_language_map` (
  `document_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `documentText` text NOT NULL,
  PRIMARY KEY (`document_id`,`language_iso639-1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_documents`
--

DROP TABLE IF EXISTS `g_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_documents` (
  `id` int NOT NULL AUTO_INCREMENT,
  `doctype` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `g_families`
--

DROP TABLE IF EXISTS `g_families`;
/*!50001 DROP VIEW IF EXISTS `g_families`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_families` AS SELECT 
 1 AS `id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `g_family`
--

DROP TABLE IF EXISTS `g_family`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_family` (
  `id` int NOT NULL,
  `sortorder` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_family_language_map`
--

DROP TABLE IF EXISTS `g_family_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_family_language_map` (
  `family_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`family_id`,`language_iso639-1`),
  KEY `fk_family_language_map_family1_idx` (`family_id`),
  KEY `fk_family_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_family_language_map_family1` FOREIGN KEY (`family_id`) REFERENCES `g_family` (`id`),
  CONSTRAINT `fk_family_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_fixing`
--

DROP TABLE IF EXISTS `g_fixing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_fixing` (
  `id` int NOT NULL,
  `sortorder` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_fixing_language_map`
--

DROP TABLE IF EXISTS `g_fixing_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_fixing_language_map` (
  `fixing_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`fixing_id`,`language_iso639-1`),
  KEY `fk_fixing_language_map_fixing1_idx` (`fixing_id`),
  KEY `fk_fixing_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_fixing_language_map_fixing1` FOREIGN KEY (`fixing_id`) REFERENCES `g_fixing` (`id`),
  CONSTRAINT `fk_fixing_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `g_fixings`
--

DROP TABLE IF EXISTS `g_fixings`;
/*!50001 DROP VIEW IF EXISTS `g_fixings`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_fixings` AS SELECT 
 1 AS `id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `g_health_safety`
--

DROP TABLE IF EXISTS `g_health_safety`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_health_safety` (
  `id` int NOT NULL,
  `sortorder` int DEFAULT NULL,
  `picture` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_health_safety_group`
--

DROP TABLE IF EXISTS `g_health_safety_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_health_safety_group` (
  `id` int NOT NULL,
  `sortorder` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_health_safety_group_language_map`
--

DROP TABLE IF EXISTS `g_health_safety_group_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_health_safety_group_language_map` (
  `health_safety_group_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`health_safety_group_id`,`language_iso639-1`),
  KEY `fk_health_safety_group_language_map_health_safety_group1_idx` (`health_safety_group_id`),
  KEY `fk_health_safety_group_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_health_safety_group_language_map_health_safety_group1` FOREIGN KEY (`health_safety_group_id`) REFERENCES `g_health_safety_group` (`id`),
  CONSTRAINT `fk_health_safety_group_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_health_safety_health_safety_group_map`
--

DROP TABLE IF EXISTS `g_health_safety_health_safety_group_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_health_safety_health_safety_group_map` (
  `health_safety_id` int NOT NULL,
  `health_safety_group_id` int NOT NULL,
  PRIMARY KEY (`health_safety_id`,`health_safety_group_id`),
  KEY `fk_health_safety_health_safety_group_map_health_safety1_idx` (`health_safety_id`),
  KEY `fk_health_safety_health_safety_group_map_health_safety_grou_idx` (`health_safety_group_id`),
  CONSTRAINT `fk_health_safety_health_safety_group_map_health_safety1` FOREIGN KEY (`health_safety_id`) REFERENCES `g_health_safety` (`id`),
  CONSTRAINT `fk_health_safety_health_safety_group_map_health_safety_group1` FOREIGN KEY (`health_safety_group_id`) REFERENCES `g_health_safety_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_health_safety_language_map`
--

DROP TABLE IF EXISTS `g_health_safety_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_health_safety_language_map` (
  `health_safety_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`health_safety_id`,`language_iso639-1`),
  KEY `fk_health_safety_language_map_health_safety1_idx` (`health_safety_id`),
  KEY `fk_health_safety_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_health_safety_language_map_health_safety1` FOREIGN KEY (`health_safety_id`) REFERENCES `g_health_safety` (`id`),
  CONSTRAINT `fk_health_safety_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_health_safety_part_default_map`
--

DROP TABLE IF EXISTS `g_health_safety_part_default_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_health_safety_part_default_map` (
  `g_health_safety_id` int NOT NULL,
  `g_part_id` int NOT NULL,
  PRIMARY KEY (`g_health_safety_id`,`g_part_id`),
  KEY `fk_g_health_safety_part_default_map_g_part1_idx` (`g_part_id`),
  CONSTRAINT `fk_g_health_safety_part_default_map_g_health_safety1` FOREIGN KEY (`g_health_safety_id`) REFERENCES `g_health_safety` (`id`),
  CONSTRAINT `fk_g_health_safety_part_default_map_g_part1` FOREIGN KEY (`g_part_id`) REFERENCES `g_part` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_language`
--

DROP TABLE IF EXISTS `g_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_language` (
  `iso639-1` varchar(2) NOT NULL,
  `iso639-2` varchar(3) DEFAULT NULL,
  `ietf` text,
  `active` varchar(45) DEFAULT NULL,
  `flag` longblob,
  PRIMARY KEY (`iso639-1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_language_language_map`
--

DROP TABLE IF EXISTS `g_language_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_language_language_map` (
  `language_iso639-1` varchar(2) NOT NULL,
  `translation_language_iso639-1` varchar(2) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`language_iso639-1`,`translation_language_iso639-1`),
  KEY `fk_language_language_map_language1_idx` (`language_iso639-1`),
  KEY `fk_language_language_map_language2_idx` (`translation_language_iso639-1`),
  CONSTRAINT `fk_language_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`),
  CONSTRAINT `fk_language_language_map_language2` FOREIGN KEY (`translation_language_iso639-1`) REFERENCES `g_language` (`iso639-1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_mainarea`
--

DROP TABLE IF EXISTS `g_mainarea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_mainarea` (
  `id` int NOT NULL,
  `picture_enabled` blob,
  `picture_disabled` blob,
  `picture_active` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_mainarea_button_material`
--

DROP TABLE IF EXISTS `g_mainarea_button_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_mainarea_button_material` (
  `area_id` int NOT NULL,
  `material_id` int NOT NULL,
  `button_picture_id` int NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `comment` text,
  `sortorder` int DEFAULT NULL,
  PRIMARY KEY (`area_id`,`material_id`,`button_picture_id`),
  KEY `fk_mainarea_button_material_mainarea1_idx` (`area_id`),
  KEY `fk_mainarea_button_material_button_picture1_idx` (`button_picture_id`),
  KEY `fk_mainarea_button_material_material1_idx` (`material_id`),
  CONSTRAINT `fk_mainarea_button_material_button_picture1` FOREIGN KEY (`button_picture_id`) REFERENCES `g_button_picture` (`id`),
  CONSTRAINT `fk_mainarea_button_material_mainarea1` FOREIGN KEY (`area_id`) REFERENCES `g_mainarea` (`id`),
  CONSTRAINT `fk_mainarea_button_material_material1` FOREIGN KEY (`material_id`) REFERENCES `g_material` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_mainarea_button_part`
--

DROP TABLE IF EXISTS `g_mainarea_button_part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_mainarea_button_part` (
  `area_id` int NOT NULL,
  `part_id` int NOT NULL,
  `button_picture_id` int NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `comment` text,
  `sortorder` int DEFAULT NULL,
  PRIMARY KEY (`area_id`,`part_id`,`button_picture_id`),
  KEY `fk_mainarea_button_part_mainarea1_idx` (`area_id`),
  KEY `fk_mainarea_button_part_part1_idx` (`part_id`),
  KEY `fk_mainarea_button_part_button_picture1_idx` (`button_picture_id`),
  CONSTRAINT `fk_mainarea_button_part_button_picture1` FOREIGN KEY (`button_picture_id`) REFERENCES `g_button_picture` (`id`),
  CONSTRAINT `fk_mainarea_button_part_g_area1` FOREIGN KEY (`area_id`) REFERENCES `g_area` (`id`),
  CONSTRAINT `fk_mainarea_button_part_part1` FOREIGN KEY (`part_id`) REFERENCES `g_part` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_mainarea_language_map`
--

DROP TABLE IF EXISTS `g_mainarea_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_mainarea_language_map` (
  `mainarea_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`mainarea_id`,`language_iso639-1`),
  KEY `fk_mainarea_language_map_mainarea1_idx` (`mainarea_id`),
  KEY `fk_mainarea_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_mainarea_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`),
  CONSTRAINT `fk_mainarea_language_map_mainarea1` FOREIGN KEY (`mainarea_id`) REFERENCES `g_mainarea` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_manufacturer`
--

DROP TABLE IF EXISTS `g_manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_manufacturer` (
  `id` int NOT NULL,
  `sortorder` int DEFAULT NULL,
  `logo` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_manufacturer_language_map`
--

DROP TABLE IF EXISTS `g_manufacturer_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_manufacturer_language_map` (
  `manufacturer_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`manufacturer_id`,`language_iso639-1`),
  KEY `fk_manufacturer_language_map_manufacturer1_idx` (`manufacturer_id`),
  KEY `fk_manufacturer_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_manufacturer_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`),
  CONSTRAINT `fk_manufacturer_language_map_manufacturer1` FOREIGN KEY (`manufacturer_id`) REFERENCES `g_manufacturer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_material`
--

DROP TABLE IF EXISTS `g_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_material` (
  `id` int NOT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `caution` text,
  `comment` text,
  `g_family_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_material_language_map`
--

DROP TABLE IF EXISTS `g_material_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_material_language_map` (
  `material_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`material_id`,`language_iso639-1`),
  KEY `fk_material_language_map_material1_idx` (`material_id`),
  KEY `fk_material_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_material_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_material_language_map_material1` FOREIGN KEY (`material_id`) REFERENCES `g_material` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `g_materials`
--

DROP TABLE IF EXISTS `g_materials`;
/*!50001 DROP VIEW IF EXISTS `g_materials`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_materials` AS SELECT 
 1 AS `id`,
 1 AS `enabled`,
 1 AS `caution`,
 1 AS `comment`,
 1 AS `g_family_id`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `g_method`
--

DROP TABLE IF EXISTS `g_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_method` (
  `id` int NOT NULL,
  `sortorder` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_method_language_map`
--

DROP TABLE IF EXISTS `g_method_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_method_language_map` (
  `method_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`method_id`,`language_iso639-1`),
  KEY `fk_method_language_map_method1_idx` (`method_id`),
  KEY `fk_method_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_method_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`),
  CONSTRAINT `fk_method_language_map_method1` FOREIGN KEY (`method_id`) REFERENCES `g_method` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `g_methods`
--

DROP TABLE IF EXISTS `g_methods`;
/*!50001 DROP VIEW IF EXISTS `g_methods`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_methods` AS SELECT 
 1 AS `id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `g_part`
--

DROP TABLE IF EXISTS `g_part`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_part` (
  `id` int NOT NULL,
  `default_picture` blob,
  `enabled` tinyint(1) DEFAULT NULL,
  `caution` text,
  `comment` text,
  `picture_path` text,
  `g_area_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_g_part_g_area_id` (`g_area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_part_language_map`
--

DROP TABLE IF EXISTS `g_part_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_part_language_map` (
  `part_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`part_id`,`language_iso639-1`),
  KEY `fk_part_language_map_part1_idx` (`part_id`),
  KEY `fk_part_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_part_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`),
  CONSTRAINT `fk_part_language_map_part1` FOREIGN KEY (`part_id`) REFERENCES `g_part` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `g_parts`
--

DROP TABLE IF EXISTS `g_parts`;
/*!50001 DROP VIEW IF EXISTS `g_parts`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_parts` AS SELECT 
 1 AS `id`,
 1 AS `default_picture`,
 1 AS `enabled`,
 1 AS `caution`,
 1 AS `comment`,
 1 AS `picture_path`,
 1 AS `mainarea_id`,
 1 AS `area_id`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `g_position`
--

DROP TABLE IF EXISTS `g_position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_position` (
  `id` int NOT NULL,
  `sortorder` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_position_language_map`
--

DROP TABLE IF EXISTS `g_position_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_position_language_map` (
  `position_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`position_id`,`language_iso639-1`),
  KEY `fk_position_language_map_position1_idx` (`position_id`),
  KEY `fk_position_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_position_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`),
  CONSTRAINT `fk_position_language_map_position1` FOREIGN KEY (`position_id`) REFERENCES `g_position` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `g_positions`
--

DROP TABLE IF EXISTS `g_positions`;
/*!50001 DROP VIEW IF EXISTS `g_positions`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_positions` AS SELECT 
 1 AS `id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `g_pyro`
--

DROP TABLE IF EXISTS `g_pyro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_pyro` (
  `id` int NOT NULL,
  `list` int DEFAULT NULL,
  `legend` varchar(5) DEFAULT NULL,
  `adapter_enabled` tinyint(1) DEFAULT NULL,
  `sortorder` int DEFAULT NULL,
  `g_part_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_pyro_column`
--

DROP TABLE IF EXISTS `g_pyro_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_pyro_column` (
  `column_id` int NOT NULL AUTO_INCREMENT,
  `list` int DEFAULT NULL,
  `xml` text,
  `sortorder` int DEFAULT NULL,
  PRIMARY KEY (`column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_pyro_column_language_map`
--

DROP TABLE IF EXISTS `g_pyro_column_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_pyro_column_language_map` (
  `g_pyro_column_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`g_pyro_column_id`,`language_iso639-1`),
  KEY `fk_g_pyro_column_language_map_g_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_g_pyro_column_language_map_g_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`),
  CONSTRAINT `fk_g_pyro_column_language_map_g_pyro_column1` FOREIGN KEY (`g_pyro_column_id`) REFERENCES `g_pyro_column` (`column_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_pyro_column_value`
--

DROP TABLE IF EXISTS `g_pyro_column_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_pyro_column_value` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pyro_column_id` int DEFAULT NULL,
  `short` text,
  `sortorder` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=263 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_pyro_column_value_language_map`
--

DROP TABLE IF EXISTS `g_pyro_column_value_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_pyro_column_value_language_map` (
  `g_pyro_column_value_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`g_pyro_column_value_id`,`language_iso639-1`),
  KEY `fk_pyro_columnvalue_language_map_pyro_column1_idx` (`g_pyro_column_value_id`),
  KEY `fk_pyro_columnvalue_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_pyro_columnvalue_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`),
  CONSTRAINT `fk_pyro_columnvalue_language_map_pyro_columnvalue1` FOREIGN KEY (`g_pyro_column_value_id`) REFERENCES `g_pyro_column_value` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_pyro_coord`
--

DROP TABLE IF EXISTS `g_pyro_coord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_pyro_coord` (
  `g_pyro_position_id` int NOT NULL,
  `sort_order` int NOT NULL,
  `x` int DEFAULT NULL,
  `y` int DEFAULT NULL,
  PRIMARY KEY (`g_pyro_position_id`,`sort_order`),
  CONSTRAINT `fk_g_pyro_coord_g_pyro_position1` FOREIGN KEY (`g_pyro_position_id`) REFERENCES `g_pyro_position` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_pyro_language_map`
--

DROP TABLE IF EXISTS `g_pyro_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_pyro_language_map` (
  `pyro_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`pyro_id`,`language_iso639-1`),
  KEY `fk_pyro_language_map_pyro1_idx` (`pyro_id`),
  KEY `fk_pyro_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_pyro_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`),
  CONSTRAINT `fk_pyro_language_map_pyro1` FOREIGN KEY (`pyro_id`) REFERENCES `g_pyro` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_pyro_position`
--

DROP TABLE IF EXISTS `g_pyro_position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_pyro_position` (
  `id` int NOT NULL AUTO_INCREMENT,
  `g_pyro_id` int NOT NULL,
  `position` int NOT NULL,
  PRIMARY KEY (`id`,`g_pyro_id`,`position`),
  KEY `fk_g_pyro_position_g_pyro1_idx` (`g_pyro_id`),
  CONSTRAINT `fk_g_pyro_position_g_pyro1` FOREIGN KEY (`g_pyro_id`) REFERENCES `g_pyro` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_recycling`
--

DROP TABLE IF EXISTS `g_recycling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_recycling` (
  `id` int NOT NULL,
  `sortorder` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_recycling_language_map`
--

DROP TABLE IF EXISTS `g_recycling_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_recycling_language_map` (
  `recycling_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`recycling_id`,`language_iso639-1`),
  KEY `fk_recycling_language_map_recycling1_idx` (`recycling_id`),
  KEY `fk_recycling_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_recycling_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`),
  CONSTRAINT `fk_recycling_language_map_recycling1` FOREIGN KEY (`recycling_id`) REFERENCES `g_recycling` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `g_recyclings`
--

DROP TABLE IF EXISTS `g_recyclings`;
/*!50001 DROP VIEW IF EXISTS `g_recyclings`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_recyclings` AS SELECT 
 1 AS `id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `g_territory`
--

DROP TABLE IF EXISTS `g_territory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_territory` (
  `id` int NOT NULL,
  `sortorder` int NOT NULL DEFAULT '99',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_territory_language_map`
--

DROP TABLE IF EXISTS `g_territory_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_territory_language_map` (
  `territory_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`territory_id`,`language_iso639-1`),
  KEY `fk_territory_language_map_territory1_idx` (`territory_id`),
  KEY `fk_territory_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_territory_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`),
  CONSTRAINT `fk_territory_language_map_territory1` FOREIGN KEY (`territory_id`) REFERENCES `g_territory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_tool`
--

DROP TABLE IF EXISTS `g_tool`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_tool` (
  `id` int NOT NULL,
  `info` longblob,
  `sortorder` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_tool_language_map`
--

DROP TABLE IF EXISTS `g_tool_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_tool_language_map` (
  `tool_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`tool_id`,`language_iso639-1`),
  KEY `fk_tool_language_map_tool1_idx` (`tool_id`),
  KEY `fk_tool_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_tool_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`),
  CONSTRAINT `fk_tool_language_map_tool1` FOREIGN KEY (`tool_id`) REFERENCES `g_tool` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `g_tools`
--

DROP TABLE IF EXISTS `g_tools`;
/*!50001 DROP VIEW IF EXISTS `g_tools`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_tools` AS SELECT 
 1 AS `id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `g_translation`
--

DROP TABLE IF EXISTS `g_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_translation` (
  `id` int NOT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_translation_language_map`
--

DROP TABLE IF EXISTS `g_translation_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_translation_language_map` (
  `translation_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`translation_id`,`language_iso639-1`),
  KEY `fk_translation_language_map_translation1_idx` (`translation_id`),
  KEY `fk_translation_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_translation_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_translation_language_map_translation1` FOREIGN KEY (`translation_id`) REFERENCES `g_translation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `g_world_countries`
--

DROP TABLE IF EXISTS `g_world_countries`;
/*!50001 DROP VIEW IF EXISTS `g_world_countries`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_world_countries` AS SELECT 
 1 AS `world_country_id`,
 1 AS `language_iso639-1`,
 1 AS `country_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `g_world_country`
--

DROP TABLE IF EXISTS `g_world_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_world_country` (
  `id` int NOT NULL,
  `iso3166-2` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_world_country_language_map`
--

DROP TABLE IF EXISTS `g_world_country_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_world_country_language_map` (
  `world_country_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`world_country_id`,`language_iso639-1`),
  KEY `fk_world_country_language_map_country1_idx` (`world_country_id`),
  KEY `fk_world_country_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_world_country_language_map_country1` FOREIGN KEY (`world_country_id`) REFERENCES `g_world_country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_world_country_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_world_territory`
--

DROP TABLE IF EXISTS `g_world_territory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_world_territory` (
  `id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_world_territory_country_map`
--

DROP TABLE IF EXISTS `g_world_territory_country_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_world_territory_country_map` (
  `world_territory_id` int NOT NULL,
  `world_country_id` int NOT NULL,
  PRIMARY KEY (`world_territory_id`,`world_country_id`),
  KEY `g_world_territory_country_map_country_fkey_idx` (`world_country_id`),
  CONSTRAINT `g_world_territory_country_map_country_fkey` FOREIGN KEY (`world_country_id`) REFERENCES `g_world_country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `g_world_territory_country_map_territory_fkey` FOREIGN KEY (`world_territory_id`) REFERENCES `g_world_territory` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_world_territory_language_map`
--

DROP TABLE IF EXISTS `g_world_territory_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_world_territory_language_map` (
  `world_territory_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`world_territory_id`,`language_iso639-1`),
  KEY `g_world_territory_language_map_language_fkey_idx` (`language_iso639-1`),
  CONSTRAINT `g_world_territory_language_map_id_fkey` FOREIGN KEY (`world_territory_id`) REFERENCES `g_world_territory` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `g_world_territory_language_map_language_fkey` FOREIGN KEY (`language_iso639-1`) REFERENCES `g_language` (`iso639-1`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `language_original_language_view`
--

DROP TABLE IF EXISTS `language_original_language_view`;
/*!50001 DROP VIEW IF EXISTS `language_original_language_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `language_original_language_view` AS SELECT 
 1 AS `language_iso639-1`,
 1 AS `translation_language_iso639-1`,
 1 AS `text`,
 1 AS `original_lang`,
 1 AS `english`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `manufacturer_view`
--

DROP TABLE IF EXISTS `manufacturer_view`;
/*!50001 DROP VIEW IF EXISTS `manufacturer_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `manufacturer_view` AS SELECT 
 1 AS `id`,
 1 AS `logo`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `missing_translations`
--

DROP TABLE IF EXISTS `missing_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `missing_translations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `missing_translation` text,
  `missing_language` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3431 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `old_sys__languages`
--

DROP TABLE IF EXISTS `old_sys__languages`;
/*!50001 DROP VIEW IF EXISTS `old_sys__languages`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `old_sys__languages` AS SELECT 
 1 AS `id`,
 1 AS `table_id`,
 1 AS `en`,
 1 AS `de`,
 1 AS `bg`,
 1 AS `cs`,
 1 AS `da`,
 1 AS `el`,
 1 AS `es`,
 1 AS `et`,
 1 AS `fi`,
 1 AS `fr`,
 1 AS `hr`,
 1 AS `hu`,
 1 AS `is`,
 1 AS `it`,
 1 AS `ko`,
 1 AS `lt`,
 1 AS `lv`,
 1 AS `mk`,
 1 AS `nl`,
 1 AS `no`,
 1 AS `pl`,
 1 AS `pt`,
 1 AS `ro`,
 1 AS `ru`,
 1 AS `sk`,
 1 AS `sl`,
 1 AS `sq`,
 1 AS `sr`,
 1 AS `sv`,
 1 AS `tr`,
 1 AS `zh`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `old_sys__translation_table`
--

DROP TABLE IF EXISTS `old_sys__translation_table`;
/*!50001 DROP VIEW IF EXISTS `old_sys__translation_table`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `old_sys__translation_table` AS SELECT 
 1 AS `id`,
 1 AS `text`,
 1 AS `en`,
 1 AS `de`,
 1 AS `bg`,
 1 AS `cs`,
 1 AS `da`,
 1 AS `el`,
 1 AS `es`,
 1 AS `et`,
 1 AS `fi`,
 1 AS `fr`,
 1 AS `hr`,
 1 AS `hu`,
 1 AS `is`,
 1 AS `it`,
 1 AS `ko`,
 1 AS `lt`,
 1 AS `lv`,
 1 AS `mk`,
 1 AS `nl`,
 1 AS `no`,
 1 AS `pl`,
 1 AS `pt`,
 1 AS `ro`,
 1 AS `ru`,
 1 AS `sk`,
 1 AS `sl`,
 1 AS `sq`,
 1 AS `sr`,
 1 AS `sv`,
 1 AS `tr`,
 1 AS `zh`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `part_quantity_view`
--

DROP TABLE IF EXISTS `part_quantity_view`;
/*!50001 DROP VIEW IF EXISTS `part_quantity_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `part_quantity_view` AS SELECT 
 1 AS `id`,
 1 AS `area_id`,
 1 AS `caution`,
 1 AS `text`,
 1 AS `language_iso639-1`,
 1 AS `mainarea_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `systemsetting`
--

DROP TABLE IF EXISTS `systemsetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `systemsetting` (
  `id` varchar(15) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `translations_view`
--

DROP TABLE IF EXISTS `translations_view`;
/*!50001 DROP VIEW IF EXISTS `translations_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `translations_view` AS SELECT 
 1 AS `id`,
 1 AS `english`,
 1 AS `text`,
 1 AS `language_iso639-1`*/;
SET character_set_client = @saved_cs_client;

--
-- Current Database: `idis_mastertest`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `idis_mastertest` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `idis_mastertest`;

--
-- Table structure for table `add_info`
--

DROP TABLE IF EXISTS `add_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26392 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `add_info_description_language_map`
--

DROP TABLE IF EXISTS `add_info_description_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info_description_language_map` (
  `component_id` int NOT NULL,
  `add_info_id` int NOT NULL,
  `add_info_typ` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `headline` text NOT NULL,
  `description` text NOT NULL,
  `icon` varchar(255) NOT NULL,
  PRIMARY KEY (`component_id`,`add_info_id`,`add_info_typ`,`language_iso639-1`),
  KEY `fk_add_info_description_language_map_add_info_id` (`add_info_id`),
  KEY `fk_component_description_language_map_add_ino_typ_idx` (`add_info_typ`),
  CONSTRAINT `fk_add_info_description_language_map_add_info_id` FOREIGN KEY (`add_info_id`) REFERENCES `add_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_description_language_map_add_ino_typ` FOREIGN KEY (`add_info_typ`) REFERENCES `idis_glossary`.`g_add_info_typ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_description_language_map_component_id` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `add_info_files`
--

DROP TABLE IF EXISTS `add_info_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info_files` (
  `id` int NOT NULL AUTO_INCREMENT,
  `file` longblob NOT NULL,
  `file_name` text NOT NULL,
  `checksum` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `checksum_UNIQUE` (`checksum`)
) ENGINE=InnoDB AUTO_INCREMENT=15882 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `add_info_glossary_manufacturer_map`
--

DROP TABLE IF EXISTS `add_info_glossary_manufacturer_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info_glossary_manufacturer_map` (
  `add_info_id` int NOT NULL,
  `g_manufacturer_id` int NOT NULL,
  `identifier` text NOT NULL,
  PRIMARY KEY (`add_info_id`),
  KEY `fk_add_info_glossary_manufacturer_map_manufacturer1_idx` (`g_manufacturer_id`),
  CONSTRAINT `fk_add_info_glossary_manufacturer_map_manufacturer1` FOREIGN KEY (`g_manufacturer_id`) REFERENCES `idis_glossary`.`g_manufacturer` (`id`),
  CONSTRAINT `fk_add_info_glossary_manufacturer_zuordnung_add_info1` FOREIGN KEY (`add_info_id`) REFERENCES `add_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `add_info_language_map`
--

DROP TABLE IF EXISTS `add_info_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info_language_map` (
  `add_info_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `file_id` int NOT NULL,
  `file_checksum` text NOT NULL,
  PRIMARY KEY (`add_info_id`,`language_iso639-1`),
  KEY `fk_add_info_zuordnung_add_info1_idx` (`add_info_id`),
  KEY `fk_add_info_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_add_info_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `idis_glossary`.`g_language` (`iso639-1`),
  CONSTRAINT `fk_add_info_zuordnung_add_info1` FOREIGN KEY (`add_info_id`) REFERENCES `add_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `add_info_old_new_map`
--

DROP TABLE IF EXISTS `add_info_old_new_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info_old_new_map` (
  `old_add_id` int NOT NULL,
  `new_add_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `annex_pictures`
--

DROP TABLE IF EXISTS `annex_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `annex_pictures` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand_id` int NOT NULL,
  `model_id` int NOT NULL,
  `variant_id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `is_flash` tinyint DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15127 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `brake_servo_pictures`
--

DROP TABLE IF EXISTS `brake_servo_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brake_servo_pictures` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand_id` int NOT NULL,
  `model_id` int NOT NULL,
  `variant_id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `is_flash` tinyint DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=407 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `brand_model_variant_view`
--

DROP TABLE IF EXISTS `brand_model_variant_view`;
/*!50001 DROP VIEW IF EXISTS `brand_model_variant_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `brand_model_variant_view` AS SELECT 
 1 AS `language_short`,
 1 AS `brand_id`,
 1 AS `brand_name`,
 1 AS `model_id`,
 1 AS `model_name`,
 1 AS `variant_id`,
 1 AS `variant_name`,
 1 AS `period_from_month`,
 1 AS `period_from_year`,
 1 AS `period_to_month`,
 1 AS `period_to_year`,
 1 AS `status_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `changes`
--

DROP TABLE IF EXISTS `changes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `changes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `variant_id` int NOT NULL,
  `value` text NOT NULL,
  `checked` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `changes_old`
--

DROP TABLE IF EXISTS `changes_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `changes_old` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date_time` datetime DEFAULT NULL,
  `user` text,
  `variant` int DEFAULT NULL,
  `action` text,
  `table_changed` text,
  `value` text,
  `sql_used` text,
  `checked` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49872 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component`
--

DROP TABLE IF EXISTS `component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component` (
  `id` int NOT NULL AUTO_INCREMENT,
  `g_part_id` int DEFAULT NULL,
  `g_area_id` int DEFAULT NULL,
  `g_material_id` int DEFAULT NULL,
  `material_checked` tinyint(1) DEFAULT '0',
  `g_position_id` int DEFAULT NULL,
  `g_method_id` int DEFAULT NULL,
  `g_comment_id` int DEFAULT NULL,
  `weight` int DEFAULT NULL,
  `weight_unit` varchar(2) DEFAULT NULL,
  `weight_checked` tinyint(1) DEFAULT '0',
  `derivative_id` int DEFAULT NULL,
  `page_number` int DEFAULT '1',
  `quantity` int DEFAULT NULL,
  `serial_number` varchar(45) DEFAULT NULL,
  `variant_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_component_part1_idx` (`g_part_id`),
  KEY `fk_component_area1_idx` (`g_area_id`),
  KEY `fk_component_material1_idx` (`g_material_id`),
  KEY `fk_component_position1_idx` (`g_position_id`),
  KEY `fk_component_method1_idx` (`g_method_id`),
  KEY `fk_component_comment1_idx` (`g_comment_id`),
  KEY `fk_component_derivativ1_idx` (`derivative_id`),
  KEY `fk_component_variant1_idx` (`variant_id`),
  KEY `idx_component_g_part_id` (`g_part_id`),
  CONSTRAINT `fk_component_area1` FOREIGN KEY (`g_area_id`) REFERENCES `idis_glossary`.`g_area` (`id`),
  CONSTRAINT `fk_component_comment1` FOREIGN KEY (`g_comment_id`) REFERENCES `idis_glossary`.`g_comment` (`id`),
  CONSTRAINT `fk_component_derivativ1` FOREIGN KEY (`derivative_id`) REFERENCES `derivativ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_material1` FOREIGN KEY (`g_material_id`) REFERENCES `idis_glossary`.`g_material` (`id`),
  CONSTRAINT `fk_component_method1` FOREIGN KEY (`g_method_id`) REFERENCES `idis_glossary`.`g_method` (`id`),
  CONSTRAINT `fk_component_part1` FOREIGN KEY (`g_part_id`) REFERENCES `idis_glossary`.`g_part` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_position1` FOREIGN KEY (`g_position_id`) REFERENCES `idis_glossary`.`g_position` (`id`),
  CONSTRAINT `fk_component_variant1` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=836317 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_addinfo_map`
--

DROP TABLE IF EXISTS `component_addinfo_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_addinfo_map` (
  `component_id` int NOT NULL,
  `add_info_id` int NOT NULL,
  `add_info_typ` int NOT NULL,
  PRIMARY KEY (`component_id`,`add_info_typ`,`add_info_id`),
  KEY `fk_component_addinfo_map_add_info1_idx` (`add_info_id`),
  KEY `fk_component_addinfo_map_component1_idx` (`component_id`),
  KEY `fk_component_addinfo_map_g_addinfo_typ1_idx` (`add_info_typ`),
  CONSTRAINT `fk_component_addinfo_map_add_info1` FOREIGN KEY (`add_info_id`) REFERENCES `add_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_addinfo_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_addinfo_map_g_addinfo_typ1` FOREIGN KEY (`add_info_typ`) REFERENCES `idis_glossary`.`g_add_info_typ` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_fixing_map`
--

DROP TABLE IF EXISTS `component_fixing_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_fixing_map` (
  `component_id` int NOT NULL,
  `g_fixing_id` int NOT NULL,
  `quantity` int DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_component_fixing_zuordnung_component1_idx` (`component_id`),
  KEY `fk_component_fixing_map_fixing1_idx` (`g_fixing_id`),
  CONSTRAINT `fk_component_fixing_map_fixing1` FOREIGN KEY (`g_fixing_id`) REFERENCES `idis_glossary`.`g_fixing` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_fixing_zuordnung_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=367557 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_gas_map`
--

DROP TABLE IF EXISTS `component_gas_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_gas_map` (
  `component_id` int NOT NULL,
  `operation_mode` int DEFAULT NULL,
  `number_of_tanks` text,
  PRIMARY KEY (`component_id`),
  CONSTRAINT `fk_component_gas_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_gpyro_value_map`
--

DROP TABLE IF EXISTS `component_gpyro_value_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_gpyro_value_map` (
  `component_id` int NOT NULL,
  `g_pyro_id` int NOT NULL,
  `g_pyro_column_id` int NOT NULL,
  `g_pyro_column_value_id` int DEFAULT NULL,
  PRIMARY KEY (`component_id`,`g_pyro_id`,`g_pyro_column_id`),
  KEY `fk_component_gpyro_value_map_g_pyro_list_column1_idx` (`g_pyro_column_id`),
  KEY `fk_component_gpyro_value_map_g_pyro_column_value1_idx` (`g_pyro_column_value_id`),
  KEY `fk_component_gpyro_value_map_g_pyro1_idx` (`g_pyro_id`),
  CONSTRAINT `fk_component_gpyro_value_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_gpyro_value_map_g_pyro1` FOREIGN KEY (`g_pyro_id`) REFERENCES `idis_glossary`.`g_pyro` (`id`),
  CONSTRAINT `fk_component_gpyro_value_map_g_pyro_column_value1` FOREIGN KEY (`g_pyro_column_value_id`) REFERENCES `idis_glossary`.`g_pyro_column_value` (`id`),
  CONSTRAINT `fk_component_gpyro_value_map_g_pyro_list_column1` FOREIGN KEY (`g_pyro_column_id`) REFERENCES `idis_glossary`.`g_pyro_column` (`column_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_health_safety_map`
--

DROP TABLE IF EXISTS `component_health_safety_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_health_safety_map` (
  `component_id` int NOT NULL,
  `g_health_safety_id` int NOT NULL,
  PRIMARY KEY (`component_id`,`g_health_safety_id`),
  KEY `fk_component_health_safety_map_g_health_safety1_idx` (`g_health_safety_id`),
  CONSTRAINT `fk_component_health_safety_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_health_safety_map_g_health_safety1` FOREIGN KEY (`g_health_safety_id`) REFERENCES `idis_glossary`.`g_health_safety` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_picture_map`
--

DROP TABLE IF EXISTS `component_picture_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_picture_map` (
  `component_id` int NOT NULL,
  `picture` longblob NOT NULL,
  PRIMARY KEY (`component_id`),
  CONSTRAINT `component_picture_map_component_id_fk` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_pyro_adaptor_map`
--

DROP TABLE IF EXISTS `component_pyro_adaptor_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_pyro_adaptor_map` (
  `component_id` int NOT NULL,
  `pyro_adaptor_id` int NOT NULL,
  PRIMARY KEY (`component_id`,`pyro_adaptor_id`),
  KEY `fk_component_pyro_adaptor_map_pyro_adaptor1_idx` (`pyro_adaptor_id`),
  CONSTRAINT `fk_component_pyro_adaptor_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_pyro_adaptor_map_pyro_adaptor1` FOREIGN KEY (`pyro_adaptor_id`) REFERENCES `pyro_adaptor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_recycling_map`
--

DROP TABLE IF EXISTS `component_recycling_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_recycling_map` (
  `component_id` int NOT NULL,
  `g_recycling_id` int NOT NULL,
  PRIMARY KEY (`component_id`,`g_recycling_id`),
  KEY `fk_component_recycling_map_g_recycling1_idx` (`g_recycling_id`),
  CONSTRAINT `fk_component_recycling_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_recycling_map_g_recycling1` FOREIGN KEY (`g_recycling_id`) REFERENCES `idis_glossary`.`g_recycling` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_tool_map`
--

DROP TABLE IF EXISTS `component_tool_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_tool_map` (
  `component_id` int NOT NULL,
  `g_tool_id` int NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_component_tool_map_tool1_idx` (`g_tool_id`),
  KEY `fk_component_tool_zuordnung_component1` (`component_id`),
  CONSTRAINT `fk_component_tool_map_tool1` FOREIGN KEY (`g_tool_id`) REFERENCES `idis_glossary`.`g_tool` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_tool_zuordnung_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=718707 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `components_with_add_info`
--

DROP TABLE IF EXISTS `components_with_add_info`;
/*!50001 DROP VIEW IF EXISTS `components_with_add_info`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `components_with_add_info` AS SELECT 
 1 AS `id`,
 1 AS `g_part_id`,
 1 AS `add_info_id`,
 1 AS `add_info_typ`,
 1 AS `variant_id`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `has_language`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `derivativ`
--

DROP TABLE IF EXISTS `derivativ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `derivativ` (
  `id` int NOT NULL AUTO_INCREMENT,
  `variant_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_derivativ_variant1_idx` (`variant_id`),
  CONSTRAINT `fk_derivativ_variant1` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=96397 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `derivativ_g_derivativ_map`
--

DROP TABLE IF EXISTS `derivativ_g_derivativ_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `derivativ_g_derivativ_map` (
  `derivativ_id` int NOT NULL,
  `sortorder` int NOT NULL,
  `g_derivativ_id` int NOT NULL,
  PRIMARY KEY (`derivativ_id`,`sortorder`),
  KEY `fk_derivativ_glossary_derivativ_map_derivativ1_idx` (`g_derivativ_id`),
  KEY `idx_g_derivativ_id` (`g_derivativ_id`),
  CONSTRAINT `fk_derivativ_glossary_derivativ_map_derivativ1` FOREIGN KEY (`g_derivativ_id`) REFERENCES `idis_glossary`.`g_derivativ` (`id`),
  CONSTRAINT `fk_derivativ_glossary_derivativ_zuordnung_derivativ1` FOREIGN KEY (`derivativ_id`) REFERENCES `derivativ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `derivativ_g_pyro_column_text`
--

DROP TABLE IF EXISTS `derivativ_g_pyro_column_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `derivativ_g_pyro_column_text` (
  `derivativ_id` int NOT NULL,
  `g_pyro_id` int NOT NULL,
  `g_pyro_column_id` int NOT NULL,
  `text` text,
  PRIMARY KEY (`derivativ_id`,`g_pyro_column_id`,`g_pyro_id`),
  KEY `fk_derivativ_g_pyro_column_text_g_pyro_column1_idx` (`g_pyro_column_id`),
  KEY `fk_derivativ_g_pyro_column_text_g_pyro_id_idx` (`g_pyro_id`),
  CONSTRAINT `fk_derivativ_g_pyro_column_text_derivativ1` FOREIGN KEY (`derivativ_id`) REFERENCES `derivativ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_derivativ_g_pyro_column_text_g_pyro1` FOREIGN KEY (`g_pyro_id`) REFERENCES `idis_glossary`.`g_pyro` (`id`),
  CONSTRAINT `fk_derivativ_g_pyro_column_text_g_pyro_column1` FOREIGN KEY (`g_pyro_column_id`) REFERENCES `idis_glossary`.`g_pyro_column` (`column_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `derivativ_g_pyro_value_map`
--

DROP TABLE IF EXISTS `derivativ_g_pyro_value_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `derivativ_g_pyro_value_map` (
  `derivativ_id` int NOT NULL,
  `g_pyro_id` int NOT NULL,
  `g_pyro_column_id` int NOT NULL,
  `g_pyro_column_value_id` int DEFAULT NULL,
  PRIMARY KEY (`derivativ_id`,`g_pyro_id`,`g_pyro_column_id`),
  KEY `fk_derivativ_g_pyro_value_map_g_pyro_list_column1_idx` (`g_pyro_column_id`),
  KEY `fk_derivativ_g_pyro_value_map_g_pyro_column_value1_idx` (`g_pyro_column_value_id`),
  KEY `fk_derivativ_g_pyro_value_map_g_pyro1_idx` (`g_pyro_id`),
  CONSTRAINT `fk_derivativ_g_pyro_value_map_derivativ1` FOREIGN KEY (`derivativ_id`) REFERENCES `derivativ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_derivativ_g_pyro_value_map_g_pyro1` FOREIGN KEY (`g_pyro_id`) REFERENCES `idis_glossary`.`g_pyro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_derivativ_g_pyro_value_map_g_pyro_column_value1` FOREIGN KEY (`g_pyro_column_value_id`) REFERENCES `idis_glossary`.`g_pyro_column_value` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_derivativ_g_pyro_value_map_g_pyro_list_column1` FOREIGN KEY (`g_pyro_column_id`) REFERENCES `idis_glossary`.`g_pyro_column` (`column_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `derivativ_name`
--

DROP TABLE IF EXISTS `derivativ_name`;
/*!50001 DROP VIEW IF EXISTS `derivativ_name`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `derivativ_name` AS SELECT 
 1 AS `id`,
 1 AS `language_iso639-1`,
 1 AS `derivativ_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `derivatives`
--

DROP TABLE IF EXISTS `derivatives`;
/*!50001 DROP VIEW IF EXISTS `derivatives`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `derivatives` AS SELECT 
 1 AS `id`,
 1 AS `variant_id`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `sortorder`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `dismantling_pictures`
--

DROP TABLE IF EXISTS `dismantling_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dismantling_pictures` (
  `id` int NOT NULL AUTO_INCREMENT,
  `variant_id` int NOT NULL,
  `model_id` int NOT NULL,
  `brand_id` int NOT NULL,
  `area_id` int NOT NULL,
  `page_num` int NOT NULL,
  `name` text NOT NULL,
  `is_flash` tinyint DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=442841 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `fixing_qty_view`
--

DROP TABLE IF EXISTS `fixing_qty_view`;
/*!50001 DROP VIEW IF EXISTS `fixing_qty_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `fixing_qty_view` AS SELECT 
 1 AS `id`,
 1 AS `component_id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `quantity`,
 1 AS `quantityId`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `g_fixings_component_mapping_view`
--

DROP TABLE IF EXISTS `g_fixings_component_mapping_view`;
/*!50001 DROP VIEW IF EXISTS `g_fixings_component_mapping_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_fixings_component_mapping_view` AS SELECT 
 1 AS `id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `component_id`,
 1 AS `quantity`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `g_recycling_component_mapping_view`
--

DROP TABLE IF EXISTS `g_recycling_component_mapping_view`;
/*!50001 DROP VIEW IF EXISTS `g_recycling_component_mapping_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_recycling_component_mapping_view` AS SELECT 
 1 AS `id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `component_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `g_tool_component_mapping_view`
--

DROP TABLE IF EXISTS `g_tool_component_mapping_view`;
/*!50001 DROP VIEW IF EXISTS `g_tool_component_mapping_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_tool_component_mapping_view` AS SELECT 
 1 AS `id`,
 1 AS `info`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `component_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `last_selected_variants`
--

DROP TABLE IF EXISTS `last_selected_variants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `last_selected_variants` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `variant_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_id_idx` (`user_id`),
  KEY `fk_variant_id_idx` (`variant_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `idis_users`.`user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_variant_id` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3886 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `model`
--

DROP TABLE IF EXISTS `model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model` (
  `id` int NOT NULL AUTO_INCREMENT,
  `g_brand_id` int NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_model_brand1_idx` (`g_brand_id`),
  CONSTRAINT `fk_model_brand1` FOREIGN KEY (`g_brand_id`) REFERENCES `idis_glossary`.`g_brand` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6892 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `model_language_map`
--

DROP TABLE IF EXISTS `model_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_language_map` (
  `model_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`model_id`,`language_iso639-1`),
  KEY `fk_model_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_model_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `idis_glossary`.`g_language` (`iso639-1`),
  CONSTRAINT `fk_model_language_zuordnung_model1` FOREIGN KEY (`model_id`) REFERENCES `model` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `models`
--

DROP TABLE IF EXISTS `models`;
/*!50001 DROP VIEW IF EXISTS `models`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `models` AS SELECT 
 1 AS `model_id`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `g_brand_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `part_area_view`
--

DROP TABLE IF EXISTS `part_area_view`;
/*!50001 DROP VIEW IF EXISTS `part_area_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `part_area_view` AS SELECT 
 1 AS `variant_id`,
 1 AS `id`,
 1 AS `default_picture`,
 1 AS `enabled`,
 1 AS `caution`,
 1 AS `comment`,
 1 AS `area_id`,
 1 AS `mainarea_id`,
 1 AS `picture_enabled`,
 1 AS `picture_disabled`,
 1 AS `picture_active`,
 1 AS `language_iso639-1`,
 1 AS `areaName`,
 1 AS `partName`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `part_component_view`
--

DROP TABLE IF EXISTS `part_component_view`;
/*!50001 DROP VIEW IF EXISTS `part_component_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `part_component_view` AS SELECT 
 1 AS `id`,
 1 AS `variant_id`,
 1 AS `mainarea_id`,
 1 AS `area_id`,
 1 AS `caution`,
 1 AS `component_id`,
 1 AS `quantity`,
 1 AS `derivative_id`,
 1 AS `g_material_id`,
 1 AS `text`,
 1 AS `language_iso639-1`,
 1 AS `position_id`,
 1 AS `position_text`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `part_sno_view`
--

DROP TABLE IF EXISTS `part_sno_view`;
/*!50001 DROP VIEW IF EXISTS `part_sno_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `part_sno_view` AS SELECT 
 1 AS `id`,
 1 AS `g_area_id`,
 1 AS `serial_number`,
 1 AS `variant_id`,
 1 AS `weight`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pyro_adaptor`
--

DROP TABLE IF EXISTS `pyro_adaptor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pyro_adaptor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `g_brand_id` int NOT NULL,
  `text` text,
  PRIMARY KEY (`id`),
  KEY `fk_pyro_adaptor_brand1_idx` (`g_brand_id`),
  CONSTRAINT `fk_pyro_adaptor_brand1` FOREIGN KEY (`g_brand_id`) REFERENCES `idis_glossary`.`g_brand` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `pyro_component_view`
--

DROP TABLE IF EXISTS `pyro_component_view`;
/*!50001 DROP VIEW IF EXISTS `pyro_component_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `pyro_component_view` AS SELECT 
 1 AS `variant_id`,
 1 AS `derivative_id`,
 1 AS `component_id`,
 1 AS `part_id`,
 1 AS `pyro_id`,
 1 AS `column_index`,
 1 AS `column_value`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `supercap_pictures`
--

DROP TABLE IF EXISTS `supercap_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `supercap_pictures` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand_id` int NOT NULL,
  `model_id` int NOT NULL,
  `variant_id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `is_flash` tinyint DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1132 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `user_brand_model_variant_map`
--

DROP TABLE IF EXISTS `user_brand_model_variant_map`;
/*!50001 DROP VIEW IF EXISTS `user_brand_model_variant_map`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `user_brand_model_variant_map` AS SELECT 
 1 AS `user_id`,
 1 AS `brand_id`,
 1 AS `model_id`,
 1 AS `variant_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `variant`
--

DROP TABLE IF EXISTS `variant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant` (
  `id` int NOT NULL AUTO_INCREMENT,
  `model_id` int NOT NULL,
  `variant_status_id` int NOT NULL,
  `period_from_month` int DEFAULT NULL,
  `period_from_year` int DEFAULT NULL,
  `period_to_month` int DEFAULT NULL,
  `period_to_year` int DEFAULT NULL,
  `caravan` tinyint(1) DEFAULT NULL,
  `version` int DEFAULT NULL,
  `creation_user_id` int DEFAULT NULL,
  `creation_time` datetime DEFAULT NULL,
  `first_release_user_id` int DEFAULT NULL,
  `first_release_time` datetime DEFAULT NULL,
  `last_change_user_id` int DEFAULT NULL,
  `last_change_time` datetime DEFAULT NULL,
  `prerelease_user_id` int DEFAULT NULL,
  `prerelease_time` datetime DEFAULT NULL,
  `release_user_id` int DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  `picture` longblob,
  PRIMARY KEY (`id`),
  KEY `fk_variant_variant_status1_idx` (`variant_status_id`),
  KEY `fk_variant_model_idx` (`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14682 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `variant_country_map`
--

DROP TABLE IF EXISTS `variant_country_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant_country_map` (
  `variant_id` int NOT NULL,
  `g_country_id` int NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`variant_id`,`g_country_id`),
  KEY `fk_variant_country_map_country1_idx` (`g_country_id`),
  CONSTRAINT `fk_variant_country_map_country1` FOREIGN KEY (`g_country_id`) REFERENCES `idis_glossary`.`g_country` (`id`),
  CONSTRAINT `fk_variant_country_zuordnung_variant1` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `variant_language_map`
--

DROP TABLE IF EXISTS `variant_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant_language_map` (
  `variant_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`variant_id`,`language_iso639-1`),
  KEY `fk_variant_language_zuordnung_variant1_idx` (`variant_id`),
  KEY `fk_variant_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_variant_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `idis_glossary`.`g_language` (`iso639-1`),
  CONSTRAINT `fk_variant_language_zuordnung_variant1` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `variant_picture_thumbnail`
--

DROP TABLE IF EXISTS `variant_picture_thumbnail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant_picture_thumbnail` (
  `variant_id` int NOT NULL,
  `thumbnaildata` mediumblob,
  PRIMARY KEY (`variant_id`),
  CONSTRAINT `variant_picture_thumbnail_variant_id_fkey` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `variant_status`
--

DROP TABLE IF EXISTS `variant_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant_status` (
  `id` int NOT NULL,
  `logo` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `variant_status_language_map`
--

DROP TABLE IF EXISTS `variant_status_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant_status_language_map` (
  `variant_status_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`variant_status_id`,`language_iso639-1`),
  KEY `fk_variant_status_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_variant_status_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `idis_glossary`.`g_language` (`iso639-1`),
  CONSTRAINT `fk_variant_status_language_zuordnung_variant_status1` FOREIGN KEY (`variant_status_id`) REFERENCES `variant_status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `variants`
--

DROP TABLE IF EXISTS `variants`;
/*!50001 DROP VIEW IF EXISTS `variants`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `variants` AS SELECT 
 1 AS `id`,
 1 AS `model_id`,
 1 AS `variant_status_id`,
 1 AS `period_from_month`,
 1 AS `period_from_year`,
 1 AS `period_to_month`,
 1 AS `period_to_year`,
 1 AS `caravan`,
 1 AS `version`,
 1 AS `creation_user_id`,
 1 AS `creation_time`,
 1 AS `first_release_user_id`,
 1 AS `first_release_time`,
 1 AS `last_change_user_id`,
 1 AS `last_change_time`,
 1 AS `prerelease_user_id`,
 1 AS `release_user_id`,
 1 AS `release_time`,
 1 AS `picture`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `vehicle_information`
--

DROP TABLE IF EXISTS `vehicle_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vehicle_information` (
  `variant_id` int NOT NULL,
  `kerb_mass` text NOT NULL,
  `luggage_volume` text NOT NULL,
  `axle_base` text NOT NULL,
  `variant_height` text NOT NULL,
  `variant_width` text NOT NULL,
  `variant_length` text NOT NULL,
  `model_identification` text NOT NULL,
  `size_of_tire` text NOT NULL,
  `fuel_tank_capacity` text NOT NULL,
  `transmission` text NOT NULL,
  `fuel_type` text NOT NULL,
  PRIMARY KEY (`variant_id`),
  CONSTRAINT `fk_vehicle_information_variant_id1` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `idis_office`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `idis_office` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `idis_office`;

--
-- Table structure for table `add_info`
--

DROP TABLE IF EXISTS `add_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26416 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `add_info_description_language_map`
--

DROP TABLE IF EXISTS `add_info_description_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info_description_language_map` (
  `component_id` int NOT NULL,
  `add_info_id` int NOT NULL,
  `add_info_typ` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `headline` text NOT NULL,
  `description` text NOT NULL,
  `icon` varchar(255) NOT NULL,
  PRIMARY KEY (`component_id`,`add_info_id`,`add_info_typ`,`language_iso639-1`),
  KEY `fk_add_info_description_language_map_add_info_id` (`add_info_id`),
  KEY `fk_component_description_language_map_add_ino_typ_idx` (`add_info_typ`),
  CONSTRAINT `fk_add_info_description_language_map_add_info_id` FOREIGN KEY (`add_info_id`) REFERENCES `add_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_description_language_map_add_ino_typ` FOREIGN KEY (`add_info_typ`) REFERENCES `idis_glossary`.`g_add_info_typ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_description_language_map_component_id` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `add_info_files`
--

DROP TABLE IF EXISTS `add_info_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info_files` (
  `id` int NOT NULL AUTO_INCREMENT,
  `file` longblob NOT NULL,
  `file_name` text NOT NULL,
  `checksum` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `checksum_UNIQUE` (`checksum`)
) ENGINE=InnoDB AUTO_INCREMENT=15906 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `add_info_glossary_manufacturer_map`
--

DROP TABLE IF EXISTS `add_info_glossary_manufacturer_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info_glossary_manufacturer_map` (
  `add_info_id` int NOT NULL,
  `g_manufacturer_id` int NOT NULL,
  `identifier` text NOT NULL,
  PRIMARY KEY (`add_info_id`),
  KEY `fk_add_info_glossary_manufacturer_map_manufacturer1_idx` (`g_manufacturer_id`),
  CONSTRAINT `fk_add_info_glossary_manufacturer_map_manufacturer1` FOREIGN KEY (`g_manufacturer_id`) REFERENCES `idis_glossary`.`g_manufacturer` (`id`),
  CONSTRAINT `fk_add_info_glossary_manufacturer_zuordnung_add_info1` FOREIGN KEY (`add_info_id`) REFERENCES `add_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `add_info_language_map`
--

DROP TABLE IF EXISTS `add_info_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info_language_map` (
  `add_info_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `file_id` int NOT NULL,
  `file_checksum` text NOT NULL,
  PRIMARY KEY (`add_info_id`,`language_iso639-1`),
  KEY `fk_add_info_zuordnung_add_info1_idx` (`add_info_id`),
  KEY `fk_add_info_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_add_info_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `idis_glossary`.`g_language` (`iso639-1`),
  CONSTRAINT `fk_add_info_zuordnung_add_info1` FOREIGN KEY (`add_info_id`) REFERENCES `add_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `add_info_old_new_map`
--

DROP TABLE IF EXISTS `add_info_old_new_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `add_info_old_new_map` (
  `old_add_id` int NOT NULL,
  `new_add_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `annex_pictures`
--

DROP TABLE IF EXISTS `annex_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `annex_pictures` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand_id` int NOT NULL,
  `model_id` int NOT NULL,
  `variant_id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `is_flash` tinyint DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5255 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `brake_servo_pictures`
--

DROP TABLE IF EXISTS `brake_servo_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brake_servo_pictures` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand_id` int NOT NULL,
  `model_id` int NOT NULL,
  `variant_id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `is_flash` tinyint DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `brand_model_variant_view`
--

DROP TABLE IF EXISTS `brand_model_variant_view`;
/*!50001 DROP VIEW IF EXISTS `brand_model_variant_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `brand_model_variant_view` AS SELECT 
 1 AS `language_short`,
 1 AS `brand_id`,
 1 AS `brand_name`,
 1 AS `model_id`,
 1 AS `model_name`,
 1 AS `variant_id`,
 1 AS `variant_name`,
 1 AS `period_from_month`,
 1 AS `period_from_year`,
 1 AS `period_to_month`,
 1 AS `period_to_year`,
 1 AS `status_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `changes`
--

DROP TABLE IF EXISTS `changes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `changes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `variant_id` int NOT NULL,
  `value` text NOT NULL,
  `checked` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=356450 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `changes_old`
--

DROP TABLE IF EXISTS `changes_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `changes_old` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date_time` datetime DEFAULT NULL,
  `user` text,
  `variant` int DEFAULT NULL,
  `action` text,
  `table_changed` text,
  `value` text,
  `sql_used` text,
  `checked` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=689193 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component`
--

DROP TABLE IF EXISTS `component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component` (
  `id` int NOT NULL AUTO_INCREMENT,
  `g_part_id` int DEFAULT NULL,
  `g_area_id` int DEFAULT NULL,
  `g_material_id` int DEFAULT NULL,
  `material_checked` tinyint(1) DEFAULT '0',
  `g_position_id` int DEFAULT NULL,
  `g_method_id` int DEFAULT NULL,
  `g_comment_id` int DEFAULT NULL,
  `weight` int DEFAULT NULL,
  `weight_unit` varchar(2) DEFAULT NULL,
  `weight_checked` tinyint(1) DEFAULT '0',
  `derivative_id` int DEFAULT NULL,
  `page_number` int DEFAULT '1',
  `quantity` int DEFAULT NULL,
  `serial_number` varchar(45) DEFAULT NULL,
  `variant_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_component_part1_idx` (`g_part_id`),
  KEY `fk_component_area1_idx` (`g_area_id`),
  KEY `fk_component_material1_idx` (`g_material_id`),
  KEY `fk_component_position1_idx` (`g_position_id`),
  KEY `fk_component_method1_idx` (`g_method_id`),
  KEY `fk_component_comment1_idx` (`g_comment_id`),
  KEY `fk_component_derivativ1_idx` (`derivative_id`),
  KEY `fk_component_variant1_idx` (`variant_id`),
  KEY `idx_component_g_part_id` (`g_part_id`),
  CONSTRAINT `fk_component_area1` FOREIGN KEY (`g_area_id`) REFERENCES `idis_glossary`.`g_area` (`id`),
  CONSTRAINT `fk_component_comment1` FOREIGN KEY (`g_comment_id`) REFERENCES `idis_glossary`.`g_comment` (`id`),
  CONSTRAINT `fk_component_derivativ1` FOREIGN KEY (`derivative_id`) REFERENCES `derivativ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_material1` FOREIGN KEY (`g_material_id`) REFERENCES `idis_glossary`.`g_material` (`id`),
  CONSTRAINT `fk_component_method1` FOREIGN KEY (`g_method_id`) REFERENCES `idis_glossary`.`g_method` (`id`),
  CONSTRAINT `fk_component_part1` FOREIGN KEY (`g_part_id`) REFERENCES `idis_glossary`.`g_part` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_position1` FOREIGN KEY (`g_position_id`) REFERENCES `idis_glossary`.`g_position` (`id`),
  CONSTRAINT `fk_component_variant1` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=836389 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_addinfo_map`
--

DROP TABLE IF EXISTS `component_addinfo_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_addinfo_map` (
  `component_id` int NOT NULL,
  `add_info_id` int NOT NULL,
  `add_info_typ` int NOT NULL,
  PRIMARY KEY (`component_id`,`add_info_typ`,`add_info_id`),
  KEY `fk_component_addinfo_map_add_info1_idx` (`add_info_id`),
  KEY `fk_component_addinfo_map_component1_idx` (`component_id`),
  KEY `fk_component_addinfo_map_g_addinfo_typ1_idx` (`add_info_typ`),
  CONSTRAINT `fk_component_addinfo_map_add_info1` FOREIGN KEY (`add_info_id`) REFERENCES `add_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_addinfo_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_addinfo_map_g_addinfo_typ1` FOREIGN KEY (`add_info_typ`) REFERENCES `idis_glossary`.`g_add_info_typ` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_fixing_map`
--

DROP TABLE IF EXISTS `component_fixing_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_fixing_map` (
  `component_id` int NOT NULL,
  `g_fixing_id` int NOT NULL,
  `quantity` int DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_component_fixing_zuordnung_component1` (`component_id`),
  KEY `fk_component_fixing_map_fixing1` (`g_fixing_id`),
  CONSTRAINT `fk_component_fixing_map_fixing1` FOREIGN KEY (`g_fixing_id`) REFERENCES `idis_glossary`.`g_fixing` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_fixing_zuordnung_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=367581 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_gas_map`
--

DROP TABLE IF EXISTS `component_gas_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_gas_map` (
  `component_id` int NOT NULL,
  `operation_mode` int DEFAULT NULL,
  `number_of_tanks` text,
  PRIMARY KEY (`component_id`),
  CONSTRAINT `fk_component_gas_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_gpyro_value_map`
--

DROP TABLE IF EXISTS `component_gpyro_value_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_gpyro_value_map` (
  `component_id` int NOT NULL,
  `g_pyro_id` int NOT NULL,
  `g_pyro_column_id` int NOT NULL,
  `g_pyro_column_value_id` int DEFAULT NULL,
  PRIMARY KEY (`component_id`,`g_pyro_id`,`g_pyro_column_id`),
  KEY `fk_component_gpyro_value_map_g_pyro_list_column1_idx` (`g_pyro_column_id`),
  KEY `fk_component_gpyro_value_map_g_pyro_column_value1_idx` (`g_pyro_column_value_id`),
  KEY `fk_component_gpyro_value_map_g_pyro1_idx` (`g_pyro_id`),
  CONSTRAINT `fk_component_gpyro_value_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_gpyro_value_map_g_pyro1` FOREIGN KEY (`g_pyro_id`) REFERENCES `idis_glossary`.`g_pyro` (`id`),
  CONSTRAINT `fk_component_gpyro_value_map_g_pyro_column_value1` FOREIGN KEY (`g_pyro_column_value_id`) REFERENCES `idis_glossary`.`g_pyro_column_value` (`id`),
  CONSTRAINT `fk_component_gpyro_value_map_g_pyro_list_column1` FOREIGN KEY (`g_pyro_column_id`) REFERENCES `idis_glossary`.`g_pyro_column` (`column_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_health_safety_map`
--

DROP TABLE IF EXISTS `component_health_safety_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_health_safety_map` (
  `component_id` int NOT NULL,
  `g_health_safety_id` int NOT NULL,
  PRIMARY KEY (`component_id`,`g_health_safety_id`),
  KEY `fk_component_health_safety_map_g_health_safety1_idx` (`g_health_safety_id`),
  CONSTRAINT `fk_component_health_safety_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_health_safety_map_g_health_safety1` FOREIGN KEY (`g_health_safety_id`) REFERENCES `idis_glossary`.`g_health_safety` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_picture_map`
--

DROP TABLE IF EXISTS `component_picture_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_picture_map` (
  `component_id` int NOT NULL,
  `picture` longblob NOT NULL,
  PRIMARY KEY (`component_id`),
  CONSTRAINT `component_picture_map_component_id_fk` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_pyro_adaptor_map`
--

DROP TABLE IF EXISTS `component_pyro_adaptor_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_pyro_adaptor_map` (
  `component_id` int NOT NULL,
  `pyro_adaptor_id` int NOT NULL,
  PRIMARY KEY (`component_id`,`pyro_adaptor_id`),
  KEY `fk_component_pyro_adaptor_map_pyro_adaptor1_idx` (`pyro_adaptor_id`),
  CONSTRAINT `fk_component_pyro_adaptor_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_pyro_adaptor_map_pyro_adaptor1` FOREIGN KEY (`pyro_adaptor_id`) REFERENCES `pyro_adaptor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_recycling_map`
--

DROP TABLE IF EXISTS `component_recycling_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_recycling_map` (
  `component_id` int NOT NULL,
  `g_recycling_id` int NOT NULL,
  PRIMARY KEY (`component_id`,`g_recycling_id`),
  KEY `fk_component_recycling_map_g_recycling1_idx` (`g_recycling_id`),
  CONSTRAINT `fk_component_recycling_map_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_recycling_map_g_recycling1` FOREIGN KEY (`g_recycling_id`) REFERENCES `idis_glossary`.`g_recycling` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component_tool_map`
--

DROP TABLE IF EXISTS `component_tool_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component_tool_map` (
  `component_id` int NOT NULL,
  `g_tool_id` int NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_component_tool_zuordnung_component1` (`component_id`),
  KEY `fk_component_tool_map_tool1` (`g_tool_id`),
  CONSTRAINT `fk_component_tool_map_tool1` FOREIGN KEY (`g_tool_id`) REFERENCES `idis_glossary`.`g_tool` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_component_tool_zuordnung_component1` FOREIGN KEY (`component_id`) REFERENCES `component` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=718711 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `components_with_add_info`
--

DROP TABLE IF EXISTS `components_with_add_info`;
/*!50001 DROP VIEW IF EXISTS `components_with_add_info`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `components_with_add_info` AS SELECT 
 1 AS `id`,
 1 AS `g_part_id`,
 1 AS `add_info_id`,
 1 AS `add_info_typ`,
 1 AS `variant_id`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `has_language`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `derivativ`
--

DROP TABLE IF EXISTS `derivativ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `derivativ` (
  `id` int NOT NULL AUTO_INCREMENT,
  `variant_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_derivativ_variant1_idx` (`variant_id`),
  CONSTRAINT `fk_derivativ_variant1` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=96661 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `derivativ_g_derivativ_map`
--

DROP TABLE IF EXISTS `derivativ_g_derivativ_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `derivativ_g_derivativ_map` (
  `derivativ_id` int NOT NULL,
  `sortorder` int NOT NULL,
  `g_derivativ_id` int NOT NULL,
  PRIMARY KEY (`derivativ_id`,`sortorder`),
  KEY `fk_derivativ_glossary_derivativ_map_derivativ1_idx` (`g_derivativ_id`),
  KEY `idx_g_derivativ_id` (`g_derivativ_id`),
  CONSTRAINT `fk_derivativ_glossary_derivativ_map_derivativ1` FOREIGN KEY (`g_derivativ_id`) REFERENCES `idis_glossary`.`g_derivativ` (`id`),
  CONSTRAINT `fk_derivativ_glossary_derivativ_zuordnung_derivativ1` FOREIGN KEY (`derivativ_id`) REFERENCES `derivativ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `derivativ_g_pyro_column_text`
--

DROP TABLE IF EXISTS `derivativ_g_pyro_column_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `derivativ_g_pyro_column_text` (
  `derivativ_id` int NOT NULL,
  `g_pyro_id` int NOT NULL,
  `g_pyro_column_id` int NOT NULL,
  `text` text,
  PRIMARY KEY (`derivativ_id`,`g_pyro_column_id`,`g_pyro_id`),
  KEY `fk_derivativ_g_pyro_column_text_g_pyro_column1_idx` (`g_pyro_column_id`),
  KEY `fk_derivativ_g_pyro_column_text_g_pyro_id_idx` (`g_pyro_id`),
  CONSTRAINT `fk_derivativ_g_pyro_column_text_derivativ1` FOREIGN KEY (`derivativ_id`) REFERENCES `derivativ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_derivativ_g_pyro_column_text_g_pyro1` FOREIGN KEY (`g_pyro_id`) REFERENCES `idis_glossary`.`g_pyro` (`id`),
  CONSTRAINT `fk_derivativ_g_pyro_column_text_g_pyro_column1` FOREIGN KEY (`g_pyro_column_id`) REFERENCES `idis_glossary`.`g_pyro_column` (`column_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `derivativ_g_pyro_value_map`
--

DROP TABLE IF EXISTS `derivativ_g_pyro_value_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `derivativ_g_pyro_value_map` (
  `derivativ_id` int NOT NULL,
  `g_pyro_id` int NOT NULL,
  `g_pyro_column_id` int NOT NULL,
  `g_pyro_column_value_id` int DEFAULT NULL,
  PRIMARY KEY (`derivativ_id`,`g_pyro_id`,`g_pyro_column_id`),
  KEY `fk_derivativ_g_pyro_value_map_g_pyro_list_column1_idx` (`g_pyro_column_id`),
  KEY `fk_derivativ_g_pyro_value_map_g_pyro_column_value1_idx` (`g_pyro_column_value_id`),
  KEY `fk_derivativ_g_pyro_value_map_g_pyro1_idx` (`g_pyro_id`),
  CONSTRAINT `fk_derivativ_g_pyro_value_map_derivativ1` FOREIGN KEY (`derivativ_id`) REFERENCES `derivativ` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_derivativ_g_pyro_value_map_g_pyro1` FOREIGN KEY (`g_pyro_id`) REFERENCES `idis_glossary`.`g_pyro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_derivativ_g_pyro_value_map_g_pyro_column_value1` FOREIGN KEY (`g_pyro_column_value_id`) REFERENCES `idis_glossary`.`g_pyro_column_value` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_derivativ_g_pyro_value_map_g_pyro_list_column1` FOREIGN KEY (`g_pyro_column_id`) REFERENCES `idis_glossary`.`g_pyro_column` (`column_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `derivativ_name`
--

DROP TABLE IF EXISTS `derivativ_name`;
/*!50001 DROP VIEW IF EXISTS `derivativ_name`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `derivativ_name` AS SELECT 
 1 AS `id`,
 1 AS `language_iso639-1`,
 1 AS `derivativ_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `derivatives`
--

DROP TABLE IF EXISTS `derivatives`;
/*!50001 DROP VIEW IF EXISTS `derivatives`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `derivatives` AS SELECT 
 1 AS `id`,
 1 AS `variant_id`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `sortorder`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `dismantling_pictures`
--

DROP TABLE IF EXISTS `dismantling_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dismantling_pictures` (
  `id` int NOT NULL AUTO_INCREMENT,
  `variant_id` int NOT NULL,
  `model_id` int NOT NULL,
  `brand_id` int NOT NULL,
  `area_id` int NOT NULL,
  `page_num` int NOT NULL,
  `name` text NOT NULL,
  `is_flash` tinyint DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=133848 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `fixing_qty_view`
--

DROP TABLE IF EXISTS `fixing_qty_view`;
/*!50001 DROP VIEW IF EXISTS `fixing_qty_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `fixing_qty_view` AS SELECT 
 1 AS `id`,
 1 AS `component_id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `quantity`,
 1 AS `quantityId`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `g_fixings_component_mapping_view`
--

DROP TABLE IF EXISTS `g_fixings_component_mapping_view`;
/*!50001 DROP VIEW IF EXISTS `g_fixings_component_mapping_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_fixings_component_mapping_view` AS SELECT 
 1 AS `id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `component_id`,
 1 AS `quantity`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `g_recycling_component_mapping_view`
--

DROP TABLE IF EXISTS `g_recycling_component_mapping_view`;
/*!50001 DROP VIEW IF EXISTS `g_recycling_component_mapping_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_recycling_component_mapping_view` AS SELECT 
 1 AS `id`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `component_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `g_tool_component_mapping_view`
--

DROP TABLE IF EXISTS `g_tool_component_mapping_view`;
/*!50001 DROP VIEW IF EXISTS `g_tool_component_mapping_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `g_tool_component_mapping_view` AS SELECT 
 1 AS `id`,
 1 AS `info`,
 1 AS `sortorder`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `component_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `glossary_mainarea_button`
--

DROP TABLE IF EXISTS `glossary_mainarea_button`;
/*!50001 DROP VIEW IF EXISTS `glossary_mainarea_button`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `glossary_mainarea_button` AS SELECT 
 1 AS `mainarea_id`,
 1 AS `glossary_part`,
 1 AS `id`,
 1 AS `button_picture_id`,
 1 AS `active`,
 1 AS `comment`,
 1 AS `sortorder`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `last_selected_variants`
--

DROP TABLE IF EXISTS `last_selected_variants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `last_selected_variants` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `variant_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_id_idx` (`user_id`),
  KEY `fk_variant_id_idx` (`variant_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `idis_users`.`user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_variant_id` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=90043 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `model`
--

DROP TABLE IF EXISTS `model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model` (
  `id` int NOT NULL AUTO_INCREMENT,
  `g_brand_id` int NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_model_brand1_idx` (`g_brand_id`),
  CONSTRAINT `fk_model_brand1` FOREIGN KEY (`g_brand_id`) REFERENCES `idis_glossary`.`g_brand` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6901 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `model_language_map`
--

DROP TABLE IF EXISTS `model_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `model_language_map` (
  `model_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`model_id`,`language_iso639-1`),
  KEY `fk_model_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_model_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `idis_glossary`.`g_language` (`iso639-1`),
  CONSTRAINT `fk_model_language_zuordnung_model1` FOREIGN KEY (`model_id`) REFERENCES `model` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `models`
--

DROP TABLE IF EXISTS `models`;
/*!50001 DROP VIEW IF EXISTS `models`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `models` AS SELECT 
 1 AS `model_id`,
 1 AS `language_iso639-1`,
 1 AS `text`,
 1 AS `g_brand_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `part_area_view`
--

DROP TABLE IF EXISTS `part_area_view`;
/*!50001 DROP VIEW IF EXISTS `part_area_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `part_area_view` AS SELECT 
 1 AS `variant_id`,
 1 AS `id`,
 1 AS `default_picture`,
 1 AS `enabled`,
 1 AS `caution`,
 1 AS `comment`,
 1 AS `area_id`,
 1 AS `mainarea_id`,
 1 AS `picture_enabled`,
 1 AS `picture_disabled`,
 1 AS `picture_active`,
 1 AS `language_iso639-1`,
 1 AS `areaName`,
 1 AS `partName`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `part_component_view`
--

DROP TABLE IF EXISTS `part_component_view`;
/*!50001 DROP VIEW IF EXISTS `part_component_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `part_component_view` AS SELECT 
 1 AS `id`,
 1 AS `variant_id`,
 1 AS `mainarea_id`,
 1 AS `area_id`,
 1 AS `caution`,
 1 AS `component_id`,
 1 AS `quantity`,
 1 AS `derivative_id`,
 1 AS `g_material_id`,
 1 AS `text`,
 1 AS `language_iso639-1`,
 1 AS `position_id`,
 1 AS `position_text`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `part_sno_view`
--

DROP TABLE IF EXISTS `part_sno_view`;
/*!50001 DROP VIEW IF EXISTS `part_sno_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `part_sno_view` AS SELECT 
 1 AS `id`,
 1 AS `g_area_id`,
 1 AS `serial_number`,
 1 AS `variant_id`,
 1 AS `weight`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pyro_adaptor`
--

DROP TABLE IF EXISTS `pyro_adaptor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pyro_adaptor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `g_brand_id` int NOT NULL,
  `text` text,
  PRIMARY KEY (`id`),
  KEY `fk_pyro_adaptor_brand1_idx` (`g_brand_id`),
  CONSTRAINT `fk_pyro_adaptor_brand1` FOREIGN KEY (`g_brand_id`) REFERENCES `idis_glossary`.`g_brand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `pyro_component_view`
--

DROP TABLE IF EXISTS `pyro_component_view`;
/*!50001 DROP VIEW IF EXISTS `pyro_component_view`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `pyro_component_view` AS SELECT 
 1 AS `variant_id`,
 1 AS `derivative_id`,
 1 AS `component_id`,
 1 AS `part_id`,
 1 AS `pyro_id`,
 1 AS `column_index`,
 1 AS `column_value`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `supercap_pictures`
--

DROP TABLE IF EXISTS `supercap_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `supercap_pictures` (
  `id` int NOT NULL AUTO_INCREMENT,
  `brand_id` int NOT NULL,
  `model_id` int NOT NULL,
  `variant_id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `is_flash` tinyint DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=427 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `user_brand_model_variant_map`
--

DROP TABLE IF EXISTS `user_brand_model_variant_map`;
/*!50001 DROP VIEW IF EXISTS `user_brand_model_variant_map`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `user_brand_model_variant_map` AS SELECT 
 1 AS `user_id`,
 1 AS `brand_id`,
 1 AS `model_id`,
 1 AS `variant_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `variant`
--

DROP TABLE IF EXISTS `variant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant` (
  `id` int NOT NULL AUTO_INCREMENT,
  `model_id` int NOT NULL,
  `variant_status_id` int NOT NULL,
  `period_from_month` int DEFAULT NULL,
  `period_from_year` int DEFAULT NULL,
  `period_to_month` int DEFAULT NULL,
  `period_to_year` int DEFAULT NULL,
  `caravan` tinyint(1) DEFAULT NULL,
  `version` int DEFAULT NULL,
  `creation_user_id` int DEFAULT NULL,
  `creation_time` datetime DEFAULT NULL,
  `first_release_user_id` int DEFAULT NULL,
  `first_release_time` datetime DEFAULT NULL,
  `last_change_user_id` int DEFAULT NULL,
  `last_change_time` datetime DEFAULT NULL,
  `prerelease_user_id` int DEFAULT NULL,
  `prerelease_time` datetime DEFAULT NULL,
  `release_user_id` int DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  `picture` longblob,
  PRIMARY KEY (`id`),
  KEY `fk_variant_model1_idx` (`model_id`),
  KEY `fk_variant_variant_status1_idx` (`variant_status_id`),
  CONSTRAINT `fk_variant_model1` FOREIGN KEY (`model_id`) REFERENCES `model` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14701 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `variant_country_map`
--

DROP TABLE IF EXISTS `variant_country_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant_country_map` (
  `variant_id` int NOT NULL,
  `g_country_id` int NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`variant_id`,`g_country_id`),
  KEY `fk_variant_country_map_country1_idx` (`g_country_id`),
  CONSTRAINT `fk_variant_country_map_country1` FOREIGN KEY (`g_country_id`) REFERENCES `idis_glossary`.`g_country` (`id`),
  CONSTRAINT `fk_variant_country_zuordnung_variant1` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `variant_language_map`
--

DROP TABLE IF EXISTS `variant_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant_language_map` (
  `variant_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`variant_id`,`language_iso639-1`),
  KEY `fk_variant_language_zuordnung_variant1_idx` (`variant_id`),
  KEY `fk_variant_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_variant_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `idis_glossary`.`g_language` (`iso639-1`),
  CONSTRAINT `fk_variant_language_zuordnung_variant1` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `variant_picture_thumbnail`
--

DROP TABLE IF EXISTS `variant_picture_thumbnail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant_picture_thumbnail` (
  `variant_id` int NOT NULL,
  `thumbnaildata` mediumblob,
  PRIMARY KEY (`variant_id`),
  CONSTRAINT `variant_picture_thumbnail_variant_id_fkey` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `variant_status`
--

DROP TABLE IF EXISTS `variant_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant_status` (
  `id` int NOT NULL,
  `logo` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `variant_status_language_map`
--

DROP TABLE IF EXISTS `variant_status_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `variant_status_language_map` (
  `variant_status_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`variant_status_id`,`language_iso639-1`),
  KEY `fk_variant_status_language_map_language1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_variant_status_language_map_language1` FOREIGN KEY (`language_iso639-1`) REFERENCES `idis_glossary`.`g_language` (`iso639-1`),
  CONSTRAINT `fk_variant_status_language_zuordnung_variant_status1` FOREIGN KEY (`variant_status_id`) REFERENCES `variant_status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `variants`
--

DROP TABLE IF EXISTS `variants`;
/*!50001 DROP VIEW IF EXISTS `variants`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `variants` AS SELECT 
 1 AS `id`,
 1 AS `model_id`,
 1 AS `variant_status_id`,
 1 AS `period_from_month`,
 1 AS `period_from_year`,
 1 AS `period_to_month`,
 1 AS `period_to_year`,
 1 AS `caravan`,
 1 AS `version`,
 1 AS `creation_user_id`,
 1 AS `creation_time`,
 1 AS `first_release_user_id`,
 1 AS `first_release_time`,
 1 AS `last_change_user_id`,
 1 AS `last_change_time`,
 1 AS `prerelease_user_id`,
 1 AS `release_user_id`,
 1 AS `release_time`,
 1 AS `picture`,
 1 AS `language_iso639-1`,
 1 AS `text`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `vehicle_information`
--

DROP TABLE IF EXISTS `vehicle_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vehicle_information` (
  `variant_id` int NOT NULL,
  `kerb_mass` text NOT NULL,
  `luggage_volume` text NOT NULL,
  `axle_base` text NOT NULL,
  `variant_height` text NOT NULL,
  `variant_width` text NOT NULL,
  `variant_length` text NOT NULL,
  `model_identification` text NOT NULL,
  `size_of_tire` text NOT NULL,
  `fuel_tank_capacity` text NOT NULL,
  `transmission` text NOT NULL,
  `fuel_type` text NOT NULL,
  PRIMARY KEY (`variant_id`),
  CONSTRAINT `fk_vehicle_information_variant_id1` FOREIGN KEY (`variant_id`) REFERENCES `variant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `idis_users`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `idis_users` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `idis_users`;

--
-- Table structure for table `dismantler`
--

DROP TABLE IF EXISTS `dismantler`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dismantler` (
  `id` int NOT NULL AUTO_INCREMENT,
  `lastname` text NOT NULL,
  `firstname` text NOT NULL,
  `pwd` text NOT NULL,
  `language` varchar(2) DEFAULT NULL,
  `country` int DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `company` text,
  `adress` text,
  `zip` text,
  `city` text,
  `company_country` int DEFAULT '0',
  `phone` text,
  `fax` text,
  `last_login` date DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `useconditions_accepted` tinyint(1) NOT NULL DEFAULT '0',
  `privacy_statement_accepted` tinyint(1) NOT NULL DEFAULT '0',
  `receive_mails` tinyint(1) NOT NULL DEFAULT '0',
  `receive_system_notification` tinyint(1) NOT NULL DEFAULT '0',
  `receive_newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `recycler` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dismantler_login_name_UNIQUE` (`delete_date`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17417 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dismantler_activity_map`
--

DROP TABLE IF EXISTS `dismantler_activity_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dismantler_activity_map` (
  `user_id` int NOT NULL,
  `activity_id` int NOT NULL,
  PRIMARY KEY (`user_id`,`activity_id`),
  KEY `fk_dismantler_activity_map_activity_id1_idx` (`activity_id`),
  CONSTRAINT `fk_dismantler_activity_map_activity_id1` FOREIGN KEY (`activity_id`) REFERENCES `idis_glossary`.`g_activity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_dismantler_activity_map_user_id1` FOREIGN KEY (`user_id`) REFERENCES `dismantler` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `logins`
--

DROP TABLE IF EXISTS `logins`;
/*!50001 DROP VIEW IF EXISTS `logins`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `logins` AS SELECT 
 1 AS `email`,
 1 AS `delete_date`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `messages` (
  `id` int NOT NULL AUTO_INCREMENT,
  `from_user` int NOT NULL,
  `to_user` int NOT NULL,
  `subject` text NOT NULL,
  `message` text NOT NULL,
  `date_sent` date NOT NULL,
  `unread` tinyint(1) NOT NULL,
  `is_release_request_message` tinyint(1) NOT NULL,
  `release_request_variant_id` int DEFAULT NULL,
  `is_delete_request_message` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=136469 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `permission_name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `requested_release_map`
--

DROP TABLE IF EXISTS `requested_release_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `requested_release_map` (
  `requesting_user_id` int NOT NULL,
  `requested_variant_id` int NOT NULL,
  PRIMARY KEY (`requesting_user_id`,`requested_variant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id` int NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role_language_map`
--

DROP TABLE IF EXISTS `role_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_language_map` (
  `role_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`language_iso639-1`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role_permission_map`
--

DROP TABLE IF EXISTS `role_permission_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_permission_map` (
  `role_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`),
  KEY `role_permission_map_permission_FK_idx` (`permission_id`),
  CONSTRAINT `role_permission_map_permission_FK` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!50001 DROP VIEW IF EXISTS `roles`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `roles` AS SELECT 
 1 AS `id`,
 1 AS `text`,
 1 AS `language_iso639-1`,
 1 AS `translated`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `system_news`
--

DROP TABLE IF EXISTS `system_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `system_news` (
  `id` int NOT NULL AUTO_INCREMENT,
  `news` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `date` date NOT NULL,
  `changing_user_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=676 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `lastname` text NOT NULL,
  `firstname` text NOT NULL,
  `pwd` text NOT NULL,
  `language` varchar(2) DEFAULT NULL,
  `country` int DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `company` text,
  `adress` text,
  `zip` text,
  `city` text,
  `company_country` int DEFAULT '0',
  `phone` text,
  `fax` text,
  `last_login` date DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL,
  `useconditions_accepted` tinyint(1) NOT NULL DEFAULT '0',
  `privacy_statement_accepted` tinyint(1) NOT NULL DEFAULT '0',
  `receive_mails` tinyint(1) NOT NULL DEFAULT '0',
  `receive_system_notification` tinyint(1) NOT NULL DEFAULT '0',
  `receive_newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `isMastertestUser` tinyint(1) NOT NULL DEFAULT '0',
  `login_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_login_name_UNIQUE` (`delete_date`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17416 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_brand_map`
--

DROP TABLE IF EXISTS `user_brand_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_brand_map` (
  `user_id` int NOT NULL,
  `brand_id` int NOT NULL,
  `role_id_for_brand` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`,`brand_id`),
  KEY `user_brand_map_brand_fk_idx` (`brand_id`),
  CONSTRAINT `user_brand_map_brand_fk` FOREIGN KEY (`brand_id`) REFERENCES `idis_glossary`.`g_brand` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_brand_map_user_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_g_country_permission_map`
--

DROP TABLE IF EXISTS `user_g_country_permission_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_g_country_permission_map` (
  `user_id` int NOT NULL,
  `g_country_id` int NOT NULL,
  `brand_id` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`g_country_id`,`brand_id`),
  CONSTRAINT `user_g_country_permission_map_user_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_password_reset`
--

DROP TABLE IF EXISTS `user_password_reset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_password_reset` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `hash` varchar(45) NOT NULL,
  `expiry_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_password_reset_userid` (`user_id`),
  CONSTRAINT `FK_user_password_reset_userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_role_map`
--

DROP TABLE IF EXISTS `user_role_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_role_map` (
  `user_id` int NOT NULL,
  `role_id` int NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_role_map_role_FK_idx` (`role_id`),
  CONSTRAINT `user_role_map_role_FK` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_role_map_user_FK` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_variant_assignment_map`
--

DROP TABLE IF EXISTS `user_variant_assignment_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_variant_assignment_map` (
  `user_id` int NOT NULL,
  `variant_id` int NOT NULL,
  `variant_brand_id` int NOT NULL,
  PRIMARY KEY (`user_id`,`variant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `idis_web`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `idis_web` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `idis_web`;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admins` (
  `id` int NOT NULL AUTO_INCREMENT,
  `login` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `approved_members`
--

DROP TABLE IF EXISTS `approved_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `approved_members` (
  `id` int NOT NULL AUTO_INCREMENT,
  `assoc_id` int DEFAULT NULL,
  `companyName` varchar(255) DEFAULT NULL,
  `contact_first_name` varchar(255) DEFAULT NULL,
  `contact_last_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `zipCode` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phoneNo` varchar(255) DEFAULT NULL,
  `ulanguage` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `activity` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'Orange',
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4136 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assoc_users`
--

DROP TABLE IF EXISTS `assoc_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `assoc_users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `assoc_id` int DEFAULT NULL,
  `companyName` varchar(255) DEFAULT NULL,
  `contact_first_name` varchar(255) DEFAULT NULL,
  `contact_last_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `zipCode` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phoneNo` varchar(255) DEFAULT NULL,
  `ulanguage` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `activity` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Red',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14912 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `associations`
--

DROP TABLE IF EXISTS `associations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `associations` (
  `assoc_id` int NOT NULL AUTO_INCREMENT,
  `companyName` varchar(255) DEFAULT NULL,
  `contact_first_name` varchar(255) DEFAULT NULL,
  `contact_last_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `zipCode` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phoneNo` varchar(255) DEFAULT NULL,
  `ulanguage` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `activated` varchar(255) NOT NULL DEFAULT 'No',
  `active` varchar(255) DEFAULT NULL,
  `resetToken` varchar(255) DEFAULT NULL,
  `resetComplete` varchar(255) DEFAULT NULL,
  `email_verified` varchar(255) NOT NULL DEFAULT 'No',
  `GDPR` varchar(45) DEFAULT 'No',
  PRIMARY KEY (`assoc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=606 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `ergebnisse`
--

DROP TABLE IF EXISTS `ergebnisse`;
/*!50001 DROP VIEW IF EXISTS `ergebnisse`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `ergebnisse` AS SELECT 
 1 AS `id`,
 1 AS `DownloadLimit`,
 1 AS `CompanyName`,
 1 AS `ContactName`,
 1 AS `Address`,
 1 AS `Email`,
 1 AS `ZipCode`,
 1 AS `City`,
 1 AS `Country`,
 1 AS `PhoneNo`,
 1 AS `Language`,
 1 AS `Dismantling`,
 1 AS `Shredding`,
 1 AS `UsedParts`,
 1 AS `MaterialRecycling`,
 1 AS `ActivityOther`,
 1 AS `receive_newsletter`,
 1 AS `CarwreckTreatment`,
 1 AS `Employees`,
 1 AS `FromWhom`,
 1 AS `DoYouAlreadyUse`,
 1 AS `LastVersion`,
 1 AS `StillUseIDIS`,
 1 AS `OnPC`,
 1 AS `AsPrintOut`,
 1 AS `Workshop`,
 1 AS `Office`,
 1 AS `Elsewhere`,
 1 AS `DoYouKnowWebUpdate`,
 1 AS `DoYouUpdate`,
 1 AS `OnlyDVD`,
 1 AS `DVDWeb`,
 1 AS `FullWeb`,
 1 AS `HowOftenDoYouUse`,
 1 AS `PreTreatmentInf`,
 1 AS `AirbagNeutralization`,
 1 AS `HeavyMetalLocation`,
 1 AS `MaterialSelection`,
 1 AS `ToolsAndMethods`,
 1 AS `PurposeOther`,
 1 AS `IfYouDontUse`,
 1 AS `NoPCOrOS`,
 1 AS `NotUserfriendly`,
 1 AS `NotTheRightLanguage`,
 1 AS `TooDifficult`,
 1 AS `TakesTooLong`,
 1 AS `CDEasyInstall`,
 1 AS `CDEasyUse`,
 1 AS `CDEasyBrowsing`,
 1 AS `BrowsingWebData`,
 1 AS `IDISModeldatacomplete`,
 1 AS `PreTreatmentInfcomplete`,
 1 AS `MaterialInfUseful`,
 1 AS `ToolsUseful`,
 1 AS `PurposeMissing`,
 1 AS `ReasonMissing`,
 1 AS `CommentMissing`,
 1 AS `OtherSuggestion`,
 1 AS `OrderIDIS`,
 1 AS `SentDate`,
 1 AS `DownloadKey`,
 1 AS `OrderDate`,
 1 AS `QuestSent`,
 1 AS `QuestReplied`,
 1 AS `QuestCode`,
 1 AS `Others`,
 1 AS `Reason`,
 1 AS `OrderDate2`,
 1 AS `Waitlist`,
 1 AS `ResentDate`,
 1 AS `Bemerkung`,
 1 AS `Carwrecks2002`,
 1 AS `Carwrecks2003`,
 1 AS `Carwrecks2005`,
 1 AS `Carwrecks2006`,
 1 AS `Carwrecks2007`,
 1 AS `Carwrecks2008`,
 1 AS `Carwrecks2009`,
 1 AS `Carwrecks2010`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ergebnisse_basis`
--

DROP TABLE IF EXISTS `ergebnisse_basis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ergebnisse_basis` (
  `id` int NOT NULL AUTO_INCREMENT,
  `DownloadLimit` smallint NOT NULL DEFAULT '0',
  `CompanyName` text NOT NULL,
  `contact_first_name` text NOT NULL,
  `contact_last_name` text NOT NULL,
  `Address` text NOT NULL,
  `Email` text NOT NULL,
  `ZipCode` text NOT NULL,
  `City` text NOT NULL,
  `Country` text,
  `PhoneNo` text,
  `Language` varchar(2) NOT NULL,
  `Dismantling` text,
  `Shredding` text,
  `UsedParts` text,
  `MaterialRecycling` text,
  `ActivityOther` text,
  `receive_newsletter` tinyint(1) DEFAULT '0',
  `CarwreckTreatment` text,
  `Employees` text,
  `FromWhom` text,
  `DoYouAlreadyUse` text,
  `LastVersion` text,
  `StillUseIDIS` text,
  `OnPC` text,
  `AsPrintOut` text,
  `Workshop` text,
  `Office` text,
  `Elsewhere` text,
  `DoYouKnowWebUpdate` text,
  `DoYouUpdate` text,
  `OnlyDVD` text,
  `DVDWeb` text,
  `FullWeb` text,
  `HowOftenDoYouUse` text,
  `PreTreatmentInf` text,
  `AirbagNeutralization` text,
  `HeavyMetalLocation` text,
  `MaterialSelection` text,
  `ToolsAndMethods` text,
  `PurposeOther` text,
  `IfYouDontUse` text,
  `NoPCOrOS` text,
  `NotUserfriendly` text,
  `NotTheRightLanguage` text,
  `TooDifficult` text,
  `TakesTooLong` text,
  `CDEasyInstall` text,
  `CDEasyUse` text,
  `CDEasyBrowsing` text,
  `BrowsingWebData` text,
  `IDISModeldatacomplete` text,
  `PreTreatmentInfcomplete` text,
  `MaterialInfUseful` text,
  `ToolsUseful` text,
  `PurposeMissing` text,
  `ReasonMissing` text,
  `CommentMissing` text,
  `OtherSuggestion` text,
  `OrderIDIS` varchar(50) DEFAULT NULL,
  `SentDate` datetime DEFAULT NULL,
  `DownloadKey` text,
  `OrderDate` datetime DEFAULT NULL,
  `QuestSent` datetime DEFAULT NULL,
  `QuestReplied` smallint DEFAULT NULL,
  `QuestCode` varchar(6) DEFAULT NULL,
  `Others` smallint NOT NULL DEFAULT '0',
  `Reason` text,
  `OrderDate2` varchar(20) DEFAULT NULL,
  `Waitlist` text,
  `ResentDate` datetime DEFAULT NULL,
  `Bemerkung` varchar(100) DEFAULT NULL,
  `Carwrecks2002` text,
  `Carwrecks2003` text,
  `Carwrecks2005` varchar(50) DEFAULT NULL,
  `Carwrecks2006` varchar(50) DEFAULT NULL,
  `Carwrecks2007` varchar(50) DEFAULT NULL,
  `Carwrecks2008` varchar(50) DEFAULT NULL,
  `Carwrecks2009` varchar(50) DEFAULT NULL,
  `Carwrecks2010` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=377916 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_translation`
--

DROP TABLE IF EXISTS `g_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_translation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1708 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `g_translation_language_map`
--

DROP TABLE IF EXISTS `g_translation_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `g_translation_language_map` (
  `translation_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`translation_id`,`language_iso639-1`),
  KEY `fk_g_translation_language_map_language_iso639-1_idx` (`language_iso639-1`),
  CONSTRAINT `fk_g_translation_language_map_language_iso639-1` FOREIGN KEY (`language_iso639-1`) REFERENCES `idis_glossary`.`g_language` (`iso639-1`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_g_translation_language_map_translation_id` FOREIGN KEY (`translation_id`) REFERENCES `g_translation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `missing_translation`
--

DROP TABLE IF EXISTS `missing_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `missing_translation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6797 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `part_reuse_brand_link`
--

DROP TABLE IF EXISTS `part_reuse_brand_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `part_reuse_brand_link` (
  `id` int NOT NULL,
  `brand_id` int NOT NULL,
  `link` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reference_brand_link_brand_id_fkey_idx` (`brand_id`),
  CONSTRAINT `part_reuse_brand_link_brand_id_fkey` FOREIGN KEY (`brand_id`) REFERENCES `idis_glossary`.`g_brand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `part_reuse_manufacturer_contact`
--

DROP TABLE IF EXISTS `part_reuse_manufacturer_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `part_reuse_manufacturer_contact` (
  `manufacturer_id` int NOT NULL,
  `contact_mail` text NOT NULL,
  PRIMARY KEY (`manufacturer_id`),
  CONSTRAINT `part_reuse_manufacturer_contact_manufacturer_id_fkey` FOREIGN KEY (`manufacturer_id`) REFERENCES `idis_glossary`.`g_manufacturer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `part_reuse_manufacturer_link`
--

DROP TABLE IF EXISTS `part_reuse_manufacturer_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `part_reuse_manufacturer_link` (
  `id` int NOT NULL,
  `manufacturer_id` int NOT NULL,
  `link` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reference_manufacturer_link_manufacturer_id_fkey_idx` (`manufacturer_id`),
  CONSTRAINT `part_reuse_manufacturer_link_manufacturer_id_fkey` FOREIGN KEY (`manufacturer_id`) REFERENCES `idis_glossary`.`g_manufacturer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `question` (
  `id` int NOT NULL,
  `field` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `question_language_map`
--

DROP TABLE IF EXISTS `question_language_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `question_language_map` (
  `question_id` int NOT NULL,
  `language_iso639-1` varchar(2) NOT NULL,
  `text` text,
  PRIMARY KEY (`question_id`,`language_iso639-1`),
  KEY `question_language_map_language_iso639-1_fkey_idx` (`language_iso639-1`),
  CONSTRAINT `question_language_map_language_iso639-1_fkey` FOREIGN KEY (`language_iso639-1`) REFERENCES `idis_glossary`.`g_language` (`iso639-1`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `question_language_map_question_id_fkey` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reference_tool`
--

DROP TABLE IF EXISTS `reference_tool`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reference_tool` (
  `id` int NOT NULL AUTO_INCREMENT,
  `toolname` text,
  `mainarea` int DEFAULT NULL,
  `company_name` text,
  `street` text,
  `house` text,
  `postal_code` text,
  `town` text,
  `country` int DEFAULT NULL,
  `web` text,
  `lastname` text,
  `firstname` text,
  `phone` text,
  `fax` text,
  `email` text,
  `last_change` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `show` tinyint(1) NOT NULL DEFAULT '0',
  `status` int NOT NULL DEFAULT '0',
  `released` datetime DEFAULT NULL,
  `validation_reminder` datetime DEFAULT NULL,
  `validator` int DEFAULT NULL,
  `reason` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_activation`
--

DROP TABLE IF EXISTS `user_activation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_activation` (
  `active` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `mysql`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `mysql` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `mysql`;

--
-- Table structure for table `columns_priv`
--

DROP TABLE IF EXISTS `columns_priv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `columns_priv` (
  `Host` char(255) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT '',
  `Db` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `User` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Table_name` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Column_name` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Column_priv` set('Select','Insert','Update','References') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`Host`,`Db`,`User`,`Table_name`,`Column_name`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Column privileges';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `component`
--

DROP TABLE IF EXISTS `component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `component` (
  `component_id` int unsigned NOT NULL AUTO_INCREMENT,
  `component_group_id` int unsigned NOT NULL,
  `component_urn` text NOT NULL,
  PRIMARY KEY (`component_id`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Components';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `db`
--

DROP TABLE IF EXISTS `db`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `db` (
  `Host` char(255) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT '',
  `Db` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `User` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Select_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Insert_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Update_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Delete_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Create_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Drop_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Grant_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `References_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Index_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Alter_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Create_tmp_table_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Lock_tables_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Create_view_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Show_view_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Create_routine_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Alter_routine_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Execute_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Event_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Trigger_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`Host`,`Db`,`User`),
  KEY `User` (`User`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Database privileges';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `default_roles`
--

DROP TABLE IF EXISTS `default_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `default_roles` (
  `HOST` char(255) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT '',
  `USER` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `DEFAULT_ROLE_HOST` char(255) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT '%',
  `DEFAULT_ROLE_USER` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`HOST`,`USER`,`DEFAULT_ROLE_HOST`,`DEFAULT_ROLE_USER`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Default roles';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `engine_cost`
--

DROP TABLE IF EXISTS `engine_cost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `engine_cost` (
  `engine_name` varchar(64) NOT NULL,
  `device_type` int NOT NULL,
  `cost_name` varchar(64) NOT NULL,
  `cost_value` float DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comment` varchar(1024) DEFAULT NULL,
  `default_value` float GENERATED ALWAYS AS ((case `cost_name` when _utf8mb3'io_block_read_cost' then 1.0 when _utf8mb3'memory_block_read_cost' then 0.25 else NULL end)) VIRTUAL,
  PRIMARY KEY (`cost_name`,`engine_name`,`device_type`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `func`
--

DROP TABLE IF EXISTS `func`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `func` (
  `name` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ret` tinyint NOT NULL DEFAULT '0',
  `dl` char(128) COLLATE utf8_bin NOT NULL DEFAULT '',
  `type` enum('function','aggregate') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`name`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='User defined functions';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `global_grants`
--

DROP TABLE IF EXISTS `global_grants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `global_grants` (
  `USER` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `HOST` char(255) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT '',
  `PRIV` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `WITH_GRANT_OPTION` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`USER`,`HOST`,`PRIV`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Extended global grants';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gtid_executed`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE IF NOT EXISTS `gtid_executed` (
  `source_uuid` char(36) NOT NULL COMMENT 'uuid of the source where the transaction was originally executed.',
  `interval_start` bigint NOT NULL COMMENT 'First number of interval.',
  `interval_end` bigint NOT NULL COMMENT 'Last number of interval.',
  PRIMARY KEY (`source_uuid`,`interval_start`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `help_category`
--

DROP TABLE IF EXISTS `help_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `help_category` (
  `help_category_id` smallint unsigned NOT NULL,
  `name` char(64) NOT NULL,
  `parent_category_id` smallint unsigned DEFAULT NULL,
  `url` text NOT NULL,
  PRIMARY KEY (`help_category_id`),
  UNIQUE KEY `name` (`name`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='help categories';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `help_keyword`
--

DROP TABLE IF EXISTS `help_keyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `help_keyword` (
  `help_keyword_id` int unsigned NOT NULL,
  `name` char(64) NOT NULL,
  PRIMARY KEY (`help_keyword_id`),
  UNIQUE KEY `name` (`name`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='help keywords';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `help_relation`
--

DROP TABLE IF EXISTS `help_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `help_relation` (
  `help_topic_id` int unsigned NOT NULL,
  `help_keyword_id` int unsigned NOT NULL,
  PRIMARY KEY (`help_keyword_id`,`help_topic_id`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='keyword-topic relation';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `help_topic`
--

DROP TABLE IF EXISTS `help_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `help_topic` (
  `help_topic_id` int unsigned NOT NULL,
  `name` char(64) NOT NULL,
  `help_category_id` smallint unsigned NOT NULL,
  `description` text NOT NULL,
  `example` text NOT NULL,
  `url` text NOT NULL,
  PRIMARY KEY (`help_topic_id`),
  UNIQUE KEY `name` (`name`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='help topics';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `password_history`
--

DROP TABLE IF EXISTS `password_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_history` (
  `Host` char(255) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT '',
  `User` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Password_timestamp` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `Password` text COLLATE utf8_bin,
  PRIMARY KEY (`Host`,`User`,`Password_timestamp` DESC)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Password history for user accounts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `plugin`
--

DROP TABLE IF EXISTS `plugin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plugin` (
  `name` varchar(64) NOT NULL DEFAULT '',
  `dl` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`name`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='MySQL plugins';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `procs_priv`
--

DROP TABLE IF EXISTS `procs_priv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `procs_priv` (
  `Host` char(255) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT '',
  `Db` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `User` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Routine_name` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `Routine_type` enum('FUNCTION','PROCEDURE') COLLATE utf8_bin NOT NULL,
  `Grantor` varchar(288) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Proc_priv` set('Execute','Alter Routine','Grant') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Host`,`Db`,`User`,`Routine_name`,`Routine_type`),
  KEY `Grantor` (`Grantor`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Procedure privileges';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `proxies_priv`
--

DROP TABLE IF EXISTS `proxies_priv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `proxies_priv` (
  `Host` char(255) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT '',
  `User` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Proxied_host` char(255) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT '',
  `Proxied_user` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `With_grant` tinyint(1) NOT NULL DEFAULT '0',
  `Grantor` varchar(288) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Host`,`User`,`Proxied_host`,`Proxied_user`),
  KEY `Grantor` (`Grantor`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='User proxy privileges';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role_edges`
--

DROP TABLE IF EXISTS `role_edges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_edges` (
  `FROM_HOST` char(255) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT '',
  `FROM_USER` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `TO_HOST` char(255) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT '',
  `TO_USER` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `WITH_ADMIN_OPTION` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`FROM_HOST`,`FROM_USER`,`TO_HOST`,`TO_USER`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Role hierarchy and role grants';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `server_cost`
--

DROP TABLE IF EXISTS `server_cost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `server_cost` (
  `cost_name` varchar(64) NOT NULL,
  `cost_value` float DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comment` varchar(1024) DEFAULT NULL,
  `default_value` float GENERATED ALWAYS AS ((case `cost_name` when _utf8mb3'disk_temptable_create_cost' then 20.0 when _utf8mb3'disk_temptable_row_cost' then 0.5 when _utf8mb3'key_compare_cost' then 0.05 when _utf8mb3'memory_temptable_create_cost' then 1.0 when _utf8mb3'memory_temptable_row_cost' then 0.1 when _utf8mb3'row_evaluate_cost' then 0.1 else NULL end)) VIRTUAL,
  PRIMARY KEY (`cost_name`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `servers`
--

DROP TABLE IF EXISTS `servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `servers` (
  `Server_name` char(64) NOT NULL DEFAULT '',
  `Host` char(255) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT '',
  `Db` char(64) NOT NULL DEFAULT '',
  `Username` char(64) NOT NULL DEFAULT '',
  `Password` char(64) NOT NULL DEFAULT '',
  `Port` int NOT NULL DEFAULT '0',
  `Socket` char(64) NOT NULL DEFAULT '',
  `Wrapper` char(64) NOT NULL DEFAULT '',
  `Owner` char(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`Server_name`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='MySQL Foreign Servers table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slave_master_info`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE IF NOT EXISTS `slave_master_info` (
  `Number_of_lines` int unsigned NOT NULL COMMENT 'Number of lines in the file.',
  `Master_log_name` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The name of the master binary log currently being read from the master.',
  `Master_log_pos` bigint unsigned NOT NULL COMMENT 'The master log position of the last read event.',
  `Host` char(255) CHARACTER SET ascii COLLATE ascii_general_ci DEFAULT NULL COMMENT 'The host name of the master.',
  `User_name` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'The user name used to connect to the master.',
  `User_password` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'The password used to connect to the master.',
  `Port` int unsigned NOT NULL COMMENT 'The network port used to connect to the master.',
  `Connect_retry` int unsigned NOT NULL COMMENT 'The period (in seconds) that the slave will wait before trying to reconnect to the master.',
  `Enabled_ssl` tinyint(1) NOT NULL COMMENT 'Indicates whether the server supports SSL connections.',
  `Ssl_ca` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'The file used for the Certificate Authority (CA) certificate.',
  `Ssl_capath` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'The path to the Certificate Authority (CA) certificates.',
  `Ssl_cert` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'The name of the SSL certificate file.',
  `Ssl_cipher` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'The name of the cipher in use for the SSL connection.',
  `Ssl_key` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'The name of the SSL key file.',
  `Ssl_verify_server_cert` tinyint(1) NOT NULL COMMENT 'Whether to verify the server certificate.',
  `Heartbeat` float NOT NULL,
  `Bind` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'Displays which interface is employed when connecting to the MySQL server',
  `Ignored_server_ids` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'The number of server IDs to be ignored, followed by the actual server IDs',
  `Uuid` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'The master server uuid.',
  `Retry_count` bigint unsigned NOT NULL COMMENT 'Number of reconnect attempts, to the master, before giving up.',
  `Ssl_crl` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'The file used for the Certificate Revocation List (CRL)',
  `Ssl_crlpath` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'The path used for Certificate Revocation List (CRL) files',
  `Enabled_auto_position` tinyint(1) NOT NULL COMMENT 'Indicates whether GTIDs will be used to retrieve events from the master.',
  `Channel_name` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'The channel on which the slave is connected to a source. Used in Multisource Replication',
  `Tls_version` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'Tls version',
  `Public_key_path` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'The file containing public key of master server.',
  `Get_public_key` tinyint(1) NOT NULL COMMENT 'Preference to get public key from master.',
  `Network_namespace` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'Network namespace used for communication with the master server.',
  `Master_compression_algorithm` char(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Compression algorithm supported for data transfer between master and slave.',
  `Master_zstd_compression_level` int unsigned NOT NULL COMMENT 'Compression level associated with zstd compression algorithm.',
  `Tls_ciphersuites` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'Ciphersuites used for TLS 1.3 communication with the master server.',
  PRIMARY KEY (`Channel_name`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Master Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slave_relay_log_info`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE IF NOT EXISTS `slave_relay_log_info` (
  `Number_of_lines` int unsigned NOT NULL COMMENT 'Number of lines in the file or rows in the table. Used to version table definitions.',
  `Relay_log_name` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'The name of the current relay log file.',
  `Relay_log_pos` bigint unsigned DEFAULT NULL COMMENT 'The relay log position of the last executed event.',
  `Master_log_name` text CHARACTER SET utf8 COLLATE utf8_bin COMMENT 'The name of the master binary log file from which the events in the relay log file were read.',
  `Master_log_pos` bigint unsigned DEFAULT NULL COMMENT 'The master log position of the last executed event.',
  `Sql_delay` int DEFAULT NULL COMMENT 'The number of seconds that the slave must lag behind the master.',
  `Number_of_workers` int unsigned DEFAULT NULL,
  `Id` int unsigned DEFAULT NULL COMMENT 'Internal Id that uniquely identifies this record.',
  `Channel_name` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'The channel on which the slave is connected to a source. Used in Multisource Replication',
  `Privilege_checks_username` char(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'Username part of PRIVILEGE_CHECKS_USER.',
  `Privilege_checks_hostname` char(255) CHARACTER SET ascii COLLATE ascii_general_ci DEFAULT NULL COMMENT 'Hostname part of PRIVILEGE_CHECKS_USER.',
  `Require_row_format` tinyint(1) NOT NULL COMMENT 'Indicates whether the channel shall only accept row based events.',
  `Require_table_primary_key_check` enum('STREAM','ON','OFF') NOT NULL DEFAULT 'STREAM' COMMENT 'Indicates what is the channel policy regarding tables having primary keys on create and alter table queries',
  PRIMARY KEY (`Channel_name`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Relay Log Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slave_worker_info`
--

DROP TABLE IF EXISTS `slave_worker_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `slave_worker_info` (
  `Id` int unsigned NOT NULL,
  `Relay_log_name` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Relay_log_pos` bigint unsigned NOT NULL,
  `Master_log_name` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Master_log_pos` bigint unsigned NOT NULL,
  `Checkpoint_relay_log_name` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Checkpoint_relay_log_pos` bigint unsigned NOT NULL,
  `Checkpoint_master_log_name` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Checkpoint_master_log_pos` bigint unsigned NOT NULL,
  `Checkpoint_seqno` int unsigned NOT NULL,
  `Checkpoint_group_size` int unsigned NOT NULL,
  `Checkpoint_group_bitmap` blob NOT NULL,
  `Channel_name` char(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'The channel on which the slave is connected to a source. Used in Multisource Replication',
  PRIMARY KEY (`Channel_name`,`Id`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Worker Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tables_priv`
--

DROP TABLE IF EXISTS `tables_priv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tables_priv` (
  `Host` char(255) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT '',
  `Db` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `User` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Table_name` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Grantor` varchar(288) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Table_priv` set('Select','Insert','Update','Delete','Create','Drop','Grant','References','Index','Alter','Create View','Show view','Trigger') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `Column_priv` set('Select','Insert','Update','References') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`Host`,`Db`,`User`,`Table_name`),
  KEY `Grantor` (`Grantor`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Table privileges';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_zone`
--

DROP TABLE IF EXISTS `time_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `time_zone` (
  `Time_zone_id` int unsigned NOT NULL AUTO_INCREMENT,
  `Use_leap_seconds` enum('Y','N') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`Time_zone_id`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Time zones';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_zone_leap_second`
--

DROP TABLE IF EXISTS `time_zone_leap_second`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `time_zone_leap_second` (
  `Transition_time` bigint NOT NULL,
  `Correction` int NOT NULL,
  PRIMARY KEY (`Transition_time`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Leap seconds information for time zones';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_zone_name`
--

DROP TABLE IF EXISTS `time_zone_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `time_zone_name` (
  `Name` char(64) NOT NULL,
  `Time_zone_id` int unsigned NOT NULL,
  PRIMARY KEY (`Name`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Time zone names';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_zone_transition`
--

DROP TABLE IF EXISTS `time_zone_transition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `time_zone_transition` (
  `Time_zone_id` int unsigned NOT NULL,
  `Transition_time` bigint NOT NULL,
  `Transition_type_id` int unsigned NOT NULL,
  PRIMARY KEY (`Time_zone_id`,`Transition_time`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Time zone transitions';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_zone_transition_type`
--

DROP TABLE IF EXISTS `time_zone_transition_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `time_zone_transition_type` (
  `Time_zone_id` int unsigned NOT NULL,
  `Transition_type_id` int unsigned NOT NULL,
  `Offset` int NOT NULL DEFAULT '0',
  `Is_DST` tinyint unsigned NOT NULL DEFAULT '0',
  `Abbreviation` char(8) NOT NULL DEFAULT '',
  PRIMARY KEY (`Time_zone_id`,`Transition_type_id`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Time zone transition types';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `Host` char(255) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL DEFAULT '',
  `User` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Select_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Insert_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Update_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Delete_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Create_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Drop_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Reload_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Shutdown_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Process_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `File_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Grant_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `References_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Index_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Alter_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Show_db_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Super_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Create_tmp_table_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Lock_tables_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Execute_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Repl_slave_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Repl_client_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Create_view_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Show_view_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Create_routine_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Alter_routine_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Create_user_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Event_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Trigger_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Create_tablespace_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `ssl_type` enum('','ANY','X509','SPECIFIED') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `ssl_cipher` blob NOT NULL,
  `x509_issuer` blob NOT NULL,
  `x509_subject` blob NOT NULL,
  `max_questions` int unsigned NOT NULL DEFAULT '0',
  `max_updates` int unsigned NOT NULL DEFAULT '0',
  `max_connections` int unsigned NOT NULL DEFAULT '0',
  `max_user_connections` int unsigned NOT NULL DEFAULT '0',
  `plugin` char(64) COLLATE utf8_bin NOT NULL DEFAULT 'caching_sha2_password',
  `authentication_string` text COLLATE utf8_bin,
  `password_expired` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `password_last_changed` timestamp NULL DEFAULT NULL,
  `password_lifetime` smallint unsigned DEFAULT NULL,
  `account_locked` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Create_role_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Drop_role_priv` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N',
  `Password_reuse_history` smallint unsigned DEFAULT NULL,
  `Password_reuse_time` smallint unsigned DEFAULT NULL,
  `Password_require_current` enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `User_attributes` json DEFAULT NULL,
  PRIMARY KEY (`Host`,`User`)
) /*!50100 TABLESPACE `mysql` */ ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin STATS_PERSISTENT=0 ROW_FORMAT=DYNAMIC COMMENT='Users and global privileges';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `general_log`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE IF NOT EXISTS `general_log` (
  `event_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `user_host` mediumtext NOT NULL,
  `thread_id` bigint unsigned NOT NULL,
  `server_id` int unsigned NOT NULL,
  `command_type` varchar(64) NOT NULL,
  `argument` mediumblob NOT NULL
) ENGINE=CSV DEFAULT CHARSET=utf8 COMMENT='General log';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slow_log`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE IF NOT EXISTS `slow_log` (
  `start_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `user_host` mediumtext NOT NULL,
  `query_time` time(6) NOT NULL,
  `lock_time` time(6) NOT NULL,
  `rows_sent` int NOT NULL,
  `rows_examined` int NOT NULL,
  `db` varchar(512) NOT NULL,
  `last_insert_id` int NOT NULL,
  `insert_id` int NOT NULL,
  `server_id` int unsigned NOT NULL,
  `sql_text` mediumblob NOT NULL,
  `thread_id` bigint unsigned NOT NULL
) ENGINE=CSV DEFAULT CHARSET=utf8 COMMENT='Slow log';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `idis_data`
--

USE `idis_data`;

--
-- Final view structure for view `brand_model_variant_view`
--

/*!50001 DROP VIEW IF EXISTS `brand_model_variant_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `brand_model_variant_view` AS select `vlm`.`language_iso639-1` AS `language_short`,`b`.`id` AS `brand_id`,`blm`.`text` AS `brand_name`,`m`.`id` AS `model_id`,`mlm`.`text` AS `model_name`,`v`.`id` AS `variant_id`,`vlm`.`text` AS `variant_name`,`v`.`period_from_month` AS `period_from_month`,`v`.`period_from_year` AS `period_from_year`,`v`.`period_to_month` AS `period_to_month`,`v`.`period_to_year` AS `period_to_year`,`v`.`variant_status_id` AS `status_id` from (((((`variant` `v` join `variant_language_map` `vlm` on((`vlm`.`variant_id` = `v`.`id`))) join `model` `m` on((`m`.`id` = `v`.`model_id`))) join `model_language_map` `mlm` on(((`mlm`.`model_id` = `m`.`id`) and (`mlm`.`language_iso639-1` = `vlm`.`language_iso639-1`)))) join `idis_glossary`.`g_brand` `b` on((`b`.`id` = `m`.`g_brand_id`))) join `idis_glossary`.`g_brand_language_map` `blm` on(((`blm`.`brand_id` = `b`.`id`) and (`blm`.`language_iso639-1` = `vlm`.`language_iso639-1`)))) order by `blm`.`text`,`mlm`.`text`,`vlm`.`text`,`v`.`period_from_year`,`v`.`period_from_month`,`v`.`period_to_year`,`v`.`period_to_month` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `components_with_add_info`
--

/*!50001 DROP VIEW IF EXISTS `components_with_add_info`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `components_with_add_info` AS select `c`.`id` AS `id`,`c`.`g_part_id` AS `g_part_id`,`cam`.`add_info_id` AS `add_info_id`,`cam`.`add_info_typ` AS `add_info_typ`,`c`.`variant_id` AS `variant_id`,`plm`.`language_iso639-1` AS `language_iso639-1`,`plm`.`text` AS `text`,(case when (`alm`.`language_iso639-1` is null) then false else true end) AS `has_language` from ((((`component_addinfo_map` `cam` join `component` `c` on((`c`.`id` = `cam`.`component_id`))) join `idis_glossary`.`g_part` `p` on((`c`.`g_part_id` = `p`.`id`))) join `idis_glossary`.`g_part_language_map` `plm` on((`p`.`id` = `plm`.`part_id`))) left join `add_info_language_map` `alm` on(((`cam`.`add_info_id` = `alm`.`add_info_id`) and (`plm`.`language_iso639-1` = `alm`.`language_iso639-1`)))) order by `c`.`g_area_id`,`plm`.`text` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `derivativ_name`
--

/*!50001 DROP VIEW IF EXISTS `derivativ_name`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `derivativ_name` AS select `d`.`id` AS `id`,`d`.`language_iso639-1` AS `language_iso639-1`,group_concat(`d`.`text` order by `d`.`sortorder` ASC separator ' ') AS `derivativ_name` from `derivatives` `d` group by `d`.`id`,`d`.`language_iso639-1` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `derivatives`
--

/*!50001 DROP VIEW IF EXISTS `derivatives`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `derivatives` AS select `d`.`id` AS `id`,`d`.`variant_id` AS `variant_id`,`gdlm`.`language_iso639-1` AS `language_iso639-1`,`gdlm`.`text` AS `text`,`dm`.`sortorder` AS `sortorder` from (((`derivativ` `d` join `derivativ_g_derivativ_map` `dm` on((`d`.`id` = `dm`.`derivativ_id`))) join `idis_glossary`.`g_derivativ` `gd` on((`gd`.`id` = `dm`.`g_derivativ_id`))) join `idis_glossary`.`g_derivativ_language_map` `gdlm` on((`gd`.`id` = `gdlm`.`derivativ_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `fixing_qty_view`
--

/*!50001 DROP VIEW IF EXISTS `fixing_qty_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `fixing_qty_view` AS select `f`.`id` AS `id`,`m`.`component_id` AS `component_id`,`f`.`sortorder` AS `sortorder`,`lm`.`language_iso639-1` AS `language_iso639-1`,`lm`.`text` AS `text`,`m`.`quantity` AS `quantity`,`m`.`id` AS `quantityId` from ((`idis_glossary`.`g_fixing` `f` join `idis_glossary`.`g_fixing_language_map` `lm` on((`f`.`id` = `lm`.`fixing_id`))) join `component_fixing_map` `m` on((`m`.`g_fixing_id` = `f`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_fixings_component_mapping_view`
--

/*!50001 DROP VIEW IF EXISTS `g_fixings_component_mapping_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_fixings_component_mapping_view` AS select `f`.`id` AS `id`,`f`.`sortorder` AS `sortorder`,`flm`.`language_iso639-1` AS `language_iso639-1`,`flm`.`text` AS `text`,`cfm`.`component_id` AS `component_id`,`cfm`.`quantity` AS `quantity` from ((`idis_glossary`.`g_fixing` `f` join `component_fixing_map` `cfm` on((`cfm`.`g_fixing_id` = `f`.`id`))) join `idis_glossary`.`g_fixing_language_map` `flm` on((`f`.`id` = `flm`.`fixing_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_recycling_component_mapping_view`
--

/*!50001 DROP VIEW IF EXISTS `g_recycling_component_mapping_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_recycling_component_mapping_view` AS select `t`.`id` AS `id`,`t`.`sortorder` AS `sortorder`,`tlm`.`language_iso639-1` AS `language_iso639-1`,`tlm`.`text` AS `text`,`c`.`component_id` AS `component_id` from ((`idis_glossary`.`g_recycling` `t` join `idis_glossary`.`g_recycling_language_map` `tlm` on((`t`.`id` = `tlm`.`recycling_id`))) join `component_recycling_map` `c` on((`c`.`g_recycling_id` = `t`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_tool_component_mapping_view`
--

/*!50001 DROP VIEW IF EXISTS `g_tool_component_mapping_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_tool_component_mapping_view` AS select `t`.`id` AS `id`,`t`.`info` AS `info`,`t`.`sortorder` AS `sortorder`,`tlm`.`language_iso639-1` AS `language_iso639-1`,`tlm`.`text` AS `text`,`c`.`component_id` AS `component_id` from ((`idis_glossary`.`g_tool` `t` join `idis_glossary`.`g_tool_language_map` `tlm` on((`t`.`id` = `tlm`.`tool_id`))) join `component_tool_map` `c` on((`c`.`g_tool_id` = `t`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `models`
--

/*!50001 DROP VIEW IF EXISTS `models`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `models` AS select `model_language_map`.`model_id` AS `model_id`,`model_language_map`.`language_iso639-1` AS `language_iso639-1`,`model_language_map`.`text` AS `text`,`model`.`g_brand_id` AS `g_brand_id` from (`model_language_map` join `model` on((`model_language_map`.`model_id` = `model`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `part_area_view`
--

/*!50001 DROP VIEW IF EXISTS `part_area_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `part_area_view` AS select `c`.`variant_id` AS `variant_id`,`p`.`id` AS `id`,`p`.`default_picture` AS `default_picture`,`p`.`enabled` AS `enabled`,`p`.`caution` AS `caution`,`p`.`comment` AS `comment`,`p`.`g_area_id` AS `area_id`,`a`.`mainarea_id` AS `mainarea_id`,`a`.`picture_enabled` AS `picture_enabled`,`a`.`picture_disabled` AS `picture_disabled`,`a`.`picture_active` AS `picture_active`,`alm`.`language_iso639-1` AS `language_iso639-1`,`alm`.`text` AS `areaName`,`plm`.`text` AS `partName` from ((((`idis_glossary`.`g_part` `p` join `idis_glossary`.`g_area` `a` on((`a`.`id` = `p`.`g_area_id`))) join `idis_glossary`.`g_area_language_map` `alm` on((`a`.`id` = `alm`.`area_id`))) join `idis_glossary`.`g_part_language_map` `plm` on(((`p`.`id` = `plm`.`part_id`) and (`plm`.`language_iso639-1` = `alm`.`language_iso639-1`)))) join `component` `c` on((`c`.`g_part_id` = `p`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `part_component_view`
--

/*!50001 DROP VIEW IF EXISTS `part_component_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `part_component_view` AS select `p`.`id` AS `id`,`c`.`variant_id` AS `variant_id`,`a`.`mainarea_id` AS `mainarea_id`,`a`.`id` AS `area_id`,`p`.`caution` AS `caution`,`c`.`id` AS `component_id`,`c`.`quantity` AS `quantity`,`c`.`derivative_id` AS `derivative_id`,`c`.`g_material_id` AS `g_material_id`,`plm`.`text` AS `text`,`plm`.`language_iso639-1` AS `language_iso639-1`,`pos`.`id` AS `position_id`,`pos`.`text` AS `position_text` from ((((`component` `c` join `idis_glossary`.`g_part` `p` on((`c`.`g_part_id` = `p`.`id`))) join `idis_glossary`.`g_part_language_map` `plm` on((`p`.`id` = `plm`.`part_id`))) join `idis_glossary`.`g_area` `a` on((`a`.`id` = `p`.`g_area_id`))) join `idis_glossary`.`g_positions` `pos` on(((`pos`.`id` = `c`.`g_position_id`) and (`pos`.`language_iso639-1` = `plm`.`language_iso639-1`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `part_sno_view`
--

/*!50001 DROP VIEW IF EXISTS `part_sno_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `part_sno_view` AS select `c`.`id` AS `id`,`c`.`g_area_id` AS `g_area_id`,`c`.`serial_number` AS `serial_number`,`c`.`variant_id` AS `variant_id`,`c`.`weight` AS `weight`,`plm`.`language_iso639-1` AS `language_iso639-1`,`plm`.`text` AS `text` from (`component` `c` join `idis_glossary`.`g_part_language_map` `plm` on((`c`.`g_part_id` = `plm`.`part_id`))) where (`c`.`serial_number` is not null) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pyro_component_view`
--

/*!50001 DROP VIEW IF EXISTS `pyro_component_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `pyro_component_view` AS select `c`.`variant_id` AS `variant_id`,`c`.`derivative_id` AS `derivative_id`,`c`.`id` AS `component_id`,`p`.`id` AS `part_id`,`gp`.`id` AS `pyro_id`,`gpc`.`sortorder` AS `column_index`,`gpcv`.`short` AS `column_value` from ((((((`component` `c` join `idis_glossary`.`g_part` `p` on((`c`.`g_part_id` = `p`.`id`))) join `idis_glossary`.`g_area` `a` on((`a`.`id` = `p`.`g_area_id`))) join `component_gpyro_value_map` `cgpvm` on((`c`.`id` = `cgpvm`.`component_id`))) join `idis_glossary`.`g_pyro` `gp` on((`p`.`id` = `gp`.`g_part_id`))) join `idis_glossary`.`g_pyro_column` `gpc` on(((`cgpvm`.`g_pyro_column_id` = `gpc`.`column_id`) and (`gp`.`list` = `gpc`.`list`)))) join `idis_glossary`.`g_pyro_column_value` `gpcv` on((`gpcv`.`id` = `cgpvm`.`g_pyro_column_value_id`))) where (`a`.`mainarea_id` = 2) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_brand_model_variant_map`
--

/*!50001 DROP VIEW IF EXISTS `user_brand_model_variant_map`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `user_brand_model_variant_map` AS select `u`.`id` AS `user_id`,`bmvv`.`brand_id` AS `brand_id`,`bmvv`.`model_id` AS `model_id`,`bmvv`.`variant_id` AS `variant_id` from ((((`brand_model_variant_view` `bmvv` join `idis_glossary`.`g_brand_g_country_map` `bcm` on((`bcm`.`g_brand_id` = `bmvv`.`brand_id`))) join `model` `m` on(((`m`.`id` = `bmvv`.`model_id`) and (`m`.`active` = true)))) join `variant_country_map` `vcm` on((`vcm`.`variant_id` = `bmvv`.`variant_id`))) join `idis_users`.`user` `u` on(((`u`.`language` = `bmvv`.`language_short`) and (`u`.`country` = `bcm`.`g_country_id`) and (`u`.`country` = `vcm`.`g_country_id`)))) where (`bmvv`.`status_id` >= 0) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `variants`
--

/*!50001 DROP VIEW IF EXISTS `variants`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `variants` AS select `v`.`id` AS `id`,`v`.`model_id` AS `model_id`,`v`.`variant_status_id` AS `variant_status_id`,`v`.`period_from_month` AS `period_from_month`,`v`.`period_from_year` AS `period_from_year`,`v`.`period_to_month` AS `period_to_month`,`v`.`period_to_year` AS `period_to_year`,`v`.`caravan` AS `caravan`,`v`.`version` AS `version`,`v`.`creation_user_id` AS `creation_user_id`,`v`.`creation_time` AS `creation_time`,`v`.`first_release_user_id` AS `first_release_user_id`,`v`.`first_release_time` AS `first_release_time`,`v`.`last_change_user_id` AS `last_change_user_id`,`v`.`last_change_time` AS `last_change_time`,`v`.`prerelease_user_id` AS `prerelease_user_id`,`v`.`release_user_id` AS `release_user_id`,`v`.`release_time` AS `release_time`,`v`.`picture` AS `picture`,`vlm`.`language_iso639-1` AS `language_iso639-1`,`vlm`.`text` AS `text` from (`variant` `v` join `variant_language_map` `vlm` on((`vlm`.`variant_id` = `v`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Current Database: `idis_glossary`
--

USE `idis_glossary`;

--
-- Final view structure for view `activity_areas`
--

/*!50001 DROP VIEW IF EXISTS `activity_areas`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `activity_areas` AS select `g_area`.`id` AS `id`,`g_area`.`mainarea_id` AS `mainarea_id`,`g_area`.`picture_enabled` AS `picture_enabled`,`g_area`.`picture_disabled` AS `picture_disabled`,`g_area`.`picture_active` AS `picture_active`,`g_area_language_map`.`area_id` AS `area_id`,`g_area_language_map`.`language_iso639-1` AS `language_iso639-1`,`g_area_language_map`.`text` AS `text` from (`g_area` join `g_area_language_map` on((`g_area`.`id` = `g_area_language_map`.`area_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `brand_view`
--

/*!50001 DROP VIEW IF EXISTS `brand_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `brand_view` AS select `g_brand`.`id` AS `id`,`g_brand`.`manufacturer_id` AS `manufacturer_id`,`g_brand`.`logo` AS `logo`,`g_brand`.`active` AS `active`,`g_brand_language_map`.`language_iso639-1` AS `language_iso639-1`,`g_brand_language_map`.`text` AS `text` from (`g_brand` join `g_brand_language_map` on((`g_brand`.`id` = `g_brand_language_map`.`brand_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `button_picture_view`
--

/*!50001 DROP VIEW IF EXISTS `button_picture_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `button_picture_view` AS select `gmbp`.`area_id` AS `area_id`,`gmbp`.`part_id` AS `id`,0 AS `is_material`,`gmbp`.`comment` AS `comment`,`gmbp`.`sortorder` AS `sortorder`,`gbp`.`comment` AS `picture_comment`,`gbp`.`picture_enabled` AS `picture` from (`g_mainarea_button_part` `gmbp` join `g_button_picture` `gbp` on((`gbp`.`id` = `gmbp`.`button_picture_id`))) where (`gmbp`.`active` = true) union all select `gmbm`.`area_id` AS `area_id`,`gmbm`.`material_id` AS `id`,1 AS `is_material`,`gmbm`.`comment` AS `comment`,`gmbm`.`sortorder` AS `sortorder`,`gbp`.`comment` AS `picture_comment`,`gbp`.`picture_enabled` AS `picture` from (`g_mainarea_button_material` `gmbm` join `g_button_picture` `gbp` on((`gbp`.`id` = `gmbm`.`button_picture_id`))) where (`gmbm`.`active` = true) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `documents`
--

/*!50001 DROP VIEW IF EXISTS `documents`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `documents` AS select `d`.`id` AS `id`,`d`.`doctype` AS `doctype`,`dlm`.`language_iso639-1` AS `language_iso639-1`,`dlm`.`documentText` AS `documentText` from (`g_documents` `d` join `g_document_language_map` `dlm` on((`d`.`id` = `dlm`.`document_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_comments`
--

/*!50001 DROP VIEW IF EXISTS `g_comments`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_comments` AS select `c`.`id` AS `id`,`c`.`sortorder` AS `sortorder`,`clm`.`language_iso639-1` AS `language_iso639-1`,`clm`.`text` AS `text` from (`g_comment` `c` join `g_comment_language_map` `clm` on((`c`.`id` = `clm`.`comment_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_countries`
--

/*!50001 DROP VIEW IF EXISTS `g_countries`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_countries` AS select `c`.`id` AS `country_id`,`c`.`flag` AS `flag`,`clm`.`language_iso639-1` AS `language_iso639-1`,`clm`.`text` AS `country_name`,(case when (`ctm`.`territory_id` is null) then 0 else `ctm`.`territory_id` end) AS `territory_id`,(case when (`tlm`.`text` is null) then '' else `tlm`.`text` end) AS `territory_name` from (((`g_country` `c` join `g_country_language_map` `clm` on((`c`.`id` = `clm`.`country_id`))) left join `g_country_territory_map` `ctm` on((`c`.`id` = `ctm`.`country_id`))) left join `g_territory_language_map` `tlm` on(((`tlm`.`territory_id` = `ctm`.`territory_id`) and (`tlm`.`language_iso639-1` = `clm`.`language_iso639-1`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_derivativ_groups`
--

/*!50001 DROP VIEW IF EXISTS `g_derivativ_groups`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_derivativ_groups` AS select `g`.`id` AS `id`,`g`.`sortorder` AS `sortorder`,`glm`.`language_iso639-1` AS `language_iso639-1`,`glm`.`text` AS `text` from (`g_derivativ_group` `g` join `g_derivativ_group_language_map` `glm` on((`g`.`id` = `glm`.`derivativ_group_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_derivatives`
--

/*!50001 DROP VIEW IF EXISTS `g_derivatives`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_derivatives` AS select `d`.`id` AS `id`,`d`.`derivativ_group_id` AS `derivativ_group_id`,`d`.`sortorder` AS `sortorder`,`gl`.`language_iso639-1` AS `language_iso639-1`,`gl`.`text` AS `text` from (`g_derivativ` `d` join `g_derivativ_language_map` `gl` on((`gl`.`derivativ_id` = `d`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_families`
--

/*!50001 DROP VIEW IF EXISTS `g_families`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_families` AS select `g_family`.`id` AS `id`,`g_family`.`sortorder` AS `sortorder`,`g_family_language_map`.`language_iso639-1` AS `language_iso639-1`,`g_family_language_map`.`text` AS `text` from (`g_family` join `g_family_language_map` on((`g_family`.`id` = `g_family_language_map`.`family_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_fixings`
--

/*!50001 DROP VIEW IF EXISTS `g_fixings`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_fixings` AS select `f`.`id` AS `id`,`f`.`sortorder` AS `sortorder`,`fm`.`language_iso639-1` AS `language_iso639-1`,`fm`.`text` AS `text` from (`g_fixing` `f` join `g_fixing_language_map` `fm` on((`f`.`id` = `fm`.`fixing_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_materials`
--

/*!50001 DROP VIEW IF EXISTS `g_materials`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_materials` AS select `m`.`id` AS `id`,`m`.`enabled` AS `enabled`,`m`.`caution` AS `caution`,`m`.`comment` AS `comment`,`m`.`g_family_id` AS `g_family_id`,`mlm`.`language_iso639-1` AS `language_iso639-1`,`mlm`.`text` AS `text` from (`g_material` `m` join `g_material_language_map` `mlm` on((`m`.`id` = `mlm`.`material_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_methods`
--

/*!50001 DROP VIEW IF EXISTS `g_methods`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_methods` AS select `m`.`id` AS `id`,`m`.`sortorder` AS `sortorder`,`mlm`.`language_iso639-1` AS `language_iso639-1`,`mlm`.`text` AS `text` from (`g_method` `m` join `g_method_language_map` `mlm` on((`m`.`id` = `mlm`.`method_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_parts`
--

/*!50001 DROP VIEW IF EXISTS `g_parts`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_parts` AS select `p`.`id` AS `id`,`p`.`default_picture` AS `default_picture`,`p`.`enabled` AS `enabled`,`p`.`caution` AS `caution`,`p`.`comment` AS `comment`,`p`.`picture_path` AS `picture_path`,`a`.`mainarea_id` AS `mainarea_id`,`p`.`g_area_id` AS `area_id`,`plm`.`language_iso639-1` AS `language_iso639-1`,`plm`.`text` AS `text` from ((`g_part` `p` join `g_part_language_map` `plm` on((`p`.`id` = `plm`.`part_id`))) join `g_area` `a` on((`a`.`id` = `p`.`g_area_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_positions`
--

/*!50001 DROP VIEW IF EXISTS `g_positions`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_positions` AS select `p`.`id` AS `id`,`p`.`sortorder` AS `sortorder`,`plm`.`language_iso639-1` AS `language_iso639-1`,`plm`.`text` AS `text` from (`g_position` `p` join `g_position_language_map` `plm` on((`p`.`id` = `plm`.`position_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_recyclings`
--

/*!50001 DROP VIEW IF EXISTS `g_recyclings`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_recyclings` AS select `r`.`id` AS `id`,`r`.`sortorder` AS `sortorder`,`rl`.`language_iso639-1` AS `language_iso639-1`,`rl`.`text` AS `text` from (`g_recycling` `r` join `g_recycling_language_map` `rl` on((`r`.`id` = `rl`.`recycling_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_tools`
--

/*!50001 DROP VIEW IF EXISTS `g_tools`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_tools` AS select `m`.`id` AS `id`,`m`.`sortorder` AS `sortorder`,`mlm`.`language_iso639-1` AS `language_iso639-1`,`mlm`.`text` AS `text` from (`g_tool` `m` join `g_tool_language_map` `mlm` on((`m`.`id` = `mlm`.`tool_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_world_countries`
--

/*!50001 DROP VIEW IF EXISTS `g_world_countries`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_world_countries` AS select `c`.`id` AS `world_country_id`,`clm`.`language_iso639-1` AS `language_iso639-1`,`clm`.`text` AS `country_name` from (`g_world_country` `c` join `g_world_country_language_map` `clm` on((`c`.`id` = `clm`.`world_country_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `language_original_language_view`
--

/*!50001 DROP VIEW IF EXISTS `language_original_language_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `language_original_language_view` AS select `l`.`language_iso639-1` AS `language_iso639-1`,`l`.`translation_language_iso639-1` AS `translation_language_iso639-1`,`l`.`text` AS `text`,`l2`.`text` AS `original_lang`,`l3`.`text` AS `english` from ((`g_language_language_map` `l` join `g_language_language_map` `l2` on(((`l`.`language_iso639-1` = `l2`.`language_iso639-1`) and (`l2`.`translation_language_iso639-1` = `l2`.`language_iso639-1`)))) join `g_language_language_map` `l3` on(((`l`.`language_iso639-1` = `l3`.`language_iso639-1`) and (`l3`.`translation_language_iso639-1` = 'en')))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `manufacturer_view`
--

/*!50001 DROP VIEW IF EXISTS `manufacturer_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `manufacturer_view` AS select `m`.`id` AS `id`,`m`.`logo` AS `logo`,`mlm`.`language_iso639-1` AS `language_iso639-1`,`mlm`.`text` AS `text` from (`g_manufacturer` `m` join `g_manufacturer_language_map` `mlm` on((`m`.`id` = `mlm`.`manufacturer_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `old_sys__languages`
--

/*!50001 DROP VIEW IF EXISTS `old_sys__languages`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `old_sys__languages` AS select `g_language`.`iso639-1` AS `id`,(case when (`g_language`.`iso639-1` in ('FI','NO','CS','ET','EL','HU','LV')) then 1 when (`g_language`.`iso639-1` in ('LT','PL','SK','SL','KO','RO','BG','HR')) then 2 when (`g_language`.`iso639-1` in ('RU','SR','MK','IS','ZH','TR','SQ')) then 3 else NULL end) AS `table_id`,`en`.`text` AS `en`,`de`.`text` AS `de`,`bg`.`text` AS `bg`,`cs`.`text` AS `cs`,`da`.`text` AS `da`,`el`.`text` AS `el`,`es`.`text` AS `es`,`et`.`text` AS `et`,`fi`.`text` AS `fi`,`fr`.`text` AS `fr`,`hr`.`text` AS `hr`,`hu`.`text` AS `hu`,`is`.`text` AS `is`,`it`.`text` AS `it`,`ko`.`text` AS `ko`,`lt`.`text` AS `lt`,`lv`.`text` AS `lv`,`mk`.`text` AS `mk`,`nl`.`text` AS `nl`,`no`.`text` AS `no`,`pl`.`text` AS `pl`,`pt`.`text` AS `pt`,`ro`.`text` AS `ro`,`ru`.`text` AS `ru`,`sk`.`text` AS `sk`,`sl`.`text` AS `sl`,`sq`.`text` AS `sq`,`sr`.`text` AS `sr`,`sv`.`text` AS `sv`,`tr`.`text` AS `tr`,`zh`.`text` AS `zh` from (((((((((((((((((((((((((((((((`g_language` left join `g_language_language_map` `en` on(((`g_language`.`iso639-1` = `en`.`language_iso639-1`) and (`en`.`translation_language_iso639-1` = 'en')))) left join `g_language_language_map` `de` on(((`de`.`language_iso639-1` = `en`.`language_iso639-1`) and (`de`.`translation_language_iso639-1` = 'de')))) left join `g_language_language_map` `bg` on(((`bg`.`language_iso639-1` = `en`.`language_iso639-1`) and (`bg`.`translation_language_iso639-1` = 'bg')))) left join `g_language_language_map` `cs` on(((`cs`.`language_iso639-1` = `en`.`language_iso639-1`) and (`cs`.`translation_language_iso639-1` = 'cs')))) left join `g_language_language_map` `da` on(((`da`.`language_iso639-1` = `en`.`language_iso639-1`) and (`da`.`translation_language_iso639-1` = 'da')))) left join `g_language_language_map` `el` on(((`el`.`language_iso639-1` = `en`.`language_iso639-1`) and (`el`.`translation_language_iso639-1` = 'el')))) left join `g_language_language_map` `es` on(((`es`.`language_iso639-1` = `en`.`language_iso639-1`) and (`es`.`translation_language_iso639-1` = 'es')))) left join `g_language_language_map` `et` on(((`et`.`language_iso639-1` = `en`.`language_iso639-1`) and (`et`.`translation_language_iso639-1` = 'et')))) left join `g_language_language_map` `fi` on(((`fi`.`language_iso639-1` = `en`.`language_iso639-1`) and (`fi`.`translation_language_iso639-1` = 'fi')))) left join `g_language_language_map` `fr` on(((`fr`.`language_iso639-1` = `en`.`language_iso639-1`) and (`fr`.`translation_language_iso639-1` = 'fr')))) left join `g_language_language_map` `hr` on(((`hr`.`language_iso639-1` = `en`.`language_iso639-1`) and (`hr`.`translation_language_iso639-1` = 'hr')))) left join `g_language_language_map` `hu` on(((`hu`.`language_iso639-1` = `en`.`language_iso639-1`) and (`hu`.`translation_language_iso639-1` = 'hu')))) left join `g_language_language_map` `is` on(((`is`.`language_iso639-1` = `en`.`language_iso639-1`) and (`is`.`translation_language_iso639-1` = 'is')))) left join `g_language_language_map` `it` on(((`it`.`language_iso639-1` = `en`.`language_iso639-1`) and (`it`.`translation_language_iso639-1` = 'it')))) left join `g_language_language_map` `ko` on(((`ko`.`language_iso639-1` = `en`.`language_iso639-1`) and (`ko`.`translation_language_iso639-1` = 'ko')))) left join `g_language_language_map` `lt` on(((`lt`.`language_iso639-1` = `en`.`language_iso639-1`) and (`lt`.`translation_language_iso639-1` = 'lt')))) left join `g_language_language_map` `lv` on(((`lv`.`language_iso639-1` = `en`.`language_iso639-1`) and (`lv`.`translation_language_iso639-1` = 'lv')))) left join `g_language_language_map` `mk` on(((`mk`.`language_iso639-1` = `en`.`language_iso639-1`) and (`mk`.`translation_language_iso639-1` = 'mk')))) left join `g_language_language_map` `nl` on(((`nl`.`language_iso639-1` = `en`.`language_iso639-1`) and (`nl`.`translation_language_iso639-1` = 'nl')))) left join `g_language_language_map` `no` on(((`no`.`language_iso639-1` = `en`.`language_iso639-1`) and (`no`.`translation_language_iso639-1` = 'no')))) left join `g_language_language_map` `pl` on(((`pl`.`language_iso639-1` = `en`.`language_iso639-1`) and (`pl`.`translation_language_iso639-1` = 'pl')))) left join `g_language_language_map` `pt` on(((`pt`.`language_iso639-1` = `en`.`language_iso639-1`) and (`pt`.`translation_language_iso639-1` = 'pt')))) left join `g_language_language_map` `ro` on(((`ro`.`language_iso639-1` = `en`.`language_iso639-1`) and (`ro`.`translation_language_iso639-1` = 'ro')))) left join `g_language_language_map` `ru` on(((`ru`.`language_iso639-1` = `en`.`language_iso639-1`) and (`ru`.`translation_language_iso639-1` = 'ru')))) left join `g_language_language_map` `sk` on(((`sk`.`language_iso639-1` = `en`.`language_iso639-1`) and (`sk`.`translation_language_iso639-1` = 'sk')))) left join `g_language_language_map` `sl` on(((`sl`.`language_iso639-1` = `en`.`language_iso639-1`) and (`sl`.`translation_language_iso639-1` = 'sl')))) left join `g_language_language_map` `sq` on(((`sq`.`language_iso639-1` = `en`.`language_iso639-1`) and (`sq`.`translation_language_iso639-1` = 'sq')))) left join `g_language_language_map` `sr` on(((`sr`.`language_iso639-1` = `en`.`language_iso639-1`) and (`sr`.`translation_language_iso639-1` = 'sr')))) left join `g_language_language_map` `sv` on(((`sv`.`language_iso639-1` = `en`.`language_iso639-1`) and (`sv`.`translation_language_iso639-1` = 'sv')))) left join `g_language_language_map` `tr` on(((`tr`.`language_iso639-1` = `en`.`language_iso639-1`) and (`tr`.`translation_language_iso639-1` = 'tr')))) left join `g_language_language_map` `zh` on(((`zh`.`language_iso639-1` = `en`.`language_iso639-1`) and (`zh`.`translation_language_iso639-1` = 'zh')))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `old_sys__translation_table`
--

/*!50001 DROP VIEW IF EXISTS `old_sys__translation_table`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `old_sys__translation_table` AS select `g_translation`.`id` AS `id`,`g_translation`.`text` AS `text`,`en`.`text` AS `en`,`de`.`text` AS `de`,`bg`.`text` AS `bg`,`cs`.`text` AS `cs`,`da`.`text` AS `da`,`el`.`text` AS `el`,`es`.`text` AS `es`,`et`.`text` AS `et`,`fi`.`text` AS `fi`,`fr`.`text` AS `fr`,`hr`.`text` AS `hr`,`hu`.`text` AS `hu`,`is`.`text` AS `is`,`it`.`text` AS `it`,`ko`.`text` AS `ko`,`lt`.`text` AS `lt`,`lv`.`text` AS `lv`,`mk`.`text` AS `mk`,`nl`.`text` AS `nl`,`no`.`text` AS `no`,`pl`.`text` AS `pl`,`pt`.`text` AS `pt`,`ro`.`text` AS `ro`,`ru`.`text` AS `ru`,`sk`.`text` AS `sk`,`sl`.`text` AS `sl`,`sq`.`text` AS `sq`,`sr`.`text` AS `sr`,`sv`.`text` AS `sv`,`tr`.`text` AS `tr`,`zh`.`text` AS `zh` from (((((((((((((((((((((((((((((((`g_translation` left join `g_translation_language_map` `en` on(((`g_translation`.`id` = `en`.`translation_id`) and (`en`.`language_iso639-1` = 'en')))) left join `g_translation_language_map` `de` on(((`de`.`translation_id` = `en`.`translation_id`) and (`de`.`language_iso639-1` = 'de')))) left join `g_translation_language_map` `bg` on(((`bg`.`translation_id` = `en`.`translation_id`) and (`bg`.`language_iso639-1` = 'bg')))) left join `g_translation_language_map` `cs` on(((`cs`.`translation_id` = `en`.`translation_id`) and (`cs`.`language_iso639-1` = 'cs')))) left join `g_translation_language_map` `da` on(((`da`.`translation_id` = `en`.`translation_id`) and (`da`.`language_iso639-1` = 'da')))) left join `g_translation_language_map` `el` on(((`el`.`translation_id` = `en`.`translation_id`) and (`el`.`language_iso639-1` = 'el')))) left join `g_translation_language_map` `es` on(((`es`.`translation_id` = `en`.`translation_id`) and (`es`.`language_iso639-1` = 'es')))) left join `g_translation_language_map` `et` on(((`et`.`translation_id` = `en`.`translation_id`) and (`et`.`language_iso639-1` = 'et')))) left join `g_translation_language_map` `fi` on(((`fi`.`translation_id` = `en`.`translation_id`) and (`fi`.`language_iso639-1` = 'fi')))) left join `g_translation_language_map` `fr` on(((`fr`.`translation_id` = `en`.`translation_id`) and (`fr`.`language_iso639-1` = 'fr')))) left join `g_translation_language_map` `hr` on(((`hr`.`translation_id` = `en`.`translation_id`) and (`hr`.`language_iso639-1` = 'hr')))) left join `g_translation_language_map` `hu` on(((`hu`.`translation_id` = `en`.`translation_id`) and (`hu`.`language_iso639-1` = 'hu')))) left join `g_translation_language_map` `is` on(((`is`.`translation_id` = `en`.`translation_id`) and (`is`.`language_iso639-1` = 'is')))) left join `g_translation_language_map` `it` on(((`it`.`translation_id` = `en`.`translation_id`) and (`it`.`language_iso639-1` = 'it')))) left join `g_translation_language_map` `ko` on(((`ko`.`translation_id` = `en`.`translation_id`) and (`ko`.`language_iso639-1` = 'ko')))) left join `g_translation_language_map` `lt` on(((`lt`.`translation_id` = `en`.`translation_id`) and (`lt`.`language_iso639-1` = 'lt')))) left join `g_translation_language_map` `lv` on(((`lv`.`translation_id` = `en`.`translation_id`) and (`lv`.`language_iso639-1` = 'lv')))) left join `g_translation_language_map` `mk` on(((`mk`.`translation_id` = `en`.`translation_id`) and (`mk`.`language_iso639-1` = 'mk')))) left join `g_translation_language_map` `nl` on(((`nl`.`translation_id` = `en`.`translation_id`) and (`nl`.`language_iso639-1` = 'nl')))) left join `g_translation_language_map` `no` on(((`no`.`translation_id` = `en`.`translation_id`) and (`no`.`language_iso639-1` = 'no')))) left join `g_translation_language_map` `pl` on(((`pl`.`translation_id` = `en`.`translation_id`) and (`pl`.`language_iso639-1` = 'pl')))) left join `g_translation_language_map` `pt` on(((`pt`.`translation_id` = `en`.`translation_id`) and (`pt`.`language_iso639-1` = 'pt')))) left join `g_translation_language_map` `ro` on(((`ro`.`translation_id` = `en`.`translation_id`) and (`ro`.`language_iso639-1` = 'ro')))) left join `g_translation_language_map` `ru` on(((`ru`.`translation_id` = `en`.`translation_id`) and (`ru`.`language_iso639-1` = 'ru')))) left join `g_translation_language_map` `sk` on(((`sk`.`translation_id` = `en`.`translation_id`) and (`sk`.`language_iso639-1` = 'sk')))) left join `g_translation_language_map` `sl` on(((`sl`.`translation_id` = `en`.`translation_id`) and (`sl`.`language_iso639-1` = 'sl')))) left join `g_translation_language_map` `sq` on(((`sq`.`translation_id` = `en`.`translation_id`) and (`sq`.`language_iso639-1` = 'sq')))) left join `g_translation_language_map` `sr` on(((`sr`.`translation_id` = `en`.`translation_id`) and (`sr`.`language_iso639-1` = 'sr')))) left join `g_translation_language_map` `sv` on(((`sv`.`translation_id` = `en`.`translation_id`) and (`sv`.`language_iso639-1` = 'sv')))) left join `g_translation_language_map` `tr` on(((`tr`.`translation_id` = `en`.`translation_id`) and (`tr`.`language_iso639-1` = 'tr')))) left join `g_translation_language_map` `zh` on(((`zh`.`translation_id` = `en`.`translation_id`) and (`zh`.`language_iso639-1` = 'zh')))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `part_quantity_view`
--

/*!50001 DROP VIEW IF EXISTS `part_quantity_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `part_quantity_view` AS select `p`.`id` AS `id`,`p`.`area_id` AS `area_id`,`p`.`caution` AS `caution`,`p`.`text` AS `text`,`p`.`language_iso639-1` AS `language_iso639-1`,`p`.`mainarea_id` AS `mainarea_id` from `g_parts` `p` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `translations_view`
--

/*!50001 DROP VIEW IF EXISTS `translations_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `translations_view` AS select `t`.`id` AS `id`,`t`.`text` AS `english`,`tlm`.`text` AS `text`,`tlm`.`language_iso639-1` AS `language_iso639-1` from (`g_translation` `t` join `g_translation_language_map` `tlm` on((`t`.`id` = `tlm`.`translation_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Current Database: `idis_mastertest`
--

USE `idis_mastertest`;

--
-- Final view structure for view `brand_model_variant_view`
--

/*!50001 DROP VIEW IF EXISTS `brand_model_variant_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `brand_model_variant_view` AS select `vlm`.`language_iso639-1` AS `language_short`,`b`.`id` AS `brand_id`,`blm`.`text` AS `brand_name`,`m`.`id` AS `model_id`,`mlm`.`text` AS `model_name`,`v`.`id` AS `variant_id`,`vlm`.`text` AS `variant_name`,`v`.`period_from_month` AS `period_from_month`,`v`.`period_from_year` AS `period_from_year`,`v`.`period_to_month` AS `period_to_month`,`v`.`period_to_year` AS `period_to_year`,`v`.`variant_status_id` AS `status_id` from (((((`variant` `v` join `variant_language_map` `vlm` on((`vlm`.`variant_id` = `v`.`id`))) join `model` `m` on((`m`.`id` = `v`.`model_id`))) join `model_language_map` `mlm` on(((`mlm`.`model_id` = `m`.`id`) and (`mlm`.`language_iso639-1` = `vlm`.`language_iso639-1`)))) join `idis_glossary`.`g_brand` `b` on((`b`.`id` = `m`.`g_brand_id`))) join `idis_glossary`.`g_brand_language_map` `blm` on(((`blm`.`brand_id` = `b`.`id`) and (`blm`.`language_iso639-1` = `vlm`.`language_iso639-1`)))) order by `blm`.`text`,`mlm`.`text`,`vlm`.`text`,`v`.`period_from_year`,`v`.`period_from_month`,`v`.`period_to_year`,`v`.`period_to_month` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `components_with_add_info`
--

/*!50001 DROP VIEW IF EXISTS `components_with_add_info`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `components_with_add_info` AS select `c`.`id` AS `id`,`c`.`g_part_id` AS `g_part_id`,`cam`.`add_info_id` AS `add_info_id`,`cam`.`add_info_typ` AS `add_info_typ`,`c`.`variant_id` AS `variant_id`,`plm`.`language_iso639-1` AS `language_iso639-1`,`plm`.`text` AS `text`,(case when (`alm`.`language_iso639-1` is null) then false else true end) AS `has_language` from ((((`component_addinfo_map` `cam` join `component` `c` on((`c`.`id` = `cam`.`component_id`))) join `idis_glossary`.`g_part` `p` on((`c`.`g_part_id` = `p`.`id`))) join `idis_glossary`.`g_part_language_map` `plm` on((`p`.`id` = `plm`.`part_id`))) left join `add_info_language_map` `alm` on(((`cam`.`add_info_id` = `alm`.`add_info_id`) and (`plm`.`language_iso639-1` = `alm`.`language_iso639-1`)))) order by `c`.`g_area_id`,`plm`.`text` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `derivativ_name`
--

/*!50001 DROP VIEW IF EXISTS `derivativ_name`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `derivativ_name` AS select `d`.`id` AS `id`,`d`.`language_iso639-1` AS `language_iso639-1`,group_concat(`d`.`text` order by `d`.`sortorder` ASC separator ' ') AS `derivativ_name` from `derivatives` `d` group by `d`.`id`,`d`.`language_iso639-1` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `derivatives`
--

/*!50001 DROP VIEW IF EXISTS `derivatives`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `derivatives` AS select `d`.`id` AS `id`,`d`.`variant_id` AS `variant_id`,`gdlm`.`language_iso639-1` AS `language_iso639-1`,`gdlm`.`text` AS `text`,`dm`.`sortorder` AS `sortorder` from (((`derivativ` `d` join `derivativ_g_derivativ_map` `dm` on((`d`.`id` = `dm`.`derivativ_id`))) join `idis_glossary`.`g_derivativ` `gd` on((`gd`.`id` = `dm`.`g_derivativ_id`))) join `idis_glossary`.`g_derivativ_language_map` `gdlm` on((`gd`.`id` = `gdlm`.`derivativ_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `fixing_qty_view`
--

/*!50001 DROP VIEW IF EXISTS `fixing_qty_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `fixing_qty_view` AS select `f`.`id` AS `id`,`m`.`component_id` AS `component_id`,`f`.`sortorder` AS `sortorder`,`lm`.`language_iso639-1` AS `language_iso639-1`,`lm`.`text` AS `text`,`m`.`quantity` AS `quantity`,`m`.`id` AS `quantityId` from ((`idis_glossary`.`g_fixing` `f` join `idis_glossary`.`g_fixing_language_map` `lm` on((`f`.`id` = `lm`.`fixing_id`))) join `component_fixing_map` `m` on((`m`.`g_fixing_id` = `f`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_fixings_component_mapping_view`
--

/*!50001 DROP VIEW IF EXISTS `g_fixings_component_mapping_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_fixings_component_mapping_view` AS select `f`.`id` AS `id`,`f`.`sortorder` AS `sortorder`,`flm`.`language_iso639-1` AS `language_iso639-1`,`flm`.`text` AS `text`,`cfm`.`component_id` AS `component_id`,`cfm`.`quantity` AS `quantity` from ((`idis_glossary`.`g_fixing` `f` join `component_fixing_map` `cfm` on((`cfm`.`g_fixing_id` = `f`.`id`))) join `idis_glossary`.`g_fixing_language_map` `flm` on((`f`.`id` = `flm`.`fixing_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_recycling_component_mapping_view`
--

/*!50001 DROP VIEW IF EXISTS `g_recycling_component_mapping_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_recycling_component_mapping_view` AS select `t`.`id` AS `id`,`t`.`sortorder` AS `sortorder`,`tlm`.`language_iso639-1` AS `language_iso639-1`,`tlm`.`text` AS `text`,`c`.`component_id` AS `component_id` from ((`idis_glossary`.`g_recycling` `t` join `idis_glossary`.`g_recycling_language_map` `tlm` on((`t`.`id` = `tlm`.`recycling_id`))) join `component_recycling_map` `c` on((`c`.`g_recycling_id` = `t`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_tool_component_mapping_view`
--

/*!50001 DROP VIEW IF EXISTS `g_tool_component_mapping_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_tool_component_mapping_view` AS select `t`.`id` AS `id`,`t`.`info` AS `info`,`t`.`sortorder` AS `sortorder`,`tlm`.`language_iso639-1` AS `language_iso639-1`,`tlm`.`text` AS `text`,`c`.`component_id` AS `component_id` from ((`idis_glossary`.`g_tool` `t` join `idis_glossary`.`g_tool_language_map` `tlm` on((`t`.`id` = `tlm`.`tool_id`))) join `component_tool_map` `c` on((`c`.`g_tool_id` = `t`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `models`
--

/*!50001 DROP VIEW IF EXISTS `models`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `models` AS select `model_language_map`.`model_id` AS `model_id`,`model_language_map`.`language_iso639-1` AS `language_iso639-1`,`model_language_map`.`text` AS `text`,`model`.`g_brand_id` AS `g_brand_id` from (`model_language_map` join `model` on((`model_language_map`.`model_id` = `model`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `part_area_view`
--

/*!50001 DROP VIEW IF EXISTS `part_area_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `part_area_view` AS select `c`.`variant_id` AS `variant_id`,`p`.`id` AS `id`,`p`.`default_picture` AS `default_picture`,`p`.`enabled` AS `enabled`,`p`.`caution` AS `caution`,`p`.`comment` AS `comment`,`p`.`g_area_id` AS `area_id`,`a`.`mainarea_id` AS `mainarea_id`,`a`.`picture_enabled` AS `picture_enabled`,`a`.`picture_disabled` AS `picture_disabled`,`a`.`picture_active` AS `picture_active`,`alm`.`language_iso639-1` AS `language_iso639-1`,`alm`.`text` AS `areaName`,`plm`.`text` AS `partName` from ((((`idis_glossary`.`g_part` `p` join `idis_glossary`.`g_area` `a` on((`a`.`id` = `p`.`g_area_id`))) join `idis_glossary`.`g_area_language_map` `alm` on((`a`.`id` = `alm`.`area_id`))) join `idis_glossary`.`g_part_language_map` `plm` on(((`p`.`id` = `plm`.`part_id`) and (`plm`.`language_iso639-1` = `alm`.`language_iso639-1`)))) join `component` `c` on((`c`.`g_part_id` = `p`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `part_component_view`
--

/*!50001 DROP VIEW IF EXISTS `part_component_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `part_component_view` AS select `p`.`id` AS `id`,`c`.`variant_id` AS `variant_id`,`a`.`mainarea_id` AS `mainarea_id`,`a`.`id` AS `area_id`,`p`.`caution` AS `caution`,`c`.`id` AS `component_id`,`c`.`quantity` AS `quantity`,`c`.`derivative_id` AS `derivative_id`,`c`.`g_material_id` AS `g_material_id`,`plm`.`text` AS `text`,`plm`.`language_iso639-1` AS `language_iso639-1`,`pos`.`id` AS `position_id`,`pos`.`text` AS `position_text` from ((((`component` `c` join `idis_glossary`.`g_part` `p` on((`c`.`g_part_id` = `p`.`id`))) join `idis_glossary`.`g_part_language_map` `plm` on((`p`.`id` = `plm`.`part_id`))) join `idis_glossary`.`g_area` `a` on((`a`.`id` = `p`.`g_area_id`))) join `idis_glossary`.`g_positions` `pos` on(((`pos`.`id` = `c`.`g_position_id`) and (`pos`.`language_iso639-1` = `plm`.`language_iso639-1`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `part_sno_view`
--

/*!50001 DROP VIEW IF EXISTS `part_sno_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `part_sno_view` AS select `c`.`id` AS `id`,`c`.`g_area_id` AS `g_area_id`,`c`.`serial_number` AS `serial_number`,`c`.`variant_id` AS `variant_id`,`c`.`weight` AS `weight`,`plm`.`language_iso639-1` AS `language_iso639-1`,`plm`.`text` AS `text` from (`component` `c` join `idis_glossary`.`g_part_language_map` `plm` on((`c`.`g_part_id` = `plm`.`part_id`))) where (`c`.`serial_number` is not null) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pyro_component_view`
--

/*!50001 DROP VIEW IF EXISTS `pyro_component_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `pyro_component_view` AS select `c`.`variant_id` AS `variant_id`,`c`.`derivative_id` AS `derivative_id`,`c`.`id` AS `component_id`,`p`.`id` AS `part_id`,`gp`.`id` AS `pyro_id`,`gpc`.`sortorder` AS `column_index`,`gpcv`.`short` AS `column_value` from ((((((`component` `c` join `idis_glossary`.`g_part` `p` on((`c`.`g_part_id` = `p`.`id`))) join `idis_glossary`.`g_area` `a` on((`a`.`id` = `p`.`g_area_id`))) join `component_gpyro_value_map` `cgpvm` on((`c`.`id` = `cgpvm`.`component_id`))) join `idis_glossary`.`g_pyro` `gp` on((`p`.`id` = `gp`.`g_part_id`))) join `idis_glossary`.`g_pyro_column` `gpc` on(((`cgpvm`.`g_pyro_column_id` = `gpc`.`column_id`) and (`gp`.`list` = `gpc`.`list`)))) join `idis_glossary`.`g_pyro_column_value` `gpcv` on((`gpcv`.`id` = `cgpvm`.`g_pyro_column_value_id`))) where (`a`.`mainarea_id` = 2) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_brand_model_variant_map`
--

/*!50001 DROP VIEW IF EXISTS `user_brand_model_variant_map`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `user_brand_model_variant_map` AS select `u`.`id` AS `user_id`,`bmvv`.`brand_id` AS `brand_id`,`bmvv`.`model_id` AS `model_id`,`bmvv`.`variant_id` AS `variant_id` from ((((`brand_model_variant_view` `bmvv` join `idis_glossary`.`g_brand_g_country_map` `bcm` on((`bcm`.`g_brand_id` = `bmvv`.`brand_id`))) join `model` `m` on(((`m`.`id` = `bmvv`.`model_id`) and (`m`.`active` = true)))) join `variant_country_map` `vcm` on((`vcm`.`variant_id` = `bmvv`.`variant_id`))) join `idis_users`.`user` `u` on(((`u`.`language` = `bmvv`.`language_short`) and (`u`.`country` = `bcm`.`g_country_id`) and (`u`.`country` = `vcm`.`g_country_id`)))) where (`bmvv`.`status_id` >= 0) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `variants`
--

/*!50001 DROP VIEW IF EXISTS `variants`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `variants` AS select `v`.`id` AS `id`,`v`.`model_id` AS `model_id`,`v`.`variant_status_id` AS `variant_status_id`,`v`.`period_from_month` AS `period_from_month`,`v`.`period_from_year` AS `period_from_year`,`v`.`period_to_month` AS `period_to_month`,`v`.`period_to_year` AS `period_to_year`,`v`.`caravan` AS `caravan`,`v`.`version` AS `version`,`v`.`creation_user_id` AS `creation_user_id`,`v`.`creation_time` AS `creation_time`,`v`.`first_release_user_id` AS `first_release_user_id`,`v`.`first_release_time` AS `first_release_time`,`v`.`last_change_user_id` AS `last_change_user_id`,`v`.`last_change_time` AS `last_change_time`,`v`.`prerelease_user_id` AS `prerelease_user_id`,`v`.`release_user_id` AS `release_user_id`,`v`.`release_time` AS `release_time`,`v`.`picture` AS `picture`,`vlm`.`language_iso639-1` AS `language_iso639-1`,`vlm`.`text` AS `text` from (`variant` `v` join `variant_language_map` `vlm` on((`vlm`.`variant_id` = `v`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Current Database: `idis_office`
--

USE `idis_office`;

--
-- Final view structure for view `brand_model_variant_view`
--

/*!50001 DROP VIEW IF EXISTS `brand_model_variant_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `brand_model_variant_view` AS select `vlm`.`language_iso639-1` AS `language_short`,`b`.`id` AS `brand_id`,`blm`.`text` AS `brand_name`,`m`.`id` AS `model_id`,`mlm`.`text` AS `model_name`,`v`.`id` AS `variant_id`,`vlm`.`text` AS `variant_name`,`v`.`period_from_month` AS `period_from_month`,`v`.`period_from_year` AS `period_from_year`,`v`.`period_to_month` AS `period_to_month`,`v`.`period_to_year` AS `period_to_year`,`v`.`variant_status_id` AS `status_id` from (((((`variant` `v` join `variant_language_map` `vlm` on((`vlm`.`variant_id` = `v`.`id`))) join `model` `m` on((`m`.`id` = `v`.`model_id`))) join `model_language_map` `mlm` on(((`mlm`.`model_id` = `m`.`id`) and (`mlm`.`language_iso639-1` = `vlm`.`language_iso639-1`)))) join `idis_glossary`.`g_brand` `b` on((`b`.`id` = `m`.`g_brand_id`))) join `idis_glossary`.`g_brand_language_map` `blm` on(((`blm`.`brand_id` = `b`.`id`) and (`blm`.`language_iso639-1` = `vlm`.`language_iso639-1`)))) order by `blm`.`text`,`mlm`.`text`,`vlm`.`text`,`v`.`period_from_year`,`v`.`period_from_month`,`v`.`period_to_year`,`v`.`period_to_month` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `components_with_add_info`
--

/*!50001 DROP VIEW IF EXISTS `components_with_add_info`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `components_with_add_info` AS select `c`.`id` AS `id`,`c`.`g_part_id` AS `g_part_id`,`cam`.`add_info_id` AS `add_info_id`,`cam`.`add_info_typ` AS `add_info_typ`,`c`.`variant_id` AS `variant_id`,`plm`.`language_iso639-1` AS `language_iso639-1`,`plm`.`text` AS `text`,(case when (`alm`.`language_iso639-1` is null) then false else true end) AS `has_language` from ((((`component_addinfo_map` `cam` join `component` `c` on((`c`.`id` = `cam`.`component_id`))) join `idis_glossary`.`g_part` `p` on((`c`.`g_part_id` = `p`.`id`))) join `idis_glossary`.`g_part_language_map` `plm` on((`p`.`id` = `plm`.`part_id`))) left join `add_info_language_map` `alm` on(((`cam`.`add_info_id` = `alm`.`add_info_id`) and (`plm`.`language_iso639-1` = `alm`.`language_iso639-1`)))) order by `c`.`g_area_id`,`plm`.`text` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `derivativ_name`
--

/*!50001 DROP VIEW IF EXISTS `derivativ_name`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `derivativ_name` AS select `d`.`id` AS `id`,`d`.`language_iso639-1` AS `language_iso639-1`,group_concat(`d`.`text` order by `d`.`sortorder` ASC separator ' ') AS `derivativ_name` from `derivatives` `d` group by `d`.`id`,`d`.`language_iso639-1` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `derivatives`
--

/*!50001 DROP VIEW IF EXISTS `derivatives`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `derivatives` AS select `d`.`id` AS `id`,`d`.`variant_id` AS `variant_id`,`gdlm`.`language_iso639-1` AS `language_iso639-1`,`gdlm`.`text` AS `text`,`dm`.`sortorder` AS `sortorder` from (((`derivativ` `d` join `derivativ_g_derivativ_map` `dm` on((`d`.`id` = `dm`.`derivativ_id`))) join `idis_glossary`.`g_derivativ` `gd` on((`gd`.`id` = `dm`.`g_derivativ_id`))) join `idis_glossary`.`g_derivativ_language_map` `gdlm` on((`gd`.`id` = `gdlm`.`derivativ_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `fixing_qty_view`
--

/*!50001 DROP VIEW IF EXISTS `fixing_qty_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `fixing_qty_view` AS select `f`.`id` AS `id`,`m`.`component_id` AS `component_id`,`f`.`sortorder` AS `sortorder`,`lm`.`language_iso639-1` AS `language_iso639-1`,`lm`.`text` AS `text`,`m`.`quantity` AS `quantity`,`m`.`id` AS `quantityId` from ((`idis_glossary`.`g_fixing` `f` join `idis_glossary`.`g_fixing_language_map` `lm` on((`f`.`id` = `lm`.`fixing_id`))) join `component_fixing_map` `m` on((`m`.`g_fixing_id` = `f`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_fixings_component_mapping_view`
--

/*!50001 DROP VIEW IF EXISTS `g_fixings_component_mapping_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_fixings_component_mapping_view` AS select `f`.`id` AS `id`,`f`.`sortorder` AS `sortorder`,`flm`.`language_iso639-1` AS `language_iso639-1`,`flm`.`text` AS `text`,`cfm`.`component_id` AS `component_id`,`cfm`.`quantity` AS `quantity` from ((`idis_glossary`.`g_fixing` `f` join `component_fixing_map` `cfm` on((`cfm`.`g_fixing_id` = `f`.`id`))) join `idis_glossary`.`g_fixing_language_map` `flm` on((`f`.`id` = `flm`.`fixing_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_recycling_component_mapping_view`
--

/*!50001 DROP VIEW IF EXISTS `g_recycling_component_mapping_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_recycling_component_mapping_view` AS select `t`.`id` AS `id`,`t`.`sortorder` AS `sortorder`,`tlm`.`language_iso639-1` AS `language_iso639-1`,`tlm`.`text` AS `text`,`c`.`component_id` AS `component_id` from ((`idis_glossary`.`g_recycling` `t` join `idis_glossary`.`g_recycling_language_map` `tlm` on((`t`.`id` = `tlm`.`recycling_id`))) join `component_recycling_map` `c` on((`c`.`g_recycling_id` = `t`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `g_tool_component_mapping_view`
--

/*!50001 DROP VIEW IF EXISTS `g_tool_component_mapping_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `g_tool_component_mapping_view` AS select `t`.`id` AS `id`,`t`.`info` AS `info`,`t`.`sortorder` AS `sortorder`,`tlm`.`language_iso639-1` AS `language_iso639-1`,`tlm`.`text` AS `text`,`c`.`component_id` AS `component_id` from ((`idis_glossary`.`g_tool` `t` join `idis_glossary`.`g_tool_language_map` `tlm` on((`t`.`id` = `tlm`.`tool_id`))) join `component_tool_map` `c` on((`c`.`g_tool_id` = `t`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `glossary_mainarea_button`
--

/*!50001 DROP VIEW IF EXISTS `glossary_mainarea_button`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `glossary_mainarea_button` AS select `idis_glossary`.`g_mainarea_button_part`.`area_id` AS `mainarea_id`,'glossary_part' AS `glossary_part`,`idis_glossary`.`g_mainarea_button_part`.`part_id` AS `id`,`idis_glossary`.`g_mainarea_button_part`.`button_picture_id` AS `button_picture_id`,`idis_glossary`.`g_mainarea_button_part`.`active` AS `active`,`idis_glossary`.`g_mainarea_button_part`.`comment` AS `comment`,`idis_glossary`.`g_mainarea_button_part`.`sortorder` AS `sortorder` from `idis_glossary`.`g_mainarea_button_part` union select `idis_glossary`.`g_mainarea_button_material`.`area_id` AS `mainarea_id`,'glossary_material' AS `glossary_material`,`idis_glossary`.`g_mainarea_button_material`.`material_id` AS `id`,`idis_glossary`.`g_mainarea_button_material`.`button_picture_id` AS `button_picture_id`,`idis_glossary`.`g_mainarea_button_material`.`active` AS `active`,`idis_glossary`.`g_mainarea_button_material`.`comment` AS `comment`,`idis_glossary`.`g_mainarea_button_material`.`sortorder` AS `sortorder` from `idis_glossary`.`g_mainarea_button_material` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `models`
--

/*!50001 DROP VIEW IF EXISTS `models`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `models` AS select `model_language_map`.`model_id` AS `model_id`,`model_language_map`.`language_iso639-1` AS `language_iso639-1`,`model_language_map`.`text` AS `text`,`model`.`g_brand_id` AS `g_brand_id` from (`model_language_map` join `model` on((`model_language_map`.`model_id` = `model`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `part_area_view`
--

/*!50001 DROP VIEW IF EXISTS `part_area_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `part_area_view` AS select `c`.`variant_id` AS `variant_id`,`p`.`id` AS `id`,`p`.`default_picture` AS `default_picture`,`p`.`enabled` AS `enabled`,`p`.`caution` AS `caution`,`p`.`comment` AS `comment`,`p`.`g_area_id` AS `area_id`,`a`.`mainarea_id` AS `mainarea_id`,`a`.`picture_enabled` AS `picture_enabled`,`a`.`picture_disabled` AS `picture_disabled`,`a`.`picture_active` AS `picture_active`,`alm`.`language_iso639-1` AS `language_iso639-1`,`alm`.`text` AS `areaName`,`plm`.`text` AS `partName` from ((((`idis_glossary`.`g_part` `p` join `idis_glossary`.`g_area` `a` on((`a`.`id` = `p`.`g_area_id`))) join `idis_glossary`.`g_area_language_map` `alm` on((`a`.`id` = `alm`.`area_id`))) join `idis_glossary`.`g_part_language_map` `plm` on(((`p`.`id` = `plm`.`part_id`) and (`plm`.`language_iso639-1` = `alm`.`language_iso639-1`)))) join `component` `c` on((`c`.`g_part_id` = `p`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `part_component_view`
--

/*!50001 DROP VIEW IF EXISTS `part_component_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `part_component_view` AS select `p`.`id` AS `id`,`c`.`variant_id` AS `variant_id`,`a`.`mainarea_id` AS `mainarea_id`,`a`.`id` AS `area_id`,`p`.`caution` AS `caution`,`c`.`id` AS `component_id`,`c`.`quantity` AS `quantity`,`c`.`derivative_id` AS `derivative_id`,`c`.`g_material_id` AS `g_material_id`,`plm`.`text` AS `text`,`plm`.`language_iso639-1` AS `language_iso639-1`,`pos`.`id` AS `position_id`,`pos`.`text` AS `position_text` from ((((`component` `c` join `idis_glossary`.`g_part` `p` on((`c`.`g_part_id` = `p`.`id`))) join `idis_glossary`.`g_part_language_map` `plm` on((`p`.`id` = `plm`.`part_id`))) join `idis_glossary`.`g_area` `a` on((`a`.`id` = `p`.`g_area_id`))) join `idis_glossary`.`g_positions` `pos` on(((`pos`.`id` = `c`.`g_position_id`) and (`pos`.`language_iso639-1` = `plm`.`language_iso639-1`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `part_sno_view`
--

/*!50001 DROP VIEW IF EXISTS `part_sno_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `part_sno_view` AS select `c`.`id` AS `id`,`c`.`g_area_id` AS `g_area_id`,`c`.`serial_number` AS `serial_number`,`c`.`variant_id` AS `variant_id`,`c`.`weight` AS `weight`,`plm`.`language_iso639-1` AS `language_iso639-1`,`plm`.`text` AS `text` from (`component` `c` join `idis_glossary`.`g_part_language_map` `plm` on((`c`.`g_part_id` = `plm`.`part_id`))) where (`c`.`serial_number` is not null) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `pyro_component_view`
--

/*!50001 DROP VIEW IF EXISTS `pyro_component_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `pyro_component_view` AS select `c`.`variant_id` AS `variant_id`,`c`.`derivative_id` AS `derivative_id`,`c`.`id` AS `component_id`,`p`.`id` AS `part_id`,`gp`.`id` AS `pyro_id`,`gpc`.`sortorder` AS `column_index`,`gpcv`.`short` AS `column_value` from ((((((`component` `c` join `idis_glossary`.`g_part` `p` on((`c`.`g_part_id` = `p`.`id`))) join `idis_glossary`.`g_area` `a` on((`a`.`id` = `p`.`g_area_id`))) join `component_gpyro_value_map` `cgpvm` on((`c`.`id` = `cgpvm`.`component_id`))) join `idis_glossary`.`g_pyro` `gp` on((`p`.`id` = `gp`.`g_part_id`))) join `idis_glossary`.`g_pyro_column` `gpc` on(((`cgpvm`.`g_pyro_column_id` = `gpc`.`column_id`) and (`gp`.`list` = `gpc`.`list`)))) join `idis_glossary`.`g_pyro_column_value` `gpcv` on((`gpcv`.`id` = `cgpvm`.`g_pyro_column_value_id`))) where (`a`.`mainarea_id` = 2) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_brand_model_variant_map`
--

/*!50001 DROP VIEW IF EXISTS `user_brand_model_variant_map`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `user_brand_model_variant_map` AS select `u`.`id` AS `user_id`,`bmvv`.`brand_id` AS `brand_id`,`bmvv`.`model_id` AS `model_id`,`bmvv`.`variant_id` AS `variant_id` from ((((`brand_model_variant_view` `bmvv` join `idis_users`.`user` `u` on((`u`.`language` = `bmvv`.`language_short`))) left join `idis_users`.`user_role_map` `urm` on((`u`.`id` = `urm`.`user_id`))) left join `idis_users`.`user_variant_assignment_map` `uvam` on(((`uvam`.`variant_id` = `bmvv`.`variant_id`) and (`uvam`.`user_id` = `u`.`id`)))) left join `variant` `v` on(((`v`.`id` = `bmvv`.`variant_id`) and (`v`.`creation_user_id` = `u`.`id`)))) where (((`urm`.`role_id` = 6) or (`uvam`.`variant_id` is not null) or (`v`.`creation_user_id` is not null) or ((`urm`.`role_id` >= 4) and ((select count(`t1`.`g_country_id`) from (`variant_country_map` `t1` left join (select `idis_users`.`user_g_country_permission_map`.`g_country_id` AS `g_country_id` from `idis_users`.`user_g_country_permission_map` where ((`idis_users`.`user_g_country_permission_map`.`user_id` = `u`.`id`) and (`idis_users`.`user_g_country_permission_map`.`brand_id` = `bmvv`.`brand_id`))) `t2` on((`t1`.`g_country_id` = `t2`.`g_country_id`))) where ((`t1`.`active` = 1) and (`t1`.`variant_id` = `bmvv`.`variant_id`) and (`t2`.`g_country_id` is null))) = 0))) and (`bmvv`.`status_id` >= 0)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `variants`
--

/*!50001 DROP VIEW IF EXISTS `variants`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `variants` AS select `v`.`id` AS `id`,`v`.`model_id` AS `model_id`,`v`.`variant_status_id` AS `variant_status_id`,`v`.`period_from_month` AS `period_from_month`,`v`.`period_from_year` AS `period_from_year`,`v`.`period_to_month` AS `period_to_month`,`v`.`period_to_year` AS `period_to_year`,`v`.`caravan` AS `caravan`,`v`.`version` AS `version`,`v`.`creation_user_id` AS `creation_user_id`,`v`.`creation_time` AS `creation_time`,`v`.`first_release_user_id` AS `first_release_user_id`,`v`.`first_release_time` AS `first_release_time`,`v`.`last_change_user_id` AS `last_change_user_id`,`v`.`last_change_time` AS `last_change_time`,`v`.`prerelease_user_id` AS `prerelease_user_id`,`v`.`release_user_id` AS `release_user_id`,`v`.`release_time` AS `release_time`,`v`.`picture` AS `picture`,`vlm`.`language_iso639-1` AS `language_iso639-1`,`vlm`.`text` AS `text` from (`variant` `v` join `variant_language_map` `vlm` on((`vlm`.`variant_id` = `v`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Current Database: `idis_users`
--

USE `idis_users`;

--
-- Final view structure for view `logins`
--

/*!50001 DROP VIEW IF EXISTS `logins`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `logins` AS select 1 AS `email`,1 AS `delete_date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `roles`
--

/*!50001 DROP VIEW IF EXISTS `roles`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `roles` AS select `r`.`id` AS `id`,`r`.`text` AS `text`,`lm`.`language_iso639-1` AS `language_iso639-1`,`lm`.`text` AS `translated` from (`role` `r` join `role_language_map` `lm` on((`lm`.`role_id` = `r`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Current Database: `idis_web`
--

USE `idis_web`;

--
-- Final view structure for view `ergebnisse`
--

/*!50001 DROP VIEW IF EXISTS `ergebnisse`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `ergebnisse` AS select `ergebnisse_basis`.`id` AS `id`,`ergebnisse_basis`.`DownloadLimit` AS `DownloadLimit`,`ergebnisse_basis`.`CompanyName` AS `CompanyName`,concat(`ergebnisse_basis`.`contact_first_name`,' ',`ergebnisse_basis`.`contact_last_name`) AS `ContactName`,`ergebnisse_basis`.`Address` AS `Address`,`ergebnisse_basis`.`Email` AS `Email`,`ergebnisse_basis`.`ZipCode` AS `ZipCode`,`ergebnisse_basis`.`City` AS `City`,`ergebnisse_basis`.`Country` AS `Country`,`ergebnisse_basis`.`PhoneNo` AS `PhoneNo`,`ergebnisse_basis`.`Language` AS `Language`,`ergebnisse_basis`.`Dismantling` AS `Dismantling`,`ergebnisse_basis`.`Shredding` AS `Shredding`,`ergebnisse_basis`.`UsedParts` AS `UsedParts`,`ergebnisse_basis`.`MaterialRecycling` AS `MaterialRecycling`,`ergebnisse_basis`.`ActivityOther` AS `ActivityOther`,`ergebnisse_basis`.`receive_newsletter` AS `receive_newsletter`,`ergebnisse_basis`.`CarwreckTreatment` AS `CarwreckTreatment`,`ergebnisse_basis`.`Employees` AS `Employees`,`ergebnisse_basis`.`FromWhom` AS `FromWhom`,`ergebnisse_basis`.`DoYouAlreadyUse` AS `DoYouAlreadyUse`,`ergebnisse_basis`.`LastVersion` AS `LastVersion`,`ergebnisse_basis`.`StillUseIDIS` AS `StillUseIDIS`,`ergebnisse_basis`.`OnPC` AS `OnPC`,`ergebnisse_basis`.`AsPrintOut` AS `AsPrintOut`,`ergebnisse_basis`.`Workshop` AS `Workshop`,`ergebnisse_basis`.`Office` AS `Office`,`ergebnisse_basis`.`Elsewhere` AS `Elsewhere`,`ergebnisse_basis`.`DoYouKnowWebUpdate` AS `DoYouKnowWebUpdate`,`ergebnisse_basis`.`DoYouUpdate` AS `DoYouUpdate`,`ergebnisse_basis`.`OnlyDVD` AS `OnlyDVD`,`ergebnisse_basis`.`DVDWeb` AS `DVDWeb`,`ergebnisse_basis`.`FullWeb` AS `FullWeb`,`ergebnisse_basis`.`HowOftenDoYouUse` AS `HowOftenDoYouUse`,`ergebnisse_basis`.`PreTreatmentInf` AS `PreTreatmentInf`,`ergebnisse_basis`.`AirbagNeutralization` AS `AirbagNeutralization`,`ergebnisse_basis`.`HeavyMetalLocation` AS `HeavyMetalLocation`,`ergebnisse_basis`.`MaterialSelection` AS `MaterialSelection`,`ergebnisse_basis`.`ToolsAndMethods` AS `ToolsAndMethods`,`ergebnisse_basis`.`PurposeOther` AS `PurposeOther`,`ergebnisse_basis`.`IfYouDontUse` AS `IfYouDontUse`,`ergebnisse_basis`.`NoPCOrOS` AS `NoPCOrOS`,`ergebnisse_basis`.`NotUserfriendly` AS `NotUserfriendly`,`ergebnisse_basis`.`NotTheRightLanguage` AS `NotTheRightLanguage`,`ergebnisse_basis`.`TooDifficult` AS `TooDifficult`,`ergebnisse_basis`.`TakesTooLong` AS `TakesTooLong`,`ergebnisse_basis`.`CDEasyInstall` AS `CDEasyInstall`,`ergebnisse_basis`.`CDEasyUse` AS `CDEasyUse`,`ergebnisse_basis`.`CDEasyBrowsing` AS `CDEasyBrowsing`,`ergebnisse_basis`.`BrowsingWebData` AS `BrowsingWebData`,`ergebnisse_basis`.`IDISModeldatacomplete` AS `IDISModeldatacomplete`,`ergebnisse_basis`.`PreTreatmentInfcomplete` AS `PreTreatmentInfcomplete`,`ergebnisse_basis`.`MaterialInfUseful` AS `MaterialInfUseful`,`ergebnisse_basis`.`ToolsUseful` AS `ToolsUseful`,`ergebnisse_basis`.`PurposeMissing` AS `PurposeMissing`,`ergebnisse_basis`.`ReasonMissing` AS `ReasonMissing`,`ergebnisse_basis`.`CommentMissing` AS `CommentMissing`,`ergebnisse_basis`.`OtherSuggestion` AS `OtherSuggestion`,`ergebnisse_basis`.`OrderIDIS` AS `OrderIDIS`,`ergebnisse_basis`.`SentDate` AS `SentDate`,`ergebnisse_basis`.`DownloadKey` AS `DownloadKey`,`ergebnisse_basis`.`OrderDate` AS `OrderDate`,`ergebnisse_basis`.`QuestSent` AS `QuestSent`,`ergebnisse_basis`.`QuestReplied` AS `QuestReplied`,`ergebnisse_basis`.`QuestCode` AS `QuestCode`,`ergebnisse_basis`.`Others` AS `Others`,`ergebnisse_basis`.`Reason` AS `Reason`,`ergebnisse_basis`.`OrderDate2` AS `OrderDate2`,`ergebnisse_basis`.`Waitlist` AS `Waitlist`,`ergebnisse_basis`.`ResentDate` AS `ResentDate`,`ergebnisse_basis`.`Bemerkung` AS `Bemerkung`,`ergebnisse_basis`.`Carwrecks2002` AS `Carwrecks2002`,`ergebnisse_basis`.`Carwrecks2003` AS `Carwrecks2003`,`ergebnisse_basis`.`Carwrecks2005` AS `Carwrecks2005`,`ergebnisse_basis`.`Carwrecks2006` AS `Carwrecks2006`,`ergebnisse_basis`.`Carwrecks2007` AS `Carwrecks2007`,`ergebnisse_basis`.`Carwrecks2008` AS `Carwrecks2008`,`ergebnisse_basis`.`Carwrecks2009` AS `Carwrecks2009`,`ergebnisse_basis`.`Carwrecks2010` AS `Carwrecks2010` from `ergebnisse_basis` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Current Database: `mysql`
--

USE `mysql`;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!50606 SET GLOBAL INNODB_STATS_AUTO_RECALC=@OLD_INNODB_STATS_AUTO_RECALC */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-22  9:13:39
