-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: 
-- -- ----------------------------------------------------
-- Server version	8.0.21


set session_replication_role to replica;

--
-- Current Database: "idis_glossary"
--

CREATE SCHEMA IF NOT EXISTS "idis_glossary";
SET search_path TO "idis_glossary";

--
-- Temporary view structure for view "activity_areas"
--

DROP TABLE IF EXISTS "activity_areas";

-- SET @saved_cs_client     = @@character_set_client;

-- /*!50001 CREATE VIEW "activity_areas" AS SELECT 
-- 1 AS "id",
-- 1 AS "mainarea_id",
-- 1 AS "picture_enabled",
-- 1 AS "picture_disabled",
-- 1 AS "picture_active",
-- 1 AS "area_id",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "brand_view"
--

DROP TABLE IF EXISTS "brand_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "brand_view" AS SELECT
-- 1 AS "id",
-- 1 AS "manufacturer_id",
-- 1 AS "logo",
-- 1 AS "active",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "button_picture_view"
--

DROP TABLE IF EXISTS "button_picture_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "button_picture_view" AS SELECT
-- 1 AS "area_id",
-- 1 AS "id",
-- 1 AS "is_material",
-- 1 AS "comment",
-- 1 AS "sortorder",
-- 1 AS "picture_comment",
-- 1 AS "picture"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "documents"
--

DROP TABLE IF EXISTS "documents";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "documents" AS SELECT
-- 1 AS "id",
-- 1 AS "doctype",
-- 1 AS "language_iso639-1",
-- 1 AS "documentText"*/;
-- SET character_set_client = @saved_cs_client;


--
-- Dropping Tables
--
DROP TABLE IF EXISTS "g_material_language_map" CASCADE;
DROP TABLE IF EXISTS "g_material" CASCADE;
DROP TABLE IF EXISTS "g_mainarea_language_map" CASCADE;
DROP TABLE IF EXISTS "g_mainarea_button_part" CASCADE;
DROP TABLE IF EXISTS "g_mainarea_button_material" CASCADE;
DROP TABLE IF EXISTS "g_mainarea" CASCADE;
DROP TABLE IF EXISTS "g_add_info_typ_part_map" CASCADE;
DROP TABLE IF EXISTS "g_add_info_typ_language_map" CASCADE;
DROP TABLE IF EXISTS "g_add_info_typ" CASCADE;
DROP TABLE IF EXISTS "g_activity_language_map" CASCADE;
DROP TABLE IF EXISTS "g_activity" CASCADE;
DROP TABLE IF EXISTS "g_part_language_map" CASCADE;
DROP TABLE IF EXISTS "g_part" CASCADE;
DROP TABLE IF EXISTS "g_language_language_map" CASCADE;
DROP TABLE IF EXISTS "g_language" CASCADE;
DROP TABLE IF EXISTS "g_manufacturer" CASCADE;
DROP TABLE IF EXISTS "g_territory" CASCADE;
DROP TABLE IF EXISTS "g_country" CASCADE;
DROP TABLE IF EXISTS "g_brand" CASCADE;
DROP TABLE IF EXISTS "g_comment" CASCADE;
DROP TABLE IF EXISTS "g_derivativ" CASCADE;
DROP TABLE IF EXISTS "g_derivativ_group" CASCADE;
DROP TABLE IF EXISTS "g_pyro_position" CASCADE;
DROP TABLE IF EXISTS "g_family" CASCADE;
DROP TABLE IF EXISTS "g_fixing" CASCADE;
DROP TABLE IF EXISTS "g_health_safety" CASCADE;
DROP TABLE IF EXISTS "g_health_safety_group" CASCADE;
DROP TABLE IF EXISTS "g_method" CASCADE;
DROP TABLE IF EXISTS "g_position" CASCADE;
DROP TABLE IF EXISTS "g_pyro_column" CASCADE;
DROP TABLE IF EXISTS "g_pyro_column_value" CASCADE;
DROP TABLE IF EXISTS "g_pyro" CASCADE;
DROP TABLE IF EXISTS "g_recycling" CASCADE;
DROP TABLE IF EXISTS "g_tool" CASCADE;
DROP TABLE IF EXISTS "g_translation" CASCADE;
DROP TABLE IF EXISTS "g_world_country" CASCADE;
DROP TABLE IF EXISTS "g_world_territory"CASCADE;


--
-- Table structure for table "g_language"
--



CREATE TABLE "g_language" (
  "iso639-1" varchar(2) NOT NULL,
  "iso639-2" varchar(3) DEFAULT NULL,
  "ietf" text,
  "active" varchar(45) DEFAULT NULL,
  "flag" bytea,
  PRIMARY KEY ("iso639-1")
);


--
-- Table structure for table "g_language_language_map"
--



CREATE TABLE "g_language_language_map" (
  "language_iso639-1" varchar(2) NOT NULL,
  "translation_language_iso639-1" varchar(2) NOT NULL,
  "text" text NOT NULL,
  PRIMARY KEY ("language_iso639-1","translation_language_iso639-1"),
  CONSTRAINT "fk_language_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1"),
  CONSTRAINT "fk_language_language_map_language2" FOREIGN KEY ("translation_language_iso639-1") REFERENCES "g_language" ("iso639-1")
);


--
-- Table structure for table "g_activity"
--


CREATE TABLE "g_activity" (
  "id" int NOT NULL,
  "sortorder" int DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_activity_language_map"
--



CREATE TABLE "g_activity_language_map" (
  "activity_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("activity_id","language_iso639-1"),
  CONSTRAINT "fk_g_activity_language_map_activity_id1" FOREIGN KEY ("activity_id") REFERENCES "g_activity" ("id"),
  CONSTRAINT "fk_g_activity_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "g_part"
--



CREATE TABLE "g_part" (
  "id" int NOT NULL,
  "default_picture" bytea,
  "enabled" bit DEFAULT NULL,
  "caution" text,
  "comment" text,
  "picture_path" text,
  "g_area_id" int NOT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_part_language_map"
--



CREATE TABLE "g_part_language_map" (
  "part_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("part_id","language_iso639-1"),
  CONSTRAINT "fk_part_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1"),
  CONSTRAINT "fk_part_language_map_part1" FOREIGN KEY ("part_id") REFERENCES "g_part" ("id")
);


--
-- Table structure for table "g_add_info_typ"
--



CREATE TABLE "g_add_info_typ" (
  "id" int NOT NULL,
  "is_common_document" bit DEFAULT '0',
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_add_info_typ_language_map"
--



CREATE TABLE "g_add_info_typ_language_map" (
  "add_info_typ_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  "link" text,
  PRIMARY KEY ("add_info_typ_id","language_iso639-1"),
  CONSTRAINT "fk_add_info_typ_language_map_add_info_typ1" FOREIGN KEY ("add_info_typ_id") REFERENCES "g_add_info_typ" ("id"),
  CONSTRAINT "fk_add_info_typ_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1")
);


--
-- Table structure for table "g_add_info_typ_part_map"
--



CREATE TABLE "g_add_info_typ_part_map" (
  "add_info_typ_id" int NOT NULL,
  "part_id" int NOT NULL,
  PRIMARY KEY ("add_info_typ_id","part_id"),
  CONSTRAINT "fk_add_info_typ_part_map_add_info_typ1" FOREIGN KEY ("add_info_typ_id") REFERENCES "g_add_info_typ" ("id"),
  CONSTRAINT "fk_add_info_typ_part_map_part1" FOREIGN KEY ("part_id") REFERENCES "g_part" ("id")
);


--
-- Table structure for table "g_mainarea"
--



CREATE TABLE "g_mainarea" (
  "id" int NOT NULL,
  "picture_enabled" bytea,
  "picture_disabled" bytea,
  "picture_active" bytea,
  PRIMARY KEY ("id")
);

--
-- Table structure for table "g_material"
--



CREATE TABLE "g_material" (
  "id" int NOT NULL,
  "enabled" bit DEFAULT NULL,
  "caution" text,
  "comment" text,
  "g_family_id" int NOT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_material_language_map"
--



CREATE TABLE "g_material_language_map" (
  "material_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("material_id","language_iso639-1"),


  CONSTRAINT "fk_material_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_material_language_map_material1" FOREIGN KEY ("material_id") REFERENCES "g_material" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Temporary view structure for view "g_materials"
--

DROP TABLE IF EXISTS "g_materials";

-- SET @saved_cs_client     = @@character_set_client;

-- /*!50001 CREATE VIEW "g_materials" AS SELECT 
-- 1 AS "id",
-- 1 AS "enabled",
-- 1 AS "caution",
-- 1 AS "comment",
-- 1 AS "g_family_id",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;


--
-- Table structure for table "g_button_picture"
--

DROP TABLE IF EXISTS "g_button_picture";


CREATE TABLE "g_button_picture" (
  "id" int NOT NULL ,
  "comment" text,
  "picture_enabled" bytea,
  "picture_disabled" bytea,
  PRIMARY KEY ("id")
);

--
-- Table structure for table "g_mainarea_button_material"
--



CREATE TABLE "g_mainarea_button_material" (
  "area_id" int NOT NULL,
  "material_id" int NOT NULL,
  "button_picture_id" int NOT NULL,
  "active" bit(1) DEFAULT NULL,
  "comment" text,
  "sortorder" int DEFAULT NULL,
  PRIMARY KEY ("area_id","material_id","button_picture_id"),



  CONSTRAINT "fk_mainarea_button_material_button_picture1" FOREIGN KEY ("button_picture_id") REFERENCES "g_button_picture" ("id"),
  CONSTRAINT "fk_mainarea_button_material_mainarea1" FOREIGN KEY ("area_id") REFERENCES "g_mainarea" ("id"),
  CONSTRAINT "fk_mainarea_button_material_material1" FOREIGN KEY ("material_id") REFERENCES "g_material" ("id")
);

--
-- Table structure for table "g_area"
--

DROP TABLE IF EXISTS "g_area" CASCADE;


CREATE TABLE "g_area" (
  "id" int NOT NULL,
  "mainarea_id" int NOT NULL,
  "picture_enabled" varchar(50) DEFAULT NULL,
  "picture_disabled" varchar(50) DEFAULT NULL,
  "picture_active" varchar(50) DEFAULT NULL,
  "tabindex" int NOT NULL,
  "tablink" varchar(100) NOT NULL,
  "name" varchar(100) NOT NULL,
  "is_mainarea" bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_area_mainarea1" FOREIGN KEY ("mainarea_id") REFERENCES "g_mainarea" ("id")
);

--
-- Table structure for table "g_mainarea_button_part"
--



CREATE TABLE "g_mainarea_button_part" (
  "area_id" int NOT NULL,
  "part_id" int NOT NULL,
  "button_picture_id" int NOT NULL,
  "active" bit DEFAULT NULL,
  "comment" text,
  "sortorder" int DEFAULT NULL,
  PRIMARY KEY ("area_id","part_id","button_picture_id"),



  CONSTRAINT "fk_mainarea_button_part_button_picture1" FOREIGN KEY ("button_picture_id") REFERENCES "g_button_picture" ("id"),
  CONSTRAINT "fk_mainarea_button_part_g_area1" FOREIGN KEY ("area_id") REFERENCES "g_area" ("id"),
  CONSTRAINT "fk_mainarea_button_part_part1" FOREIGN KEY ("part_id") REFERENCES "g_part" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "g_mainarea_language_map"
--



CREATE TABLE "g_mainarea_language_map" (
  "mainarea_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("mainarea_id","language_iso639-1"),


  CONSTRAINT "fk_mainarea_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1"),
  CONSTRAINT "fk_mainarea_language_map_mainarea1" FOREIGN KEY ("mainarea_id") REFERENCES "g_mainarea" ("id")
);


--
-- Table structure for table "g_area_language_map"
--

DROP TABLE IF EXISTS "g_area_language_map";


CREATE TABLE "g_area_language_map" (
  "area_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text NOT NULL,
  PRIMARY KEY ("area_id","language_iso639-1"),
  CONSTRAINT "fk_area_language_map_area1" FOREIGN KEY ("area_id") REFERENCES "g_area" ("id"),
  CONSTRAINT "fk_area_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1")
);

--
-- Table structure for table "g_manufacturer"
--



CREATE TABLE "g_manufacturer" (
  "id" int NOT NULL,
  "sortorder" int DEFAULT NULL,
  "logo" bytea,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_manufacturer_language_map"
--

DROP TABLE IF EXISTS "g_manufacturer_language_map";


CREATE TABLE "g_manufacturer_language_map" (
  "manufacturer_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("manufacturer_id","language_iso639-1"),


  CONSTRAINT "fk_manufacturer_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1"),
  CONSTRAINT "fk_manufacturer_language_map_manufacturer1" FOREIGN KEY ("manufacturer_id") REFERENCES "g_manufacturer" ("id")
);

--
-- Table structure for table "g_territory"
--

CREATE TABLE "g_territory" (
  "id" int NOT NULL,
  "sortorder" int NOT NULL DEFAULT '99',
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_territory_language_map"
--

DROP TABLE IF EXISTS "g_territory_language_map";


CREATE TABLE "g_territory_language_map" (
  "territory_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("territory_id","language_iso639-1"),


  CONSTRAINT "fk_territory_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1"),
  CONSTRAINT "fk_territory_language_map_territory1" FOREIGN KEY ("territory_id") REFERENCES "g_territory" ("id")
);


--
-- Table structure for table "g_country"
--


CREATE TABLE "g_country" (
  "id" int NOT NULL,
  "flag" bytea NOT NULL,
  "disclaimer_text_id" int DEFAULT '-1',
  "iso3166-1" varchar(2) DEFAULT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "iso3166-1_UNIQUE" UNIQUE ("iso3166-1")
);


--
-- Table structure for table "g_country_language_map"
--

DROP TABLE IF EXISTS "g_country_language_map";


CREATE TABLE "g_country_language_map" (
  "country_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("country_id","language_iso639-1"),


  CONSTRAINT "fk_country_language_map_country1" FOREIGN KEY ("country_id") REFERENCES "g_country" ("id"),
  CONSTRAINT "fk_country_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1")
);


--
-- Table structure for table "g_country_territory_map"
--

DROP TABLE IF EXISTS "g_country_territory_map";


CREATE TABLE "g_country_territory_map" (
  "country_id" int NOT NULL,
  "territory_id" int NOT NULL,
  PRIMARY KEY ("country_id","territory_id"),


  CONSTRAINT "fk_country_territory_map_country1" FOREIGN KEY ("country_id") REFERENCES "g_country" ("id"),
  CONSTRAINT "fk_country_territory_map_territory1" FOREIGN KEY ("territory_id") REFERENCES "g_territory" ("id")
);


--
-- Table structure for table "g_brand"
--


CREATE TABLE "g_brand" (
  "id" int NOT NULL,
  "manufacturer_id" int NOT NULL,
  "logo" bytea,
  "bankrupt" bit DEFAULT '0',
  "bankrupt_year" int DEFAULT '0',
  "active" bit DEFAULT '1',
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_g_brand_g_manufacturer1" FOREIGN KEY ("manufacturer_id") REFERENCES "g_manufacturer" ("id")
);


--
-- Table structure for table "g_brand_g_country_map"
--

DROP TABLE IF EXISTS "g_brand_g_country_map";


CREATE TABLE "g_brand_g_country_map" (
  "g_brand_id" int NOT NULL,
  "g_country_id" int NOT NULL,
  PRIMARY KEY ("g_brand_id","g_country_id"),
  CONSTRAINT "g_brand_g_country_map_g_brand_fk" FOREIGN KEY ("g_brand_id") REFERENCES "g_brand" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "g_brand_g_country_map_g_country_fk" FOREIGN KEY ("g_country_id") REFERENCES "g_country" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "g_brand_language_map"
--

DROP TABLE IF EXISTS "g_brand_language_map";


CREATE TABLE "g_brand_language_map" (
  "brand_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text NOT NULL,
  PRIMARY KEY ("brand_id","language_iso639-1"),
  CONSTRAINT "fk_brand_language_map_brand1" FOREIGN KEY ("brand_id") REFERENCES "g_brand" ("id"),
  CONSTRAINT "fk_brand_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1")
);


--
-- Table structure for table "g_comment"
--

CREATE TABLE "g_comment" (
  "id" int NOT NULL,
  "sortorder" int DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_comment_language_map"
--

DROP TABLE IF EXISTS "g_comment_language_map";


CREATE TABLE "g_comment_language_map" (
  "comment_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text NOT NULL,
  PRIMARY KEY ("comment_id","language_iso639-1"),
  CONSTRAINT "fk_comment_language_map_comment1" FOREIGN KEY ("comment_id") REFERENCES "g_comment" ("id"),
  CONSTRAINT "fk_comment_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1")
);


--
-- Temporary view structure for view "g_comments"
--

DROP TABLE IF EXISTS "g_comments";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_comments" AS SELECT
-- 1 AS "id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "g_common_documents"
--

DROP TABLE IF EXISTS "g_common_documents";


CREATE TABLE "g_common_documents" (
  "identifier" varchar(45) NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "document" bytea NOT NULL,
  "version" text,
  "show_in_addinfo_management" bit DEFAULT '1',
  PRIMARY KEY ("identifier","language_iso639-1")
);


--
-- Temporary view structure for view "g_countries"
--

DROP TABLE IF EXISTS "g_countries";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_countries" AS SELECT
-- 1 AS "country_id",
-- 1 AS "flag",
-- 1 AS "language_iso639-1",
-- 1 AS "country_name",
-- 1 AS "territory_id",
-- 1 AS "territory_name"*/;
-- SET character_set_client = @saved_cs_client;


--
-- Table structure for table "g_derivativ_group"
--

CREATE TABLE "g_derivativ_group" (
  "id" int NOT NULL,
  "sortorder" int NOT NULL DEFAULT '0',
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_derivativ_group_language_map"
--

DROP TABLE IF EXISTS "g_derivativ_group_language_map";


CREATE TABLE "g_derivativ_group_language_map" (
  "derivativ_group_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("derivativ_group_id","language_iso639-1"),


  CONSTRAINT "fk_derivativ_group_language_map_derivativ_group1" FOREIGN KEY ("derivativ_group_id") REFERENCES "g_derivativ_group" ("id"),
  CONSTRAINT "fk_derivativ_group_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1")
);


--
-- Temporary view structure for view "g_derivativ_groups"
--

DROP TABLE IF EXISTS "g_derivativ_groups";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_derivativ_groups" AS SELECT
-- 1 AS "id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;


--
-- Table structure for table "g_derivativ"
--



CREATE TABLE "g_derivativ" (
  "id" int NOT NULL,
  "derivativ_group_id" int NOT NULL,
  "sortorder" int DEFAULT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_derivativ_derivativ_group1" FOREIGN KEY ("derivativ_group_id") REFERENCES "g_derivativ_group" ("id")
);

--
-- Table structure for table "g_derivativ_language_map"
--

DROP TABLE IF EXISTS "g_derivativ_language_map";


CREATE TABLE "g_derivativ_language_map" (
  "derivativ_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("derivativ_id","language_iso639-1"),


  CONSTRAINT "fk_derivativ_language_map_derivativ1" FOREIGN KEY ("derivativ_id") REFERENCES "g_derivativ" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_derivativ_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Temporary view structure for view "g_derivatives"
--

DROP TABLE IF EXISTS "g_derivatives";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_derivatives" AS SELECT
-- 1 AS "id",
-- 1 AS "derivativ_group_id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "g_document_language_map"
--

DROP TABLE IF EXISTS "g_document_language_map";


CREATE TABLE "g_document_language_map" (
  "document_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "documentText" text NOT NULL,
  PRIMARY KEY ("document_id","language_iso639-1")
);


--
-- Table structure for table "g_documents"
--

DROP TABLE IF EXISTS "g_documents";


CREATE TABLE "g_documents" (
  "id" int NOT NULL ,
  "doctype" varchar(45) NOT NULL,
  PRIMARY KEY ("id")
);


--
-- Temporary view structure for view "g_families"
--

DROP TABLE IF EXISTS "g_families";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_families" AS SELECT
-- 1 AS "id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "g_family"
--



CREATE TABLE "g_family" (
  "id" int NOT NULL,
  "sortorder" int DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_family_language_map"
--

DROP TABLE IF EXISTS "g_family_language_map";


CREATE TABLE "g_family_language_map" (
  "family_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("family_id","language_iso639-1"),


  CONSTRAINT "fk_family_language_map_family1" FOREIGN KEY ("family_id") REFERENCES "g_family" ("id"),
  CONSTRAINT "fk_family_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1")
);


--
-- Table structure for table "g_fixing"
--

CREATE TABLE "g_fixing" (
  "id" int NOT NULL,
  "sortorder" int DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_fixing_language_map"
--

DROP TABLE IF EXISTS "g_fixing_language_map";


CREATE TABLE "g_fixing_language_map" (
  "fixing_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("fixing_id","language_iso639-1"),


  CONSTRAINT "fk_fixing_language_map_fixing1" FOREIGN KEY ("fixing_id") REFERENCES "g_fixing" ("id"),
  CONSTRAINT "fk_fixing_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1")
);


--
-- Temporary view structure for view "g_fixings"
--

DROP TABLE IF EXISTS "g_fixings";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_fixings" AS SELECT
-- 1 AS "id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "g_health_safety"
--


CREATE TABLE "g_health_safety" (
  "id" int NOT NULL,
  "sortorder" int DEFAULT NULL,
  "picture" bytea,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_health_safety_group"
--


CREATE TABLE "g_health_safety_group" (
  "id" int NOT NULL,
  "sortorder" int DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_health_safety_group_language_map"
--

DROP TABLE IF EXISTS "g_health_safety_group_language_map";


CREATE TABLE "g_health_safety_group_language_map" (
  "health_safety_group_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("health_safety_group_id","language_iso639-1"),


  CONSTRAINT "fk_health_safety_group_language_map_health_safety_group1" FOREIGN KEY ("health_safety_group_id") REFERENCES "g_health_safety_group" ("id"),
  CONSTRAINT "fk_health_safety_group_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1")
);


--
-- Table structure for table "g_health_safety_health_safety_group_map"
--

DROP TABLE IF EXISTS "g_health_safety_health_safety_group_map";


CREATE TABLE "g_health_safety_health_safety_group_map" (
  "health_safety_id" int NOT NULL,
  "health_safety_group_id" int NOT NULL,
  PRIMARY KEY ("health_safety_id","health_safety_group_id"),


  CONSTRAINT "fk_health_safety_health_safety_group_map_health_safety1" FOREIGN KEY ("health_safety_id") REFERENCES "g_health_safety" ("id"),
  CONSTRAINT "fk_health_safety_health_safety_group_map_health_safety_group1" FOREIGN KEY ("health_safety_group_id") REFERENCES "g_health_safety_group" ("id")
);


--
-- Table structure for table "g_health_safety_language_map"
--

DROP TABLE IF EXISTS "g_health_safety_language_map";


CREATE TABLE "g_health_safety_language_map" (
  "health_safety_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("health_safety_id","language_iso639-1"),


  CONSTRAINT "fk_health_safety_language_map_health_safety1" FOREIGN KEY ("health_safety_id") REFERENCES "g_health_safety" ("id"),
  CONSTRAINT "fk_health_safety_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1")
);


--
-- Table structure for table "g_health_safety_part_default_map"
--

DROP TABLE IF EXISTS "g_health_safety_part_default_map";


CREATE TABLE "g_health_safety_part_default_map" (
  "g_health_safety_id" int NOT NULL,
  "g_part_id" int NOT NULL,
  PRIMARY KEY ("g_health_safety_id","g_part_id"),

  CONSTRAINT "fk_g_health_safety_part_default_map_g_health_safety1" FOREIGN KEY ("g_health_safety_id") REFERENCES "g_health_safety" ("id"),
  CONSTRAINT "fk_g_health_safety_part_default_map_g_part1" FOREIGN KEY ("g_part_id") REFERENCES "g_part" ("id")
);


--
-- Table structure for table "g_method"
--


CREATE TABLE "g_method" (
  "id" int NOT NULL,
  "sortorder" int DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_method_language_map"
--

DROP TABLE IF EXISTS "g_method_language_map";


CREATE TABLE "g_method_language_map" (
  "method_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("method_id","language_iso639-1"),


  CONSTRAINT "fk_method_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1"),
  CONSTRAINT "fk_method_language_map_method1" FOREIGN KEY ("method_id") REFERENCES "g_method" ("id")
);


--
-- Temporary view structure for view "g_methods"
--

DROP TABLE IF EXISTS "g_methods";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_methods" AS SELECT
-- 1 AS "id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "g_parts"
--

DROP TABLE IF EXISTS "g_parts";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_parts" AS SELECT
-- 1 AS "id",
-- 1 AS "default_picture",
-- 1 AS "enabled",
-- 1 AS "caution",
-- 1 AS "comment",
-- 1 AS "picture_path",
-- 1 AS "mainarea_id",
-- 1 AS "area_id",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "g_position"
--



CREATE TABLE "g_position" (
  "id" int NOT NULL,
  "sortorder" int DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_position_language_map"
--

DROP TABLE IF EXISTS "g_position_language_map";


CREATE TABLE "g_position_language_map" (
  "position_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("position_id","language_iso639-1"),


  CONSTRAINT "fk_position_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1"),
  CONSTRAINT "fk_position_language_map_position1" FOREIGN KEY ("position_id") REFERENCES "g_position" ("id")
);


--
-- Temporary view structure for view "g_positions"
--

DROP TABLE IF EXISTS "g_positions";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_positions" AS SELECT
-- 1 AS "id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "g_pyro"
--


CREATE TABLE "g_pyro" (
  "id" int NOT NULL,
  "list" int DEFAULT NULL,
  "legend" varchar(5) DEFAULT NULL,
  "adapter_enabled" bit DEFAULT NULL,
  "sortorder" int DEFAULT NULL,
  "g_part_id" int DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_pyro_column"
--


CREATE TABLE "g_pyro_column" (
  "column_id" int NOT NULL ,
  "list" int DEFAULT NULL,
  "xml" text,
  "sortorder" int DEFAULT NULL,
  PRIMARY KEY ("column_id")
);


--
-- Table structure for table "g_pyro_column_language_map"
--

DROP TABLE IF EXISTS "g_pyro_column_language_map";


CREATE TABLE "g_pyro_column_language_map" (
  "g_pyro_column_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("g_pyro_column_id","language_iso639-1"),

  CONSTRAINT "fk_g_pyro_column_language_map_g_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1"),
  CONSTRAINT "fk_g_pyro_column_language_map_g_pyro_column1" FOREIGN KEY ("g_pyro_column_id") REFERENCES "g_pyro_column" ("column_id")
);


--
-- Table structure for table "g_pyro_column_value"
--


CREATE TABLE "g_pyro_column_value" (
  "id" int NOT NULL ,
  "pyro_column_id" int DEFAULT NULL,
  "short" text,
  "sortorder" int DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_pyro_column_value_language_map"
--

DROP TABLE IF EXISTS "g_pyro_column_value_language_map";


CREATE TABLE "g_pyro_column_value_language_map" (
  "g_pyro_column_value_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text NOT NULL,
  PRIMARY KEY ("g_pyro_column_value_id","language_iso639-1"),


  CONSTRAINT "fk_pyro_columnvalue_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1"),
  CONSTRAINT "fk_pyro_columnvalue_language_map_pyro_columnvalue1" FOREIGN KEY ("g_pyro_column_value_id") REFERENCES "g_pyro_column_value" ("id")
);

--
-- Table structure for table "g_pyro_position"
--

CREATE TABLE "g_pyro_position" (
  "id" int NOT NULL ,
  "g_pyro_id" int NOT NULL,
  "position" int NOT NULL,
  PRIMARY KEY ("id"),

  CONSTRAINT "fk_g_pyro_position_g_pyro1" FOREIGN KEY ("g_pyro_id") REFERENCES "g_pyro" ("id")
);


--
-- Table structure for table "g_pyro_coord"
--

DROP TABLE IF EXISTS "g_pyro_coord";


CREATE TABLE "g_pyro_coord" (
  "g_pyro_position_id" int NOT NULL,
  "sort_order" int NOT NULL,
  "x" int DEFAULT NULL,
  "y" int DEFAULT NULL,
  PRIMARY KEY ("g_pyro_position_id","sort_order"),
  CONSTRAINT "fk_g_pyro_coord_g_pyro_position1" FOREIGN KEY ("g_pyro_position_id") REFERENCES "g_pyro_position" ("id")
);


--
-- Table structure for table "g_pyro_language_map"
--

DROP TABLE IF EXISTS "g_pyro_language_map";


CREATE TABLE "g_pyro_language_map" (
  "pyro_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("pyro_id","language_iso639-1"),


  CONSTRAINT "fk_pyro_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1"),
  CONSTRAINT "fk_pyro_language_map_pyro1" FOREIGN KEY ("pyro_id") REFERENCES "g_pyro" ("id")
);



--
-- Table structure for table "g_recycling"
--



CREATE TABLE "g_recycling" (
  "id" int NOT NULL,
  "sortorder" int DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_recycling_language_map"
--

DROP TABLE IF EXISTS "g_recycling_language_map";


CREATE TABLE "g_recycling_language_map" (
  "recycling_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("recycling_id","language_iso639-1"),


  CONSTRAINT "fk_recycling_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1"),
  CONSTRAINT "fk_recycling_language_map_recycling1" FOREIGN KEY ("recycling_id") REFERENCES "g_recycling" ("id")
);


--
-- Temporary view structure for view "g_recyclings"
--

DROP TABLE IF EXISTS "g_recyclings";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_recyclings" AS SELECT
-- 1 AS "id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;



--
-- Table structure for table "g_tool"
--


CREATE TABLE "g_tool" (
  "id" int NOT NULL,
  "info" bytea,
  "sortorder" int DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_tool_language_map"
--

DROP TABLE IF EXISTS "g_tool_language_map";


CREATE TABLE "g_tool_language_map" (
  "tool_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("tool_id","language_iso639-1"),


  CONSTRAINT "fk_tool_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1"),
  CONSTRAINT "fk_tool_language_map_tool1" FOREIGN KEY ("tool_id") REFERENCES "g_tool" ("id")
);


--
-- Temporary view structure for view "g_tools"
--

DROP TABLE IF EXISTS "g_tools";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_tools" AS SELECT
-- 1 AS "id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "g_translation"
--


CREATE TABLE "g_translation" (
  "id" int NOT NULL,
  "text" text,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_translation_language_map"
--

DROP TABLE IF EXISTS "g_translation_language_map";


CREATE TABLE "g_translation_language_map" (
  "translation_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("translation_id","language_iso639-1"),


  CONSTRAINT "fk_translation_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_translation_language_map_translation1" FOREIGN KEY ("translation_id") REFERENCES "g_translation" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Temporary view structure for view "g_world_countries"
--

DROP TABLE IF EXISTS "g_world_countries";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_world_countries" AS SELECT
-- 1 AS "world_country_id",
-- 1 AS "language_iso639-1",
-- 1 AS "country_name"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "g_world_country"
--

CREATE TABLE "g_world_country" (
  "id" int NOT NULL,
  "iso3166-2" varchar(2) DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_world_country_language_map"
--

DROP TABLE IF EXISTS "g_world_country_language_map";


CREATE TABLE "g_world_country_language_map" (
  "world_country_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("world_country_id","language_iso639-1"),


  CONSTRAINT "fk_world_country_language_map_country1" FOREIGN KEY ("world_country_id") REFERENCES "g_world_country" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_world_country_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "g_world_territory"
--


CREATE TABLE "g_world_territory" (
  "id" int NOT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_world_territory_country_map"
--

DROP TABLE IF EXISTS "g_world_territory_country_map";


CREATE TABLE "g_world_territory_country_map" (
  "world_territory_id" int NOT NULL,
  "world_country_id" int NOT NULL,
  PRIMARY KEY ("world_territory_id","world_country_id"),

  CONSTRAINT "g_world_territory_country_map_country_fkey" FOREIGN KEY ("world_country_id") REFERENCES "g_world_country" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "g_world_territory_country_map_territory_fkey" FOREIGN KEY ("world_territory_id") REFERENCES "g_world_territory" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "g_world_territory_language_map"
--

DROP TABLE IF EXISTS "g_world_territory_language_map";


CREATE TABLE "g_world_territory_language_map" (
  "world_territory_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("world_territory_id","language_iso639-1"),

  CONSTRAINT "g_world_territory_language_map_id_fkey" FOREIGN KEY ("world_territory_id") REFERENCES "g_world_territory" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "g_world_territory_language_map_language_fkey" FOREIGN KEY ("language_iso639-1") REFERENCES "g_language" ("iso639-1") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Temporary view structure for view "language_original_language_view"
--

DROP TABLE IF EXISTS "language_original_language_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "language_original_language_view" AS SELECT
-- 1 AS "language_iso639-1",
-- 1 AS "translation_language_iso639-1",
-- 1 AS "text",
-- 1 AS "original_lang",
-- 1 AS "english"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "manufacturer_view"
--

DROP TABLE IF EXISTS "manufacturer_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "manufacturer_view" AS SELECT
-- 1 AS "id",
-- 1 AS "logo",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "missing_translations"
--

DROP TABLE IF EXISTS "missing_translations";


CREATE TABLE "missing_translations" (
  "id" int NOT NULL ,
  "missing_translation" text,
  "missing_language" text,
  PRIMARY KEY ("id")
);


--
-- Temporary view structure for view "old_sys__languages"
--

DROP TABLE IF EXISTS "old_sys__languages";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "old_sys__languages" AS SELECT
-- 1 AS "id",
-- 1 AS "table_id",
-- 1 AS "en",
-- 1 AS "de",
-- 1 AS "bg",
-- 1 AS "cs",
-- 1 AS "da",
-- 1 AS "el",
-- 1 AS "es",
-- 1 AS "et",
-- 1 AS "fi",
-- 1 AS "fr",
-- 1 AS "hr",
-- 1 AS "hu",
-- 1 AS "is",
-- 1 AS "it",
-- 1 AS "ko",
-- 1 AS "lt",
-- 1 AS "lv",
-- 1 AS "mk",
-- 1 AS "nl",
-- 1 AS "no",
-- 1 AS "pl",
-- 1 AS "pt",
-- 1 AS "ro",
-- 1 AS "ru",
-- 1 AS "sk",
-- 1 AS "sl",
-- 1 AS "sq",
-- 1 AS "sr",
-- 1 AS "sv",
-- 1 AS "tr",
-- 1 AS "zh"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "old_sys__translation_table"
--

DROP TABLE IF EXISTS "old_sys__translation_table";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "old_sys__translation_table" AS SELECT
-- 1 AS "id",
-- 1 AS "text",
-- 1 AS "en",
-- 1 AS "de",
-- 1 AS "bg",
-- 1 AS "cs",
-- 1 AS "da",
-- 1 AS "el",
-- 1 AS "es",
-- 1 AS "et",
-- 1 AS "fi",
-- 1 AS "fr",
-- 1 AS "hr",
-- 1 AS "hu",
-- 1 AS "is",
-- 1 AS "it",
-- 1 AS "ko",
-- 1 AS "lt",
-- 1 AS "lv",
-- 1 AS "mk",
-- 1 AS "nl",
-- 1 AS "no",
-- 1 AS "pl",
-- 1 AS "pt",
-- 1 AS "ro",
-- 1 AS "ru",
-- 1 AS "sk",
-- 1 AS "sl",
-- 1 AS "sq",
-- 1 AS "sr",
-- 1 AS "sv",
-- 1 AS "tr",
-- 1 AS "zh"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "part_quantity_view"
--

DROP TABLE IF EXISTS "part_quantity_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "part_quantity_view" AS SELECT
-- 1 AS "id",
-- 1 AS "area_id",
-- 1 AS "caution",
-- 1 AS "text",
-- 1 AS "language_iso639-1",
-- 1 AS "mainarea_id"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "systemsetting"
--

DROP TABLE IF EXISTS "systemsetting";


CREATE TABLE "systemsetting" (
  "id" varchar(15) NOT NULL,
  "value" text,
  PRIMARY KEY ("id")
);


--
-- Temporary view structure for view "translations_view"
--

DROP TABLE IF EXISTS "translations_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "translations_view" AS SELECT
-- 1 AS "id",
-- 1 AS "english",
-- 1 AS "text",
-- 1 AS "language_iso639-1"*/;
-- SET character_set_client = @saved_cs_client;












--
-- Current Database: "idis_data"
--
CREATE SCHEMA IF NOT EXISTS "idis_data";
SET search_path TO "idis_data";


DROP TABLE IF EXISTS "add_info" CASCADE;
DROP TABLE IF EXISTS "model" CASCADE;
DROP TABLE IF EXISTS "variant" CASCADE;
DROP TABLE IF EXISTS "derivativ" CASCADE;
DROP TABLE IF EXISTS "component" CASCADE;
DROP TABLE IF EXISTS "pyro_adaptor" CASCADE;
DROP TABLE IF EXISTS "variant_status" CASCADE;


--
-- Table structure for table "add_info"
--

CREATE TABLE "add_info" (
  "id" int NOT NULL ,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "model"
--


CREATE TABLE "model" (
  "id" int NOT NULL ,
  "g_brand_id" int NOT NULL,
  "active" bit NOT NULL DEFAULT '1',
  PRIMARY KEY ("id"),

  CONSTRAINT "fk_model_brand1" FOREIGN KEY ("g_brand_id") REFERENCES "idis_glossary"."g_brand" ("id")
);


--
-- Table structure for table "variant"
--


CREATE TABLE "variant" (
  "id" int NOT NULL ,
  "model_id" int NOT NULL,
  "variant_status_id" int NOT NULL,
  "period_from_month" int DEFAULT NULL,
  "period_from_year" int DEFAULT NULL,
  "period_to_month" int DEFAULT NULL,
  "period_to_year" int DEFAULT NULL,
  "caravan" bit DEFAULT NULL,
  "version" int DEFAULT NULL,
  "creation_user_id" int DEFAULT NULL,
  "creation_time" date DEFAULT NULL,
  "first_release_user_id" int DEFAULT NULL,
  "first_release_time" date DEFAULT NULL,
  "last_change_user_id" int DEFAULT NULL,
  "last_change_time" date DEFAULT NULL,
  "prerelease_user_id" int DEFAULT NULL,
  "prerelease_time" date DEFAULT NULL,
  "release_user_id" int DEFAULT NULL,
  "release_time" date DEFAULT NULL,
  "picture" bytea,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_variant_model1" FOREIGN KEY ("model_id") REFERENCES "model" ("id")
);


--
-- Table structure for table "derivativ"
--

DROP TABLE IF EXISTS "derivativ";


CREATE TABLE "derivativ" (
  "id" int NOT NULL ,
  "variant_id" int NOT NULL,
  PRIMARY KEY ("id"),

  CONSTRAINT "fk_derivativ_variant1" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "component"
--


CREATE TABLE "component" (
  "id" int NOT NULL ,
  "g_part_id" int DEFAULT NULL,
  "g_area_id" int DEFAULT NULL,
  "g_material_id" int DEFAULT NULL,
  "material_checked" bit DEFAULT '0',
  "g_position_id" int DEFAULT NULL,
  "g_method_id" int DEFAULT NULL,
  "g_comment_id" int DEFAULT NULL,
  "weight" int DEFAULT NULL,
  "weight_unit" varchar(2) DEFAULT NULL,
  "weight_checked" bit DEFAULT '0',
  "derivative_id" int DEFAULT NULL,
  "page_number" int DEFAULT '1',
  "quantity" int DEFAULT NULL,
  "serial_number" varchar(45) DEFAULT NULL,
  "variant_id" int DEFAULT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_component_area1" FOREIGN KEY ("g_area_id") REFERENCES "idis_glossary"."g_area" ("id"),
  CONSTRAINT "fk_component_comment1" FOREIGN KEY ("g_comment_id") REFERENCES "idis_glossary"."g_comment" ("id"),
  CONSTRAINT "fk_component_derivativ1" FOREIGN KEY ("derivative_id") REFERENCES "derivativ" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_material1" FOREIGN KEY ("g_material_id") REFERENCES "idis_glossary"."g_material" ("id"),
  CONSTRAINT "fk_component_method1" FOREIGN KEY ("g_method_id") REFERENCES "idis_glossary"."g_method" ("id"),
  CONSTRAINT "fk_component_part1" FOREIGN KEY ("g_part_id") REFERENCES "idis_glossary"."g_part" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_position1" FOREIGN KEY ("g_position_id") REFERENCES "idis_glossary"."g_position" ("id"),
  CONSTRAINT "fk_component_variant1" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "add_info_description_language_map"
--

DROP TABLE IF EXISTS "add_info_description_language_map";


CREATE TABLE "add_info_description_language_map" (
  "component_id" int NOT NULL,
  "add_info_id" int NOT NULL,
  "add_info_typ" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "headline" text NOT NULL,
  "description" text NOT NULL,
  "icon" varchar(255) NOT NULL,
  PRIMARY KEY ("component_id","add_info_id","add_info_typ","language_iso639-1"),
  CONSTRAINT "fk_add_info_description_language_map_add_info_id" FOREIGN KEY ("add_info_id") REFERENCES "add_info" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_description_language_map_add_ino_typ" FOREIGN KEY ("add_info_typ") REFERENCES "idis_glossary"."g_add_info_typ" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_description_language_map_component_id" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "add_info_files"
--

DROP TABLE IF EXISTS "add_info_files";


CREATE TABLE "add_info_files" (
  "id" int NOT NULL ,
  "file" bytea NOT NULL,
  "file_name" text NOT NULL,
  "checksum" varchar(40) NOT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "checksum_UNIQUE" UNIQUE ("checksum")
);


--
-- Table structure for table "add_info_glossary_manufacturer_map"
--

DROP TABLE IF EXISTS "add_info_glossary_manufacturer_map";


CREATE TABLE "add_info_glossary_manufacturer_map" (
  "add_info_id" int NOT NULL,
  "g_manufacturer_id" int NOT NULL,
  "identifier" text NOT NULL,
  PRIMARY KEY ("add_info_id"),

  CONSTRAINT "fk_add_info_glossary_manufacturer_map_manufacturer1" FOREIGN KEY ("g_manufacturer_id") REFERENCES "idis_glossary"."g_manufacturer" ("id"),
  CONSTRAINT "fk_add_info_glossary_manufacturer_zuordnung_add_info1" FOREIGN KEY ("add_info_id") REFERENCES "add_info" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "add_info_language_map"
--

DROP TABLE IF EXISTS "add_info_language_map";


CREATE TABLE "add_info_language_map" (
  "add_info_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "file_id" int NOT NULL,
  "file_checksum" text NOT NULL,
  PRIMARY KEY ("add_info_id","language_iso639-1"),


  CONSTRAINT "fk_add_info_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "idis_glossary"."g_language" ("iso639-1"),
  CONSTRAINT "fk_add_info_zuordnung_add_info1" FOREIGN KEY ("add_info_id") REFERENCES "add_info" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "add_info_old_new_map"
--

DROP TABLE IF EXISTS "add_info_old_new_map";


CREATE TABLE "add_info_old_new_map" (
  "old_add_id" int NOT NULL,
  "new_add_id" int NOT NULL
);


--
-- Table structure for table "annex_pictures"
--

DROP TABLE IF EXISTS "annex_pictures";


CREATE TABLE "annex_pictures" (
  "id" int NOT NULL ,
  "brand_id" int NOT NULL,
  "model_id" int NOT NULL,
  "variant_id" int NOT NULL,
  "name" varchar(45) NOT NULL,
  "is_flash" bit DEFAULT '1',
  PRIMARY KEY ("id")
);


--
-- Table structure for table "brake_servo_pictures"
--

DROP TABLE IF EXISTS "brake_servo_pictures";


CREATE TABLE "brake_servo_pictures" (
  "id" int NOT NULL ,
  "brand_id" int NOT NULL,
  "model_id" int NOT NULL,
  "variant_id" int NOT NULL,
  "name" varchar(45) NOT NULL,
  "is_flash" bit DEFAULT '1',
  PRIMARY KEY ("id")
);


--
-- Temporary view structure for view "brand_model_variant_view"
--

DROP TABLE IF EXISTS "brand_model_variant_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "brand_model_variant_view" AS SELECT
-- 1 AS "language_short",
-- 1 AS "brand_id",
-- 1 AS "brand_name",
-- 1 AS "model_id",
-- 1 AS "model_name",
-- 1 AS "variant_id",
-- 1 AS "variant_name",
-- 1 AS "period_from_month",
-- 1 AS "period_from_year",
-- 1 AS "period_to_month",
-- 1 AS "period_to_year",
-- 1 AS "status_id"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "changes"
--

DROP TABLE IF EXISTS "changes";


CREATE TABLE "changes" (
  "id" int NOT NULL ,
  "user_id" int NOT NULL,
  "time" date NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "variant_id" int NOT NULL,
  "value" text NOT NULL,
  "checked" bit NOT NULL DEFAULT '0',
  PRIMARY KEY ("id")
);


--
-- Table structure for table "changes_old"
--

DROP TABLE IF EXISTS "changes_old";


CREATE TABLE "changes_old" (
  "id" int NOT NULL ,
  "date_time" date DEFAULT NULL,
  "user" text,
  "variant" int DEFAULT NULL,
  "action" text,
  "table_changed" text,
  "value" text,
  "sql_used" text,
  "checked" bit DEFAULT NULL,
  PRIMARY KEY ("id")
);




--
-- Table structure for table "component_addinfo_map"
--

DROP TABLE IF EXISTS "component_addinfo_map";


CREATE TABLE "component_addinfo_map" (
  "component_id" int NOT NULL,
  "add_info_id" int NOT NULL,
  "add_info_typ" int NOT NULL,
  PRIMARY KEY ("component_id","add_info_typ","add_info_id"),
  CONSTRAINT "fk_component_addinfo_map_add_info1" FOREIGN KEY ("add_info_id") REFERENCES "add_info" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_addinfo_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_addinfo_map_g_addinfo_typ1" FOREIGN KEY ("add_info_typ") REFERENCES "idis_glossary"."g_add_info_typ" ("id")
);


--
-- Table structure for table "component_fixing_map"
--

DROP TABLE IF EXISTS "component_fixing_map";


CREATE TABLE "component_fixing_map" (
  "component_id" int NOT NULL,
  "g_fixing_id" int NOT NULL,
  "quantity" int DEFAULT NULL,
  "id" int NOT NULL ,
  PRIMARY KEY ("id"),


  CONSTRAINT "fk_component_fixing_map_fixing1" FOREIGN KEY ("g_fixing_id") REFERENCES "idis_glossary"."g_fixing" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_fixing_zuordnung_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "component_gas_map"
--

DROP TABLE IF EXISTS "component_gas_map";


CREATE TABLE "component_gas_map" (
  "component_id" int NOT NULL,
  "operation_mode" int DEFAULT NULL,
  "number_of_tanks" text,
  PRIMARY KEY ("component_id"),
  CONSTRAINT "fk_component_gas_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "component_gpyro_value_map"
--

DROP TABLE IF EXISTS "component_gpyro_value_map";


CREATE TABLE "component_gpyro_value_map" (
  "component_id" int NOT NULL,
  "g_pyro_id" int NOT NULL,
  "g_pyro_column_id" int NOT NULL,
  "g_pyro_column_value_id" int DEFAULT NULL,
  PRIMARY KEY ("component_id","g_pyro_id","g_pyro_column_id"),



  CONSTRAINT "fk_component_gpyro_value_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_gpyro_value_map_g_pyro1" FOREIGN KEY ("g_pyro_id") REFERENCES "idis_glossary"."g_pyro" ("id"),
  CONSTRAINT "fk_component_gpyro_value_map_g_pyro_column_value1" FOREIGN KEY ("g_pyro_column_value_id") REFERENCES "idis_glossary"."g_pyro_column_value" ("id"),
  CONSTRAINT "fk_component_gpyro_value_map_g_pyro_list_column1" FOREIGN KEY ("g_pyro_column_id") REFERENCES "idis_glossary"."g_pyro_column" ("column_id")
);


--
-- Table structure for table "component_health_safety_map"
--

DROP TABLE IF EXISTS "component_health_safety_map";


CREATE TABLE "component_health_safety_map" (
  "component_id" int NOT NULL,
  "g_health_safety_id" int NOT NULL,
  PRIMARY KEY ("component_id","g_health_safety_id"),

  CONSTRAINT "fk_component_health_safety_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_health_safety_map_g_health_safety1" FOREIGN KEY ("g_health_safety_id") REFERENCES "idis_glossary"."g_health_safety" ("id")
);


--
-- Table structure for table "component_picture_map"
--

DROP TABLE IF EXISTS "component_picture_map";


CREATE TABLE "component_picture_map" (
  "component_id" int NOT NULL,
  "picture" bytea NOT NULL,
  PRIMARY KEY ("component_id"),
  CONSTRAINT "component_picture_map_component_id_fk" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "pyro_adaptor"
--


CREATE TABLE "pyro_adaptor" (
  "id" int NOT NULL ,
  "g_brand_id" int NOT NULL,
  "text" text,
  PRIMARY KEY ("id"),

  CONSTRAINT "fk_pyro_adaptor_brand1" FOREIGN KEY ("g_brand_id") REFERENCES "idis_glossary"."g_brand" ("id")
);


--
-- Table structure for table "component_pyro_adaptor_map"
--

DROP TABLE IF EXISTS "component_pyro_adaptor_map";


CREATE TABLE "component_pyro_adaptor_map" (
  "component_id" int NOT NULL,
  "pyro_adaptor_id" int NOT NULL,
  PRIMARY KEY ("component_id","pyro_adaptor_id"),
  CONSTRAINT "fk_component_pyro_adaptor_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_pyro_adaptor_map_pyro_adaptor1" FOREIGN KEY ("pyro_adaptor_id") REFERENCES "pyro_adaptor" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "component_recycling_map"
--

DROP TABLE IF EXISTS "component_recycling_map";


CREATE TABLE "component_recycling_map" (
  "component_id" int NOT NULL,
  "g_recycling_id" int NOT NULL,
  PRIMARY KEY ("component_id","g_recycling_id"),

  CONSTRAINT "fk_component_recycling_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_recycling_map_g_recycling1" FOREIGN KEY ("g_recycling_id") REFERENCES "idis_glossary"."g_recycling" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "component_tool_map"
--

DROP TABLE IF EXISTS "component_tool_map";


CREATE TABLE "component_tool_map" (
  "component_id" int NOT NULL,
  "g_tool_id" int NOT NULL,
  "id" int NOT NULL ,
  PRIMARY KEY ("id"),


  CONSTRAINT "fk_component_tool_map_tool1" FOREIGN KEY ("g_tool_id") REFERENCES "idis_glossary"."g_tool" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_tool_zuordnung_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Temporary view structure for view "components_with_add_info"
--

DROP TABLE IF EXISTS "components_with_add_info";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "components_with_add_info" AS SELECT
-- 1 AS "id",
-- 1 AS "g_part_id",
-- 1 AS "add_info_id",
-- 1 AS "add_info_typ",
-- 1 AS "variant_id",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "has_language"*/;
-- SET character_set_client = @saved_cs_client;



--
-- Table structure for table "derivativ_g_derivativ_map"
--

DROP TABLE IF EXISTS "derivativ_g_derivativ_map";


CREATE TABLE "derivativ_g_derivativ_map" (
  "derivativ_id" int NOT NULL,
  "sortorder" int NOT NULL,
  "g_derivativ_id" int NOT NULL,
  PRIMARY KEY ("derivativ_id","sortorder"),


  CONSTRAINT "fk_derivativ_glossary_derivativ_map_derivativ1" FOREIGN KEY ("g_derivativ_id") REFERENCES "idis_glossary"."g_derivativ" ("id"),
  CONSTRAINT "fk_derivativ_glossary_derivativ_zuordnung_derivativ1" FOREIGN KEY ("derivativ_id") REFERENCES "derivativ" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "derivativ_g_pyro_column_text"
--

DROP TABLE IF EXISTS "derivativ_g_pyro_column_text";


CREATE TABLE "derivativ_g_pyro_column_text" (
  "derivativ_id" int NOT NULL,
  "g_pyro_id" int NOT NULL,
  "g_pyro_column_id" int NOT NULL,
  "text" text,
  PRIMARY KEY ("derivativ_id","g_pyro_column_id","g_pyro_id"),


  CONSTRAINT "fk_derivativ_g_pyro_column_text_derivativ1" FOREIGN KEY ("derivativ_id") REFERENCES "derivativ" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_derivativ_g_pyro_column_text_g_pyro1" FOREIGN KEY ("g_pyro_id") REFERENCES "idis_glossary"."g_pyro" ("id"),
  CONSTRAINT "fk_derivativ_g_pyro_column_text_g_pyro_column1" FOREIGN KEY ("g_pyro_column_id") REFERENCES "idis_glossary"."g_pyro_column" ("column_id")
);


--
-- Table structure for table "derivativ_g_pyro_value_map"
--

DROP TABLE IF EXISTS "derivativ_g_pyro_value_map";


CREATE TABLE "derivativ_g_pyro_value_map" (
  "derivativ_id" int NOT NULL,
  "g_pyro_id" int NOT NULL,
  "g_pyro_column_id" int NOT NULL,
  "g_pyro_column_value_id" int DEFAULT NULL,
  PRIMARY KEY ("derivativ_id","g_pyro_id","g_pyro_column_id"),



  CONSTRAINT "fk_derivativ_g_pyro_value_map_derivativ1" FOREIGN KEY ("derivativ_id") REFERENCES "derivativ" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_derivativ_g_pyro_value_map_g_pyro1" FOREIGN KEY ("g_pyro_id") REFERENCES "idis_glossary"."g_pyro" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_derivativ_g_pyro_value_map_g_pyro_column_value1" FOREIGN KEY ("g_pyro_column_value_id") REFERENCES "idis_glossary"."g_pyro_column_value" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_derivativ_g_pyro_value_map_g_pyro_list_column1" FOREIGN KEY ("g_pyro_column_id") REFERENCES "idis_glossary"."g_pyro_column" ("column_id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Temporary view structure for view "derivativ_name"
--

DROP TABLE IF EXISTS "derivativ_name";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "derivativ_name" AS SELECT
-- 1 AS "id",
-- 1 AS "language_iso639-1",
-- 1 AS "derivativ_name"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "derivatives"
--

DROP TABLE IF EXISTS "derivatives";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "derivatives" AS SELECT
-- 1 AS "id",
-- 1 AS "variant_id",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "sortorder"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "dismantling_pictures"
--

DROP TABLE IF EXISTS "dismantling_pictures";


CREATE TABLE "dismantling_pictures" (
  "id" int NOT NULL ,
  "variant_id" int NOT NULL,
  "model_id" int NOT NULL,
  "brand_id" int NOT NULL,
  "area_id" int NOT NULL,
  "page_num" int NOT NULL,
  "name" text NOT NULL,
  "is_flash" bit DEFAULT '1',
  PRIMARY KEY ("id")
);


--
-- Temporary view structure for view "fixing_qty_view"
--

DROP TABLE IF EXISTS "fixing_qty_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "fixing_qty_view" AS SELECT
-- 1 AS "id",
-- 1 AS "component_id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "quantity",
-- 1 AS "quantityId"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "g_fixings_component_mapping_view"
--

DROP TABLE IF EXISTS "g_fixings_component_mapping_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_fixings_component_mapping_view" AS SELECT
-- 1 AS "id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "component_id",
-- 1 AS "quantity"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "g_recycling_component_mapping_view"
--

DROP TABLE IF EXISTS "g_recycling_component_mapping_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_recycling_component_mapping_view" AS SELECT
-- 1 AS "id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "component_id"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "g_tool_component_mapping_view"
--

DROP TABLE IF EXISTS "g_tool_component_mapping_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_tool_component_mapping_view" AS SELECT
-- 1 AS "id",
-- 1 AS "info",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "component_id"*/;
-- SET character_set_client = @saved_cs_client;


--
-- Table structure for table "model_language_map"
--

DROP TABLE IF EXISTS "model_language_map";


CREATE TABLE "model_language_map" (
  "model_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("model_id","language_iso639-1"),

  CONSTRAINT "fk_model_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "idis_glossary"."g_language" ("iso639-1"),
  CONSTRAINT "fk_model_language_zuordnung_model1" FOREIGN KEY ("model_id") REFERENCES "model" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Temporary view structure for view "models"
--

DROP TABLE IF EXISTS "models";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "models" AS SELECT
-- 1 AS "model_id",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "g_brand_id"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "part_area_view"
--

DROP TABLE IF EXISTS "part_area_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "part_area_view" AS SELECT
-- 1 AS "variant_id",
-- 1 AS "id",
-- 1 AS "default_picture",
-- 1 AS "enabled",
-- 1 AS "caution",
-- 1 AS "comment",
-- 1 AS "area_id",
-- 1 AS "mainarea_id",
-- 1 AS "picture_enabled",
-- 1 AS "picture_disabled",
-- 1 AS "picture_active",
-- 1 AS "language_iso639-1",
-- 1 AS "areaName",
-- 1 AS "partName"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "part_component_view"
--

DROP TABLE IF EXISTS "part_component_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "part_component_view" AS SELECT
-- 1 AS "id",
-- 1 AS "variant_id",
-- 1 AS "mainarea_id",
-- 1 AS "area_id",
-- 1 AS "caution",
-- 1 AS "component_id",
-- 1 AS "quantity",
-- 1 AS "derivative_id",
-- 1 AS "g_material_id",
-- 1 AS "text",
-- 1 AS "language_iso639-1",
-- 1 AS "position_id",
-- 1 AS "position_text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "part_sno_view"
--

DROP TABLE IF EXISTS "part_sno_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "part_sno_view" AS SELECT
-- 1 AS "id",
-- 1 AS "g_area_id",
-- 1 AS "serial_number",
-- 1 AS "variant_id",
-- 1 AS "weight",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;



--
-- Temporary view structure for view "pyro_component_view"
--

DROP TABLE IF EXISTS "pyro_component_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "pyro_component_view" AS SELECT
-- 1 AS "variant_id",
-- 1 AS "derivative_id",
-- 1 AS "component_id",
-- 1 AS "part_id",
-- 1 AS "pyro_id",
-- 1 AS "column_index",
-- 1 AS "column_value"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "supercap_pictures"
--

DROP TABLE IF EXISTS "supercap_pictures";


CREATE TABLE "supercap_pictures" (
  "id" int NOT NULL ,
  "brand_id" int NOT NULL,
  "model_id" int NOT NULL,
  "variant_id" int NOT NULL,
  "name" varchar(45) NOT NULL,
  "is_flash" bit DEFAULT '1',
  PRIMARY KEY ("id")
);


--
-- Temporary view structure for view "user_brand_model_variant_map"
--

DROP TABLE IF EXISTS "user_brand_model_variant_map";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "user_brand_model_variant_map" AS SELECT
-- 1 AS "user_id",
-- 1 AS "brand_id",
-- 1 AS "model_id",
-- 1 AS "variant_id"*/;
-- SET character_set_client = @saved_cs_client;



--
-- Table structure for table "variant_country_map"
--

DROP TABLE IF EXISTS "variant_country_map";


CREATE TABLE "variant_country_map" (
  "variant_id" int NOT NULL,
  "g_country_id" int NOT NULL,
  "active" bit DEFAULT NULL,
  PRIMARY KEY ("variant_id","g_country_id"),
  CONSTRAINT "fk_variant_country_map_country1" FOREIGN KEY ("g_country_id") REFERENCES "idis_glossary"."g_country" ("id"),
  CONSTRAINT "fk_variant_country_zuordnung_variant1" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "variant_language_map"
--

DROP TABLE IF EXISTS "variant_language_map";


CREATE TABLE "variant_language_map" (
  "variant_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text NOT NULL,
  PRIMARY KEY ("variant_id","language_iso639-1"),


  CONSTRAINT "fk_variant_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "idis_glossary"."g_language" ("iso639-1"),
  CONSTRAINT "fk_variant_language_zuordnung_variant1" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "variant_picture_thumbnail"
--

DROP TABLE IF EXISTS "variant_picture_thumbnail";


CREATE TABLE "variant_picture_thumbnail" (
  "variant_id" int NOT NULL,
  "thumbnaildata" bytea,
  PRIMARY KEY ("variant_id"),
  CONSTRAINT "variant_picture_thumbnail_variant_id_fkey" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "variant_status"
--


CREATE TABLE "variant_status" (
  "id" int NOT NULL,
  "logo" bytea,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "variant_status_language_map"
--

DROP TABLE IF EXISTS "variant_status_language_map";


CREATE TABLE "variant_status_language_map" (
  "variant_status_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text NOT NULL,
  PRIMARY KEY ("variant_status_id","language_iso639-1"),

  CONSTRAINT "fk_variant_status_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "idis_glossary"."g_language" ("iso639-1"),
  CONSTRAINT "fk_variant_status_language_zuordnung_variant_status1" FOREIGN KEY ("variant_status_id") REFERENCES "variant_status" ("id")
);


--
-- Temporary view structure for view "variants"
--

DROP TABLE IF EXISTS "variants";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "variants" AS SELECT
-- 1 AS "id",
-- 1 AS "model_id",
-- 1 AS "variant_status_id",
-- 1 AS "period_from_month",
-- 1 AS "period_from_year",
-- 1 AS "period_to_month",
-- 1 AS "period_to_year",
-- 1 AS "caravan",
-- 1 AS "version",
-- 1 AS "creation_user_id",
-- 1 AS "creation_time",
-- 1 AS "first_release_user_id",
-- 1 AS "first_release_time",
-- 1 AS "last_change_user_id",
-- 1 AS "last_change_time",
-- 1 AS "prerelease_user_id",
-- 1 AS "release_user_id",
-- 1 AS "release_time",
-- 1 AS "picture",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "vehicle_information"
--

DROP TABLE IF EXISTS "vehicle_information";


CREATE TABLE "vehicle_information" (
  "variant_id" int NOT NULL,
  "kerb_mass" text NOT NULL,
  "luggage_volume" text NOT NULL,
  "axle_base" text NOT NULL,
  "variant_height" text NOT NULL,
  "variant_width" text NOT NULL,
  "variant_length" text NOT NULL,
  "model_identification" text NOT NULL,
  "size_of_tire" text NOT NULL,
  "fuel_tank_capacity" text NOT NULL,
  "transmission" text NOT NULL,
  "fuel_type" text NOT NULL,
  PRIMARY KEY ("variant_id"),
  CONSTRAINT "fk_vehicle_information_variant_id1" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Current Database: "idis_mastertest"
--

CREATE SCHEMA IF NOT EXISTS "idis_mastertest";
SET search_path TO "idis_mastertest";


DROP TABLE IF EXISTS "add_info" CASCADE;
DROP TABLE IF EXISTS "model" CASCADE;
DROP TABLE IF EXISTS "variant" CASCADE;
DROP TABLE IF EXISTS "derivativ" CASCADE;
DROP TABLE IF EXISTS "component" CASCADE;
DROP TABLE IF EXISTS "pyro_adaptor" CASCADE;
DROP TABLE IF EXISTS "variant_status" CASCADE;



--
-- Table structure for table "add_info"
--

CREATE TABLE "add_info" (
  "id" int NOT NULL ,
  PRIMARY KEY ("id")
);

--
-- Table structure for table "model"
--


CREATE TABLE "model" (
  "id" int NOT NULL ,
  "g_brand_id" int NOT NULL,
  "active" bit NOT NULL DEFAULT '1',
  PRIMARY KEY ("id"),

  CONSTRAINT "fk_model_brand1" FOREIGN KEY ("g_brand_id") REFERENCES "idis_glossary"."g_brand" ("id")
);


--
-- Table structure for table "variant"
--


CREATE TABLE "variant" (
  "id" int NOT NULL ,
  "model_id" int NOT NULL,
  "variant_status_id" int NOT NULL,
  "period_from_month" int DEFAULT NULL,
  "period_from_year" int DEFAULT NULL,
  "period_to_month" int DEFAULT NULL,
  "period_to_year" int DEFAULT NULL,
  "caravan" bit DEFAULT NULL,
  "version" int DEFAULT NULL,
  "creation_user_id" int DEFAULT NULL,
  "creation_time" date DEFAULT NULL,
  "first_release_user_id" int DEFAULT NULL,
  "first_release_time" date DEFAULT NULL,
  "last_change_user_id" int DEFAULT NULL,
  "last_change_time" date DEFAULT NULL,
  "prerelease_user_id" int DEFAULT NULL,
  "prerelease_time" date DEFAULT NULL,
  "release_user_id" int DEFAULT NULL,
  "release_time" date DEFAULT NULL,
  "picture" bytea,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_variant_model1" FOREIGN KEY ("model_id") REFERENCES "model" ("id")
);


--
-- Table structure for table "derivativ"
--

DROP TABLE IF EXISTS "derivativ";


CREATE TABLE "derivativ" (
  "id" int NOT NULL ,
  "variant_id" int NOT NULL,
  PRIMARY KEY ("id"),

  CONSTRAINT "fk_derivativ_variant1" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);



--
-- Table structure for table "component"
--

CREATE TABLE "component" (
  "id" int NOT NULL ,
  "g_part_id" int DEFAULT NULL,
  "g_area_id" int DEFAULT NULL,
  "g_material_id" int DEFAULT NULL,
  "material_checked" bit DEFAULT '0',
  "g_position_id" int DEFAULT NULL,
  "g_method_id" int DEFAULT NULL,
  "g_comment_id" int DEFAULT NULL,
  "weight" int DEFAULT NULL,
  "weight_unit" varchar(2) DEFAULT NULL,
  "weight_checked" bit DEFAULT '0',
  "derivative_id" int DEFAULT NULL,
  "page_number" int DEFAULT '1',
  "quantity" int DEFAULT NULL,
  "serial_number" varchar(45) DEFAULT NULL,
  "variant_id" int DEFAULT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_component_area1" FOREIGN KEY ("g_area_id") REFERENCES "idis_glossary"."g_area" ("id"),
  CONSTRAINT "fk_component_comment1" FOREIGN KEY ("g_comment_id") REFERENCES "idis_glossary"."g_comment" ("id"),
  CONSTRAINT "fk_component_derivativ1" FOREIGN KEY ("derivative_id") REFERENCES "derivativ" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_material1" FOREIGN KEY ("g_material_id") REFERENCES "idis_glossary"."g_material" ("id"),
  CONSTRAINT "fk_component_method1" FOREIGN KEY ("g_method_id") REFERENCES "idis_glossary"."g_method" ("id"),
  CONSTRAINT "fk_component_part1" FOREIGN KEY ("g_part_id") REFERENCES "idis_glossary"."g_part" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_position1" FOREIGN KEY ("g_position_id") REFERENCES "idis_glossary"."g_position" ("id"),
  CONSTRAINT "fk_component_variant1" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

--
-- Table structure for table "add_info_description_language_map"
--

DROP TABLE IF EXISTS "add_info_description_language_map";


CREATE TABLE "add_info_description_language_map" (
  "component_id" int NOT NULL,
  "add_info_id" int NOT NULL,
  "add_info_typ" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "headline" text NOT NULL,
  "description" text NOT NULL,
  "icon" varchar(255) NOT NULL,
  PRIMARY KEY ("component_id","add_info_id","add_info_typ","language_iso639-1"),
  CONSTRAINT "fk_add_info_description_language_map_add_info_id" FOREIGN KEY ("add_info_id") REFERENCES "add_info" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_description_language_map_add_ino_typ" FOREIGN KEY ("add_info_typ") REFERENCES "idis_glossary"."g_add_info_typ" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_description_language_map_component_id" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "add_info_files"
--

DROP TABLE IF EXISTS "add_info_files";


CREATE TABLE "add_info_files" (
  "id" int NOT NULL ,
  "file" bytea NOT NULL,
  "file_name" text NOT NULL,
  "checksum" varchar(40) NOT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "checksum_UNIQUE" UNIQUE ("checksum")
);


--
-- Table structure for table "add_info_glossary_manufacturer_map"
--

DROP TABLE IF EXISTS "add_info_glossary_manufacturer_map";


CREATE TABLE "add_info_glossary_manufacturer_map" (
  "add_info_id" int NOT NULL,
  "g_manufacturer_id" int NOT NULL,
  "identifier" text NOT NULL,
  PRIMARY KEY ("add_info_id"),

  CONSTRAINT "fk_add_info_glossary_manufacturer_map_manufacturer1" FOREIGN KEY ("g_manufacturer_id") REFERENCES "idis_glossary"."g_manufacturer" ("id"),
  CONSTRAINT "fk_add_info_glossary_manufacturer_zuordnung_add_info1" FOREIGN KEY ("add_info_id") REFERENCES "add_info" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "add_info_language_map"
--

DROP TABLE IF EXISTS "add_info_language_map";


CREATE TABLE "add_info_language_map" (
  "add_info_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "file_id" int NOT NULL,
  "file_checksum" text NOT NULL,
  PRIMARY KEY ("add_info_id","language_iso639-1"),


  CONSTRAINT "fk_add_info_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "idis_glossary"."g_language" ("iso639-1"),
  CONSTRAINT "fk_add_info_zuordnung_add_info1" FOREIGN KEY ("add_info_id") REFERENCES "add_info" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "add_info_old_new_map"
--

DROP TABLE IF EXISTS "add_info_old_new_map";


CREATE TABLE "add_info_old_new_map" (
  "old_add_id" int NOT NULL,
  "new_add_id" int NOT NULL
);


--
-- Table structure for table "annex_pictures"
--

DROP TABLE IF EXISTS "annex_pictures";


CREATE TABLE "annex_pictures" (
  "id" int NOT NULL ,
  "brand_id" int NOT NULL,
  "model_id" int NOT NULL,
  "variant_id" int NOT NULL,
  "name" varchar(45) NOT NULL,
  "is_flash" bit DEFAULT '1',
  PRIMARY KEY ("id")
);


--
-- Table structure for table "brake_servo_pictures"
--

DROP TABLE IF EXISTS "brake_servo_pictures";


CREATE TABLE "brake_servo_pictures" (
  "id" int NOT NULL ,
  "brand_id" int NOT NULL,
  "model_id" int NOT NULL,
  "variant_id" int NOT NULL,
  "name" varchar(45) NOT NULL,
  "is_flash" bit DEFAULT '1',
  PRIMARY KEY ("id")
);


--
-- Temporary view structure for view "brand_model_variant_view"
--

DROP TABLE IF EXISTS "brand_model_variant_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "brand_model_variant_view" AS SELECT
-- 1 AS "language_short",
-- 1 AS "brand_id",
-- 1 AS "brand_name",
-- 1 AS "model_id",
-- 1 AS "model_name",
-- 1 AS "variant_id",
-- 1 AS "variant_name",
-- 1 AS "period_from_month",
-- 1 AS "period_from_year",
-- 1 AS "period_to_month",
-- 1 AS "period_to_year",
-- 1 AS "status_id"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "changes"
--

DROP TABLE IF EXISTS "changes";


CREATE TABLE "changes" (
  "id" int NOT NULL ,
  "user_id" int NOT NULL,
  "time" date NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "variant_id" int NOT NULL,
  "value" text NOT NULL,
  "checked" bit NOT NULL DEFAULT '0',
  PRIMARY KEY ("id")
);


--
-- Table structure for table "changes_old"
--

DROP TABLE IF EXISTS "changes_old";


CREATE TABLE "changes_old" (
  "id" int NOT NULL ,
  "date_time" date DEFAULT NULL,
  "user" text,
  "variant" int DEFAULT NULL,
  "action" text,
  "table_changed" text,
  "value" text,
  "sql_used" text,
  "checked" bit DEFAULT NULL,
  PRIMARY KEY ("id")
);




--
-- Table structure for table "component_addinfo_map"
--

DROP TABLE IF EXISTS "component_addinfo_map";


CREATE TABLE "component_addinfo_map" (
  "component_id" int NOT NULL,
  "add_info_id" int NOT NULL,
  "add_info_typ" int NOT NULL,
  PRIMARY KEY ("component_id","add_info_typ","add_info_id"),



  CONSTRAINT "fk_component_addinfo_map_add_info1" FOREIGN KEY ("add_info_id") REFERENCES "add_info" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_addinfo_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_addinfo_map_g_addinfo_typ1" FOREIGN KEY ("add_info_typ") REFERENCES "idis_glossary"."g_add_info_typ" ("id")
);


--
-- Table structure for table "component_fixing_map"
--

DROP TABLE IF EXISTS "component_fixing_map";


CREATE TABLE "component_fixing_map" (
  "component_id" int NOT NULL,
  "g_fixing_id" int NOT NULL,
  "quantity" int DEFAULT NULL,
  "id" int NOT NULL ,
  PRIMARY KEY ("id"),


  CONSTRAINT "fk_component_fixing_map_fixing1" FOREIGN KEY ("g_fixing_id") REFERENCES "idis_glossary"."g_fixing" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_fixing_zuordnung_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "component_gas_map"
--

DROP TABLE IF EXISTS "component_gas_map";


CREATE TABLE "component_gas_map" (
  "component_id" int NOT NULL,
  "operation_mode" int DEFAULT NULL,
  "number_of_tanks" text,
  PRIMARY KEY ("component_id"),
  CONSTRAINT "fk_component_gas_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "component_gpyro_value_map"
--

DROP TABLE IF EXISTS "component_gpyro_value_map";


CREATE TABLE "component_gpyro_value_map" (
  "component_id" int NOT NULL,
  "g_pyro_id" int NOT NULL,
  "g_pyro_column_id" int NOT NULL,
  "g_pyro_column_value_id" int DEFAULT NULL,
  PRIMARY KEY ("component_id","g_pyro_id","g_pyro_column_id"),



  CONSTRAINT "fk_component_gpyro_value_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_gpyro_value_map_g_pyro1" FOREIGN KEY ("g_pyro_id") REFERENCES "idis_glossary"."g_pyro" ("id"),
  CONSTRAINT "fk_component_gpyro_value_map_g_pyro_column_value1" FOREIGN KEY ("g_pyro_column_value_id") REFERENCES "idis_glossary"."g_pyro_column_value" ("id"),
  CONSTRAINT "fk_component_gpyro_value_map_g_pyro_list_column1" FOREIGN KEY ("g_pyro_column_id") REFERENCES "idis_glossary"."g_pyro_column" ("column_id")
);


--
-- Table structure for table "component_health_safety_map"
--

DROP TABLE IF EXISTS "component_health_safety_map";


CREATE TABLE "component_health_safety_map" (
  "component_id" int NOT NULL,
  "g_health_safety_id" int NOT NULL,
  PRIMARY KEY ("component_id","g_health_safety_id"),

  CONSTRAINT "fk_component_health_safety_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_health_safety_map_g_health_safety1" FOREIGN KEY ("g_health_safety_id") REFERENCES "idis_glossary"."g_health_safety" ("id")
);


--
-- Table structure for table "component_picture_map"
--

DROP TABLE IF EXISTS "component_picture_map";


CREATE TABLE "component_picture_map" (
  "component_id" int NOT NULL,
  "picture" bytea NOT NULL,
  PRIMARY KEY ("component_id"),
  CONSTRAINT "component_picture_map_component_id_fk" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "pyro_adaptor"
--


CREATE TABLE "pyro_adaptor" (
  "id" int NOT NULL ,
  "g_brand_id" int NOT NULL,
  "text" text,
  PRIMARY KEY ("id"),

  CONSTRAINT "fk_pyro_adaptor_brand1" FOREIGN KEY ("g_brand_id") REFERENCES "idis_glossary"."g_brand" ("id")
);


--
-- Table structure for table "component_pyro_adaptor_map"
--

DROP TABLE IF EXISTS "component_pyro_adaptor_map";


CREATE TABLE "component_pyro_adaptor_map" (
  "component_id" int NOT NULL,
  "pyro_adaptor_id" int NOT NULL,
  PRIMARY KEY ("component_id","pyro_adaptor_id"),
  CONSTRAINT "fk_component_pyro_adaptor_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_pyro_adaptor_map_pyro_adaptor1" FOREIGN KEY ("pyro_adaptor_id") REFERENCES "pyro_adaptor" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "component_recycling_map"
--

DROP TABLE IF EXISTS "component_recycling_map";


CREATE TABLE "component_recycling_map" (
  "component_id" int NOT NULL,
  "g_recycling_id" int NOT NULL,
  PRIMARY KEY ("component_id","g_recycling_id"),

  CONSTRAINT "fk_component_recycling_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_recycling_map_g_recycling1" FOREIGN KEY ("g_recycling_id") REFERENCES "idis_glossary"."g_recycling" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "component_tool_map"
--

DROP TABLE IF EXISTS "component_tool_map";


CREATE TABLE "component_tool_map" (
  "component_id" int NOT NULL,
  "g_tool_id" int NOT NULL,
  "id" int NOT NULL ,
  PRIMARY KEY ("id"),


  CONSTRAINT "fk_component_tool_map_tool1" FOREIGN KEY ("g_tool_id") REFERENCES "idis_glossary"."g_tool" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_tool_zuordnung_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Temporary view structure for view "components_with_add_info"
--

DROP TABLE IF EXISTS "components_with_add_info";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "components_with_add_info" AS SELECT
-- 1 AS "id",
-- 1 AS "g_part_id",
-- 1 AS "add_info_id",
-- 1 AS "add_info_typ",
-- 1 AS "variant_id",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "has_language"*/;
-- SET character_set_client = @saved_cs_client;


--
-- Table structure for table "derivativ_g_derivativ_map"
--

DROP TABLE IF EXISTS "derivativ_g_derivativ_map";


CREATE TABLE "derivativ_g_derivativ_map" (
  "derivativ_id" int NOT NULL,
  "sortorder" int NOT NULL,
  "g_derivativ_id" int NOT NULL,
  PRIMARY KEY ("derivativ_id","sortorder"),


  CONSTRAINT "fk_derivativ_glossary_derivativ_map_derivativ1" FOREIGN KEY ("g_derivativ_id") REFERENCES "idis_glossary"."g_derivativ" ("id"),
  CONSTRAINT "fk_derivativ_glossary_derivativ_zuordnung_derivativ1" FOREIGN KEY ("derivativ_id") REFERENCES "derivativ" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "derivativ_g_pyro_column_text"
--

DROP TABLE IF EXISTS "derivativ_g_pyro_column_text";


CREATE TABLE "derivativ_g_pyro_column_text" (
  "derivativ_id" int NOT NULL,
  "g_pyro_id" int NOT NULL,
  "g_pyro_column_id" int NOT NULL,
  "text" text,
  PRIMARY KEY ("derivativ_id","g_pyro_column_id","g_pyro_id"),


  CONSTRAINT "fk_derivativ_g_pyro_column_text_derivativ1" FOREIGN KEY ("derivativ_id") REFERENCES "derivativ" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_derivativ_g_pyro_column_text_g_pyro1" FOREIGN KEY ("g_pyro_id") REFERENCES "idis_glossary"."g_pyro" ("id"),
  CONSTRAINT "fk_derivativ_g_pyro_column_text_g_pyro_column1" FOREIGN KEY ("g_pyro_column_id") REFERENCES "idis_glossary"."g_pyro_column" ("column_id")
);


--
-- Table structure for table "derivativ_g_pyro_value_map"
--

DROP TABLE IF EXISTS "derivativ_g_pyro_value_map";


CREATE TABLE "derivativ_g_pyro_value_map" (
  "derivativ_id" int NOT NULL,
  "g_pyro_id" int NOT NULL,
  "g_pyro_column_id" int NOT NULL,
  "g_pyro_column_value_id" int DEFAULT NULL,
  PRIMARY KEY ("derivativ_id","g_pyro_id","g_pyro_column_id"),



  CONSTRAINT "fk_derivativ_g_pyro_value_map_derivativ1" FOREIGN KEY ("derivativ_id") REFERENCES "derivativ" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_derivativ_g_pyro_value_map_g_pyro1" FOREIGN KEY ("g_pyro_id") REFERENCES "idis_glossary"."g_pyro" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_derivativ_g_pyro_value_map_g_pyro_column_value1" FOREIGN KEY ("g_pyro_column_value_id") REFERENCES "idis_glossary"."g_pyro_column_value" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_derivativ_g_pyro_value_map_g_pyro_list_column1" FOREIGN KEY ("g_pyro_column_id") REFERENCES "idis_glossary"."g_pyro_column" ("column_id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Temporary view structure for view "derivativ_name"
--

DROP TABLE IF EXISTS "derivativ_name";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "derivativ_name" AS SELECT
-- 1 AS "id",
-- 1 AS "language_iso639-1",
-- 1 AS "derivativ_name"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "derivatives"
--

DROP TABLE IF EXISTS "derivatives";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "derivatives" AS SELECT
-- 1 AS "id",
-- 1 AS "variant_id",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "sortorder"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "dismantling_pictures"
--

DROP TABLE IF EXISTS "dismantling_pictures";


CREATE TABLE "dismantling_pictures" (
  "id" int NOT NULL ,
  "variant_id" int NOT NULL,
  "model_id" int NOT NULL,
  "brand_id" int NOT NULL,
  "area_id" int NOT NULL,
  "page_num" int NOT NULL,
  "name" text NOT NULL,
  "is_flash" bit DEFAULT '1',
  PRIMARY KEY ("id")
);


--
-- Temporary view structure for view "fixing_qty_view"
--

DROP TABLE IF EXISTS "fixing_qty_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "fixing_qty_view" AS SELECT
-- 1 AS "id",
-- 1 AS "component_id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "quantity",
-- 1 AS "quantityId"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "g_fixings_component_mapping_view"
--

DROP TABLE IF EXISTS "g_fixings_component_mapping_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_fixings_component_mapping_view" AS SELECT
-- 1 AS "id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "component_id",
-- 1 AS "quantity"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "g_recycling_component_mapping_view"
--

DROP TABLE IF EXISTS "g_recycling_component_mapping_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_recycling_component_mapping_view" AS SELECT
-- 1 AS "id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "component_id"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "g_tool_component_mapping_view"
--

DROP TABLE IF EXISTS "g_tool_component_mapping_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_tool_component_mapping_view" AS SELECT
-- 1 AS "id",
-- 1 AS "info",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "component_id"*/;
-- SET character_set_client = @saved_cs_client;


--
-- Table structure for table "model"
--



--
-- Table structure for table "model_language_map"
--

DROP TABLE IF EXISTS "model_language_map";


CREATE TABLE "model_language_map" (
  "model_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("model_id","language_iso639-1"),

  CONSTRAINT "fk_model_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "idis_glossary"."g_language" ("iso639-1"),
  CONSTRAINT "fk_model_language_zuordnung_model1" FOREIGN KEY ("model_id") REFERENCES "model" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Temporary view structure for view "models"
--

DROP TABLE IF EXISTS "models";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "models" AS SELECT
-- 1 AS "model_id",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "g_brand_id"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "part_area_view"
--

DROP TABLE IF EXISTS "part_area_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "part_area_view" AS SELECT
-- 1 AS "variant_id",
-- 1 AS "id",
-- 1 AS "default_picture",
-- 1 AS "enabled",
-- 1 AS "caution",
-- 1 AS "comment",
-- 1 AS "area_id",
-- 1 AS "mainarea_id",
-- 1 AS "picture_enabled",
-- 1 AS "picture_disabled",
-- 1 AS "picture_active",
-- 1 AS "language_iso639-1",
-- 1 AS "areaName",
-- 1 AS "partName"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "part_component_view"
--

DROP TABLE IF EXISTS "part_component_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "part_component_view" AS SELECT
-- 1 AS "id",
-- 1 AS "variant_id",
-- 1 AS "mainarea_id",
-- 1 AS "area_id",
-- 1 AS "caution",
-- 1 AS "component_id",
-- 1 AS "quantity",
-- 1 AS "derivative_id",
-- 1 AS "g_material_id",
-- 1 AS "text",
-- 1 AS "language_iso639-1",
-- 1 AS "position_id",
-- 1 AS "position_text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "part_sno_view"
--

DROP TABLE IF EXISTS "part_sno_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "part_sno_view" AS SELECT
-- 1 AS "id",
-- 1 AS "g_area_id",
-- 1 AS "serial_number",
-- 1 AS "variant_id",
-- 1 AS "weight",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;


--
-- Temporary view structure for view "pyro_component_view"
--

DROP TABLE IF EXISTS "pyro_component_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "pyro_component_view" AS SELECT
-- 1 AS "variant_id",
-- 1 AS "derivative_id",
-- 1 AS "component_id",
-- 1 AS "part_id",
-- 1 AS "pyro_id",
-- 1 AS "column_index",
-- 1 AS "column_value"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "supercap_pictures"
--

DROP TABLE IF EXISTS "supercap_pictures";


CREATE TABLE "supercap_pictures" (
  "id" int NOT NULL ,
  "brand_id" int NOT NULL,
  "model_id" int NOT NULL,
  "variant_id" int NOT NULL,
  "name" varchar(45) NOT NULL,
  "is_flash" bit DEFAULT '1',
  PRIMARY KEY ("id")
);


--
-- Temporary view structure for view "user_brand_model_variant_map"
--

DROP TABLE IF EXISTS "user_brand_model_variant_map";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "user_brand_model_variant_map" AS SELECT
-- 1 AS "user_id",
-- 1 AS "brand_id",
-- 1 AS "model_id",
-- 1 AS "variant_id"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "variant"
--



--
-- Table structure for table "variant_country_map"
--

DROP TABLE IF EXISTS "variant_country_map";


CREATE TABLE "variant_country_map" (
  "variant_id" int NOT NULL,
  "g_country_id" int NOT NULL,
  "active" bit DEFAULT NULL,
  PRIMARY KEY ("variant_id","g_country_id"),

  CONSTRAINT "fk_variant_country_map_country1" FOREIGN KEY ("g_country_id") REFERENCES "idis_glossary"."g_country" ("id"),
  CONSTRAINT "fk_variant_country_zuordnung_variant1" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "variant_language_map"
--

DROP TABLE IF EXISTS "variant_language_map";


CREATE TABLE "variant_language_map" (
  "variant_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text NOT NULL,
  PRIMARY KEY ("variant_id","language_iso639-1"),


  CONSTRAINT "fk_variant_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "idis_glossary"."g_language" ("iso639-1"),
  CONSTRAINT "fk_variant_language_zuordnung_variant1" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "variant_picture_thumbnail"
--

DROP TABLE IF EXISTS "variant_picture_thumbnail";


CREATE TABLE "variant_picture_thumbnail" (
  "variant_id" int NOT NULL,
  "thumbnaildata" bytea,
  PRIMARY KEY ("variant_id"),
  CONSTRAINT "variant_picture_thumbnail_variant_id_fkey" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "variant_status"
--


CREATE TABLE "variant_status" (
  "id" int NOT NULL,
  "logo" bytea,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "variant_status_language_map"
--

DROP TABLE IF EXISTS "variant_status_language_map";


CREATE TABLE "variant_status_language_map" (
  "variant_status_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text NOT NULL,
  PRIMARY KEY ("variant_status_id","language_iso639-1"),

  CONSTRAINT "fk_variant_status_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "idis_glossary"."g_language" ("iso639-1"),
  CONSTRAINT "fk_variant_status_language_zuordnung_variant_status1" FOREIGN KEY ("variant_status_id") REFERENCES "variant_status" ("id")
);


--
-- Temporary view structure for view "variants"
--

DROP TABLE IF EXISTS "variants";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "variants" AS SELECT
-- 1 AS "id",
-- 1 AS "model_id",
-- 1 AS "variant_status_id",
-- 1 AS "period_from_month",
-- 1 AS "period_from_year",
-- 1 AS "period_to_month",
-- 1 AS "period_to_year",
-- 1 AS "caravan",
-- 1 AS "version",
-- 1 AS "creation_user_id",
-- 1 AS "creation_time",
-- 1 AS "first_release_user_id",
-- 1 AS "first_release_time",
-- 1 AS "last_change_user_id",
-- 1 AS "last_change_time",
-- 1 AS "prerelease_user_id",
-- 1 AS "release_user_id",
-- 1 AS "release_time",
-- 1 AS "picture",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "vehicle_information"
--

DROP TABLE IF EXISTS "vehicle_information";


CREATE TABLE "vehicle_information" (
  "variant_id" int NOT NULL,
  "kerb_mass" text NOT NULL,
  "luggage_volume" text NOT NULL,
  "axle_base" text NOT NULL,
  "variant_height" text NOT NULL,
  "variant_width" text NOT NULL,
  "variant_length" text NOT NULL,
  "model_identification" text NOT NULL,
  "size_of_tire" text NOT NULL,
  "fuel_tank_capacity" text NOT NULL,
  "transmission" text NOT NULL,
  "fuel_type" text NOT NULL,
  PRIMARY KEY ("variant_id"),
  CONSTRAINT "fk_vehicle_information_variant_id1" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Current Database: "idis_office"
--

CREATE SCHEMA IF NOT EXISTS "idis_office";
SET search_path TO "idis_office";


DROP TABLE IF EXISTS "add_info" CASCADE;
DROP TABLE IF EXISTS "model" CASCADE;
DROP TABLE IF EXISTS "variant" CASCADE;
DROP TABLE IF EXISTS "derivativ" CASCADE;
DROP TABLE IF EXISTS "component" CASCADE;
DROP TABLE IF EXISTS "pyro_adaptor" CASCADE;
DROP TABLE IF EXISTS "variant_status" CASCADE;


--
-- Table structure for table "add_info"
--

CREATE TABLE "add_info" (
  "id" int NOT NULL ,
  PRIMARY KEY ("id")
);

--
-- Table structure for table "model"
--


CREATE TABLE "model" (
  "id" int NOT NULL ,
  "g_brand_id" int NOT NULL,
  "active" bit NOT NULL DEFAULT '1',
  PRIMARY KEY ("id"),

  CONSTRAINT "fk_model_brand1" FOREIGN KEY ("g_brand_id") REFERENCES "idis_glossary"."g_brand" ("id")
);


--
-- Table structure for table "variant"
--


CREATE TABLE "variant" (
  "id" int NOT NULL ,
  "model_id" int NOT NULL,
  "variant_status_id" int NOT NULL,
  "period_from_month" int DEFAULT NULL,
  "period_from_year" int DEFAULT NULL,
  "period_to_month" int DEFAULT NULL,
  "period_to_year" int DEFAULT NULL,
  "caravan" bit DEFAULT NULL,
  "version" int DEFAULT NULL,
  "creation_user_id" int DEFAULT NULL,
  "creation_time" date DEFAULT NULL,
  "first_release_user_id" int DEFAULT NULL,
  "first_release_time" date DEFAULT NULL,
  "last_change_user_id" int DEFAULT NULL,
  "last_change_time" date DEFAULT NULL,
  "prerelease_user_id" int DEFAULT NULL,
  "prerelease_time" date DEFAULT NULL,
  "release_user_id" int DEFAULT NULL,
  "release_time" date DEFAULT NULL,
  "picture" bytea,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_variant_model1" FOREIGN KEY ("model_id") REFERENCES "model" ("id")
);


--
-- Table structure for table "derivativ"
--

DROP TABLE IF EXISTS "derivativ";


CREATE TABLE "derivativ" (
  "id" int NOT NULL ,
  "variant_id" int NOT NULL,
  PRIMARY KEY ("id"),

  CONSTRAINT "fk_derivativ_variant1" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);



--
-- Table structure for table "component"
--

CREATE TABLE "component" (
  "id" int NOT NULL ,
  "g_part_id" int DEFAULT NULL,
  "g_area_id" int DEFAULT NULL,
  "g_material_id" int DEFAULT NULL,
  "material_checked" bit DEFAULT '0',
  "g_position_id" int DEFAULT NULL,
  "g_method_id" int DEFAULT NULL,
  "g_comment_id" int DEFAULT NULL,
  "weight" int DEFAULT NULL,
  "weight_unit" varchar(2) DEFAULT NULL,
  "weight_checked" bit DEFAULT '0',
  "derivative_id" int DEFAULT NULL,
  "page_number" int DEFAULT '1',
  "quantity" int DEFAULT NULL,
  "serial_number" varchar(45) DEFAULT NULL,
  "variant_id" int DEFAULT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "fk_component_area1" FOREIGN KEY ("g_area_id") REFERENCES "idis_glossary"."g_area" ("id"),
  CONSTRAINT "fk_component_comment1" FOREIGN KEY ("g_comment_id") REFERENCES "idis_glossary"."g_comment" ("id"),
  CONSTRAINT "fk_component_derivativ1" FOREIGN KEY ("derivative_id") REFERENCES "derivativ" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_material1" FOREIGN KEY ("g_material_id") REFERENCES "idis_glossary"."g_material" ("id"),
  CONSTRAINT "fk_component_method1" FOREIGN KEY ("g_method_id") REFERENCES "idis_glossary"."g_method" ("id"),
  CONSTRAINT "fk_component_part1" FOREIGN KEY ("g_part_id") REFERENCES "idis_glossary"."g_part" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_position1" FOREIGN KEY ("g_position_id") REFERENCES "idis_glossary"."g_position" ("id"),
  CONSTRAINT "fk_component_variant1" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "add_info_description_language_map"
--

DROP TABLE IF EXISTS "add_info_description_language_map";


CREATE TABLE "add_info_description_language_map" (
  "component_id" int NOT NULL,
  "add_info_id" int NOT NULL,
  "add_info_typ" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "headline" text NOT NULL,
  "description" text NOT NULL,
  "icon" varchar(255) NOT NULL,
  PRIMARY KEY ("component_id","add_info_id","add_info_typ","language_iso639-1"),
  CONSTRAINT "fk_add_info_description_language_map_add_info_id" FOREIGN KEY ("add_info_id") REFERENCES "add_info" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_description_language_map_add_ino_typ" FOREIGN KEY ("add_info_typ") REFERENCES "idis_glossary"."g_add_info_typ" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_description_language_map_component_id" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "add_info_files"
--

DROP TABLE IF EXISTS "add_info_files";


CREATE TABLE "add_info_files" (
  "id" int NOT NULL ,
  "file" bytea NOT NULL,
  "file_name" text NOT NULL,
  "checksum" varchar(40) NOT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "checksum_UNIQUE" UNIQUE ("checksum")
);


--
-- Table structure for table "add_info_glossary_manufacturer_map"
--

DROP TABLE IF EXISTS "add_info_glossary_manufacturer_map";


CREATE TABLE "add_info_glossary_manufacturer_map" (
  "add_info_id" int NOT NULL,
  "g_manufacturer_id" int NOT NULL,
  "identifier" text NOT NULL,
  PRIMARY KEY ("add_info_id"),

  CONSTRAINT "fk_add_info_glossary_manufacturer_map_manufacturer1" FOREIGN KEY ("g_manufacturer_id") REFERENCES "idis_glossary"."g_manufacturer" ("id"),
  CONSTRAINT "fk_add_info_glossary_manufacturer_zuordnung_add_info1" FOREIGN KEY ("add_info_id") REFERENCES "add_info" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "add_info_language_map"
--

DROP TABLE IF EXISTS "add_info_language_map";


CREATE TABLE "add_info_language_map" (
  "add_info_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "file_id" int NOT NULL,
  "file_checksum" text NOT NULL,
  PRIMARY KEY ("add_info_id","language_iso639-1"),


  CONSTRAINT "fk_add_info_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "idis_glossary"."g_language" ("iso639-1"),
  CONSTRAINT "fk_add_info_zuordnung_add_info1" FOREIGN KEY ("add_info_id") REFERENCES "add_info" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "add_info_old_new_map"
--

DROP TABLE IF EXISTS "add_info_old_new_map";


CREATE TABLE "add_info_old_new_map" (
  "old_add_id" int NOT NULL,
  "new_add_id" int NOT NULL
);


--
-- Table structure for table "annex_pictures"
--

DROP TABLE IF EXISTS "annex_pictures";


CREATE TABLE "annex_pictures" (
  "id" int NOT NULL ,
  "brand_id" int NOT NULL,
  "model_id" int NOT NULL,
  "variant_id" int NOT NULL,
  "name" varchar(45) NOT NULL,
  "is_flash" bit DEFAULT '1',
  PRIMARY KEY ("id")
);


--
-- Table structure for table "brake_servo_pictures"
--

DROP TABLE IF EXISTS "brake_servo_pictures";


CREATE TABLE "brake_servo_pictures" (
  "id" int NOT NULL ,
  "brand_id" int NOT NULL,
  "model_id" int NOT NULL,
  "variant_id" int NOT NULL,
  "name" varchar(45) NOT NULL,
  "is_flash" bit DEFAULT '1',
  PRIMARY KEY ("id")
);


--
-- Temporary view structure for view "brand_model_variant_view"
--

DROP TABLE IF EXISTS "brand_model_variant_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "brand_model_variant_view" AS SELECT
-- 1 AS "language_short",
-- 1 AS "brand_id",
-- 1 AS "brand_name",
-- 1 AS "model_id",
-- 1 AS "model_name",
-- 1 AS "variant_id",
-- 1 AS "variant_name",
-- 1 AS "period_from_month",
-- 1 AS "period_from_year",
-- 1 AS "period_to_month",
-- 1 AS "period_to_year",
-- 1 AS "status_id"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "changes"
--

DROP TABLE IF EXISTS "changes";


CREATE TABLE "changes" (
  "id" int NOT NULL ,
  "user_id" int NOT NULL,
  "time" date NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "variant_id" int NOT NULL,
  "value" text NOT NULL,
  "checked" bit NOT NULL DEFAULT '0',
  PRIMARY KEY ("id")
);


--
-- Table structure for table "changes_old"
--

DROP TABLE IF EXISTS "changes_old";


CREATE TABLE "changes_old" (
  "id" int NOT NULL ,
  "date_time" date DEFAULT NULL,
  "user" text,
  "variant" int DEFAULT NULL,
  "action" text,
  "table_changed" text,
  "value" text,
  "sql_used" text,
  "checked" bit DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "component_addinfo_map"
--

DROP TABLE IF EXISTS "component_addinfo_map";


CREATE TABLE "component_addinfo_map" (
  "component_id" int NOT NULL,
  "add_info_id" int NOT NULL,
  "add_info_typ" int NOT NULL,
  PRIMARY KEY ("component_id","add_info_typ","add_info_id"),



  CONSTRAINT "fk_component_addinfo_map_add_info1" FOREIGN KEY ("add_info_id") REFERENCES "add_info" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_addinfo_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_addinfo_map_g_addinfo_typ1" FOREIGN KEY ("add_info_typ") REFERENCES "idis_glossary"."g_add_info_typ" ("id")
);


--
-- Table structure for table "component_fixing_map"
--

DROP TABLE IF EXISTS "component_fixing_map";


CREATE TABLE "component_fixing_map" (
  "component_id" int NOT NULL,
  "g_fixing_id" int NOT NULL,
  "quantity" int DEFAULT NULL,
  "id" int NOT NULL ,
  PRIMARY KEY ("id"),


  CONSTRAINT "fk_component_fixing_map_fixing1" FOREIGN KEY ("g_fixing_id") REFERENCES "idis_glossary"."g_fixing" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_fixing_zuordnung_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "component_gas_map"
--

DROP TABLE IF EXISTS "component_gas_map";


CREATE TABLE "component_gas_map" (
  "component_id" int NOT NULL,
  "operation_mode" int DEFAULT NULL,
  "number_of_tanks" text,
  PRIMARY KEY ("component_id"),
  CONSTRAINT "fk_component_gas_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "component_gpyro_value_map"
--

DROP TABLE IF EXISTS "component_gpyro_value_map";


CREATE TABLE "component_gpyro_value_map" (
  "component_id" int NOT NULL,
  "g_pyro_id" int NOT NULL,
  "g_pyro_column_id" int NOT NULL,
  "g_pyro_column_value_id" int DEFAULT NULL,
  PRIMARY KEY ("component_id","g_pyro_id","g_pyro_column_id"),



  CONSTRAINT "fk_component_gpyro_value_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_gpyro_value_map_g_pyro1" FOREIGN KEY ("g_pyro_id") REFERENCES "idis_glossary"."g_pyro" ("id"),
  CONSTRAINT "fk_component_gpyro_value_map_g_pyro_column_value1" FOREIGN KEY ("g_pyro_column_value_id") REFERENCES "idis_glossary"."g_pyro_column_value" ("id"),
  CONSTRAINT "fk_component_gpyro_value_map_g_pyro_list_column1" FOREIGN KEY ("g_pyro_column_id") REFERENCES "idis_glossary"."g_pyro_column" ("column_id")
);


--
-- Table structure for table "component_health_safety_map"
--

DROP TABLE IF EXISTS "component_health_safety_map";


CREATE TABLE "component_health_safety_map" (
  "component_id" int NOT NULL,
  "g_health_safety_id" int NOT NULL,
  PRIMARY KEY ("component_id","g_health_safety_id"),

  CONSTRAINT "fk_component_health_safety_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_health_safety_map_g_health_safety1" FOREIGN KEY ("g_health_safety_id") REFERENCES "idis_glossary"."g_health_safety" ("id")
);


--
-- Table structure for table "component_picture_map"
--

DROP TABLE IF EXISTS "component_picture_map";


CREATE TABLE "component_picture_map" (
  "component_id" int NOT NULL,
  "picture" bytea NOT NULL,
  PRIMARY KEY ("component_id"),
  CONSTRAINT "component_picture_map_component_id_fk" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "pyro_adaptor"
--


CREATE TABLE "pyro_adaptor" (
  "id" int NOT NULL ,
  "g_brand_id" int NOT NULL,
  "text" text,
  PRIMARY KEY ("id"),

  CONSTRAINT "fk_pyro_adaptor_brand1" FOREIGN KEY ("g_brand_id") REFERENCES "idis_glossary"."g_brand" ("id")
);


--
-- Table structure for table "component_pyro_adaptor_map"
--

DROP TABLE IF EXISTS "component_pyro_adaptor_map";


CREATE TABLE "component_pyro_adaptor_map" (
  "component_id" int NOT NULL,
  "pyro_adaptor_id" int NOT NULL,
  PRIMARY KEY ("component_id","pyro_adaptor_id"),
  CONSTRAINT "fk_component_pyro_adaptor_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_pyro_adaptor_map_pyro_adaptor1" FOREIGN KEY ("pyro_adaptor_id") REFERENCES "pyro_adaptor" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "component_recycling_map"
--

DROP TABLE IF EXISTS "component_recycling_map";


CREATE TABLE "component_recycling_map" (
  "component_id" int NOT NULL,
  "g_recycling_id" int NOT NULL,
  PRIMARY KEY ("component_id","g_recycling_id"),

  CONSTRAINT "fk_component_recycling_map_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_recycling_map_g_recycling1" FOREIGN KEY ("g_recycling_id") REFERENCES "idis_glossary"."g_recycling" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "component_tool_map"
--

DROP TABLE IF EXISTS "component_tool_map";


CREATE TABLE "component_tool_map" (
  "component_id" int NOT NULL,
  "g_tool_id" int NOT NULL,
  "id" int NOT NULL ,
  PRIMARY KEY ("id"),


  CONSTRAINT "fk_component_tool_map_tool1" FOREIGN KEY ("g_tool_id") REFERENCES "idis_glossary"."g_tool" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_component_tool_zuordnung_component1" FOREIGN KEY ("component_id") REFERENCES "component" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Temporary view structure for view "components_with_add_info"
--

DROP TABLE IF EXISTS "components_with_add_info";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "components_with_add_info" AS SELECT
-- 1 AS "id",
-- 1 AS "g_part_id",
-- 1 AS "add_info_id",
-- 1 AS "add_info_typ",
-- 1 AS "variant_id",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "has_language"*/;
-- SET character_set_client = @saved_cs_client;


--
-- Table structure for table "derivativ_g_derivativ_map"
--

DROP TABLE IF EXISTS "derivativ_g_derivativ_map";


CREATE TABLE "derivativ_g_derivativ_map" (
  "derivativ_id" int NOT NULL,
  "sortorder" int NOT NULL,
  "g_derivativ_id" int NOT NULL,
  PRIMARY KEY ("derivativ_id","sortorder"),


  CONSTRAINT "fk_derivativ_glossary_derivativ_map_derivativ1" FOREIGN KEY ("g_derivativ_id") REFERENCES "idis_glossary"."g_derivativ" ("id"),
  CONSTRAINT "fk_derivativ_glossary_derivativ_zuordnung_derivativ1" FOREIGN KEY ("derivativ_id") REFERENCES "derivativ" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "derivativ_g_pyro_column_text"
--

DROP TABLE IF EXISTS "derivativ_g_pyro_column_text";


CREATE TABLE "derivativ_g_pyro_column_text" (
  "derivativ_id" int NOT NULL,
  "g_pyro_id" int NOT NULL,
  "g_pyro_column_id" int NOT NULL,
  "text" text,
  PRIMARY KEY ("derivativ_id","g_pyro_column_id","g_pyro_id"),


  CONSTRAINT "fk_derivativ_g_pyro_column_text_derivativ1" FOREIGN KEY ("derivativ_id") REFERENCES "derivativ" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_derivativ_g_pyro_column_text_g_pyro1" FOREIGN KEY ("g_pyro_id") REFERENCES "idis_glossary"."g_pyro" ("id"),
  CONSTRAINT "fk_derivativ_g_pyro_column_text_g_pyro_column1" FOREIGN KEY ("g_pyro_column_id") REFERENCES "idis_glossary"."g_pyro_column" ("column_id")
);


--
-- Table structure for table "derivativ_g_pyro_value_map"
--

DROP TABLE IF EXISTS "derivativ_g_pyro_value_map";


CREATE TABLE "derivativ_g_pyro_value_map" (
  "derivativ_id" int NOT NULL,
  "g_pyro_id" int NOT NULL,
  "g_pyro_column_id" int NOT NULL,
  "g_pyro_column_value_id" int DEFAULT NULL,
  PRIMARY KEY ("derivativ_id","g_pyro_id","g_pyro_column_id"),



  CONSTRAINT "fk_derivativ_g_pyro_value_map_derivativ1" FOREIGN KEY ("derivativ_id") REFERENCES "derivativ" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_derivativ_g_pyro_value_map_g_pyro1" FOREIGN KEY ("g_pyro_id") REFERENCES "idis_glossary"."g_pyro" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_derivativ_g_pyro_value_map_g_pyro_column_value1" FOREIGN KEY ("g_pyro_column_value_id") REFERENCES "idis_glossary"."g_pyro_column_value" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_derivativ_g_pyro_value_map_g_pyro_list_column1" FOREIGN KEY ("g_pyro_column_id") REFERENCES "idis_glossary"."g_pyro_column" ("column_id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Temporary view structure for view "derivativ_name"
--

DROP TABLE IF EXISTS "derivativ_name";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "derivativ_name" AS SELECT
-- 1 AS "id",
-- 1 AS "language_iso639-1",
-- 1 AS "derivativ_name"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "derivatives"
--

DROP TABLE IF EXISTS "derivatives";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "derivatives" AS SELECT
-- 1 AS "id",
-- 1 AS "variant_id",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "sortorder"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "dismantling_pictures"
--

DROP TABLE IF EXISTS "dismantling_pictures";


CREATE TABLE "dismantling_pictures" (
  "id" int NOT NULL ,
  "variant_id" int NOT NULL,
  "model_id" int NOT NULL,
  "brand_id" int NOT NULL,
  "area_id" int NOT NULL,
  "page_num" int NOT NULL,
  "name" text NOT NULL,
  "is_flash" bit DEFAULT '1',
  PRIMARY KEY ("id")
);


--
-- Temporary view structure for view "fixing_qty_view"
--

DROP TABLE IF EXISTS "fixing_qty_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "fixing_qty_view" AS SELECT
-- 1 AS "id",
-- 1 AS "component_id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "quantity",
-- 1 AS "quantityId"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "g_fixings_component_mapping_view"
--

DROP TABLE IF EXISTS "g_fixings_component_mapping_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_fixings_component_mapping_view" AS SELECT
-- 1 AS "id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "component_id",
-- 1 AS "quantity"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "g_recycling_component_mapping_view"
--

DROP TABLE IF EXISTS "g_recycling_component_mapping_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_recycling_component_mapping_view" AS SELECT
-- 1 AS "id",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "component_id"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "g_tool_component_mapping_view"
--

DROP TABLE IF EXISTS "g_tool_component_mapping_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "g_tool_component_mapping_view" AS SELECT
-- 1 AS "id",
-- 1 AS "info",
-- 1 AS "sortorder",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "component_id"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "glossary_mainarea_button"
--

DROP TABLE IF EXISTS "glossary_mainarea_button";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "glossary_mainarea_button" AS SELECT
-- 1 AS "mainarea_id",
-- 1 AS "glossary_part",
-- 1 AS "id",
-- 1 AS "button_picture_id",
-- 1 AS "active",
-- 1 AS "comment",
-- 1 AS "sortorder"*/;
-- SET character_set_client = @saved_cs_client;



--
-- Table structure for table "model_language_map"
--

DROP TABLE IF EXISTS "model_language_map";


CREATE TABLE "model_language_map" (
  "model_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("model_id","language_iso639-1"),

  CONSTRAINT "fk_model_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "idis_glossary"."g_language" ("iso639-1"),
  CONSTRAINT "fk_model_language_zuordnung_model1" FOREIGN KEY ("model_id") REFERENCES "model" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Temporary view structure for view "models"
--

DROP TABLE IF EXISTS "models";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "models" AS SELECT
-- 1 AS "model_id",
-- 1 AS "language_iso639-1",
-- 1 AS "text",
-- 1 AS "g_brand_id"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "part_area_view"
--

DROP TABLE IF EXISTS "part_area_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "part_area_view" AS SELECT
-- 1 AS "variant_id",
-- 1 AS "id",
-- 1 AS "default_picture",
-- 1 AS "enabled",
-- 1 AS "caution",
-- 1 AS "comment",
-- 1 AS "area_id",
-- 1 AS "mainarea_id",
-- 1 AS "picture_enabled",
-- 1 AS "picture_disabled",
-- 1 AS "picture_active",
-- 1 AS "language_iso639-1",
-- 1 AS "areaName",
-- 1 AS "partName"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "part_component_view"
--

DROP TABLE IF EXISTS "part_component_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "part_component_view" AS SELECT
-- 1 AS "id",
-- 1 AS "variant_id",
-- 1 AS "mainarea_id",
-- 1 AS "area_id",
-- 1 AS "caution",
-- 1 AS "component_id",
-- 1 AS "quantity",
-- 1 AS "derivative_id",
-- 1 AS "g_material_id",
-- 1 AS "text",
-- 1 AS "language_iso639-1",
-- 1 AS "position_id",
-- 1 AS "position_text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view "part_sno_view"
--

DROP TABLE IF EXISTS "part_sno_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "part_sno_view" AS SELECT
-- 1 AS "id",
-- 1 AS "g_area_id",
-- 1 AS "serial_number",
-- 1 AS "variant_id",
-- 1 AS "weight",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;


--
-- Temporary view structure for view "pyro_component_view"
--

DROP TABLE IF EXISTS "pyro_component_view";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "pyro_component_view" AS SELECT
-- 1 AS "variant_id",
-- 1 AS "derivative_id",
-- 1 AS "component_id",
-- 1 AS "part_id",
-- 1 AS "pyro_id",
-- 1 AS "column_index",
-- 1 AS "column_value"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "supercap_pictures"
--

DROP TABLE IF EXISTS "supercap_pictures";


CREATE TABLE "supercap_pictures" (
  "id" int NOT NULL ,
  "brand_id" int NOT NULL,
  "model_id" int NOT NULL,
  "variant_id" int NOT NULL,
  "name" varchar(45) NOT NULL,
  "is_flash" bit DEFAULT '1',
  PRIMARY KEY ("id")
);


--
-- Temporary view structure for view "user_brand_model_variant_map"
--

DROP TABLE IF EXISTS "user_brand_model_variant_map";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "user_brand_model_variant_map" AS SELECT
-- 1 AS "user_id",
-- 1 AS "brand_id",
-- 1 AS "model_id",
-- 1 AS "variant_id"*/;
-- SET character_set_client = @saved_cs_client;


--
-- Table structure for table "variant_country_map"
--

DROP TABLE IF EXISTS "variant_country_map";


CREATE TABLE "variant_country_map" (
  "variant_id" int NOT NULL,
  "g_country_id" int NOT NULL,
  "active" bit DEFAULT NULL,
  PRIMARY KEY ("variant_id","g_country_id"),

  CONSTRAINT "fk_variant_country_map_country1" FOREIGN KEY ("g_country_id") REFERENCES "idis_glossary"."g_country" ("id"),
  CONSTRAINT "fk_variant_country_zuordnung_variant1" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "variant_language_map"
--

DROP TABLE IF EXISTS "variant_language_map";


CREATE TABLE "variant_language_map" (
  "variant_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text NOT NULL,
  PRIMARY KEY ("variant_id","language_iso639-1"),


  CONSTRAINT "fk_variant_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "idis_glossary"."g_language" ("iso639-1"),
  CONSTRAINT "fk_variant_language_zuordnung_variant1" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "variant_picture_thumbnail"
--

DROP TABLE IF EXISTS "variant_picture_thumbnail";


CREATE TABLE "variant_picture_thumbnail" (
  "variant_id" int NOT NULL,
  "thumbnaildata" bytea,
  PRIMARY KEY ("variant_id"),
  CONSTRAINT "variant_picture_thumbnail_variant_id_fkey" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "variant_status"
--


CREATE TABLE "variant_status" (
  "id" int NOT NULL,
  "logo" bytea,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "variant_status_language_map"
--

DROP TABLE IF EXISTS "variant_status_language_map";


CREATE TABLE "variant_status_language_map" (
  "variant_status_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text NOT NULL,
  PRIMARY KEY ("variant_status_id","language_iso639-1"),

  CONSTRAINT "fk_variant_status_language_map_language1" FOREIGN KEY ("language_iso639-1") REFERENCES "idis_glossary"."g_language" ("iso639-1"),
  CONSTRAINT "fk_variant_status_language_zuordnung_variant_status1" FOREIGN KEY ("variant_status_id") REFERENCES "variant_status" ("id")
);


--
-- Temporary view structure for view "variants"
--

DROP TABLE IF EXISTS "variants";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "variants" AS SELECT
-- 1 AS "id",
-- 1 AS "model_id",
-- 1 AS "variant_status_id",
-- 1 AS "period_from_month",
-- 1 AS "period_from_year",
-- 1 AS "period_to_month",
-- 1 AS "period_to_year",
-- 1 AS "caravan",
-- 1 AS "version",
-- 1 AS "creation_user_id",
-- 1 AS "creation_time",
-- 1 AS "first_release_user_id",
-- 1 AS "first_release_time",
-- 1 AS "last_change_user_id",
-- 1 AS "last_change_time",
-- 1 AS "prerelease_user_id",
-- 1 AS "release_user_id",
-- 1 AS "release_time",
-- 1 AS "picture",
-- 1 AS "language_iso639-1",
-- 1 AS "text"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "vehicle_information"
--

DROP TABLE IF EXISTS "vehicle_information";


CREATE TABLE "vehicle_information" (
  "variant_id" int NOT NULL,
  "kerb_mass" text NOT NULL,
  "luggage_volume" text NOT NULL,
  "axle_base" text NOT NULL,
  "variant_height" text NOT NULL,
  "variant_width" text NOT NULL,
  "variant_length" text NOT NULL,
  "model_identification" text NOT NULL,
  "size_of_tire" text NOT NULL,
  "fuel_tank_capacity" text NOT NULL,
  "transmission" text NOT NULL,
  "fuel_type" text NOT NULL,
  PRIMARY KEY ("variant_id"),
  CONSTRAINT "fk_vehicle_information_variant_id1" FOREIGN KEY ("variant_id") REFERENCES "variant" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Current Database: "idis_users"
--

CREATE SCHEMA IF NOT EXISTS "idis_users";
SET search_path TO "idis_users";


DROP TABLE IF EXISTS "dismantler" CASCADE;
DROP TABLE IF EXISTS "permission" CASCADE;


--
-- Table structure for table "dismantler"
--


CREATE TABLE "dismantler" (
  "id" int NOT NULL ,
  "lastname" text NOT NULL,
  "firstname" text NOT NULL,
  "pwd" text NOT NULL,
  "language" varchar(2) DEFAULT NULL,
  "country" int DEFAULT NULL,
  "email" varchar(60) DEFAULT NULL,
  "company" text,
  "adress" text,
  "zip" text,
  "city" text,
  "company_country" int DEFAULT '0',
  "phone" text,
  "fax" text,
  "last_login" date DEFAULT NULL,
  "delete_date" date DEFAULT NULL,
  "useconditions_accepted" bit NOT NULL DEFAULT '0',
  "privacy_statement_accepted" bit NOT NULL DEFAULT '0',
  "receive_mails" bit NOT NULL DEFAULT '0',
  "receive_system_notification" bit NOT NULL DEFAULT '0',
  "receive_newsletter" bit NOT NULL DEFAULT '0',
  "recycler" bit NOT NULL DEFAULT '0',
  PRIMARY KEY ("id"),
  CONSTRAINT "dismantler_login_name_UNIQUE" UNIQUE ("delete_date","email")
);


--
-- Table structure for table "dismantler_activity_map"
--

DROP TABLE IF EXISTS "dismantler_activity_map";


CREATE TABLE "dismantler_activity_map" (
  "user_id" int NOT NULL,
  "activity_id" int NOT NULL,
  PRIMARY KEY ("user_id","activity_id"),

  CONSTRAINT "fk_dismantler_activity_map_activity_id1" FOREIGN KEY ("activity_id") REFERENCES "idis_glossary"."g_activity" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_dismantler_activity_map_user_id1" FOREIGN KEY ("user_id") REFERENCES "dismantler" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Temporary view structure for view "logins"
--

DROP TABLE IF EXISTS "logins";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "logins" AS SELECT
-- 1 AS "email",
-- 1 AS "delete_date"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "messages"
--

DROP TABLE IF EXISTS "messages";


CREATE TABLE "messages" (
  "id" int NOT NULL ,
  "from_user" int NOT NULL,
  "to_user" int NOT NULL,
  "subject" text NOT NULL,
  "message" text NOT NULL,
  "date_sent" date NOT NULL,
  "unread" bit NOT NULL,
  "is_release_request_message" bit NOT NULL,
  "release_request_variant_id" int DEFAULT NULL,
  "is_delete_request_message" bit DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "permission"
--


CREATE TABLE "permission" (
  "id" int NOT NULL ,
  "permission_name" text NOT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "requested_release_map"
--

DROP TABLE IF EXISTS "requested_release_map";


CREATE TABLE "requested_release_map" (
  "requesting_user_id" int NOT NULL,
  "requested_variant_id" int NOT NULL,
  PRIMARY KEY ("requesting_user_id","requested_variant_id")
);


--
-- Table structure for table "role"
--

DROP TABLE IF EXISTS "role";


CREATE TABLE "role" (
  "id" int NOT NULL,
  "text" text NOT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "role_language_map"
--

DROP TABLE IF EXISTS "role_language_map";


CREATE TABLE "role_language_map" (
  "role_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text NOT NULL,
  PRIMARY KEY ("language_iso639-1","role_id")
);


--
-- Table structure for table "role_permission_map"
--

DROP TABLE IF EXISTS "role_permission_map";


CREATE TABLE "role_permission_map" (
  "role_id" int NOT NULL,
  "permission_id" int NOT NULL,
  PRIMARY KEY ("role_id","permission_id"),

  CONSTRAINT "role_permission_map_permission_FK" FOREIGN KEY ("permission_id") REFERENCES "permission" ("id")
);


--
-- Temporary view structure for view "roles"
--

DROP TABLE IF EXISTS "roles";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "roles" AS SELECT
-- 1 AS "id",
-- 1 AS "text",
-- 1 AS "language_iso639-1",
-- 1 AS "translated"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "system_news"
--

DROP TABLE IF EXISTS "system_news";


CREATE TABLE "system_news" (
  "id" int NOT NULL ,
  "news" text NOT NULL,
  "date" date NOT NULL,
  "changing_user_id" int NOT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "user"
--

DROP TABLE IF EXISTS "user";


CREATE TABLE "user" (
  "id" int NOT NULL ,
  "lastname" text NOT NULL,
  "firstname" text NOT NULL,
  "pwd" text NOT NULL,
  "language" varchar(2) DEFAULT NULL,
  "country" int DEFAULT NULL,
  "email" varchar(255) DEFAULT NULL,
  "company" text,
  "adress" text,
  "zip" text,
  "city" text,
  "company_country" int DEFAULT '0',
  "phone" text,
  "fax" text,
  "last_login" date DEFAULT NULL,
  "delete_date" date DEFAULT NULL,
  "useconditions_accepted" bit NOT NULL DEFAULT '0',
  "privacy_statement_accepted" bit NOT NULL DEFAULT '0',
  "receive_mails" bit NOT NULL DEFAULT '0',
  "receive_system_notification" bit NOT NULL DEFAULT '0',
  "receive_newsletter" bit NOT NULL DEFAULT '0',
  "isMastertestUser" bit NOT NULL DEFAULT '0',
  "login_name" varchar(255) DEFAULT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "user_login_name_UNIQUE" UNIQUE ("delete_date","email")
);


--
-- Table structure for table "user_brand_map"
--

DROP TABLE IF EXISTS "user_brand_map";


CREATE TABLE "user_brand_map" (
  "user_id" int NOT NULL,
  "brand_id" int NOT NULL,
  "role_id_for_brand" int NOT NULL DEFAULT '1',
  PRIMARY KEY ("user_id","brand_id"),

  CONSTRAINT "user_brand_map_brand_fk" FOREIGN KEY ("brand_id") REFERENCES "idis_glossary"."g_brand" ("id") ON DELETE CASCADE,
  CONSTRAINT "user_brand_map_user_fk" FOREIGN KEY ("user_id") REFERENCES "user" ("id") ON DELETE CASCADE
);


--
-- Table structure for table "user_g_country_permission_map"
--

DROP TABLE IF EXISTS "user_g_country_permission_map";


CREATE TABLE "user_g_country_permission_map" (
  "user_id" int NOT NULL,
  "g_country_id" int NOT NULL,
  "brand_id" int NOT NULL DEFAULT '0',
  PRIMARY KEY ("user_id","g_country_id","brand_id"),
  CONSTRAINT "user_g_country_permission_map_user_fk" FOREIGN KEY ("user_id") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "user_password_reset"
--

DROP TABLE IF EXISTS "user_password_reset";


CREATE TABLE "user_password_reset" (
  "id" int NOT NULL ,
  "user_id" int DEFAULT NULL,
  "hash" varchar(45) NOT NULL,
  "expiry_date" date DEFAULT NULL,
  PRIMARY KEY ("id"),

  CONSTRAINT "FK_user_password_reset_userid" FOREIGN KEY ("user_id") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "user_role_map"
--

DROP TABLE IF EXISTS "user_role_map";


CREATE TABLE "user_role_map" (
  "user_id" int NOT NULL,
  "role_id" int NOT NULL,
  PRIMARY KEY ("user_id","role_id"),

  CONSTRAINT "user_role_map_role_FK" FOREIGN KEY ("role_id") REFERENCES "role" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "user_role_map_user_FK" FOREIGN KEY ("user_id") REFERENCES "user" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "user_variant_assignment_map"
--

DROP TABLE IF EXISTS "user_variant_assignment_map";


CREATE TABLE "user_variant_assignment_map" (
  "user_id" int NOT NULL,
  "variant_id" int NOT NULL,
  "variant_brand_id" int NOT NULL,
  PRIMARY KEY ("user_id","variant_id")
);


--
-- Current Database: "idis_web"
--

CREATE SCHEMA IF NOT EXISTS "idis_web";
SET search_path TO "idis_web";

--
-- Table structure for table "admins"
--

DROP TABLE IF EXISTS "admins";


CREATE TABLE "admins" (
  "id" int NOT NULL ,
  "login" varchar(255) DEFAULT NULL,
  "password" varchar(255) DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "approved_members"
--

DROP TABLE IF EXISTS "approved_members";


CREATE TABLE "approved_members" (
  "id" int NOT NULL ,
  "assoc_id" int DEFAULT NULL,
  "companyName" varchar(255) DEFAULT NULL,
  "contact_first_name" varchar(255) DEFAULT NULL,
  "contact_last_name" varchar(255) DEFAULT NULL,
  "address" varchar(255) DEFAULT NULL,
  "email" varchar(255) DEFAULT NULL,
  "zipCode" varchar(255) DEFAULT NULL,
  "city" varchar(255) DEFAULT NULL,
  "country" varchar(255) DEFAULT NULL,
  "phoneNo" varchar(255) DEFAULT NULL,
  "ulanguage" varchar(255) DEFAULT NULL,
  "password" varchar(255) DEFAULT NULL,
  "activity" varchar(255) DEFAULT NULL,
  "status" varchar(255) DEFAULT 'Orange',
  "user_id" int DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "assoc_users"
--

DROP TABLE IF EXISTS "assoc_users";


CREATE TABLE "assoc_users" (
  "id" int NOT NULL ,
  "assoc_id" int DEFAULT NULL,
  "companyName" varchar(255) DEFAULT NULL,
  "contact_first_name" varchar(255) DEFAULT NULL,
  "contact_last_name" varchar(255) DEFAULT NULL,
  "address" varchar(255) DEFAULT NULL,
  "email" varchar(255) DEFAULT NULL,
  "zipCode" varchar(255) DEFAULT NULL,
  "city" varchar(255) DEFAULT NULL,
  "country" varchar(255) DEFAULT NULL,
  "phoneNo" varchar(255) DEFAULT NULL,
  "ulanguage" varchar(255) DEFAULT NULL,
  "password" varchar(255) DEFAULT NULL,
  "activity" varchar(255) DEFAULT NULL,
  "status" varchar(255) NOT NULL DEFAULT 'Red',
  PRIMARY KEY ("id")
);


--
-- Table structure for table "associations"
--

DROP TABLE IF EXISTS "associations";


CREATE TABLE "associations" (
  "assoc_id" int NOT NULL ,
  "companyName" varchar(255) DEFAULT NULL,
  "contact_first_name" varchar(255) DEFAULT NULL,
  "contact_last_name" varchar(255) DEFAULT NULL,
  "address" varchar(255) DEFAULT NULL,
  "email" varchar(255) DEFAULT NULL,
  "zipCode" varchar(255) DEFAULT NULL,
  "city" varchar(255) DEFAULT NULL,
  "country" varchar(255) DEFAULT NULL,
  "phoneNo" varchar(255) DEFAULT NULL,
  "ulanguage" varchar(255) DEFAULT NULL,
  "password" varchar(255) DEFAULT NULL,
  "activated" varchar(255) NOT NULL DEFAULT 'No',
  "active" varchar(255) DEFAULT NULL,
  "resetToken" varchar(255) DEFAULT NULL,
  "resetComplete" varchar(255) DEFAULT NULL,
  "email_verified" varchar(255) NOT NULL DEFAULT 'No',
  "GDPR" varchar(45) DEFAULT 'No',
  PRIMARY KEY ("assoc_id")
);


--
-- Temporary view structure for view "ergebnisse"
--

DROP TABLE IF EXISTS "ergebnisse";

-- SET @saved_cs_client     = @@character_set_client;
--
-- /*!50001 CREATE VIEW "ergebnisse" AS SELECT
-- 1 AS "id",
-- 1 AS "DownloadLimit",
-- 1 AS "CompanyName",
-- 1 AS "ContactName",
-- 1 AS "Address",
-- 1 AS "Email",
-- 1 AS "ZipCode",
-- 1 AS "City",
-- 1 AS "Country",
-- 1 AS "PhoneNo",
-- 1 AS "Language",
-- 1 AS "Dismantling",
-- 1 AS "Shredding",
-- 1 AS "UsedParts",
-- 1 AS "MaterialRecycling",
-- 1 AS "ActivityOther",
-- 1 AS "receive_newsletter",
-- 1 AS "CarwreckTreatment",
-- 1 AS "Employees",
-- 1 AS "FromWhom",
-- 1 AS "DoYouAlreadyUse",
-- 1 AS "LastVersion",
-- 1 AS "StillUseIDIS",
-- 1 AS "OnPC",
-- 1 AS "AsPrintOut",
-- 1 AS "Workshop",
-- 1 AS "Office",
-- 1 AS "Elsewhere",
-- 1 AS "DoYouKnowWebUpdate",
-- 1 AS "DoYouUpdate",
-- 1 AS "OnlyDVD",
-- 1 AS "DVDWeb",
-- 1 AS "FullWeb",
-- 1 AS "HowOftenDoYouUse",
-- 1 AS "PreTreatmentInf",
-- 1 AS "AirbagNeutralization",
-- 1 AS "HeavyMetalLocation",
-- 1 AS "MaterialSelection",
-- 1 AS "ToolsAndMethods",
-- 1 AS "PurposeOther",
-- 1 AS "IfYouDontUse",
-- 1 AS "NoPCOrOS",
-- 1 AS "NotUserfriendly",
-- 1 AS "NotTheRightLanguage",
-- 1 AS "TooDifficult",
-- 1 AS "TakesTooLong",
-- 1 AS "CDEasyInstall",
-- 1 AS "CDEasyUse",
-- 1 AS "CDEasyBrowsing",
-- 1 AS "BrowsingWebData",
-- 1 AS "IDISModeldatacomplete",
-- 1 AS "PreTreatmentInfcomplete",
-- 1 AS "MaterialInfUseful",
-- 1 AS "ToolsUseful",
-- 1 AS "PurposeMissing",
-- 1 AS "ReasonMissing",
-- 1 AS "CommentMissing",
-- 1 AS "OtherSuggestion",
-- 1 AS "OrderIDIS",
-- 1 AS "SentDate",
-- 1 AS "DownloadKey",
-- 1 AS "OrderDate",
-- 1 AS "QuestSent",
-- 1 AS "QuestReplied",
-- 1 AS "QuestCode",
-- 1 AS "Others",
-- 1 AS "Reason",
-- 1 AS "OrderDate2",
-- 1 AS "Waitlist",
-- 1 AS "ResentDate",
-- 1 AS "Bemerkung",
-- 1 AS "Carwrecks2002",
-- 1 AS "Carwrecks2003",
-- 1 AS "Carwrecks2005",
-- 1 AS "Carwrecks2006",
-- 1 AS "Carwrecks2007",
-- 1 AS "Carwrecks2008",
-- 1 AS "Carwrecks2009",
-- 1 AS "Carwrecks2010"*/;
-- SET character_set_client = @saved_cs_client;

--
-- Table structure for table "ergebnisse_basis"
--

DROP TABLE IF EXISTS "ergebnisse_basis";


CREATE TABLE "ergebnisse_basis" (
  "id" int NOT NULL ,
  "DownloadLimit" smallint NOT NULL DEFAULT '0',
  "CompanyName" text NOT NULL,
  "contact_first_name" text NOT NULL,
  "contact_last_name" text NOT NULL,
  "Address" text NOT NULL,
  "Email" text NOT NULL,
  "ZipCode" text NOT NULL,
  "City" text NOT NULL,
  "Country" text,
  "PhoneNo" text,
  "Language" varchar(2) NOT NULL,
  "Dismantling" text,
  "Shredding" text,
  "UsedParts" text,
  "MaterialRecycling" text,
  "ActivityOther" text,
  "receive_newsletter" bit DEFAULT '0',
  "CarwreckTreatment" text,
  "Employees" text,
  "FromWhom" text,
  "DoYouAlreadyUse" text,
  "LastVersion" text,
  "StillUseIDIS" text,
  "OnPC" text,
  "AsPrintOut" text,
  "Workshop" text,
  "Office" text,
  "Elsewhere" text,
  "DoYouKnowWebUpdate" text,
  "DoYouUpdate" text,
  "OnlyDVD" text,
  "DVDWeb" text,
  "FullWeb" text,
  "HowOftenDoYouUse" text,
  "PreTreatmentInf" text,
  "AirbagNeutralization" text,
  "HeavyMetalLocation" text,
  "MaterialSelection" text,
  "ToolsAndMethods" text,
  "PurposeOther" text,
  "IfYouDontUse" text,
  "NoPCOrOS" text,
  "NotUserfriendly" text,
  "NotTheRightLanguage" text,
  "TooDifficult" text,
  "TakesTooLong" text,
  "CDEasyInstall" text,
  "CDEasyUse" text,
  "CDEasyBrowsing" text,
  "BrowsingWebData" text,
  "IDISModeldatacomplete" text,
  "PreTreatmentInfcomplete" text,
  "MaterialInfUseful" text,
  "ToolsUseful" text,
  "PurposeMissing" text,
  "ReasonMissing" text,
  "CommentMissing" text,
  "OtherSuggestion" text,
  "OrderIDIS" varchar(50) DEFAULT NULL,
  "SentDate" date DEFAULT NULL,
  "DownloadKey" text,
  "OrderDate" date DEFAULT NULL,
  "QuestSent" date DEFAULT NULL,
  "QuestReplied" smallint DEFAULT NULL,
  "QuestCode" varchar(6) DEFAULT NULL,
  "Others" smallint NOT NULL DEFAULT '0',
  "Reason" text,
  "OrderDate2" varchar(20) DEFAULT NULL,
  "Waitlist" text,
  "ResentDate" date DEFAULT NULL,
  "Bemerkung" varchar(100) DEFAULT NULL,
  "Carwrecks2002" text,
  "Carwrecks2003" text,
  "Carwrecks2005" varchar(50) DEFAULT NULL,
  "Carwrecks2006" varchar(50) DEFAULT NULL,
  "Carwrecks2007" varchar(50) DEFAULT NULL,
  "Carwrecks2008" varchar(50) DEFAULT NULL,
  "Carwrecks2009" varchar(50) DEFAULT NULL,
  "Carwrecks2010" varchar(50) DEFAULT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_translation"
--

DROP TABLE IF EXISTS "g_translation";


CREATE TABLE "g_translation" (
  "id" int NOT NULL ,
  "text" text NOT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "g_translation_language_map"
--

DROP TABLE IF EXISTS "g_translation_language_map";


CREATE TABLE "g_translation_language_map" (
  "translation_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text NOT NULL,
  PRIMARY KEY ("translation_id","language_iso639-1"),

  CONSTRAINT "fk_g_translation_language_map_language_iso639-1" FOREIGN KEY ("language_iso639-1") REFERENCES "idis_glossary"."g_language" ("iso639-1") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "fk_g_translation_language_map_translation_id" FOREIGN KEY ("translation_id") REFERENCES "g_translation" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "missing_translation"
--

DROP TABLE IF EXISTS "missing_translation";


CREATE TABLE "missing_translation" (
  "id" int NOT NULL ,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text NOT NULL,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "part_reuse_brand_link"
--

DROP TABLE IF EXISTS "part_reuse_brand_link";


CREATE TABLE "part_reuse_brand_link" (
  "id" int NOT NULL,
  "brand_id" int NOT NULL,
  "link" text NOT NULL,
  PRIMARY KEY ("id"),

  CONSTRAINT "part_reuse_brand_link_brand_id_fkey" FOREIGN KEY ("brand_id") REFERENCES "idis_glossary"."g_brand" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "part_reuse_manufacturer_contact"
--

DROP TABLE IF EXISTS "part_reuse_manufacturer_contact";


CREATE TABLE "part_reuse_manufacturer_contact" (
  "manufacturer_id" int NOT NULL,
  "contact_mail" text NOT NULL,
  PRIMARY KEY ("manufacturer_id"),
  CONSTRAINT "part_reuse_manufacturer_contact_manufacturer_id_fkey" FOREIGN KEY ("manufacturer_id") REFERENCES "idis_glossary"."g_manufacturer" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "part_reuse_manufacturer_link"
--

DROP TABLE IF EXISTS "part_reuse_manufacturer_link";


CREATE TABLE "part_reuse_manufacturer_link" (
  "id" int NOT NULL,
  "manufacturer_id" int NOT NULL,
  "link" text NOT NULL,
  PRIMARY KEY ("id"),

  CONSTRAINT "part_reuse_manufacturer_link_manufacturer_id_fkey" FOREIGN KEY ("manufacturer_id") REFERENCES "idis_glossary"."g_manufacturer" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "question"
--

DROP TABLE IF EXISTS "question";


CREATE TABLE "question" (
  "id" int NOT NULL,
  "field" text,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "question_language_map"
--

DROP TABLE IF EXISTS "question_language_map";


CREATE TABLE "question_language_map" (
  "question_id" int NOT NULL,
  "language_iso639-1" varchar(2) NOT NULL,
  "text" text,
  PRIMARY KEY ("question_id","language_iso639-1"),

  CONSTRAINT "question_language_map_language_iso639-1_fkey" FOREIGN KEY ("language_iso639-1") REFERENCES "idis_glossary"."g_language" ("iso639-1") ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT "question_language_map_question_id_fkey" FOREIGN KEY ("question_id") REFERENCES "question" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);


--
-- Table structure for table "reference_tool"
--

DROP TABLE IF EXISTS "reference_tool";


CREATE TABLE "reference_tool" (
  "id" int NOT NULL ,
  "toolname" text,
  "mainarea" int DEFAULT NULL,
  "company_name" text,
  "street" text,
  "house" text,
  "postal_code" text,
  "town" text,
  "country" int DEFAULT NULL,
  "web" text,
  "lastname" text,
  "firstname" text,
  "phone" text,
  "fax" text,
  "email" text,
  "last_change" date DEFAULT NULL,
  "created" date DEFAULT NULL,
  "show" bit NOT NULL DEFAULT '0',
  "status" int NOT NULL DEFAULT '0',
  "released" date DEFAULT NULL,
  "validation_reminder" date DEFAULT NULL,
  "validator" int DEFAULT NULL,
  "reason" text,
  PRIMARY KEY ("id")
);


--
-- Table structure for table "user_activation"
--

DROP TABLE IF EXISTS "user_activation";


CREATE TABLE "user_activation" (
  "active" varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY ("active")
);



set session_replication_role to default;