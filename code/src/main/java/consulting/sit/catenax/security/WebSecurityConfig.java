package consulting.sit.catenax.security;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

     @Override
     protected void configure(HttpSecurity http) throws Exception {
         http
                 .cors().and()
                 .csrf().disable()
                 .authorizeRequests()
                 .anyRequest().permitAll();
//                .mvcMatchers("/images/**").permitAll()
//                .mvcMatchers("/js/**").permitAll()
//                .mvcMatchers(HttpMethod.GET, "/glossary/**").permitAll()
//                .mvcMatchers("/glossary/**").hasAnyRole("Admin", "Glossaryverwalter")
//                .mvcMatchers("/project/**").hasAnyAuthority("Admin", "Editor", "Salesperson")
//                .anyRequest().authenticated()
//                 .and()
//                 .rememberMe().key("AbcdEfghIjklmNopQrsTuvXyz_0123456789");
         http.headers().frameOptions().sameOrigin();
     }
 }