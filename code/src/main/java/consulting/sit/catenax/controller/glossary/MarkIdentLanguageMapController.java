package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.MarkIdentLanguageMap;
import consulting.sit.catenax.model.glossary.MarkIdentLanguageMapPk;
import consulting.sit.catenax.model.glossary.PartLanguageMap;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.glossary.MarkIdentLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/glossary/markIdentLanguageMap")
@Slf4j
public class MarkIdentLanguageMapController extends GenericController<MarkIdentLanguageMap, MarkIdentLanguageMapPk> {
    private final MarkIdentLanguageMapService markIdentLanguageMapService;
    @Autowired
    private GenericRepository<MarkIdentLanguageMap, MarkIdentLanguageMapPk> repo;

    public MarkIdentLanguageMapController(final MarkIdentLanguageMapService markIdentLanguageMapService) {
        this.markIdentLanguageMapService = markIdentLanguageMapService;
    }

    @Autowired
    private void setRepository(GenericRepository<MarkIdentLanguageMap, MarkIdentLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<MarkIdentLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        MarkIdentLanguageMapPk pk = new MarkIdentLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<MarkIdentLanguageMap> update(@RequestBody MarkIdentLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        MarkIdentLanguageMapPk pk = new MarkIdentLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<MarkIdentLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        MarkIdentLanguageMapPk pk = new MarkIdentLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{language}")
    public ResponseEntity<List<MarkIdentLanguageMap>> findAllByLanguage(@PathVariable String language) {
        return ResponseEntity.ok(markIdentLanguageMapService.findAllByLanguage(language));
    }
}
