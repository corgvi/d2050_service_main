package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.model.glossary.Glossary;
import consulting.sit.catenax.model.glossary.TableName;
import consulting.sit.catenax.service.glossary.GlossaryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/glossary/table")
@Slf4j
public class GlossaryController {

    @Autowired
    private GlossaryService service;

    @GetMapping
    public List<Glossary> getAllValues() {
        List<Glossary> values = new ArrayList<>();
        for (TableName value : TableName.values()) {
            Glossary glossary = new Glossary();
            glossary.setName(value.getTableName());
            values.add(glossary);
        }
        return values;
    }

    @GetMapping("/{tableName}")
    public ResponseEntity<?> getModelData(@PathVariable String tableName) {
        try {
            return ResponseEntity.ok(service.getDataModel(tableName));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }
    }


}
