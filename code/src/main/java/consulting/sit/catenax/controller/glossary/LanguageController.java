package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.Language;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/language")
@Slf4j
public class LanguageController extends GenericControllerSingleId<Language, String> {
    @Autowired
    private GenericRepository<Language, String> repo;

    @Autowired
    private void setRepository(GenericRepository<Language, String> repo) {
        init(repo);
    }
}
