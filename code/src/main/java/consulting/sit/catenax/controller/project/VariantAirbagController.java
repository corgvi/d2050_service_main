package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.project.VariantAirbag;
import consulting.sit.catenax.model.project.VariantDetail;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.project.VariantAirbagService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project/variantAirbag")
@Slf4j
public class VariantAirbagController extends GenericControllerSingleId<VariantAirbag, Integer> {

    private final VariantAirbagService variantAirbagService;
    @Autowired
    private GenericRepository<VariantAirbag, Integer> repo;

    public VariantAirbagController(final VariantAirbagService variantAirbagService) {
        this.variantAirbagService = variantAirbagService;
    }

    @Autowired
    private void setRepository(GenericRepository<VariantAirbag, Integer> repo) {
        init(repo);
    }

    /**
     * Update fields of variantDetail
     *
     * @param variantAirbag, id
     * @return VariantDetail
     */
    @Operation(
            summary = "Update fields of variant."
            , description = "Update fields of project."
            , responses = {
    })
    @PatchMapping(value ="/{id}")
    public ResponseEntity<VariantAirbag> updateVariant(@RequestBody VariantAirbag variantAirbag , @PathVariable("id") Integer id) {
        return  ResponseEntity.ok(variantAirbagService.updateVariantAirBag(variantAirbag, id));
    }
}
