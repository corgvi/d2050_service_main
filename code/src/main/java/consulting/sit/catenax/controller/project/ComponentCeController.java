package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.project.ComponentCe;
import consulting.sit.catenax.model.project.VdaBalance;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.project.ComponentCeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project/componentCe")
@Slf4j
public class ComponentCeController extends GenericControllerSingleId<ComponentCe, Integer> {
    @Autowired
    private GenericRepository<ComponentCe, Integer> repo;

    public ComponentCeController(final ComponentCeService componentCeService) {
        this.componentCeService = componentCeService;
    }

    @Autowired
    private void setRepository(GenericRepository<ComponentCe, Integer> repo) {
        init(repo);
    }

    private final ComponentCeService componentCeService;
    @PatchMapping("/{id}")
    public ResponseEntity<ComponentCe> updateComponentCe(@RequestBody ComponentCe componentCe, @PathVariable("id") Integer id) {
        return  ResponseEntity.ok(componentCeService.updateComponentCe(componentCe, id));
    }

    @GetMapping("/componentMain/{componentId}")
    public ResponseEntity<ComponentCe> findByComponentId(@PathVariable Integer componentId) {
        return ResponseEntity.ok(componentCeService.findByComponentId(componentId));
    }

}
