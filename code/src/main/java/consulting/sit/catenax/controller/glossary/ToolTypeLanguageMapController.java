package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.ToolTypeLanguageMap;
import consulting.sit.catenax.model.glossary.ToolTypeLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/toolTypeLanguageMap")
@Slf4j
public class ToolTypeLanguageMapController extends GenericController<ToolTypeLanguageMap, ToolTypeLanguageMapPk> {
    @Autowired
    private GenericRepository<ToolTypeLanguageMap, ToolTypeLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<ToolTypeLanguageMap, ToolTypeLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<ToolTypeLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        ToolTypeLanguageMapPk pk = new ToolTypeLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<ToolTypeLanguageMap> update(@RequestBody ToolTypeLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        ToolTypeLanguageMapPk pk = new ToolTypeLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<ToolTypeLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        ToolTypeLanguageMapPk pk = new ToolTypeLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
