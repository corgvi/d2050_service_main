package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.FractionMain;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/fractionMain")
@Slf4j
public class FractionMainController extends GenericControllerSingleId<FractionMain, Integer> {
    @Autowired
    private GenericRepository<FractionMain, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<FractionMain, Integer> repo) {
        init(repo);
    }
}
