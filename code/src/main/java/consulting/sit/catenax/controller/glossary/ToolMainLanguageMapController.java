package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.FixingLocationLanguageMap;
import consulting.sit.catenax.model.glossary.ToolMainLanguageMap;
import consulting.sit.catenax.model.glossary.ToolMainLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.glossary.ToolMainLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/glossary/toolMainLanguageMap")
@Slf4j
public class ToolMainLanguageMapController extends GenericController<ToolMainLanguageMap, ToolMainLanguageMapPk> {
    private final ToolMainLanguageMapService toolMainLanguageMapService;

    @Autowired
    private GenericRepository<ToolMainLanguageMap, ToolMainLanguageMapPk> repo;

    public ToolMainLanguageMapController(final ToolMainLanguageMapService toolMainLanguageMapService) {
        this.toolMainLanguageMapService = toolMainLanguageMapService;
    }

    @Autowired
    private void setRepository(GenericRepository<ToolMainLanguageMap, ToolMainLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<ToolMainLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        ToolMainLanguageMapPk pk = new ToolMainLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<ToolMainLanguageMap> update(@RequestBody ToolMainLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        ToolMainLanguageMapPk pk = new ToolMainLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<ToolMainLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        ToolMainLanguageMapPk pk = new ToolMainLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{language}")
    public ResponseEntity<List<ToolMainLanguageMap>> findAllByLanguage(@PathVariable String language) {
        return ResponseEntity.ok(toolMainLanguageMapService.findAllByLanguage(language));
    }
}
