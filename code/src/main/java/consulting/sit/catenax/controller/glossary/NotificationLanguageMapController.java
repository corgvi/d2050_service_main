package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.NotificationLanguageMap;
import consulting.sit.catenax.model.glossary.NotificationLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/notificationLanguageMap")
@Slf4j
public class NotificationLanguageMapController extends GenericController<NotificationLanguageMap, NotificationLanguageMapPk> {
    @Autowired
    private GenericRepository<NotificationLanguageMap, NotificationLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<NotificationLanguageMap, NotificationLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<NotificationLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        NotificationLanguageMapPk pk = new NotificationLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<NotificationLanguageMap> update(@RequestBody NotificationLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        NotificationLanguageMapPk pk = new NotificationLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<NotificationLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        NotificationLanguageMapPk pk = new NotificationLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
