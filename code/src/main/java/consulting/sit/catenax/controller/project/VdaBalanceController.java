package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.MaterialMainLanguageMap;
import consulting.sit.catenax.model.glossary.MaterialMainLanguageMapPk;
import consulting.sit.catenax.model.project.VdaBalance;
import consulting.sit.catenax.model.project.VdaBalancePk;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.repository.project.VdaBalanceRepository;
import consulting.sit.catenax.service.project.VdaBalanceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/project/vdaBalance")
@Slf4j
public class VdaBalanceController extends GenericController<VdaBalance, VdaBalancePk> {

    @Autowired
    private VdaBalanceRepository repo;

    private final VdaBalanceService vdaBalanceService;

    public VdaBalanceController(final VdaBalanceService vdaBalanceService) {
        this.vdaBalanceService = vdaBalanceService;
    }

    @Autowired
    private void setRepository(VdaBalanceRepository repo) {
        init(repo);
    }

//    @GetMapping("/componentMain/{componentId}/materialGroup/{materialGroupId}")
//    public ResponseEntity<VdaBalance> getVdaBalances(@PathVariable Integer componentId, @PathVariable Integer materialGroupId){
//        VdaBalancePk pk = new VdaBalancePk(componentId, materialGroupId);
//    	return ResponseEntity.ok(repo.findById(pk).get());
//    }


//    @GetMapping("/componentMain/{componentId}")
//    public ResponseEntity<VdaBalance> findByComponentId(@PathVariable Integer componentId) {
//        return ResponseEntity.ok(vdaBalanceService.findByComponentId(componentId));
//    }
//
//    @PatchMapping("/{id}")
//    public ResponseEntity<VdaBalance> updateVdaBalance(@RequestBody VdaBalance vdaBalance, @PathVariable("id") Integer id) {
//        return  ResponseEntity.ok(vdaBalanceService.updateByFieldsOfVdaBalance(id,vdaBalance));
//    }

    @GetMapping("/{componentId}/{materialGroupId}")
    public ResponseEntity<VdaBalance> findById(@PathVariable Integer componentId, @PathVariable Integer materialGroupId) {
        VdaBalancePk pk = new VdaBalancePk(componentId, materialGroupId);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{componentId}/{materialGroupId}")
    public ResponseEntity<VdaBalance> update(@RequestBody VdaBalance entity, @PathVariable Integer componentId, @PathVariable Integer materialGroupId) {
        VdaBalancePk pk = new VdaBalancePk(componentId, materialGroupId);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{componentId}/{materialGroupId}")
    public ResponseEntity<VdaBalance> delete(@PathVariable Integer componentId, @PathVariable Integer materialGroupId) {
        VdaBalancePk pk = new VdaBalancePk(componentId, materialGroupId);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{componentId}")
    public ResponseEntity<List<VdaBalance>> findAllByLanguage(@PathVariable Integer componentId) {
        return ResponseEntity.ok(vdaBalanceService.findAllByComponentId(componentId));
    }

    @GetMapping("/component/{componentId}")
    public ResponseEntity<List<VdaBalance>> getVdaBalances(@PathVariable Integer componentId){
        return ResponseEntity.ok(vdaBalanceService.findByComponentId(componentId));
    }
}
