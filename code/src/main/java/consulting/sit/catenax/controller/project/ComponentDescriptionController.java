package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.project.ComponentDescription;
import consulting.sit.catenax.model.project.ComponentMarking;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.project.ComponentDescriptionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project/componentDescription")
@Slf4j
public class ComponentDescriptionController extends GenericControllerSingleId<ComponentDescription, Integer> {

    private final ComponentDescriptionService componentDescriptionService;

    @Autowired
    private GenericRepository<ComponentDescription, Integer> repo;

    public ComponentDescriptionController(final ComponentDescriptionService componentDescriptionService) {
        this.componentDescriptionService = componentDescriptionService;
    }

    @Autowired
    private void setRepository(GenericRepository<ComponentDescription, Integer> repo) {
        init(repo);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<ComponentDescription> updateComponentDescription(@RequestBody ComponentDescription componentDescription, @PathVariable("id") Integer id){
        return ResponseEntity.ok(componentDescriptionService.updateComponentDescription(componentDescription, id));
    }

    @GetMapping("/componentMain/{componentId}")
    public ResponseEntity<ComponentDescription> findByComponentId(@PathVariable Integer componentId) {
        return ResponseEntity.ok(componentDescriptionService.findByComponentId(componentId));
    }

}
