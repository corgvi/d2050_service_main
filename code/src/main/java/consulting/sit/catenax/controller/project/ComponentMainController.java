package consulting.sit.catenax.controller.project;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.dto.ComponentMainDTO;
import consulting.sit.catenax.model.project.ComponentMain;
import consulting.sit.catenax.model.project.VariantDetail;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.repository.project.ComponentMainRepository;
import consulting.sit.catenax.service.project.ComponentMainService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/project/componentMain")
@Slf4j
public class ComponentMainController extends GenericControllerSingleId<ComponentMain, Integer> {
    private final ComponentMainService service;

    @Autowired
    private GenericRepository<ComponentMain, Integer> repo;

    public ComponentMainController(final ComponentMainService service) {
        this.service = service;
    }

    @Autowired
    private void setRepository(ComponentMainRepository repo) {
        init(repo);
    }

    @GetMapping("/variant/{variantId}")
    public ResponseEntity<List<ComponentMain>> findByVariantId(@PathVariable int variantId) {
        return ResponseEntity.ok(service.findByVariantId(variantId));
    }

    @GetMapping("/variant/{variantId}/first")
    public ResponseEntity<List<ComponentMainDTO>> findAllComponentIdByVariantIdAndTopIsNull(@PathVariable int variantId) {
        return ResponseEntity.ok(service.findAllComponentIdByVariantIdAndTopIsNull(variantId));
    }

    @GetMapping("/children/{top}")
    public ResponseEntity<List<ComponentMainDTO>> findAllComponentIdByTop(@PathVariable long top) {
        return ResponseEntity.ok(service.findAllComponentIdByTop(top));
    }

    @GetMapping("/check/{componentId}/variant/{variantId}")
    public ResponseEntity<Boolean> checkExistByIdAndVariantId(@PathVariable int componentId, @PathVariable int variantId) {
        return ResponseEntity.ok(service.checkExistByIdAndVariantId(componentId, variantId));
    }

    /**
     * Update fields of ComponentMain
     *
     * @param componentMain, id
     * @return componentMain
     */
    @Operation(
            summary = "Update fields of component main."
            , description = "Update fields of component main."
            , responses = {
    })
    @PatchMapping(value ="/{id}")
    public ResponseEntity<ComponentMain> updateComponentMain(@RequestBody ComponentMain componentMain, @PathVariable("id") Integer id) {
        return  ResponseEntity.ok(service.updateByFieldsOfComponentMain(id, componentMain));
    }

    @GetMapping("/zbteil")
    public ResponseEntity<List<ComponentMain>> findAllByZbteilIsTrue() {
        return ResponseEntity.ok(service.findAllByZbTeilIsTrue());
    }
}
