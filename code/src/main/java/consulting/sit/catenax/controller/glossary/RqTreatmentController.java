package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.RqTreatment;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/rqTreatment")
@Slf4j
public class RqTreatmentController extends GenericControllerSingleId<RqTreatment, Integer> {
    @Autowired
    private GenericRepository<RqTreatment, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<RqTreatment, Integer> repo) {
        init(repo);
    }
}
