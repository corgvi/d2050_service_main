package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.ToolType;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/toolType")
@Slf4j
public class ToolTypeController extends GenericControllerSingleId<ToolType, Integer> {
    @Autowired
    private GenericRepository<ToolType, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<ToolType, Integer> repo) {
        init(repo);
    }
}
