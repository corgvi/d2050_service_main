package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.project.ComponentCe;
import consulting.sit.catenax.model.project.ComponentMarking;
import consulting.sit.catenax.model.project.ComponentReference;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.project.ComponentReferenceService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project/componentReference")
@Slf4j
public class ComponentReferenceController extends GenericControllerSingleId<ComponentReference, Integer> {

    private final ComponentReferenceService componentReferenceService;

    @Autowired
    private GenericRepository<ComponentReference, Integer> repo;

    public ComponentReferenceController(final ComponentReferenceService componentReferenceService) {
        this.componentReferenceService = componentReferenceService;
    }

    @Autowired
    private void setRepository(GenericRepository<ComponentReference, Integer> repo) {
        init(repo);
    }

    /**
     * Update fields of componentMarking
     *
     * @param componentReference, id
     * @return ComponentReference
     */
    @Operation(
            summary = "Update fields of ComponentReference."
            , description = "Update fields of ComponentReference."
            , responses = {
    })
    @PatchMapping(value ="/{id}")
    public ResponseEntity<ComponentReference> updateComponentReference(@RequestBody ComponentReference componentReference, @PathVariable("id") Integer id) {
        return  ResponseEntity.ok(componentReferenceService.updateByFieldsOfComponentReference(id, componentReference));
    }

    @GetMapping("/componentMain/{componentId}")
    public ResponseEntity<ComponentReference> findByComponentId(@PathVariable Integer componentId) {
        return ResponseEntity.ok(componentReferenceService.findByComponentId(componentId));
    }

}
