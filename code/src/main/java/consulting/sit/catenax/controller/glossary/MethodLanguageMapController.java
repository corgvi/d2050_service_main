package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.MethodLanguageMap;
import consulting.sit.catenax.model.glossary.MethodLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/methodLanguageMap")
@Slf4j
public class MethodLanguageMapController extends GenericController<MethodLanguageMap, MethodLanguageMapPk> {
    @Autowired
    private GenericRepository<MethodLanguageMap, MethodLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<MethodLanguageMap, MethodLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<MethodLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        MethodLanguageMapPk pk = new MethodLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<MethodLanguageMap> update(@RequestBody MethodLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        MethodLanguageMapPk pk = new MethodLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<MethodLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        MethodLanguageMapPk pk = new MethodLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
