package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.RgStrategy;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/rgStrategy")
@Slf4j
public class RgStrategyController extends GenericControllerSingleId<RgStrategy, Integer> {
    @Autowired
    private GenericRepository<RgStrategy, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<RgStrategy, Integer> repo) {
        init(repo);
    }
}
