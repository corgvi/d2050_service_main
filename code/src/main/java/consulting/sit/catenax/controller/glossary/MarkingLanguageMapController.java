package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.MarkingLanguageMap;
import consulting.sit.catenax.model.glossary.MarkingLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/markingLanguageMap")
@Slf4j
public class MarkingLanguageMapController extends GenericController<MarkingLanguageMap, MarkingLanguageMapPk> {
    @Autowired
    private GenericRepository<MarkingLanguageMap, MarkingLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<MarkingLanguageMap, MarkingLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<MarkingLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        MarkingLanguageMapPk pk = new MarkingLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<MarkingLanguageMap> update(@RequestBody MarkingLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        MarkingLanguageMapPk pk = new MarkingLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<MarkingLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        MarkingLanguageMapPk pk = new MarkingLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
