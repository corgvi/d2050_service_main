package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.RgMaterialGroupLanguageMap;
import consulting.sit.catenax.model.glossary.RgMaterialGroupLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/rgMaterialGroupLanguageMap")
@Slf4j
public class RgMaterialGroupLanguageMapController extends GenericController<RgMaterialGroupLanguageMap, RgMaterialGroupLanguageMapPk> {
    @Autowired
    private GenericRepository<RgMaterialGroupLanguageMap, RgMaterialGroupLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<RgMaterialGroupLanguageMap, RgMaterialGroupLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<RgMaterialGroupLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        RgMaterialGroupLanguageMapPk pk = new RgMaterialGroupLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<RgMaterialGroupLanguageMap> update(@RequestBody RgMaterialGroupLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        RgMaterialGroupLanguageMapPk pk = new RgMaterialGroupLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<RgMaterialGroupLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        RgMaterialGroupLanguageMapPk pk = new RgMaterialGroupLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
