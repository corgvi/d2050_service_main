package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.FixingDrop;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.repository.glossary.FixingDropRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/glossary/fixingDrop")
@Slf4j
public class FixingDropController extends GenericControllerSingleId<FixingDrop, Integer> {
    @Autowired
    private GenericRepository<FixingDrop, Integer> repo;

    public FixingDropController(final FixingDropRepository fixingDropRepository) {
        this.fixingDropRepository = fixingDropRepository;
    }

    @Autowired
    private void setRepository(GenericRepository<FixingDrop, Integer> repo) {
        init(repo);
    }

    private final FixingDropRepository fixingDropRepository;

    @GetMapping("/exist")
    public ResponseEntity<List<FixingDrop>> findByComponentId() {
        return ResponseEntity.ok(fixingDropRepository.findFixingDropWithoutFixingDropCode());
    }
}
