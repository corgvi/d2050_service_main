package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.ActivityAreaLanguageMap;
import consulting.sit.catenax.model.glossary.ActivityAreaLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.glossary.ActivityAreaLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/glossary/activityAreaLanguageMap")
@Slf4j
public class ActivityAreaLanguageMapController extends GenericController<ActivityAreaLanguageMap, ActivityAreaLanguageMapPk> {

    private final ActivityAreaLanguageMapService activityAreaLanguageMapService;

    @Autowired
    private GenericRepository<ActivityAreaLanguageMap, ActivityAreaLanguageMapPk> repo;

    public ActivityAreaLanguageMapController(final ActivityAreaLanguageMapService activityAreaLanguageMapService) {
        this.activityAreaLanguageMapService = activityAreaLanguageMapService;
    }

    @Autowired
    private void setRepository(GenericRepository<ActivityAreaLanguageMap, ActivityAreaLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<ActivityAreaLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        ActivityAreaLanguageMapPk pk = new ActivityAreaLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @GetMapping("/{language}")
    public ResponseEntity<List<ActivityAreaLanguageMap>> findAllByLanguage(@PathVariable String language) {
        return ResponseEntity.ok(activityAreaLanguageMapService.findAllActivityAreaLanguageMapByLanguage(language));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<ActivityAreaLanguageMap> update(@RequestBody ActivityAreaLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        ActivityAreaLanguageMapPk pk = new ActivityAreaLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<ActivityAreaLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        ActivityAreaLanguageMapPk pk = new ActivityAreaLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
