package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.RgMaterialClass;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/rgMaterialClass")
@Slf4j
public class RgMaterialClassController extends GenericControllerSingleId<RgMaterialClass, Integer> {
    @Autowired
    private GenericRepository<RgMaterialClass, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<RgMaterialClass, Integer> repo) {
        init(repo);
    }
}
