package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.UiLanguageMap;
import consulting.sit.catenax.model.glossary.UiLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/uILanguageMap")
@Slf4j
public class UILanguageMapController extends GenericController<UiLanguageMap, UiLanguageMapPk> {
    @Autowired
    private GenericRepository<UiLanguageMap, UiLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<UiLanguageMap, UiLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<UiLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        UiLanguageMapPk pk = new UiLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<UiLanguageMap> update(@RequestBody UiLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        UiLanguageMapPk pk = new UiLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<UiLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        UiLanguageMapPk pk = new UiLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
