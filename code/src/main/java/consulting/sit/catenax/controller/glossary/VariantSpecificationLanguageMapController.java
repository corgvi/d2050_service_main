package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.VariantSpecificationLanguageMap;
import consulting.sit.catenax.model.glossary.VariantSpecificationLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/variantSpecificationLanguageMap")
@Slf4j
public class VariantSpecificationLanguageMapController extends GenericController<VariantSpecificationLanguageMap, VariantSpecificationLanguageMapPk> {
    @Autowired
    private GenericRepository<VariantSpecificationLanguageMap, VariantSpecificationLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<VariantSpecificationLanguageMap, VariantSpecificationLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<VariantSpecificationLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        VariantSpecificationLanguageMapPk pk = new VariantSpecificationLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<VariantSpecificationLanguageMap> update(@RequestBody VariantSpecificationLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        VariantSpecificationLanguageMapPk pk = new VariantSpecificationLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<VariantSpecificationLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        VariantSpecificationLanguageMapPk pk = new VariantSpecificationLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
