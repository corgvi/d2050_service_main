package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.FractionWaste;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/fractionWaste")
@Slf4j
public class FractionWasteController extends GenericControllerSingleId<FractionWaste, Integer> {
    @Autowired
    private GenericRepository<FractionWaste, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<FractionWaste, Integer> repo) {
        init(repo);
    }
}
