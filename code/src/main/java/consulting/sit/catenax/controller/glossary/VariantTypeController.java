package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.VariantType;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/variantType")
@Slf4j
public class VariantTypeController extends GenericControllerSingleId<VariantType, Integer> {
    @Autowired
    private GenericRepository<VariantType, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<VariantType, Integer> repo) {
        init(repo);
    }
}
