package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.FixingDropCodeLanguageMap;
import consulting.sit.catenax.model.glossary.FixingDropCodeLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.glossary.FixingDropCodeLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/glossary/fixingDropCodeLanguageMap")
@Slf4j
public class FixingDropCodeLanguageMapController extends GenericController<FixingDropCodeLanguageMap, FixingDropCodeLanguageMapPk> {
    private final FixingDropCodeLanguageMapService fixingDropCodeLanguageMapService;

    @Autowired
    private GenericRepository<FixingDropCodeLanguageMap, FixingDropCodeLanguageMapPk> repo;

    public FixingDropCodeLanguageMapController(final FixingDropCodeLanguageMapService fixingDropCodeLanguageMapService) {
        this.fixingDropCodeLanguageMapService = fixingDropCodeLanguageMapService;
    }

    @Autowired
    private void setRepository(GenericRepository<FixingDropCodeLanguageMap, FixingDropCodeLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<FixingDropCodeLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        FixingDropCodeLanguageMapPk pk = new FixingDropCodeLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<FixingDropCodeLanguageMap> update(@RequestBody FixingDropCodeLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        FixingDropCodeLanguageMapPk pk = new FixingDropCodeLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<FixingDropCodeLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        FixingDropCodeLanguageMapPk pk = new FixingDropCodeLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/fixingDrop/{id}/{language}")
    public ResponseEntity<FixingDropCodeLanguageMap> findByFixingDropIdAndLanguage(@PathVariable int id, @PathVariable String language) {
        return ResponseEntity.ok(fixingDropCodeLanguageMapService.findByLanguageAndFixingDropId(id, language));
    }
}
