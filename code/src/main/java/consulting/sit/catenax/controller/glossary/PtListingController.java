package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.PtListing;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.glossary.PtListingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/glossary/ptListing")
@Slf4j
public class PtListingController extends GenericControllerSingleId<PtListing, Integer> {
    @Autowired
    private GenericRepository<PtListing, Integer> repo;

    public PtListingController(final PtListingService ptListingService) {
        this.ptListingService = ptListingService;
    }

    @Autowired
    private void setRepository(GenericRepository<PtListing, Integer> repo) {
        init(repo);
    }

    private final PtListingService ptListingService;
    @GetMapping("/withoutPtMaterialMap")
    public ResponseEntity<List<PtListing>> findPtListingWithoutPtMaterialMap() {
        return ResponseEntity.ok(ptListingService.findPtListingWithoutPtMaterialMap());
    }

    @GetMapping("/check/{id}")
    public ResponseEntity<Boolean> exitsById(@PathVariable int id) {
        return ResponseEntity.ok(repo.existsById(id));
    }
}
