package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.Position;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/position")
@Slf4j
public class PositionController extends GenericControllerSingleId<Position, Integer> {
    @Autowired
    private GenericRepository<Position, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<Position, Integer> repo) {
        init(repo);
    }
}
