package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.MaterialFamilyValue;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/materialFamilyValue")
@Slf4j
public class MaterialFamilyValueController extends GenericControllerSingleId<MaterialFamilyValue, Integer> {
    @Autowired
    private GenericRepository<MaterialFamilyValue, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<MaterialFamilyValue, Integer> repo) {
        init(repo);
    }
}
