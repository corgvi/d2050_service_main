package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.RgSeparability;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/rgSeparability")
@Slf4j
public class RgSeparabilityController extends GenericControllerSingleId<RgSeparability, Integer> {
    @Autowired
    private GenericRepository<RgSeparability, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<RgSeparability, Integer> repo) {
        init(repo);
    }
}
