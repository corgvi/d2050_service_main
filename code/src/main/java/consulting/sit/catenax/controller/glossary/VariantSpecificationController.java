package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.VariantSpecification;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/variantSpecification")
@Slf4j
public class VariantSpecificationController extends GenericControllerSingleId<VariantSpecification, Integer> {
    @Autowired
    private GenericRepository<VariantSpecification, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<VariantSpecification, Integer> repo) {
        init(repo);
    }
}
