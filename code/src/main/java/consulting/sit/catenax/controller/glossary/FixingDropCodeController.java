package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.FixingDropCode;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.repository.glossary.FixingDropCodeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/fixingDropCode")
@Slf4j
public class FixingDropCodeController extends GenericControllerSingleId<FixingDropCode, Integer> {
    @Autowired
    private GenericRepository<FixingDropCode, Integer> repo;

    public FixingDropCodeController(final FixingDropCodeRepository fixingDropCodeRepository) {
        this.fixingDropCodeRepository = fixingDropCodeRepository;
    }

    @Autowired
    private void setRepository(GenericRepository<FixingDropCode, Integer> repo) {
        init(repo);
    }

    private final FixingDropCodeRepository fixingDropCodeRepository;

    @GetMapping("/check/{fixingDropId}")
    public ResponseEntity<Boolean> findByComponentId(@PathVariable Integer fixingDropId) {
        return ResponseEntity.ok(fixingDropCodeRepository.existsByFixingDropId(fixingDropId));
    }
}
