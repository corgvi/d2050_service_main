package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.Ui;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/ui")
@Slf4j
public class UiController extends GenericControllerSingleId<Ui, Integer> {
    @Autowired
    private GenericRepository<Ui, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<Ui, Integer> repo) {
        init(repo);
    }
}
