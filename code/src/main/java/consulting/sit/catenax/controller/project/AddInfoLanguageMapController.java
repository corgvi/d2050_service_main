package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.project.AddInfoLanguageMap;
import consulting.sit.catenax.model.project.AddInfoLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/project/addInfoLanguageMap")
@Slf4j
public class AddInfoLanguageMapController extends GenericController<AddInfoLanguageMap, AddInfoLanguageMapPk> {
    @Autowired
    private GenericRepository<AddInfoLanguageMap, AddInfoLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<AddInfoLanguageMap, AddInfoLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<AddInfoLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        AddInfoLanguageMapPk pk = new AddInfoLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<AddInfoLanguageMap> update(@RequestBody AddInfoLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        AddInfoLanguageMapPk pk = new AddInfoLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<AddInfoLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        AddInfoLanguageMapPk pk = new AddInfoLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
