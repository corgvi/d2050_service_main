package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.project.AddInfo;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project/addInfo")
@Slf4j
public class AddInfoController extends GenericControllerSingleId<AddInfo, Integer> {
    @Autowired
    private GenericRepository<AddInfo, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<AddInfo, Integer> repo) {
        init(repo);
    }
}
