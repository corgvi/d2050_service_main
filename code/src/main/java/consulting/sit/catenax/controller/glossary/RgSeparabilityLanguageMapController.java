package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.RgSeparabilityLanguageMap;
import consulting.sit.catenax.model.glossary.RgSeparabilityLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/rgSeparabilityLanguageMap")
@Slf4j
public class RgSeparabilityLanguageMapController extends GenericController<RgSeparabilityLanguageMap, RgSeparabilityLanguageMapPk> {
    @Autowired
    private GenericRepository<RgSeparabilityLanguageMap, RgSeparabilityLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<RgSeparabilityLanguageMap, RgSeparabilityLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<RgSeparabilityLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        RgSeparabilityLanguageMapPk pk = new RgSeparabilityLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<RgSeparabilityLanguageMap> update(@RequestBody RgSeparabilityLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        RgSeparabilityLanguageMapPk pk = new RgSeparabilityLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<RgSeparabilityLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        RgSeparabilityLanguageMapPk pk = new RgSeparabilityLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
