package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.Compound;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/compound")
@Slf4j
public class CompoundController extends GenericControllerSingleId<Compound, String> {
    @Autowired
    private GenericRepository<Compound, String> repo;

    @Autowired
    private void setRepository(GenericRepository<Compound, String> repo) {
        init(repo);
    }
}
