package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.ActivityAreaLanguageMap;
import consulting.sit.catenax.model.glossary.FractionMainLanguageMap;
import consulting.sit.catenax.model.glossary.FractionMainLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.glossary.FractionMainLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/glossary/fractionMainLanguageMap")
@Slf4j
public class FractionMainLanguageMapController extends GenericController<FractionMainLanguageMap, FractionMainLanguageMapPk> {

    private final FractionMainLanguageMapService fractionMainLanguageMapService;
    @Autowired
    private GenericRepository<FractionMainLanguageMap, FractionMainLanguageMapPk> repo;

    public FractionMainLanguageMapController(final FractionMainLanguageMapService fractionMainLanguageMapService) {
        this.fractionMainLanguageMapService = fractionMainLanguageMapService;
    }

    @Autowired
    private void setRepository(GenericRepository<FractionMainLanguageMap, FractionMainLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<FractionMainLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        FractionMainLanguageMapPk pk = new FractionMainLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<FractionMainLanguageMap> update(@RequestBody FractionMainLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        FractionMainLanguageMapPk pk = new FractionMainLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<FractionMainLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        FractionMainLanguageMapPk pk = new FractionMainLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{language}")
    public ResponseEntity<List<FractionMainLanguageMap>> findAllByLanguage(@PathVariable String language) {
        return ResponseEntity.ok(fractionMainLanguageMapService.findAllByLanguage(language));
    }

}
