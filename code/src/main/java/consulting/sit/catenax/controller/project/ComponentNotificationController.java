package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.project.ComponentNotification;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project/componentNotification")
@Slf4j
public class ComponentNotificationController extends GenericControllerSingleId<ComponentNotification, Integer> {
    @Autowired
    private GenericRepository<ComponentNotification, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<ComponentNotification, Integer> repo) {
        init(repo);
    }
}
