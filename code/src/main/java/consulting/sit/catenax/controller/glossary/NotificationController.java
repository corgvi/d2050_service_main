package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.Notification;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/notification")
@Slf4j
public class NotificationController extends GenericControllerSingleId<Notification, Integer> {
    @Autowired
    private GenericRepository<Notification, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<Notification, Integer> repo) {
        init(repo);
    }
}
