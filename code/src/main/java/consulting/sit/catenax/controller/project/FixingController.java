package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.project.ComponentReference;
import consulting.sit.catenax.model.project.Fixing;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.project.FixingService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project/fixing")
@Slf4j
public class FixingController extends GenericControllerSingleId<Fixing, Integer> {

   private final FixingService fixingService;

    @Autowired
    private GenericRepository<Fixing, Integer> repo;

    public FixingController(final FixingService fixingService) {
        this.fixingService = fixingService;
    }

    @Autowired
    private void setRepository(GenericRepository<Fixing, Integer> repo) {
        init(repo);
    }

    /**
     * Update fields of Fixing
     *
     * @param fixing, id
     * @return fixing
     */
    @Operation(
            summary = "Update fields of Fixing."
            , description = "Update fields of Fixing."
            , responses = {
    })
    @PatchMapping(value ="/{id}")
    public ResponseEntity<Fixing> updateFixing(@RequestBody Fixing fixing, @PathVariable("id") Integer id) {
        return  ResponseEntity.ok(fixingService.updateByFieldsOfFixing(id, fixing));
    }

}
