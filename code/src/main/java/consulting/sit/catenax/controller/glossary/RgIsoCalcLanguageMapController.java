package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.RgIsoCalcLanguageMap;
import consulting.sit.catenax.model.glossary.RgIsoCalcLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/rgIsoCalcLanguageMap")
@Slf4j
public class RgIsoCalcLanguageMapController extends GenericController<RgIsoCalcLanguageMap, RgIsoCalcLanguageMapPk> {
    @Autowired
    private GenericRepository<RgIsoCalcLanguageMap, RgIsoCalcLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<RgIsoCalcLanguageMap, RgIsoCalcLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<RgIsoCalcLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        RgIsoCalcLanguageMapPk pk = new RgIsoCalcLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<RgIsoCalcLanguageMap> update(@RequestBody RgIsoCalcLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        RgIsoCalcLanguageMapPk pk = new RgIsoCalcLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<RgIsoCalcLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        RgIsoCalcLanguageMapPk pk = new RgIsoCalcLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
