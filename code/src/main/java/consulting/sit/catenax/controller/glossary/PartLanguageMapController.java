package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.ActivityAreaLanguageMap;
import consulting.sit.catenax.model.glossary.PartLanguageMap;
import consulting.sit.catenax.model.glossary.PartLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.glossary.PartLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/glossary/partLanguageMap")
@Slf4j
public class PartLanguageMapController extends GenericController<PartLanguageMap, PartLanguageMapPk> {

    private final PartLanguageMapService partLanguageMapService;
    @Autowired
    private GenericRepository<PartLanguageMap, PartLanguageMapPk> repo;

    public PartLanguageMapController(final PartLanguageMapService partLanguageMapService) {
        this.partLanguageMapService = partLanguageMapService;
    }

    @Autowired
    private void setRepository(GenericRepository<PartLanguageMap, PartLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<PartLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        PartLanguageMapPk pk = new PartLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<PartLanguageMap> update(@RequestBody PartLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        PartLanguageMapPk pk = new PartLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<PartLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        PartLanguageMapPk pk = new PartLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
    @GetMapping("/{language}")
    public ResponseEntity<List<PartLanguageMap>> findAllByLanguage(@PathVariable String language) {
        return ResponseEntity.ok(partLanguageMapService.findAllByLanguage(language));
    }
}
