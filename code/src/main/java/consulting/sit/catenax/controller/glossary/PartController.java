package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.Part;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/part")
@Slf4j
public class PartController extends GenericControllerSingleId<Part, Integer> {
    @Autowired
    private GenericRepository<Part, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<Part, Integer> repo) {
        init(repo);
    }
}
