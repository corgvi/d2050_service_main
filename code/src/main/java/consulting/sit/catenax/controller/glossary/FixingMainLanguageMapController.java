package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.FixingMainLanguageMap;
import consulting.sit.catenax.model.glossary.FixingMainLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.glossary.FixingMainLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/glossary/fixingMainLanguageMap")
@Slf4j
public class FixingMainLanguageMapController extends GenericController<FixingMainLanguageMap, FixingMainLanguageMapPk> {
    private final FixingMainLanguageMapService fixingMainLanguageMapService;

    @Autowired
    private GenericRepository<FixingMainLanguageMap, FixingMainLanguageMapPk> repo;

    public FixingMainLanguageMapController(final FixingMainLanguageMapService fixingMainLanguageMapService) {
        this.fixingMainLanguageMapService = fixingMainLanguageMapService;
    }

    @Autowired
    private void setRepository(GenericRepository<FixingMainLanguageMap, FixingMainLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<FixingMainLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        FixingMainLanguageMapPk pk = new FixingMainLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<FixingMainLanguageMap> update(@RequestBody FixingMainLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        FixingMainLanguageMapPk pk = new FixingMainLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<FixingMainLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        FixingMainLanguageMapPk pk = new FixingMainLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{language}")
    public ResponseEntity<List<FixingMainLanguageMap>> findAllByLanguage(@PathVariable String language) {
        return ResponseEntity.ok(fixingMainLanguageMapService.findAllByLanguage(language));
    }
}
