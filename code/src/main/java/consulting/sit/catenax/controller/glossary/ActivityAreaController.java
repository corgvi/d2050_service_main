package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.ActivityArea;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/activityArea")
@Slf4j
public class ActivityAreaController extends GenericControllerSingleId<ActivityArea, Integer> {
    @Autowired
    private GenericRepository<ActivityArea, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<ActivityArea, Integer> repo) {
        init(repo);
    }
}
