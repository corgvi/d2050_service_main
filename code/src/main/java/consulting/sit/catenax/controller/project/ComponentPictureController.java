package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.project.ComponentPicture;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project/componentPicture")
@Slf4j
public class ComponentPictureController extends GenericControllerSingleId<ComponentPicture, Integer> {
    @Autowired
    private GenericRepository<ComponentPicture, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<ComponentPicture, Integer> repo) {
        init(repo);
    }
}
