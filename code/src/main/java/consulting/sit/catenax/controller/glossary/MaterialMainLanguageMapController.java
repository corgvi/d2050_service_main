package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.ActivityAreaLanguageMap;
import consulting.sit.catenax.model.glossary.MaterialMainLanguageMap;
import consulting.sit.catenax.model.glossary.MaterialMainLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.glossary.MaterialMainLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/glossary/materialMainLanguageMap")
@Slf4j
public class MaterialMainLanguageMapController extends GenericController<MaterialMainLanguageMap, MaterialMainLanguageMapPk> {

    private final MaterialMainLanguageMapService materialMainLanguageMapService;

    @Autowired
    private GenericRepository<MaterialMainLanguageMap, MaterialMainLanguageMapPk> repo;

    public MaterialMainLanguageMapController(final MaterialMainLanguageMapService materialMainLanguageMapService) {
        this.materialMainLanguageMapService = materialMainLanguageMapService;
    }

    @Autowired
    private void setRepository(GenericRepository<MaterialMainLanguageMap, MaterialMainLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<MaterialMainLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        MaterialMainLanguageMapPk pk = new MaterialMainLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<MaterialMainLanguageMap> update(@RequestBody MaterialMainLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        MaterialMainLanguageMapPk pk = new MaterialMainLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<MaterialMainLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        MaterialMainLanguageMapPk pk = new MaterialMainLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{language}")
    public ResponseEntity<List<MaterialMainLanguageMap>> findAllByLanguage(@PathVariable String language) {
        return ResponseEntity.ok(materialMainLanguageMapService.findAllByLanguage(language));
    }
}
