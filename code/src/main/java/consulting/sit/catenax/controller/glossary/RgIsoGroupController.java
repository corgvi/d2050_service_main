package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.RgIsoGroup;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.glossary.RgIsoGroupService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/glossary/rgIsoGroup")
@Slf4j
public class RgIsoGroupController extends GenericControllerSingleId<RgIsoGroup, Integer> {
    @Autowired
    private GenericRepository<RgIsoGroup, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<RgIsoGroup, Integer> repo) {
        init(repo);
    }

    private final RgIsoGroupService rgIsoGroupService;

    public RgIsoGroupController(RgIsoGroupService rgIsoGroupService) {
        this.rgIsoGroupService = rgIsoGroupService;
    }

    @GetMapping("/withoutRgIsoGroupValue")
    public ResponseEntity<List<RgIsoGroup>> findRgIsoGroupWithoutRgIsoGroupValue () {
        return ResponseEntity.ok(rgIsoGroupService.findRgIsoGroupWithoutRgIsoGroupValue());
    }

    @GetMapping("/check/{id}")
    public ResponseEntity<Boolean> exitsById(@PathVariable int id) {
        return ResponseEntity.ok(repo.existsById(id));
    }
}
