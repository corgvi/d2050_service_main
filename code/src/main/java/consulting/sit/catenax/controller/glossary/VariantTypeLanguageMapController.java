package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.VariantTypeLanguageMap;
import consulting.sit.catenax.model.glossary.VariantTypeLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/variantTypeLanguageMap")
@Slf4j
public class VariantTypeLanguageMapController extends GenericController<VariantTypeLanguageMap, VariantTypeLanguageMapPk> {
    @Autowired
    private GenericRepository<VariantTypeLanguageMap, VariantTypeLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<VariantTypeLanguageMap, VariantTypeLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<VariantTypeLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        VariantTypeLanguageMapPk pk = new VariantTypeLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<VariantTypeLanguageMap> update(@RequestBody VariantTypeLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        VariantTypeLanguageMapPk pk = new VariantTypeLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<VariantTypeLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        VariantTypeLanguageMapPk pk = new VariantTypeLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
