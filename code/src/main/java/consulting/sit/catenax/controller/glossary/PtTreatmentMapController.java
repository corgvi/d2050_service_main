package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.PtTreatmentMap;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/ptTreatmentMap")
@Slf4j
public class PtTreatmentMapController extends GenericControllerSingleId<PtTreatmentMap, Integer> {
    @Autowired
    private GenericRepository<PtTreatmentMap, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<PtTreatmentMap, Integer> repo) {
        init(repo);
    }
}
