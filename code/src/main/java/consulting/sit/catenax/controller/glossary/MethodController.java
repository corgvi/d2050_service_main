package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.Method;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/method")
@Slf4j
public class MethodController extends GenericControllerSingleId<Method, Integer> {
    @Autowired
    private GenericRepository<Method, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<Method, Integer> repo) {
        init(repo);
    }
}
