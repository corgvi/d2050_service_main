package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.Document;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/document")
@Slf4j
public class DocumentController extends GenericControllerSingleId<Document, Integer> {
    @Autowired
    private GenericRepository<Document, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<Document, Integer> repo) {
        init(repo);
    }
}
