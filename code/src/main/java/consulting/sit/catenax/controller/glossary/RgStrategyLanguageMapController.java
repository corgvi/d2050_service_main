package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.RgStrategyLanguageMap;
import consulting.sit.catenax.model.glossary.RgStrategyLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/rgStrategyLanguageMap")
@Slf4j
public class RgStrategyLanguageMapController extends GenericController<RgStrategyLanguageMap, RgStrategyLanguageMapPk> {
    @Autowired
    private GenericRepository<RgStrategyLanguageMap, RgStrategyLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<RgStrategyLanguageMap, RgStrategyLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<RgStrategyLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        RgStrategyLanguageMapPk pk = new RgStrategyLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<RgStrategyLanguageMap> update(@RequestBody RgStrategyLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        RgStrategyLanguageMapPk pk = new RgStrategyLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<RgStrategyLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        RgStrategyLanguageMapPk pk = new RgStrategyLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
