package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.project.Variant;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.repository.project.VariantRepository;
import consulting.sit.catenax.service.project.VariantService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/project/variant")
@Slf4j
public class VariantController extends GenericControllerSingleId<Variant, Integer> {

    private final VariantService service;

    @Autowired
    private GenericRepository<Variant, Integer> repo;

    public VariantController(final VariantService service) {
        this.service = service;
    }

    @Autowired
    private void setRepository(VariantRepository repo) {
        init(repo);
    }

    @GetMapping("/project/{projectId}")
    public ResponseEntity<List<Variant>> findByProjectId(@PathVariable Integer projectId) {
        return ResponseEntity.ok(service.findByProjectId(projectId));
    }

    /**
     * Update fields of variant
     *
     * @param variant, id
     * @return Variant
     */
    @Operation(
            summary = "Update fields of variant."
            , description = "Update fields of variant."
            , responses = {
    })
    @PatchMapping("/{id}")
    public ResponseEntity<Variant> updateVariant(@RequestBody Variant variant, @PathVariable("id") Integer id) {
        return  ResponseEntity.ok(service.updateVariant(variant, id));
    }
}
