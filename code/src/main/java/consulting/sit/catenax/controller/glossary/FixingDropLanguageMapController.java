package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.FixingDropCodeLanguageMap;
import consulting.sit.catenax.model.glossary.FixingDropLanguageMap;
import consulting.sit.catenax.model.glossary.FixingDropLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.glossary.FixingDropLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/glossary/fixingDropLanguageMap")
@Slf4j
public class FixingDropLanguageMapController extends GenericController<FixingDropLanguageMap, FixingDropLanguageMapPk> {
    private final FixingDropLanguageMapService fixingDropLanguageMapService;
    @Autowired
    private GenericRepository<FixingDropLanguageMap, FixingDropLanguageMapPk> repo;

    public FixingDropLanguageMapController(final FixingDropLanguageMapService fixingDropLanguageMapService) {
        this.fixingDropLanguageMapService = fixingDropLanguageMapService;
    }

    @Autowired
    private void setRepository(GenericRepository<FixingDropLanguageMap, FixingDropLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<FixingDropLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        FixingDropLanguageMapPk pk = new FixingDropLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<FixingDropLanguageMap> update(@RequestBody FixingDropLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        FixingDropLanguageMapPk pk = new FixingDropLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<FixingDropLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        FixingDropLanguageMapPk pk = new FixingDropLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{language}")
    public ResponseEntity<List<FixingDropLanguageMap>> findAllByLanguage(@PathVariable String language) {
        return ResponseEntity.ok(fixingDropLanguageMapService.findAllByLanguage(language));
    }
}
