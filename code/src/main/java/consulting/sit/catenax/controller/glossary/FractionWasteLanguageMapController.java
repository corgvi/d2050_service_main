package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.FractionWasteLanguageMap;
import consulting.sit.catenax.model.glossary.FractionWasteLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/fractionWasteLanguageMap")
@Slf4j
public class FractionWasteLanguageMapController extends GenericController<FractionWasteLanguageMap, FractionWasteLanguageMapPk> {
    @Autowired
    private GenericRepository<FractionWasteLanguageMap, FractionWasteLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<FractionWasteLanguageMap, FractionWasteLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<FractionWasteLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        FractionWasteLanguageMapPk pk = new FractionWasteLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<FractionWasteLanguageMap> update(@RequestBody FractionWasteLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        FractionWasteLanguageMapPk pk = new FractionWasteLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<FractionWasteLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        FractionWasteLanguageMapPk pk = new FractionWasteLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }

}
