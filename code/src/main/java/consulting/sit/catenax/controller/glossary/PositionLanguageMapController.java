package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.PositionLanguageMap;
import consulting.sit.catenax.model.glossary.PositionLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/positionLanguageMap")
@Slf4j
public class PositionLanguageMapController extends GenericController<PositionLanguageMap, PositionLanguageMapPk> {
    @Autowired
    private GenericRepository<PositionLanguageMap, PositionLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<PositionLanguageMap, PositionLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<PositionLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        PositionLanguageMapPk pk = new PositionLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<PositionLanguageMap> update(@RequestBody PositionLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        PositionLanguageMapPk pk = new PositionLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<PositionLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        PositionLanguageMapPk pk = new PositionLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
