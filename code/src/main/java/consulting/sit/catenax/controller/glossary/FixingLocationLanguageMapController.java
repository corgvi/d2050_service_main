package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.FixingLocationLanguageMap;
import consulting.sit.catenax.model.glossary.FixingLocationLanguageMapPk;
import consulting.sit.catenax.model.glossary.FixingMainLanguageMap;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.glossary.FixingLocationLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/glossary/fixingLocationLanguageMap")
@Slf4j
public class FixingLocationLanguageMapController extends GenericController<FixingLocationLanguageMap, FixingLocationLanguageMapPk> {
    private final FixingLocationLanguageMapService fixingLocationLanguageMapService;
    @Autowired
    private GenericRepository<FixingLocationLanguageMap, FixingLocationLanguageMapPk> repo;

    public FixingLocationLanguageMapController(final FixingLocationLanguageMapService fixingLocationLanguageMapService) {
        this.fixingLocationLanguageMapService = fixingLocationLanguageMapService;
    }

    @Autowired
    private void setRepository(GenericRepository<FixingLocationLanguageMap, FixingLocationLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<FixingLocationLanguageMap> findById(@PathVariable String id, @PathVariable String language) {
        FixingLocationLanguageMapPk pk = new FixingLocationLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<FixingLocationLanguageMap> update(@RequestBody FixingLocationLanguageMap entity, @PathVariable String id, @PathVariable String language) {
        FixingLocationLanguageMapPk pk = new FixingLocationLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<FixingLocationLanguageMap> delete(@PathVariable String id, @PathVariable String language) {
        FixingLocationLanguageMapPk pk = new FixingLocationLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{language}")
    public ResponseEntity<List<FixingLocationLanguageMap>> findAllByLanguage(@PathVariable String language) {
        return ResponseEntity.ok(fixingLocationLanguageMapService.findAllByLanguage(language));
    }
}
