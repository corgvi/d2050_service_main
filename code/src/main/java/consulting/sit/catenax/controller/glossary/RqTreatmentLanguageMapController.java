package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.RqTreatmentLanguageMap;
import consulting.sit.catenax.model.glossary.RqTreatmentLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/rqTreatmentLanguageMap")
@Slf4j
public class RqTreatmentLanguageMapController extends GenericController<RqTreatmentLanguageMap, RqTreatmentLanguageMapPk> {
    @Autowired
    private GenericRepository<RqTreatmentLanguageMap, RqTreatmentLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<RqTreatmentLanguageMap, RqTreatmentLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<RqTreatmentLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        RqTreatmentLanguageMapPk pk = new RqTreatmentLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<RqTreatmentLanguageMap> update(@RequestBody RqTreatmentLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        RqTreatmentLanguageMapPk pk = new RqTreatmentLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<RqTreatmentLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        RqTreatmentLanguageMapPk pk = new RqTreatmentLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
