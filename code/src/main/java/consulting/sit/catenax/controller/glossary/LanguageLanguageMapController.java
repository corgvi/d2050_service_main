package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.LanguageLanguageMap;
import consulting.sit.catenax.model.glossary.LanguageLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/languageLanguageMap")
@Slf4j
public class LanguageLanguageMapController extends GenericController<LanguageLanguageMap, LanguageLanguageMapPk> {
    @Autowired
    private GenericRepository<LanguageLanguageMap, LanguageLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<LanguageLanguageMap, LanguageLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<LanguageLanguageMap> findById(@PathVariable String id, @PathVariable String language) {
        LanguageLanguageMapPk pk = new LanguageLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<LanguageLanguageMap> update(@RequestBody LanguageLanguageMap entity, @PathVariable String id, @PathVariable String language) {
        LanguageLanguageMapPk pk = new LanguageLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<LanguageLanguageMap> delete(@PathVariable String id, @PathVariable String language) {
        LanguageLanguageMapPk pk = new LanguageLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }

}
