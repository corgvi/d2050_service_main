package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.project.ComponentComp;
import consulting.sit.catenax.model.project.ComponentMain;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.project.ComponentCompService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/project/componentComp")
@Slf4j
public class ComponentCompController extends GenericControllerSingleId<ComponentComp, Integer> {

    private final ComponentCompService componentCompService;
    @Autowired
    private GenericRepository<ComponentComp, Integer> repo;

    public ComponentCompController(final ComponentCompService componentCompService) {
        this.componentCompService = componentCompService;
    }

    @Autowired
    private void setRepository(GenericRepository<ComponentComp, Integer> repo) {
        init(repo);
    }

    @GetMapping("/componentMain/{componentMainId}")
    public ResponseEntity<List<ComponentComp>> findByComponentMainId(@PathVariable int componentMainId) {
        return ResponseEntity.ok(componentCompService.findByComponentMainId(componentMainId));
    }

    /**
     * Update fields of ComponentComp
     *
     * @param componentComp, id
     * @return componentComp
     */
    @Operation(
            summary = "Update fields of component comp."
            , description = "Update fields of component comp."
            , responses = {
    })
    @PatchMapping(value ="/{id}")
    public ResponseEntity<ComponentComp> updateComponentComp(@RequestBody ComponentComp componentComp, @PathVariable("id") Integer id) {
        return  ResponseEntity.ok(componentCompService.updateByFieldsOfComponentComp(id, componentComp));
    }
}
