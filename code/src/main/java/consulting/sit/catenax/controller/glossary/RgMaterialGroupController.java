package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.RgMaterialGroup;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/rgMaterialGroup")
@Slf4j
public class RgMaterialGroupController extends GenericControllerSingleId<RgMaterialGroup, Integer> {
    @Autowired
    private GenericRepository<RgMaterialGroup, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<RgMaterialGroup, Integer> repo) {
        init(repo);
    }
}
