package consulting.sit.catenax.controller.project;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

/**
 * Image controller for managing upload and download image
 */
@RestController
@RequestMapping("/images")
@Slf4j
public class ImageController {
    @Value("${images.folder}")
    private String imagesFolder;

    @PostMapping("/upload")
    public ResponseEntity<String> uploadImage(@RequestBody MultipartFile file) {
        log.info("Upload image");
        // Get original file name
        String originalFilename = file.getOriginalFilename();
        // Get name extension
        String extension = originalFilename.substring(originalFilename.lastIndexOf("."));
        // generate url for image
        String relativeFileName = UUID.randomUUID().toString() + extension;
        Path filePath = Paths.get(imagesFolder, relativeFileName);
        try {
            Files.write(filePath, file.getBytes());
            return ResponseEntity.status(HttpStatus.OK).body(relativeFileName);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/download")
    public ResponseEntity<Resource> downloadImage(@RequestParam(required = true) String url) throws IOException {
        // Logic to retrieve the image file based on relative URL
        // For example, if images are stored in a local directory:
        String filePath = imagesFolder + "/" + url;
        String fileFormat = url.substring(url.lastIndexOf(".") + 1);

        // Set the Content-Type and Content-Disposition headers based on file format
        HttpHeaders headers = new HttpHeaders();
        if ("jpg".equalsIgnoreCase(fileFormat) || "jpeg".equalsIgnoreCase(fileFormat)) {
            headers.add(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE);
        } else if ("png".equalsIgnoreCase(fileFormat)) {
            headers.add(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE);
        }
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + url);
        Resource resource = new FileSystemResource(filePath);
        // Return the image file as a response with appropriate headers
        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(resource.contentLength())
                .body(resource);
    }
}