package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.MarkIdent;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/markIdent")
@Slf4j
public class MarkIdentController extends GenericControllerSingleId<MarkIdent, Integer> {
    @Autowired
    private GenericRepository<MarkIdent, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<MarkIdent, Integer> repo) {
        init(repo);
    }
}
