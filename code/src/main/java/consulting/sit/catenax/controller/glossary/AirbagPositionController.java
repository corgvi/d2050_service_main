package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.AirbagPosition;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/airbagPosition")
@Slf4j
public class AirbagPositionController extends GenericControllerSingleId<AirbagPosition, Integer> {
    @Autowired
    private GenericRepository<AirbagPosition, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<AirbagPosition, Integer> repo) {
        init(repo);
    }
}
