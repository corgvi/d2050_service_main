package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.MaterialMain;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.glossary.MaterialMainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/glossary/materialMain")
@Slf4j
public class MaterialMainController extends GenericControllerSingleId<MaterialMain, Integer> {
    @Autowired
    private GenericRepository<MaterialMain, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<MaterialMain, Integer> repo) {
        init(repo);
    }

    private final MaterialMainService materialMainService;

    public MaterialMainController(MaterialMainService materialMainService) {
        this.materialMainService = materialMainService;
    }

    @GetMapping("/withoutMaterialChar")
    public ResponseEntity<List<MaterialMain>> findMaterialMainWithoutMaterialChar() {
        return ResponseEntity.ok(materialMainService.findMaterialMainWithoutMaterialChar());
    }
    @GetMapping("/withoutMaterialValue")
    public ResponseEntity<List<MaterialMain>> findMaterialMainWithoutMaterialValue() {
        return ResponseEntity.ok(materialMainService.findMaterialMainWithoutMaterialValue());
    }
    @GetMapping("/withoutMaterialGroup")
    public ResponseEntity<List<MaterialMain>> findMaterialMainWithoutMaterialGroup() {
        return ResponseEntity.ok(materialMainService.findMaterialMainWithoutMaterialGroup());
    }
    @GetMapping("/withoutMaterialFamily")
    public ResponseEntity<List<MaterialMain>> findMaterialMainWithoutMaterialFamily() {
        return ResponseEntity.ok(materialMainService.findMaterialMainWithoutMaterialFamily());
    }

    @GetMapping("/check/{id}")
    public ResponseEntity<Boolean> exitsByMaterialMain(@PathVariable int id) {
        return ResponseEntity.ok(repo.existsById(id));
    }
}
