package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.project.Variant;
import consulting.sit.catenax.model.project.VariantDetail;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.project.VariantDetailService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project/variantDetail")
@Slf4j
public class VariantDetailController extends GenericControllerSingleId<VariantDetail, Integer> {

    private final VariantDetailService variantDetailService;

    @Autowired
    private GenericRepository<VariantDetail, Integer> repo;

    public VariantDetailController(final VariantDetailService variantDetailService) {
        this.variantDetailService = variantDetailService;
    }

    @Autowired
    private void setRepository(GenericRepository<VariantDetail, Integer> repo) {
        init(repo);
    }

    /**
     * Update fields of variantDetail
     *
     * @param variantDetail, id
     * @return VariantDetail
     */
    @Operation(
            summary = "Update fields of variant."
            , description = "Update fields of variant."
            , responses = {
    })
    @PatchMapping(value ="/{id}")
    public ResponseEntity<VariantDetail> updateVariant(@RequestBody VariantDetail variantDetail, @PathVariable("id") Integer id) {
        return  ResponseEntity.ok(variantDetailService.updateVariantDetail(variantDetail, id));
    }
}
