package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.project.Tool;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project/tool")
@Slf4j
public class ToolController extends GenericControllerSingleId<Tool, Integer> {
    @Autowired
    private GenericRepository<Tool, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<Tool, Integer> repo) {
        init(repo);
    }
}
