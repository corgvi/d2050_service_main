package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.MaterialValue;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/materialValue")
@Slf4j
public class MaterialValueController extends GenericControllerSingleId<MaterialValue, Integer> {
    @Autowired
    private GenericRepository<MaterialValue, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<MaterialValue, Integer> repo) {
        init(repo);
    }

    @GetMapping("/check/{id}")
    public ResponseEntity<Boolean> exitsByMaterialMain(@PathVariable int id) {
        return ResponseEntity.ok(repo.existsById(id));
    }

}
