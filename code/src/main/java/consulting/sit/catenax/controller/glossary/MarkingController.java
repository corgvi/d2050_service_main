package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.Marking;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/marking")
@Slf4j
public class MarkingController extends GenericControllerSingleId<Marking, Integer> {
    @Autowired
    private GenericRepository<Marking, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<Marking, Integer> repo) {
        init(repo);
    }
}
