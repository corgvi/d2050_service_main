package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.project.Project;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.repository.project.ProjectRepository;
import consulting.sit.catenax.service.project.ProjectService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/project/project")
@Slf4j
public class ProjectController extends GenericControllerSingleId<Project, Integer> {

    private final ProjectService projectService;

    public ProjectController(final ProjectService projectService) {
        this.projectService = projectService;
    }

    @Autowired
    private GenericRepository<Project, Integer> repo;

    @Autowired
//    @Qualifier("project")
    private void setRepository(GenericRepository<Project, Integer> repo) {
        init(repo);
    }


    /**
     * Update fields of project
     *
     * @param project, id
     * @return Project
     */
    @Operation(
            summary = "Update fields of project."
            , description = "Update fields of project."
            , responses = {
    })
    @PatchMapping( "/{id}")
    public ResponseEntity<Project> updateProject(@RequestBody Project project, @PathVariable("id") Integer id) {
        return ResponseEntity.ok(projectService.updateProject(project, id));
    }

}
