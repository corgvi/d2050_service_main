package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.FixingChar;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/fixingChar")
@Slf4j
public class FixingCharController extends GenericControllerSingleId<FixingChar, Integer> {
    @Autowired
    private GenericRepository<FixingChar, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<FixingChar, Integer> repo) {
        init(repo);
    }
}
