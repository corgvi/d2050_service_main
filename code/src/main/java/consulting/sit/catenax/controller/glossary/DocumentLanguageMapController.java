package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.DocumentLanguageMap;
import consulting.sit.catenax.model.glossary.DocumentLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/documentLanguageMap")
@Slf4j
public class DocumentLanguageMapController extends GenericController<DocumentLanguageMap, DocumentLanguageMapPk> {
    @Autowired
    private GenericRepository<DocumentLanguageMap, DocumentLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<DocumentLanguageMap, DocumentLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<DocumentLanguageMap> findById(@PathVariable Integer id, @PathVariable String language) {
        DocumentLanguageMapPk pk = new DocumentLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<DocumentLanguageMap> update(@RequestBody DocumentLanguageMap entity, @PathVariable Integer id, @PathVariable String language) {
        DocumentLanguageMapPk pk = new DocumentLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<DocumentLanguageMap> delete(@PathVariable Integer id, @PathVariable String language) {
        DocumentLanguageMapPk pk = new DocumentLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
