package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.project.ComponentConnect;
import consulting.sit.catenax.model.project.ComponentConnectPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/project/componentConnect")
@Slf4j
public class ComponentConnectController extends GenericController<ComponentConnect, ComponentConnectPk> {
    @Autowired
    private GenericRepository<ComponentConnect, ComponentConnectPk> repo;

    @Autowired
    private void setRepository(GenericRepository<ComponentConnect, ComponentConnectPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<ComponentConnect> findById(@PathVariable Integer fixingId, @PathVariable Integer componentId) {
        ComponentConnectPk pk = new ComponentConnectPk(fixingId, componentId);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<ComponentConnect> update(@RequestBody ComponentConnect entity, @PathVariable Integer fixingId, @PathVariable Integer componentId) {
        ComponentConnectPk pk = new ComponentConnectPk(fixingId, componentId);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<ComponentConnect> delete(@PathVariable Integer fixingId, @PathVariable Integer componentId) {
        ComponentConnectPk pk = new ComponentConnectPk(fixingId, componentId);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
