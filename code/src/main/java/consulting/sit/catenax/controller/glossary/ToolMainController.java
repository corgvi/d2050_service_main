package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.ToolMain;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/toolMain")
@Slf4j
public class ToolMainController extends GenericControllerSingleId<ToolMain, Integer> {
    @Autowired
    private GenericRepository<ToolMain, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<ToolMain, Integer> repo) {
        init(repo);
    }
}
