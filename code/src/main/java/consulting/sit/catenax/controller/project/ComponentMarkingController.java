package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.project.ComponentCe;
import consulting.sit.catenax.model.project.ComponentComp;
import consulting.sit.catenax.model.project.ComponentMarking;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.project.ComponentMarkingService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/project/componentMarking")
@Slf4j
public class ComponentMarkingController extends GenericControllerSingleId<ComponentMarking, Integer> {

    private final ComponentMarkingService componentMarkingService;

    @Autowired
    private GenericRepository<ComponentMarking, Integer> repo;

    public ComponentMarkingController(final ComponentMarkingService componentMarkingService) {
        this.componentMarkingService = componentMarkingService;
    }

    @Autowired
    private void setRepository(GenericRepository<ComponentMarking, Integer> repo) {
        init(repo);
    }

    /**
     * Update fields of componentMarking
     *
     * @param componentMarking, id
     * @return componentMarking
     */
    @Operation(
            summary = "Update fields of component marking."
            , description = "Update fields of component marking."
            , responses = {
    })
    @PatchMapping(value ="/{id}")
    public ResponseEntity<ComponentMarking> updateComponentMarking(@RequestBody ComponentMarking componentMarking, @PathVariable("id") Integer id) {
        return  ResponseEntity.ok(componentMarkingService.updateByFieldsOfComponentMarking(id, componentMarking));
    }

    @GetMapping("/componentMain/{componentId}")
    public ResponseEntity<ComponentMarking> findByComponentId(@PathVariable Integer componentId) {
        return ResponseEntity.ok(componentMarkingService.findByComponentId(componentId));
    }

}
