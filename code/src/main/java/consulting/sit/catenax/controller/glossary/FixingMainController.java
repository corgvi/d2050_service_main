package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.FixingMain;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/fixingMain")
@Slf4j
public class FixingMainController extends GenericControllerSingleId<FixingMain, Integer> {
    @Autowired
    private GenericRepository<FixingMain, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<FixingMain, Integer> repo) {
        init(repo);
    }
}
