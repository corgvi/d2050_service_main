package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.FixingLocation;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/fixingLocation")
@Slf4j
public class FixingLocationController extends GenericControllerSingleId<FixingLocation, String> {
    @Autowired
    private GenericRepository<FixingLocation, String> repo;

    @Autowired
    private void setRepository(GenericRepository<FixingLocation, String> repo) {
        init(repo);
    }
}
