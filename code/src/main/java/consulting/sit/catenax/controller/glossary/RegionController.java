package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.Region;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/region")
@Slf4j
public class RegionController extends GenericControllerSingleId<Region, String> {
    @Autowired
    private GenericRepository<Region, String> repo;

    @Autowired
    private void setRepository(GenericRepository<Region, String> repo) {
        init(repo);
    }
}
