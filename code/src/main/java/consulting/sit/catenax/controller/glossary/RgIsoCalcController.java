package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.glossary.RgIsoCalc;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/glossary/rgIsoCalc")
@Slf4j
public class RgIsoCalcController extends GenericControllerSingleId<RgIsoCalc, Integer> {
    @Autowired
    private GenericRepository<RgIsoCalc, Integer> repo;

    @Autowired
    private void setRepository(GenericRepository<RgIsoCalc, Integer> repo) {
        init(repo);
    }
}
