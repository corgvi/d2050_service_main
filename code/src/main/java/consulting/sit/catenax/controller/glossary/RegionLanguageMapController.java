package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.RegionLanguageMap;
import consulting.sit.catenax.model.glossary.RegionLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/glossary/regionLanguageMap")
@Slf4j
public class RegionLanguageMapController extends GenericController<RegionLanguageMap, RegionLanguageMapPk> {
    @Autowired
    private GenericRepository<RegionLanguageMap, RegionLanguageMapPk> repo;

    @Autowired
    private void setRepository(GenericRepository<RegionLanguageMap, RegionLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<RegionLanguageMap> findById(@PathVariable String id, @PathVariable String language) {
        RegionLanguageMapPk pk = new RegionLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<RegionLanguageMap> update(@RequestBody RegionLanguageMap entity, @PathVariable String id, @PathVariable String language) {
        RegionLanguageMapPk pk = new RegionLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<RegionLanguageMap> delete(@PathVariable String id, @PathVariable String language) {
        RegionLanguageMapPk pk = new RegionLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }
}
