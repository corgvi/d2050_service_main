package consulting.sit.catenax.controller.project;

import consulting.sit.catenax.controller.GenericControllerSingleId;
import consulting.sit.catenax.model.project.ComponentDepend;
import consulting.sit.catenax.projection.project.ComponentDependPlain;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.repository.project.ComponentDependRepository;
import consulting.sit.catenax.service.project.ComponentDependService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/project/componentDepend")
@Slf4j
public class ComponentDependController extends GenericControllerSingleId<ComponentDepend, Integer> {
    public ComponentDependService service;

    public void init(ComponentDependRepository repository) {
        service = new ComponentDependService();
        service.repository = repository;
        super.service = service;
        super.service.repository = service.repository;
    }

    @Autowired
    private GenericRepository<ComponentDepend, Integer> repo;

    @Autowired
    private void setRepository(ComponentDependRepository repo) {
        init(repo);
    }

    @GetMapping("/children/{componentId}")
    public ResponseEntity<List<ComponentDependPlain>> findByComponentId(@PathVariable long componentId) {
        return ResponseEntity.ok(service.findByComponentId(componentId));
    }

    /**
     * Get all componentDepend by componentMainId.
     *
     * @param componentId
     * @return ComponentDepend
     */
    @Operation(
            summary = "Get all componentDepend by componentMainId."
            , description = "Get all componentDepend by componentMainId."
            , responses = {
    })
    @GetMapping("/componentMain/{componentId}")
    public ResponseEntity<List<ComponentDepend>> findAllByComponentId(@PathVariable long componentId) {
        return ResponseEntity.ok(service.findAllByComponentId(componentId));
    }

    @PatchMapping(value ="/{id}")
    public ResponseEntity<ComponentDepend> updateComponentDepend(@RequestBody ComponentDepend componentDepend, @PathVariable("id") Integer id) {
        return  ResponseEntity.ok(service.updateByFieldOfComponentDepend(id, componentDepend));
    }

    /**
     * check componentDepend by componentMainId and abhaengigkeit.
     *
     * @param componentId, abhaengigkeit
     * @return
     */
    @Operation(
            summary = "check componentDepend by componentMainId and abhaengigkeit"
            , description = "check componentDepend by componentMainId and abhaengigkeit"
            , responses = {
    })
    @GetMapping("/check/{componentId}/{abhaengigkeit}")
    public ResponseEntity<Boolean> checkComponentDependExistByAbhaengigkeit(@PathVariable long componentId, @PathVariable long abhaengigkeit) {
        return ResponseEntity.ok(service.checkExistAbhengigkeitInComponent(componentId, abhaengigkeit));
    }
}
