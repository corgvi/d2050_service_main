package consulting.sit.catenax.controller.glossary;

import consulting.sit.catenax.controller.GenericController;
import consulting.sit.catenax.model.glossary.CompoundLanguageMap;
import consulting.sit.catenax.model.glossary.CompoundLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import consulting.sit.catenax.service.glossary.CompoundLanguageMapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/glossary/compoundLanguageMap")
@Slf4j
public class CompoundLanguageMapController extends GenericController<CompoundLanguageMap, CompoundLanguageMapPk> {
    private final CompoundLanguageMapService compoundLanguageMapService;

    @Autowired
    private GenericRepository<CompoundLanguageMap, CompoundLanguageMapPk> repo;

    public CompoundLanguageMapController(final CompoundLanguageMapService compoundLanguageMapService) {
        this.compoundLanguageMapService = compoundLanguageMapService;
    }

    @Autowired
    private void setRepository(GenericRepository<CompoundLanguageMap, CompoundLanguageMapPk> repo) {
        init(repo);
    }

    @GetMapping("/{id}/{language}")
    public ResponseEntity<CompoundLanguageMap> findById(@PathVariable String id, @PathVariable String language) {
        CompoundLanguageMapPk pk = new CompoundLanguageMapPk(id, language);
        return ResponseEntity.ok(service.findById(pk));
    }

    @PutMapping("/{id}/{language}")
    public ResponseEntity<CompoundLanguageMap> update(@RequestBody CompoundLanguageMap entity, @PathVariable String id, @PathVariable String language) {
        CompoundLanguageMapPk pk = new CompoundLanguageMapPk(id, language);
        return ResponseEntity.ok(service.update(entity, pk));
    }

    @DeleteMapping("/{id}/{language}")
    public ResponseEntity<CompoundLanguageMap> delete(@PathVariable String id, @PathVariable String language) {
        CompoundLanguageMapPk pk = new CompoundLanguageMapPk(id, language);
        service.delete(pk);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{language}")
    public ResponseEntity<List<CompoundLanguageMap>> findAllByLanguage(@PathVariable String language) {
        return ResponseEntity.ok(compoundLanguageMapService.findAllByLanguage(language));
    }
}
