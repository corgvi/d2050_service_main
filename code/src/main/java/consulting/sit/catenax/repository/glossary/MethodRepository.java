package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.Method;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MethodRepository extends GenericRepository<Method, Integer> {
}
