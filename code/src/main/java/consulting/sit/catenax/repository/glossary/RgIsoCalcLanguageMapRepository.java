package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.RgIsoCalcLanguageMap;
import consulting.sit.catenax.model.glossary.RgIsoCalcLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RgIsoCalcLanguageMapRepository extends GenericRepository<RgIsoCalcLanguageMap, RgIsoCalcLanguageMapPk> {
}
