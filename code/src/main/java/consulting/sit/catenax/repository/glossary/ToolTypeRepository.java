package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.ToolType;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ToolTypeRepository extends GenericRepository<ToolType, Integer> {
}
