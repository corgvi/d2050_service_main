package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.MarkIdentLanguageMap;
import consulting.sit.catenax.model.glossary.MarkIdentLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarkIdentLanguageMapRepository extends GenericRepository<MarkIdentLanguageMap, MarkIdentLanguageMapPk> {

    List<MarkIdentLanguageMap> findAllByMarkIdentLanguageMapPk_Language(String language);
}
