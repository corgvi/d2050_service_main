package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.VariantAirbag;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VariantAirbagRepository extends GenericRepository<VariantAirbag, Integer> {
}
