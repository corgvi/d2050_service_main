package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.ComponentComp;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComponentCompRepository extends GenericRepository<ComponentComp, Integer> {

    List<ComponentComp> findByComponentMainId(int componentMainId);
}
