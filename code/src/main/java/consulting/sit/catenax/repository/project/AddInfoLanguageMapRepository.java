package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.AddInfoLanguageMap;
import consulting.sit.catenax.model.project.AddInfoLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddInfoLanguageMapRepository extends GenericRepository<AddInfoLanguageMap, AddInfoLanguageMapPk> {
}
