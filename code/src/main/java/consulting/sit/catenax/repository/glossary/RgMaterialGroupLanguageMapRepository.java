package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.RgMaterialGroupLanguageMap;
import consulting.sit.catenax.model.glossary.RgMaterialGroupLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RgMaterialGroupLanguageMapRepository extends GenericRepository<RgMaterialGroupLanguageMap, RgMaterialGroupLanguageMapPk> {
}
