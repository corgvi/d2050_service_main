package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.PtTreatmentMap;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PtTreatmentMapRepository extends GenericRepository<PtTreatmentMap, Integer> {
}
