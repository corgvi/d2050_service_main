package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.FractionWasteLanguageMap;
import consulting.sit.catenax.model.glossary.FractionWasteLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FractionWasteLanguageMapRepository extends GenericRepository<FractionWasteLanguageMap, FractionWasteLanguageMapPk> {
}
