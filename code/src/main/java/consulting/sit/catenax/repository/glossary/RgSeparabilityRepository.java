package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.RgSeparability;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RgSeparabilityRepository extends GenericRepository<RgSeparability, Integer> {
}
