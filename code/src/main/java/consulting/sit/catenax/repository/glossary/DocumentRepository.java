package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.Document;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentRepository extends GenericRepository<Document, Integer> {
}
