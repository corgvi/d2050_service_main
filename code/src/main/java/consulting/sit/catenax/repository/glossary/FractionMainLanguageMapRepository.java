package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.FractionMainLanguageMap;
import consulting.sit.catenax.model.glossary.FractionMainLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FractionMainLanguageMapRepository extends GenericRepository<FractionMainLanguageMap, FractionMainLanguageMapPk> {

    List<FractionMainLanguageMap> findAllByFractionMainLanguageMapPk_Language(String language);
}
