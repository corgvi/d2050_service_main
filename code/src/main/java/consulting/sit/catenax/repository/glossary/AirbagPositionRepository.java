package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.AirbagPosition;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AirbagPositionRepository extends GenericRepository<AirbagPosition, Integer> {
}
