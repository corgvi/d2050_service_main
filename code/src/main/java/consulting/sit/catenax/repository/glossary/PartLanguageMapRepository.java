package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.PartLanguageMap;
import consulting.sit.catenax.model.glossary.PartLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PartLanguageMapRepository extends GenericRepository<PartLanguageMap, PartLanguageMapPk> {

    List<PartLanguageMap> findAllByPartLanguageMapPk_Language(String language);
}
