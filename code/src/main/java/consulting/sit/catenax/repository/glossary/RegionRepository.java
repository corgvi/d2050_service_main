package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.Region;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegionRepository extends GenericRepository<Region, String> {
}
