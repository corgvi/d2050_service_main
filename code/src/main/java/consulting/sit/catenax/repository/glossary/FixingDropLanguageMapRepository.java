package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.FixingDropLanguageMap;
import consulting.sit.catenax.model.glossary.FixingDropLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FixingDropLanguageMapRepository extends GenericRepository<FixingDropLanguageMap, FixingDropLanguageMapPk> {
    List<FixingDropLanguageMap> findAllByFixingDropLanguageMapPk_Language(String language);
}
