package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.FractionWaste;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FractionWasteRepository extends GenericRepository<FractionWaste, Integer> {
}
