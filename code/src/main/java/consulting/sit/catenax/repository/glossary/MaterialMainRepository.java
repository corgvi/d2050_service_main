package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.MaterialMain;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MaterialMainRepository extends GenericRepository<MaterialMain, Integer> {

    @Query("SELECT mm FROM MaterialMain mm WHERE mm.id NOT IN (SELECT mc.id FROM MaterialChar mc)")
    List<MaterialMain> findMaterialMainWithoutMaterialChar();
    @Query("SELECT mm FROM MaterialMain mm WHERE mm.id NOT IN (SELECT mf.id FROM MaterialFamily mf)")
    List<MaterialMain> findMaterialMainWithoutMaterialFamily();
    @Query("SELECT mm FROM MaterialMain mm WHERE mm.id NOT IN (SELECT mv.id FROM MaterialValue mv)")
    List<MaterialMain> findMaterialMainWithoutMaterialValue();
    @Query("SELECT mm FROM MaterialMain mm WHERE mm.id NOT IN (SELECT mg.id FROM MaterialGroup mg)")
    List<MaterialMain> findMaterialMainWithoutMaterialGroup();

}
