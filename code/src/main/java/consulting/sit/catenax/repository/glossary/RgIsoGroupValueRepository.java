package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.RgIsoGroupValue;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RgIsoGroupValueRepository extends GenericRepository<RgIsoGroupValue, Integer> {
}
