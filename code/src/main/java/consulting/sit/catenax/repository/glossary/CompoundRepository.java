package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.Compound;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompoundRepository extends GenericRepository<Compound, String> {
}
