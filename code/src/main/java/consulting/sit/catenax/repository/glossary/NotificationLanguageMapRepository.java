package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.NotificationLanguageMap;
import consulting.sit.catenax.model.glossary.NotificationLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationLanguageMapRepository extends GenericRepository<NotificationLanguageMap, NotificationLanguageMapPk> {
}
