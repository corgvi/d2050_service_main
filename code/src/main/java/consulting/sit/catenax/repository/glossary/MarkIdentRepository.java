package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.MarkIdent;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarkIdentRepository extends GenericRepository<MarkIdent, Integer> {
}
