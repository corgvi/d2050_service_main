package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.Fixing;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FixingRepository extends GenericRepository<Fixing, Integer> {
}
