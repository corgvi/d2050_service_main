package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.RgIsoGroup;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RgIsoGroupRepository extends GenericRepository<RgIsoGroup, Integer> {
    @Query("SELECT rig FROM RgIsoGroup rig WHERE rig.id NOT IN (SELECT rigv.id FROM RgIsoGroupValue rigv)")
    List<RgIsoGroup> findRgIsoGroupWithoutRgIsoGroupValue();
}
