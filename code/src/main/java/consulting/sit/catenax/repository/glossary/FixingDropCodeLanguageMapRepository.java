package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.FixingDropCodeLanguageMap;
import consulting.sit.catenax.model.glossary.FixingDropCodeLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FixingDropCodeLanguageMapRepository extends GenericRepository<FixingDropCodeLanguageMap, FixingDropCodeLanguageMapPk> {
    FixingDropCodeLanguageMap findByFixingDropCodeEntity_FixingDropIdAndFixingDropCodeLanguageMapPk_Language(int id, String language);
}
