package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.FixingLocationLanguageMap;
import consulting.sit.catenax.model.glossary.FixingLocationLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FixingLocationLanguageMapRepository extends GenericRepository<FixingLocationLanguageMap, FixingLocationLanguageMapPk> {
    List<FixingLocationLanguageMap> findAllByFixingLocationLanguageMapPk_Language(String language);
}
