package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.AddInfo;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddInfoRepository extends GenericRepository<AddInfo, Integer> {
}
