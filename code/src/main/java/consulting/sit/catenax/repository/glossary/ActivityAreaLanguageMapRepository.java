package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.ActivityAreaLanguageMap;
import consulting.sit.catenax.model.glossary.ActivityAreaLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActivityAreaLanguageMapRepository extends GenericRepository<ActivityAreaLanguageMap, ActivityAreaLanguageMapPk> {

    List<ActivityAreaLanguageMap> findAllByActivityAreaLanguageMapPk_Language(String language);
}
