package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.Part;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PartRepository extends GenericRepository<Part, Integer> {
}
