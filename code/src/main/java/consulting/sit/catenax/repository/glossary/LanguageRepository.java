package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.Language;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LanguageRepository extends GenericRepository<Language, String> {
}
