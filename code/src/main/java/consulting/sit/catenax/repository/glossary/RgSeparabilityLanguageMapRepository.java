package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.RgSeparabilityLanguageMap;
import consulting.sit.catenax.model.glossary.RgSeparabilityLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RgSeparabilityLanguageMapRepository extends GenericRepository<RgSeparabilityLanguageMap, RgSeparabilityLanguageMapPk> {
}
