package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.PositionLanguageMap;
import consulting.sit.catenax.model.glossary.PositionLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionLanguageMapRepository extends GenericRepository<PositionLanguageMap, PositionLanguageMapPk> {
}
