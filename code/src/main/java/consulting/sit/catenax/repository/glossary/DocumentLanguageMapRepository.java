package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.DocumentLanguageMap;
import consulting.sit.catenax.model.glossary.DocumentLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentLanguageMapRepository extends GenericRepository<DocumentLanguageMap, DocumentLanguageMapPk> {
}
