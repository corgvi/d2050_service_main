package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.Tool;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ToolRepository extends GenericRepository<Tool, Integer> {
}
