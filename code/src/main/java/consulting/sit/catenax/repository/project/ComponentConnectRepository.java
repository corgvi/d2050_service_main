package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.ComponentConnect;
import consulting.sit.catenax.model.project.ComponentConnectPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComponentConnectRepository extends GenericRepository<ComponentConnect, ComponentConnectPk> {
}
