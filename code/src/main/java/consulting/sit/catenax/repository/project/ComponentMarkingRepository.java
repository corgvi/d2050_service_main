package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.ComponentMarking;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComponentMarkingRepository extends GenericRepository<ComponentMarking, Integer> {

    ComponentMarking findByComponentId(int componentId);
}
