package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.FractionMain;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FractionMainRepository extends GenericRepository<FractionMain, Integer> {
}
