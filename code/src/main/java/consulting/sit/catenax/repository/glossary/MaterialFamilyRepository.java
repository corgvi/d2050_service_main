package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.MaterialFamily;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaterialFamilyRepository extends GenericRepository<MaterialFamily, Integer> {
}
