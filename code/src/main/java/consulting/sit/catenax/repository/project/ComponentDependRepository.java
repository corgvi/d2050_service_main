package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.ComponentDepend;
import consulting.sit.catenax.projection.project.ComponentDependPlain;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComponentDependRepository extends GenericRepository<ComponentDepend, Integer> {
    List<ComponentDependPlain> findByComponentId(long componentId);
    List<ComponentDepend> findAllByComponentId(long componentId);
    Boolean existsByComponentIdAndAbhaengigkeit(long componentId, long abhengigkeit);
}
