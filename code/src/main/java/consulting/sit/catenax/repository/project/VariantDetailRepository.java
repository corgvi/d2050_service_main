package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.VariantDetail;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VariantDetailRepository extends GenericRepository<VariantDetail, Integer> {
}
