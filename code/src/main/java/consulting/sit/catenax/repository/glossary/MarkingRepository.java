package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.Marking;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarkingRepository extends GenericRepository<Marking, Integer> {
}
