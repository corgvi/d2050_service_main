package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.MaterialMain;
import consulting.sit.catenax.model.glossary.PtListing;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PtListingRepository extends GenericRepository<PtListing, Integer> {

    @Query("SELECT mm FROM PtListing mm WHERE mm.id NOT IN (SELECT mg.id FROM PtMaterialMap mg)")
    List<PtListing> findPtListingWithoutPtMaterialMap();

}
