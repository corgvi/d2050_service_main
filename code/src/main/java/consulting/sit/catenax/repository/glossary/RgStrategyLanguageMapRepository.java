package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.RgStrategyLanguageMap;
import consulting.sit.catenax.model.glossary.RgStrategyLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RgStrategyLanguageMapRepository extends GenericRepository<RgStrategyLanguageMap, RgStrategyLanguageMapPk> {
}
