package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.RgStrategy;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RgStrategyRepository extends GenericRepository<RgStrategy, Integer> {
}
