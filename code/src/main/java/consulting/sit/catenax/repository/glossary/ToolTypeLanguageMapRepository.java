package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.ToolTypeLanguageMap;
import consulting.sit.catenax.model.glossary.ToolTypeLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ToolTypeLanguageMapRepository extends GenericRepository<ToolTypeLanguageMap, ToolTypeLanguageMapPk> {
}
