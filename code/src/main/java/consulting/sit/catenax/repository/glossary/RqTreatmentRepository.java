package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.RqTreatment;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RqTreatmentRepository extends GenericRepository<RqTreatment, Integer> {
}
