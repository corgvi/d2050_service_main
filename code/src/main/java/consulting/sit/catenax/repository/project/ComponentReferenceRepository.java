package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.ComponentReference;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComponentReferenceRepository extends GenericRepository<ComponentReference, Integer> {

    ComponentReference findByComponentId(int componentId);
}
