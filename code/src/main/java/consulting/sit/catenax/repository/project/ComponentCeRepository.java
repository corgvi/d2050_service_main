package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.ComponentCe;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComponentCeRepository extends GenericRepository<ComponentCe, Integer> {

    ComponentCe findByComponentId(int componentId);
}
