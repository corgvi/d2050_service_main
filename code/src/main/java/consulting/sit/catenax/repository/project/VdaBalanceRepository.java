package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.VdaBalance;
import consulting.sit.catenax.model.project.VdaBalancePk;
import consulting.sit.catenax.repository.GenericRepository;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface VdaBalanceRepository extends GenericRepository<VdaBalance, VdaBalancePk> {
    public List<VdaBalance> findByComponentId(Integer id);

    List<VdaBalance> findAllByVdaBalancePk_ComponentId(Integer componentId);
}
