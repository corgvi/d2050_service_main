package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.ComponentPicture;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComponentPictureRepository extends GenericRepository<ComponentPicture, Integer> {
}
