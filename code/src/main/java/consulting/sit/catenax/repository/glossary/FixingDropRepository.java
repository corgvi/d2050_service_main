package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.FixingDrop;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FixingDropRepository extends GenericRepository<FixingDrop, Integer> {

    @Query(value = "select a from FixingDrop as a left join FixingDropCode as b on a.id = b.fixingDropId where b.fixingDropId is null")
    List<FixingDrop> findFixingDropWithoutFixingDropCode();
}
