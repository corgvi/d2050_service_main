package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.VariantTypeLanguageMap;
import consulting.sit.catenax.model.glossary.VariantTypeLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VariantTypeLanguageMapRepository extends GenericRepository<VariantTypeLanguageMap, VariantTypeLanguageMapPk> {
}
