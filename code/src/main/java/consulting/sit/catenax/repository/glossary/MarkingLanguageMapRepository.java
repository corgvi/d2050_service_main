package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.MarkingLanguageMap;
import consulting.sit.catenax.model.glossary.MarkingLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarkingLanguageMapRepository extends GenericRepository<MarkingLanguageMap, MarkingLanguageMapPk> {
}
