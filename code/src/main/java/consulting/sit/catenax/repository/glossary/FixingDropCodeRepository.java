package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.FixingDrop;
import consulting.sit.catenax.model.glossary.FixingDropCode;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FixingDropCodeRepository extends GenericRepository<FixingDropCode, Integer> {

    Boolean existsByFixingDropId(int fixingDropId);

}
