package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.VariantType;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VariantTypeRepository extends GenericRepository<VariantType, Integer> {
}
