package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.ComponentCe;
import consulting.sit.catenax.model.project.ComponentDescription;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComponentDescriptionRepository extends GenericRepository<ComponentDescription, Integer> {
    ComponentDescription findByComponentId(int componentId);
}
