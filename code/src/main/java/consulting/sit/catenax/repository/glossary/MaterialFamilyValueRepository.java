package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.MaterialFamilyValue;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaterialFamilyValueRepository extends GenericRepository<MaterialFamilyValue, Integer> {
}
