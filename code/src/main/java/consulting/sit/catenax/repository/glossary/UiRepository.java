package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.Ui;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UiRepository extends GenericRepository<Ui, Integer> {
}
