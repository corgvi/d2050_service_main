package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.FixingMainLanguageMap;
import consulting.sit.catenax.model.glossary.FixingMainLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FixingMainLanguageMapRepository extends GenericRepository<FixingMainLanguageMap, FixingMainLanguageMapPk> {

    List<FixingMainLanguageMap> findAllByFixingMainLanguageMapPk_Language(String language);
}
