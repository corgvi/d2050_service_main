package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.ToolMain;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ToolMainRepository extends GenericRepository<ToolMain, Integer> {
}
