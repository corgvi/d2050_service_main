package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.MaterialValue;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaterialValueRepository extends GenericRepository<MaterialValue, Integer> {
}
