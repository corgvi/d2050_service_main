package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.RgIsoCalc;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RgIsoCalcRepository extends GenericRepository<RgIsoCalc, Integer> {
}
