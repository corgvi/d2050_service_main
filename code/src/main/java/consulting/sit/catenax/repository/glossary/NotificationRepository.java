package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.Notification;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepository extends GenericRepository<Notification, Integer> {
}
