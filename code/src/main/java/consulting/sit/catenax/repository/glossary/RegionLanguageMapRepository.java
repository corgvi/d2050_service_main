package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.RegionLanguageMap;
import consulting.sit.catenax.model.glossary.RegionLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegionLanguageMapRepository extends GenericRepository<RegionLanguageMap, RegionLanguageMapPk> {
}
