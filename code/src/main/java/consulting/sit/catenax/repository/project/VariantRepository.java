package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.Variant;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VariantRepository extends GenericRepository<Variant, Integer> {
    List<Variant> findByProjectId(int projectId);
}
