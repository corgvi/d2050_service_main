package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.ToolMainLanguageMap;
import consulting.sit.catenax.model.glossary.ToolMainLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ToolMainLanguageMapRepository extends GenericRepository<ToolMainLanguageMap, ToolMainLanguageMapPk> {
    List<ToolMainLanguageMap> findAllByToolMainLanguageMapPk_Language(String language);
}
