package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.VariantSpecificationLanguageMap;
import consulting.sit.catenax.model.glossary.VariantSpecificationLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VariantSpecificationLanguageMapRepository extends GenericRepository<VariantSpecificationLanguageMap, VariantSpecificationLanguageMapPk> {
}
