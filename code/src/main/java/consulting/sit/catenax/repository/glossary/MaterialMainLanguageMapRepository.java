package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.MaterialMainLanguageMap;
import consulting.sit.catenax.model.glossary.MaterialMainLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MaterialMainLanguageMapRepository extends GenericRepository<MaterialMainLanguageMap, MaterialMainLanguageMapPk> {

    List<MaterialMainLanguageMap> findAllByMaterialMainLanguageMapPk_Language(String language);
}
