package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.ComponentNotification;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComponentNotificationRepository extends GenericRepository<ComponentNotification, Integer> {
}
