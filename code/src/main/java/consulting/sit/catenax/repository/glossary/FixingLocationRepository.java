package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.FixingLocation;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FixingLocationRepository extends GenericRepository<FixingLocation, String> {
}
