package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.MaterialChar;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaterialCharRepository extends GenericRepository<MaterialChar, Integer> {
}
