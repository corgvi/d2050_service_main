package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.MaterialGroup;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaterialGroupRepository extends GenericRepository<MaterialGroup, Integer> {
}
