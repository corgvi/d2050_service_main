package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.LanguageLanguageMap;
import consulting.sit.catenax.model.glossary.LanguageLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LanguageLanguageMapRepository extends GenericRepository<LanguageLanguageMap, LanguageLanguageMapPk> {
}
