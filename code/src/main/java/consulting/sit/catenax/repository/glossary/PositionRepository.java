package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.Position;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionRepository extends GenericRepository<Position, Integer> {
}
