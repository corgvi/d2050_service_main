package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.VariantSpecification;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VariantSpecificationRepository extends GenericRepository<VariantSpecification, Integer> {
}
