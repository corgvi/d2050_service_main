package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.ActivityArea;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityAreaRepository extends GenericRepository<ActivityArea, Integer> {
}
