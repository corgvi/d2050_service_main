package consulting.sit.catenax.repository.project;

import consulting.sit.catenax.model.project.ComponentMain;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComponentMainRepository extends GenericRepository<ComponentMain, Integer> {
    List<ComponentMain> findByVariantId(int variantId);

//    List<ComponentMain> findByVariantIdAndLevelInBom(int variantId, int levelInBom);
//
//    @Query(value = "SELECT m.id FROM ComponentMain AS m WHERE m.variantId =:variantId AND m.levelInBom =:levelInBom")
//    List<Object[]> findAllComponentIdByVariantIdAndLevelInBom(int variantId, int levelInBom);

    List<ComponentMain> findAllByZbTeilIsTrue();

    @Query(value = "SELECT m.id FROM ComponentMain AS m WHERE m.variantId =:variantId AND m.top IS NULL ORDER BY m.id ASC")
    List<Object[]> findAllComponentIdByVariantIdAndTopIsNull(int variantId);

    @Query(value = "SELECT m.id FROM ComponentMain AS m WHERE m.top =:top ORDER BY m.id ASC")
    List<Object[]> findAllComponentIdByTop(long top);

    Boolean existsByIdAndVariantId(int componentId, int variantId);
}
