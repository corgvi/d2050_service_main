package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.RqTreatmentLanguageMap;
import consulting.sit.catenax.model.glossary.RqTreatmentLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RqTreatmentLanguageMapRepository extends GenericRepository<RqTreatmentLanguageMap, RqTreatmentLanguageMapPk> {
}
