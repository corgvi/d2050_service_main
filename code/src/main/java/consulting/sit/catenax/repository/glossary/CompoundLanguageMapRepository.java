package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.CompoundLanguageMap;
import consulting.sit.catenax.model.glossary.CompoundLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompoundLanguageMapRepository extends GenericRepository<CompoundLanguageMap, CompoundLanguageMapPk> {
    List<CompoundLanguageMap> findAllByCompoundLanguageMapPk_Language(String language);
}
