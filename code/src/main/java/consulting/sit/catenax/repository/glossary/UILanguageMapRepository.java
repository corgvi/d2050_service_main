package consulting.sit.catenax.repository.glossary;

import consulting.sit.catenax.model.glossary.UiLanguageMap;
import consulting.sit.catenax.model.glossary.UiLanguageMapPk;
import consulting.sit.catenax.repository.GenericRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UILanguageMapRepository extends GenericRepository<UiLanguageMap, UiLanguageMapPk
        > {
}
