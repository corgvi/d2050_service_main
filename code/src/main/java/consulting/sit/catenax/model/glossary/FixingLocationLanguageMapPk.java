package consulting.sit.catenax.model.glossary;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class FixingLocationLanguageMapPk implements Serializable {
    @Length(max = 5)
    @Column(name = "x_lage_ve", length = 5)
    private String xLageVe;

    @Length(min = 2, max = 2)
    @Column(name = "language", length=2)
    private String language;
}
