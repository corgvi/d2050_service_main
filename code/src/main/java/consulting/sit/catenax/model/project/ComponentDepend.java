package consulting.sit.catenax.model.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Entity
@Table(schema = "project", name = "p_component_depend")
@EntityListeners(AuditingEntityListener.class)
@SequenceGenerator(schema = "project", name = "component_depend_seq_gen", sequenceName = "component_depend_id_seq")
public class ComponentDepend implements ModelBaseInterface<Integer> {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "component_depend_seq_gen")
	private Integer id;

	@Column(name = "component_id")
	private Long componentId;

	@ManyToOne
	@JoinColumn(name = "component_id", referencedColumnName = "id", insertable = false, updatable = false)
	@JsonIgnore
	private ComponentMain componentMainEntity;

	@Column(name = "abhaengigkeit")
	private Long abhaengigkeit;

	@NotNull
	@Column(name = "konstruktiv", nullable = false)
	private Boolean konstruktiv;

	@NotNull
	@Column(name = "strategisch", nullable = false)
	private Boolean strategisch;

	@Column(name = "zb_part")
	private Integer zbPart;

	@ManyToOne
	@JoinColumn(name = "zb_part", referencedColumnName = "id", insertable = false, updatable = false)
	@JsonIgnore
	private ComponentMain componentMain;

	@CreatedBy
	@Column(name = "created_by")
	private String createdBy;

	@CreatedDate
	@Column(name = "created_at")
	private LocalDateTime createdAt;

	@LastModifiedBy
	@Column(name = "modified_by")
	private String modifiedBy;

	@LastModifiedDate
	@Column(name = "modified_at")
	private LocalDateTime modifiedAt;
}
