package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_document")
@SequenceGenerator(schema = "glossary", name = "document_seq_gen", sequenceName = "document_id_seq")
public class Document implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "document_seq_gen")
    private Integer id;

    @Length(max = 45)
    @Column(name = "doctype", length = 45)
    private String doctype;

    @OneToMany(mappedBy = "documentEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "documentLanguageMapPk.language")
    private Map<String, DocumentLanguageMap> documentLanguageMaps;
}