package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_tool_main")
@SequenceGenerator(schema = "glossary", name = "tool_main_seq_gen", sequenceName = "tool_main_id_seq")
public class ToolMain implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tool_main_seq_gen")
    private Integer id;

    @Column(name = "sortorder")
    private int sortorder;

    @Column(name = "g_tool_type_id")
    private Integer toolTypeId;

    @ManyToOne
    @JoinColumn(name = "g_tool_type_id", referencedColumnName = "id", insertable = false, updatable = false)
    private ToolType toolTypeEntity;

    @OneToMany(mappedBy = "toolMainEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "toolMainLanguageMapPk.language")
    private Map<String, ToolMainLanguageMap> toolMainLanguageMaps;
}
