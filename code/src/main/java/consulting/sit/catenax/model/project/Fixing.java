package consulting.sit.catenax.model.project;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import consulting.sit.catenax.model.glossary.ToolMain;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;

import consulting.sit.catenax.model.ModelBaseInterface;
import consulting.sit.catenax.model.glossary.FixingDrop;
import consulting.sit.catenax.model.glossary.FixingLocation;
import consulting.sit.catenax.model.glossary.FixingMain;
import consulting.sit.catenax.model.glossary.MaterialMain;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(schema = "project", name = "p_fixing")
@SequenceGenerator(schema = "project", name = "fixing_seq_gen", sequenceName = "fixing_id_seq")
public class Fixing implements ModelBaseInterface<Integer> {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fixing_seq_gen")
	private Integer id;

	@Column(name = "component_id")
	private Integer componentId;

	@ManyToOne
	@JoinColumn(name = "component_id", referencedColumnName = "id", insertable = false, updatable = false)
	@JsonIgnore
	private ComponentMain componentMainEntity;

	@Column(name = "verbindungselement")
	private Integer verbindungselement;

	@ManyToOne
	@JoinColumn(name = "verbindungselement", referencedColumnName = "id", insertable = false, updatable = false)
	private FixingMain fixingMainEntity;

	@Length(max = 50)
	@Column(name = "ve", length = 50)
	private String ve;

	@Length(max = 50)
	@Column(name = "groesse", length = 50)
	private String groesse;

	@Column(name = "ve_anzahl")
	private Integer veAnzahl;

	@Column(name = "ve_lage")
	private Integer veLage;

	@ManyToOne
	@JoinColumn(name = "ve_lage", referencedColumnName = "x_lage_ve", insertable = false, updatable = false)
	private FixingLocation fixingLocation;

	@Column(name = "ve_masse")
	private Integer veMasse;

	@Column(name = "ve_werkstoff")
	private Integer veWerkstoff;

	@ManyToOne
	@JoinColumn(name = "ve_werkstoff", referencedColumnName = "id", insertable = false, updatable = false)
	private MaterialMain materialMain;

	@Column(name = "verbleib")
	private Integer verbleib;

	@ManyToOne
	@JoinColumn(name = "verbleib", referencedColumnName = "id", insertable = false, updatable = false)
	private FixingDrop fixingDrop;

	@Length(max = 30)
	@Column(name = "demonteur_position", length = 30)
	private String demonteurPosition;

	@Column(name = "vertraeglichkeit")
	private Integer vertraeglichkeit;

	@Length(max = 100)
	@Column(name = "vertraeglichkeit_begruendung", length = 100)
	private String vertraeglichkeitBegruendung;

	@Column(name = "auffindbarkeit")
	private Integer auffindbarkeit;

	@Length(max = 100)
	@Column(name = "auffindbarkeit_begruendung", length = 100)
	private String auffindbarkeitBegruendung;

	@Column(name = "zugaenglichkeit")
	private Integer zugaenglichkeit;

	@Length(max = 100)
	@Column(name = "zugaenglichkeit_begruendung", length = 100)
	private String zugaenglichkeitBegruendung;

	@Column(name = "loesbarkeit")
	private Integer loesbarkeit;

	@Length(max = 100)
	@Column(name = "loesbarkeit_begruendung", length = 100)
	private String loesbarkeitBegruendung;

	@Column(name = "grobdemontage")
	private Integer grobdemontage;

	@Length(max = 100)
	@Column(name = "grobdemontage_begruendung", length = 100)
	private String grobdemontageBegruendung;

	@Column(name = "nacharbeitszeit")
	private String nacharbeitszeit;

	@Length(max = 10)
	@Column(name = "hilfsmittel", length = 10)
	private String hilfsmittel;

	@Column(name = "einsatz")
	private Boolean einsatz;

	@OneToMany(mappedBy = "fixingEntity", cascade = CascadeType.REMOVE)
	private List<ComponentConnect> componentConnects;

	@Column(name = "werkzeug")
	private Integer werkzeug;

	@ManyToOne
	@JoinColumn(name = "werkzeug", referencedColumnName = "id", insertable = false, updatable = false)
	private ToolMain toolMain;

	@CreatedBy
	@Column(name = "created_by")
	private String createdBy;

	@CreatedDate
	@Column(name = "created_at")
	private LocalDateTime createdAt;

	@LastModifiedBy
	@Column(name = "modified_by")
	private String modifiedBy;

	@LastModifiedDate
	@Column(name = "modified_at")
	private LocalDateTime modifiedAt;
}
