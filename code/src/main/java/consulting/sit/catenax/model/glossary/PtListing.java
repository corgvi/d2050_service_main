package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_pt_listing")
@SequenceGenerator(schema = "glossary", name = "pt_listing_seq_gen", sequenceName = "pt_listing_id_seq")
public class PtListing implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pt_listing_seq_gen")
    private Integer id;

    @Column(name = "company", columnDefinition = "TEXT")
    private String company;

    @Column(name = "company_street", columnDefinition = "TEXT")
    private String companyStreet;

    @Column(name = "company_zip", columnDefinition = "TEXT")
    private String companyZip;

    @Column(name = "company_city", columnDefinition = "TEXT")
    private String companyCity;

    @Column(name = "company_country", columnDefinition = "TEXT")
    private String companyCountry;

    @Column(name = "company_website", columnDefinition = "TEXT")
    private String companyWebsite;

    @Column(name = "contact_email", columnDefinition = "TEXT")
    private String contactEmail;

    @Column(name = "contact_name", columnDefinition = "TEXT")
    private String contactName;

    @Column(name = "contact_phone", columnDefinition = "TEXT")
    private String contactPhone;
}
