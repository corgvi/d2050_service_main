package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_compound_language_map")
public class CompoundLanguageMap implements ModelBaseInterface<CompoundLanguageMapPk> {
    @EmbeddedId
    private CompoundLanguageMapPk compoundLanguageMapPk;

    @Length(max = 50)
    @Column(name = "value", length = 50)
    private String value;

    @ManyToOne
    @JoinColumn(name = "verbundanteil", referencedColumnName = "verbundanteil", insertable = false, updatable = false)
    @JsonIgnore
    private Compound compoundEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(CompoundLanguageMapPk id) {
        this.compoundLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public CompoundLanguageMapPk getId() {
        return this.compoundLanguageMapPk;
    }
}
