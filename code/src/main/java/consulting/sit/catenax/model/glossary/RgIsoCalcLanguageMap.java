package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_rg_iso_calc_language_map")
public class RgIsoCalcLanguageMap implements ModelBaseInterface<RgIsoCalcLanguageMapPk> {
    @EmbeddedId
    private RgIsoCalcLanguageMapPk rgIsoCalcLanguageMapPk;

    @Column(name = "value", columnDefinition = "TEXT")
    private String value;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private RgIsoCalc rgIsoCalcEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(RgIsoCalcLanguageMapPk id) {
        this.rgIsoCalcLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public RgIsoCalcLanguageMapPk getId() {
        return this.rgIsoCalcLanguageMapPk;
    }
}
