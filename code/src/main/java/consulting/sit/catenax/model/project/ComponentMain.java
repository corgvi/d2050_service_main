package consulting.sit.catenax.model.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import consulting.sit.catenax.model.ModelBaseInterface;
import consulting.sit.catenax.model.glossary.ActivityArea;
import consulting.sit.catenax.model.glossary.FractionMain;
import consulting.sit.catenax.model.glossary.MarkIdent;
import consulting.sit.catenax.model.glossary.MaterialMain;
import consulting.sit.catenax.model.glossary.Part;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(schema = "project", name = "p_component_main")
@SequenceGenerator(schema = "project", name = "component_main_seq_gen", sequenceName = "component_main_id_seq")
public class ComponentMain implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "component_main_seq_gen")
    private Integer id;

    @Column(name = "variant_id")
    private int variantId;

    @ManyToOne
    @JoinColumn(name = "variant_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Variant variantEntity;

    @Column(name = "anzahl")
    @JsonProperty(value = "quantity")
    private Integer anzahl;

    @NotNull
    @Column(name = "zb_teil", nullable = false)
    private Boolean zbTeil;

    @Column(name = "zb_masse")
    private Long zbMasse;

    @Column(name = "masse")
    private Long masse;

    @Column(name = "m_liter")
    private Long mLiter;

    @Column(name = "bau_gruppe")
    private Long bauGruppe;

    @Column(name = "vr1")
    private Integer vr1;

    @NotNull
    @Column(name = "ausbaupflicht", nullable = false)
    private Boolean ausbaupflicht;

    @Length(max = 100)
    @Column(name = "anmerkung", length = 100)
    @JsonProperty(value = "comments")
    private String anmerkung;

    @NotNull
    @Column(name = "variante", nullable = false)
    private Boolean variante;

    @Column(name = "reused")
    private Integer reused;

    @Length(max = 50)
    @Column(name = "schadstoff", length = 50)
    private String schadstoff;

    @Column(name = "ss_anteil")
    private Integer ssAnteil;

    @Length(max = 50)
    @Column(name = "ss_comment", length = 50)
    private String ssComment;

    @Length(max = 60)
    @Column(name = "benennung_original", length = 60)
    @JsonProperty(value = "name")
    private String benennungOrginal;

    @NotNull
    @Column(name = "vergleichsteil", nullable = false)
    private Boolean vergleichsteil;

    @Length(max = 8)
    @Column(name = "cpsc", length = 8)
    private String cpsc;

    @Column(name = "position")
    private Long position;

    @Length(max = 10)
    @Column(name = "prefix", length = 10)
    private String prefix;

    @Length(max = 10)
    @Column(name = "main", length = 10)
    private String main;

    @Length(max = 10)
    @Column(name = "suffix", length = 10)
    private String suffix;

    @Length(max = 255)
    @Column(name = "ws_kombinationen", length = 255)
    private String wsKombinationen;

    @Length(max = 5)
    @Column(name = "rec_content", length = 5)
    private String recContent;

    @Column(name = "rec_category")
    private Integer recCategory;

    @Column(name = "rec_pirpcr")
    private Integer recPirPcr;

    @Column(name = "laenge")
    @JsonProperty(value = "length")
    private Integer laenge;

    @Column(name = "breite")
    @JsonProperty(value = "width")
    private Integer breite;

    @Column(name = "hoehe")
    @JsonProperty(value = "height")
    private Integer hoehe;

    @Column(name = "top")
    private Long top;

    @ManyToOne
    @JoinColumn(name = "top", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private ComponentMain parent;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.REMOVE)
    private List<ComponentMain> children;

    @Column(name = "\"left\"")
    private Long left;

    @Length(max = 50)
    @Column(name = "p_verw_text", length = 50)
    private String pVerwTxt;

    @Length(max = 50)
    @Column(name = "fu_gr", length = 50)
    private String fuGr;

    @Column(name = "bauteil_besonderheit")
    private Integer bauteilBesonderheit;

    @ManyToOne
    @JoinColumn(name = "bauteil_besonderheit", referencedColumnName = "id", insertable = false, updatable = false)
    private MarkIdent MarkIdentEntity;

    @Length(max = 3)
    @Column(name = "modul_hersteller", length = 3)
    private String modulHersteller;

    @Column(name = "verunreinigung")
    private Integer verunreinigung;

    @Column(name = "recyclat_anteil")
    private Integer rezyklatAnteil;

    @Column(name = "x_part_ref")
    private Long xPartRef;

    @Column(name = "ebene")
    private Long ebene;

    @NotNull
    @Column(name = "sa", nullable = false)
    private Boolean sa;

    @Column(name = "xcpscii_ref")
    private Long xcpsciiRef;

    @NotNull
    @Column(name = "direkt", nullable = false)
    private Boolean direkt;

    @Column(name = "level_in_bom")
    private Integer levelInBom;

    @Length(max = 200)
    @Column(name = "idem_number_BOM", length = 200)
    private String idemNumberBom;

    @NotNull
    @Column(name = "bag_coated", nullable = false)
    private Boolean bagCoated;

    @NotNull
    @Column(name = "bag_marked", nullable = false)
    private Boolean bagMarked;

    @Column(name = "component_main_benennung_id")
    private Integer componentMainBenennungId;

    @ManyToOne
    @JoinColumn(name = "component_main_benennung_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Part componentMainBenennungEntity;

    @Column(name = "activity_area_id")
    private Integer activityAreaId;

    @ManyToOne
    @JoinColumn(name = "activity_area_id", referencedColumnName = "id", insertable = false, updatable = false)
    private ActivityArea activityAreaEntity;

    @Column(name = "fraction_main_id")
    private Integer fractionMainId;

    @ManyToOne
    @JoinColumn(name = "fraction_main_id", referencedColumnName = "id", insertable = false, updatable = false)
    private FractionMain fractionMainEntity;

    @Column(name = "material_main_id")
    private Integer materialMainId;

    @ManyToOne
    @JoinColumn(name = "material_main_id", referencedColumnName = "id", insertable = false, updatable = false)
    private MaterialMain materialMainEntity;

    @OneToOne(mappedBy = "componentMainEntity", cascade = CascadeType.REMOVE)
    private ComponentCe componentCeEntity;

    @OneToOne(mappedBy = "componentMainEntity", cascade = CascadeType.REMOVE)
    private ComponentReference componentReferenceEntity;

    @OneToOne(mappedBy = "componentMainEntity", cascade = CascadeType.REMOVE)
    private ComponentMarking componentMarkingEntity;

    @OneToOne(mappedBy = "componentMainEntity", cascade = CascadeType.REMOVE)
    private ComponentDescription componentDescriptionEntity;

    @OneToMany(mappedBy = "componentMainEntity", cascade = CascadeType.REMOVE)
    private List<ComponentPicture> componentPictures;

    @OneToMany(mappedBy = "componentMainEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "vdaBalancePk.materialGroupId")
    private Map<String ,VdaBalance> vdaBalances;

    @OneToMany(mappedBy = "componentMainEntity", cascade = CascadeType.REMOVE)
    private List<ComponentDepend> componentDepends;

    @OneToMany(mappedBy = "componentMainEntity", cascade = CascadeType.REMOVE)
    private List<ComponentConnect> componentConnects;

    @OneToMany(mappedBy = "componentMainEntity", cascade = CascadeType.REMOVE)
    private List<ComponentComp> componentComps;

    @OneToMany(mappedBy = "componentMainEntity", cascade = CascadeType.REMOVE)
    private List<Fixing> fixings;

	@Column(name = "type")
	private Type type;

    @CreatedBy
    @Column(name = "created_by")
    private String createdBy;

    @CreatedDate
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @LastModifiedBy
    @Column(name = "modified_by")
    private String modifiedBy;

    @LastModifiedDate
    @Column(name = "modified_at")
    private LocalDateTime modifiedAt;
}