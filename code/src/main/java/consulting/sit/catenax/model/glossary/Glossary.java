package consulting.sit.catenax.model.glossary;

import lombok.Data;

@Data
public class Glossary {

    private String name;
}
