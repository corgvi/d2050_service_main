package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_tool_type")
@SequenceGenerator(schema = "glossary", name = "tool_type_seq_gen", sequenceName = "tool_type_id_seq")
public class ToolType implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tool_type_seq_gen")
    private Integer id;

    @OneToMany(mappedBy = "toolTypeEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "toolTypeLanguageMapPk.language")
    private Map<String, ToolTypeLanguageMap> toolTypeLanguageMaps;

}
