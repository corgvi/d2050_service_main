package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_material_value")
@SequenceGenerator(schema = "glossary", name = "material_value_seq_gen", sequenceName = "material_value_id_seq")
public class MaterialValue implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "material_value_seq_gen")
    private Integer id;

    @Column(name = "aufbereitung")
    private Double aufbereitung;

    @Column(name = "entsorgung")
    private Double entsorgung;

    @Column(name="logistik")
    private Double logistik;

    @Column(name="demontage")
    private Double demontage;

    @Column(name="neupreis")
    private Double neupreis;

    @Column(name="recyclatpreis")
    private Double recyclatpreis;

    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private MaterialMain materialMainEntity;
}
