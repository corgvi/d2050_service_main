package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_variant_specification_language_map")
public class VariantSpecificationLanguageMap implements ModelBaseInterface<VariantSpecificationLanguageMapPk> {
    @EmbeddedId
    private VariantSpecificationLanguageMapPk variantSpecificationLanguageMapPk;

    @Length(max = 30)
    @Column(name = "value", length = 30)
    private String value;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private VariantSpecification variantSpecificationEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(VariantSpecificationLanguageMapPk id) {
        this.variantSpecificationLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public VariantSpecificationLanguageMapPk getId() {
        return this.variantSpecificationLanguageMapPk;
    }
}
