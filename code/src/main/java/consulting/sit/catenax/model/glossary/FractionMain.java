package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_fraction_main")
@SequenceGenerator(schema = "glossary", name = "fraction_main_seq_gen", sequenceName = "fraction_main_id_seq")
public class FractionMain implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fraction_main_seq_gen")
    private Integer id;

    @Column(name = "active", nullable = false)
    private boolean active;

    @OneToMany(mappedBy = "fractionMainEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "fractionMainLanguageMapPk.language")
    private Map<String, FractionMainLanguageMap> fractionMainMaps;
}
