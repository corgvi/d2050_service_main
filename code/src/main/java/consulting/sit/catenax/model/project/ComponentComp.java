package consulting.sit.catenax.model.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import consulting.sit.catenax.model.glossary.Compound;
import consulting.sit.catenax.model.glossary.MaterialMain;
import lombok.Data;
import org.hibernate.annotations.Comment;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(schema = "project", name = "p_component_comp")
@SequenceGenerator(schema = "project", name = "component_comp_seq_gen", sequenceName = "component_comp_id_seq")
public class ComponentComp implements ModelBaseInterface<Integer> {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "component_comp_seq_gen")
	private Integer id;

	@Column(name = "component_main_id")
	private Integer componentMainId;

	@ManyToOne
	@JoinColumn(name = "component_main_id", referencedColumnName = "id", insertable = false, updatable = false)
	@JsonIgnore
	private ComponentMain componentMainEntity;

	@Length(max = 1)
	@Column(name = "verbund_anteil", length = 1)
	@Comment("0 leicht lösbar, 1 schwer lösbar, 2 nicht lösbar (Anteil geschätzt), h hoch, m mittel, g gering, u unbedeutend")
	private String verbundAnteil;

	@ManyToOne
	@JoinColumn(name = "verbund_anteil", referencedColumnName = "verbundanteil", insertable = false, updatable = false)
	private Compound compoundEntity;

	@Column(name = "verbund_material")
	private Integer verbundMaterial;

	@ManyToOne
	@JoinColumn(name = "verbund_material", referencedColumnName = "id", insertable = false, updatable = false)
	private MaterialMain materialMain;

	@Column(name = "v_masse")
	private Long vMasse;

	@Column(name = "anzahl_werkstoffmarkierungen")
	private Integer anzahlWerkstoffmarkierungen;

	@Length(max = 10)
	@Column(name = "prefix", length = 10)
	private String prefix;

	@Length(max = 10)
	@Column(name = "main", length = 10)
	private String main;

	@Length(max = 10)
	@Column(name = "suffix", length = 10)
	private String suffix;

	@Length(max = 5)
	@Column(name = "rec_content", length = 5)
	private String recContent;

	@Column(name = "rec_category")
	@Comment("Recycling-Kategorie (FORD)")
	private Integer recCategory;

	@Column(name = "rec_pirpcr")
	private Integer recPirPcr;

	@Length(max = 20)
	@Column(name = "ws_mark", length = 20)
	@Comment("Werkstoffmarkierung am Verbundanteil")
	private String wsMark;

	@CreatedBy
	@Column(name = "created_by")
	private String createdBy;

	@CreatedDate
	@Column(name = "created_at")
	private LocalDateTime createdAt;

	@LastModifiedBy
	@Column(name = "modified_by")
	private String modifiedBy;

	@LastModifiedDate
	@Column(name = "modified_at")
	private LocalDateTime modifiedAt;

	@Column(name = "type")
	private Type type;

	@Column(name = "unit")
	private String unit;
}
