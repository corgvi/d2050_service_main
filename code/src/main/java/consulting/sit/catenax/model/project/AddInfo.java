package consulting.sit.catenax.model.project;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "project", name = "p_addinfo")
@SequenceGenerator(schema = "project", name = "addinfo_seq_gen", sequenceName = "addinfo_id_seq")
public class AddInfo implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "addinfo_seq_gen")
    private Integer id;

    @OneToMany(mappedBy = "addInfoEntity")
    @MapKey(name = "addInfoLanguageMapPk.language")
    private Map<String, AddInfoLanguageMap> addInfoLanguageMaps;
}
