package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_region_language_map")
public class RegionLanguageMap implements ModelBaseInterface<RegionLanguageMapPk> {
    @EmbeddedId
    private RegionLanguageMapPk regionLanguageMapPk;

    @Column(name = "value", columnDefinition = "TEXT")
    private String value;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Language languageEntity;

    @ManyToOne
    @JoinColumn(name = "region_code", referencedColumnName = "region_code", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Region regionEntity;

    @Override
    public void setId(RegionLanguageMapPk id) {
        this.regionLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public RegionLanguageMapPk getId() {
        return this.regionLanguageMapPk;
    }
}
