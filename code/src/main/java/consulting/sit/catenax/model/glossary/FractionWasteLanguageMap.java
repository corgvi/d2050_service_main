package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_fraction_waste_language_map")
public class FractionWasteLanguageMap implements ModelBaseInterface<FractionWasteLanguageMapPk> {
    @EmbeddedId
    private FractionWasteLanguageMapPk fractionWasteLanguageMapPk;

    @Length(max = 70)
    @Column(name = "value", length = 70)
    private String value;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private FractionWaste fractionWasteEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(FractionWasteLanguageMapPk id) {
        this.fractionWasteLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public FractionWasteLanguageMapPk getId() {
        return this.fractionWasteLanguageMapPk;
    }
}
