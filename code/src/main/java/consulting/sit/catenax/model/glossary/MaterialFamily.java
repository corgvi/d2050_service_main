package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_material_family")
public class MaterialFamily implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    private Integer id;

    @Length(max = 10)
    @Column(name = "ws_family", length = 10)
    private String wsFamily;

    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private MaterialMain materialMainEntity;
}
