package consulting.sit.catenax.model.glossary;

public enum TableName {
    G_DOCUMENT("g_document"),
    G_ACTIVITY_AREA("g_activity_area"),
    G_LANGUAGE("g_language"),
    G_FIXING_DROP("g_fixing_drop"),
    G_FIXING_DROP_CODE("g_fixing_drop_code"),
    G_MARK_IDENT("g_mark_ident"),
    G_MARKING("g_marking"),
    G_FRACTION_MAIN("g_fraction_main"),
    G_FIXING_LOCATION("g_fixing_location"),
    G_FIXING_MAIN("g_fixing_main"),
    G_FIXING_CHAR("g_fixing_char"),
    G_COMPOUND("g_compound"),
    G_FRACTION_WASTE("g_fraction_waste"),
    G_AIRBAG_POSITION("g_airbag_position"),
    G_MATERIAL_CHAR("g_material_char"),
    G_MATERIAL_FAMILY("g_material_family"),
    G_MATERIAL_FAMILY_VALUE("g_material_family_value"),
    G_NOTIFICATION("g_notification"),
    G_MATERIAL_VALUE("g_material_value"),
    G_MATERIAL_MAIN("g_material_main"),
    G_METHOD("g_method"),
    G_MATERIAL_GROUP("g_material_group"),
    G_POSITION("g_position"),
    G_PART("g_part"),
    G_PT_LISTING("g_pt_listing"),
    G_RQ_TREATMENT("g_rq_treatment"),
    G_RG_ISO_GROUP("g_rg_iso_group"),
    G_RG_ISO_GROUP_VALUE("g_rg_iso_group_value"),
    G_RG_MATERIAL_CLASS("g_rg_material_class"),
    G_PT_MATERIAL_MAP("g_pt_material_map"),
    G_REGION("g_region"),
    G_RG_SEPARABILITY("g_rg_separability"),
    G_RG_STRATEGY("g_rg_strategy"),
    G_RG_ISO_CALC("g_rg_iso_calc"),
    G_RG_MATERIAL_GROUP("g_rg_material_group"),
    G_PT_TREATMENT_MAP("g_pt_treatment_map"),
    G_UI("g_ui"),
    G_VARIANT_SPECIFICATION("g_variant_specification"),
    G_VARIANT_TYPE("g_variant_type"),
    G_TOOL_MAIN("g_tool_main"),
    G_TOOL_TYPE("g_tool_type")
    ;
    private final String tableName;
    TableName(final String tableName) {this.tableName = tableName;}
    public String getTableName() {
        return tableName;
    }
}
