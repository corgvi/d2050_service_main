package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_material_family_value")
@SequenceGenerator(schema = "glossary", name = "material_family_value_seq_gen", sequenceName = "material_family_value_id_seq")
public class MaterialFamilyValue implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "material_family_value_seq_gen")
    private Integer id;

    @Column(name = "neu")
    private double neu;

    @Column(name = "sekundaer")
    private double sekundaer;

    @Column(name="shredder")
    private double shredder;

    @Column(name="status")
    private double status;
}
