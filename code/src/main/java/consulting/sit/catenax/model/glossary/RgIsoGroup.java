package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_rg_iso_group")
@SequenceGenerator(schema = "glossary", name = "rg_iso_group_seq_gen", sequenceName = "rg_iso_group_id_seq")
public class RgIsoGroup implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rg_iso_group_seq_gen")
    private Integer id;

    @Length(max = 50)
    @Column(name = "bezeichnung", length = 50)
    private String bezeichnung;
}
