package consulting.sit.catenax.model.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(schema = "project", name = "p_variant")
@SequenceGenerator(schema = "project", name = "variant_seq_gen", sequenceName = "variant_id_seq")
public class Variant implements ModelBaseInterface<Integer> {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "variant_seq_gen")
	private Integer id;

	@NotNull
	@Column(name = "project_id", nullable = false)
	private int projectId;

	@ManyToOne
	@JoinColumn(name = "project_id", referencedColumnName = "id", insertable = false, updatable = false)
	@JsonIgnore
	private Project projectEntity;

	@Column(name = "variant_status")
	private Integer variantStatus;

	@Column(name = "version")
	private Integer version;

	@Column(name = "variant_type")
	private Integer variantType;

	@CreatedBy
	@Column(name = "created_by")
	private String createdBy;

	@CreatedDate
	@Column(name = "created_at")
	private LocalDateTime createdAt;

	@LastModifiedBy
	@Column(name = "modified_by")
	private String modifiedBy;

	@LastModifiedDate
	@Column(name = "modified_at")
	private LocalDateTime modifiedAt;

	@NotNull
	@Column(name = "dismantling_study", nullable = false)
	private Boolean dismantlingStudy;

	@Column(name = "untersuchung_start")
	private LocalDateTime untersuchungStart;

	@Column(name = "untersuchung_ende")
	private LocalDateTime untersuchungEnde;

	@Column(name = "description", columnDefinition = "TEXT")
	private String description;

	@OneToOne(mappedBy = "variantEntity", cascade = CascadeType.REMOVE)
	private VariantDetail variantDetailEntity;

	@OneToMany(mappedBy = "variantEntity", cascade = CascadeType.REMOVE)
	@JsonIgnore
	private List<ComponentMain> componentMains;

	@OneToMany(mappedBy = "variantEntity", cascade = CascadeType.REMOVE)
	private List<VariantAirbag> variantAirbags;

	@Column(name = "name")
	private String name;
}
