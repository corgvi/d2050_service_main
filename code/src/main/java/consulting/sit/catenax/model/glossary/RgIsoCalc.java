package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_rg_iso_calc")
@SequenceGenerator(schema = "glossary", name = "rg_iso_calc_seq_gen", sequenceName = "rg_iso_calc_id_seq")
public class RgIsoCalc implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rg_iso_calc_seq_gen")
    private Integer id;

    @Column(name = "calc_group_value")
    private Integer calcGroupValue;

    @Column(name = "treeview", nullable = false)
    private boolean treeview;

    @Column(name = "mandatory_removal", nullable = false)
    private boolean mandatoryRemoval;

    @OneToMany(mappedBy = "rgIsoCalcEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "rgIsoCalcLanguageMapPk.language")
    private Map<String, RgIsoCalcLanguageMap> rgIsoCalcLanguageMaps;
}
