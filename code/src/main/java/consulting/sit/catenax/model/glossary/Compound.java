package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_compound")
public class Compound implements ModelBaseInterface<String> {
    @Id
    @Length(min = 1, max = 1)
    @Column(name = "verbundanteil", length = 1)
    private String verbundanteil;

    @Column(name = "min_verbund")
    private double minVerbund;

    @Column(name = "max_verbund")
    private double maxVerbund;

    @OneToMany(mappedBy = "compoundEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "compoundLanguageMapPk.language")
    private Map<String, CompoundLanguageMap> compoundLanguageMaps;

    @Override
    public void setId(String id) {
        this.verbundanteil = id;
    }

    @Override
    @JsonIgnore
    public String getId() {
        return this.verbundanteil;
    }
}
