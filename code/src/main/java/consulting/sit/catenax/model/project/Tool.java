package consulting.sit.catenax.model.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "project", name = "p_tool")
@SequenceGenerator(schema = "project", name = "tool_seq_gen", sequenceName = "tool_id_seq")
public class Tool implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tool_seq_gen")
    private Integer id;

    @Column(name = "component_id")
    private Integer componentId;

    @ManyToOne
    @JoinColumn(name = "component_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private ComponentMain componentMainEntity;

    @Length(max = 30)
    @Column(name = "werkzeug", length = 30)
    private String werkzeug;
}
