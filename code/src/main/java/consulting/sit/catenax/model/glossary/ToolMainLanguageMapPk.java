package consulting.sit.catenax.model.glossary;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class ToolMainLanguageMapPk implements Serializable {
    @Column(name = "id")
    private int id;

    @Length(max = 2)
    @Column(name = "language", length = 2)
    private String language;
}
