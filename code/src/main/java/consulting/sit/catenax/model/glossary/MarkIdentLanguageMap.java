package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_mark_ident_language_map")
public class MarkIdentLanguageMap implements ModelBaseInterface<MarkIdentLanguageMapPk> {
    @EmbeddedId
    private MarkIdentLanguageMapPk markIdentLanguageMapPk;

    @Length(max = 50)
    @Column(name = "value", length = 50)
    private String value;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private MarkIdent markIdentEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(MarkIdentLanguageMapPk id) {
        this.markIdentLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public MarkIdentLanguageMapPk getId() {
        return this.markIdentLanguageMapPk;
    }
}
