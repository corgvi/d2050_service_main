package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_rg_strategy_language_map")
public class RgStrategyLanguageMap implements ModelBaseInterface<RgStrategyLanguageMapPk> {
    @EmbeddedId
    private RgStrategyLanguageMapPk rgStrategyLanguageMapPk;

    @Column(name = "value", columnDefinition = "TEXT")
    private String value;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private RgStrategy rgStrategyEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(RgStrategyLanguageMapPk id) {
        this.rgStrategyLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public RgStrategyLanguageMapPk getId() {
        return this.rgStrategyLanguageMapPk;
    }
}
