package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_airbag_position")
@SequenceGenerator(schema = "glossary", name = "airbag_position_seq_gen", sequenceName = "airbag_position_id_seq")
public class AirbagPosition implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "airbag_position_seq_gen")
    private Integer id;

    @Column(name = "bezeichnung")
    private String bezeichnung;

    // TODO: Evtl. noch Languages...
}
