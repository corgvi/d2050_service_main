package consulting.sit.catenax.model.glossary;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class RegionLanguageMapPk implements Serializable {
    @Length(min = 2, max = 2)
    @Column(name = "region_code", length=2)
    private String regionCode;

    @Length(min = 2, max = 2)
    @Column(name = "language", length=2)
    private String language;
}
