package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_mark_ident")
@SequenceGenerator(schema = "glossary", name = "mark_ident_seq_gen", sequenceName = "mark_ident_id_seq")
public class MarkIdent implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mark_ident_seq_gen")
    private Integer id;

    @Column(name = "active", nullable = false)
    private boolean active;

    @OneToMany(mappedBy = "markIdentEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "markIdentLanguageMapPk.language")
    private Map<String, MarkIdentLanguageMap> markIdentLanguageMaps;
}
