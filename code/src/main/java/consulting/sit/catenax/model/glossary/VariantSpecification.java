package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_variant_specification")
@SequenceGenerator(schema = "glossary", name = "variant_type_seq_gen", sequenceName = "variant_type_id_seq")
public class VariantSpecification implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "variant_type_seq_gen")
    private Integer id;

    @Column(name = "active", nullable = false)
    private boolean active;

    @OneToMany(mappedBy = "variantSpecificationEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "variantSpecificationLanguageMapPk.language")
    private Map<String, VariantSpecificationLanguageMap> variantSpecificationLanguageMaps;
}
