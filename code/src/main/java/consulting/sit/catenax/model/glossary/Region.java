package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_region")
public class Region implements ModelBaseInterface<String> {
    @Length(min = 2, max = 2)
    @Id
    @Column(name = "region_code", length = 2)
    private String regionCode;

    @Column(name = "flag")
    private byte[] flag;

    @Column(name = "disclaimer_active", nullable = false)
    private boolean disclaimerActive;

    @Column(name = "disclaimer_text_id")
    private Integer disclaimerTextId;

    @ManyToOne
    @JoinColumn(name = "disclaimer_text_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Ui uiEntity;

    @OneToMany(mappedBy = "regionEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "regionLanguageMapPk.language")
    private Map<String, RegionLanguageMap> regionLanguageMaps;

    @Override
    public void setId(String id) {
        this.regionCode = id;
    }

    @Override
    public String getId() {
        return this.regionCode;
    }
}