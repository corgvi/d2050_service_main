package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_pt_material_map")
public class PtMaterialMap implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private PtListing ptListingEntity;

    @Column(name = "target_mat")
    private Integer targetMat;

    @ManyToOne
    @JoinColumn(name = "target_mat", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private MaterialMain materialMainEntity;
}
