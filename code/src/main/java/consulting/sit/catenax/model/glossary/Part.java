package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.Comment;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_part")
@SequenceGenerator(schema = "glossary", name = "part_seq_gen", sequenceName = "part_id_seq")
public class Part implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "part_seq_gen")
    private Integer id;

    @Column(name = "default_picture")
    private byte[] defaultPicture;

    @NotNull
    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @Column(name = "caution", columnDefinition = "TEXT")
    private String caution;

    @Column(name = "comment", columnDefinition = "TEXT")
    private String comment;

    @Column(name = "icon_path", columnDefinition = "TEXT")
    private String iconPath;

    @Length(max = 200)
    @Column(name = "gu_id", length = 200)
    @Comment("Index ISO 18542-2 ID")
    private String guId;

    @OneToMany(mappedBy = "partEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "partLanguageMapPk.language")
    private Map<String, PartLanguageMap> partLanguageMaps;
}
