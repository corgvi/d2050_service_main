package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_ui")
@SequenceGenerator(schema = "glossary", name = "ui_seq_gen", sequenceName = "ui_id_seq")
public class Ui implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ui_seq_gen")
    private Integer id;

    @Column(name = "text", columnDefinition = "TEXT")
    private String text;

    @OneToMany(mappedBy = "uiEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "uiLanguageMapPk.language")
    private Map<String, UiLanguageMap> uiLanguageMaps;
}
