package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_document_language_map")
public class DocumentLanguageMap implements ModelBaseInterface<DocumentLanguageMapPk> {
    @EmbeddedId
    private DocumentLanguageMapPk documentLanguageMapPk;

    @Column(name = "document_text", columnDefinition = "TEXT")
    private String documentText;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Language languageEntity;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Document documentEntity;

    public void setId(DocumentLanguageMapPk id) {
        this.documentLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public DocumentLanguageMapPk getId() {
        return this.documentLanguageMapPk;
    }
}
