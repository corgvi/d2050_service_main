package consulting.sit.catenax.model.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "project", name = "p_component_ce")
public class ComponentCe implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "component_ce_seq_gen")
    private Integer id;

    @Column(name = "component_id")
    private Integer componentId;

    @OneToOne
    @JoinColumn(name = "component_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private ComponentMain componentMainEntity;

    @Column(name = "ke1")
    private Long ke1;

    @Length(max = 50)
    @Column(name = "ke1_mat", length = 50)
    private String ke1Mat;

    @Column(name = "ke1_zeit")
    private Long ke1Zeit;

    @Length(max = 50)
    @Column(name = "ke1_masse", length = 50)
    private String ke1Masse;

    @Column(name = "ke2")
    private Long ke2;

    @Column(name = "ke2_zeit")
    private Long ke2Zeit;

    @Column(name = "s_time")
    private Long sTime;

    @Column(name = "masse_zeit")
    private Double masseZeit;

    @Length(max = 8)
    @Column(name = "zeit", length = 8)
    private String zeit;

    @Length(max = 8)
    @Column(name = "zeit_ist", length = 8)
    private String zeitIst;

    @Length(max = 8)
    @Column(name = "zeit_soll", length = 8)
    private String zeitSoll;
}
