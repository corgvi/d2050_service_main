package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_marking_language_map")
public class MarkingLanguageMap implements ModelBaseInterface<MarkingLanguageMapPk> {
    @EmbeddedId
    private MarkingLanguageMapPk markingLanguageMapPk;

    @Length(max = 50)
    @Column(name = "value", length = 50)
    private String value;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private Marking markingEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(MarkingLanguageMapPk id) {
        this.markingLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public MarkingLanguageMapPk getId() {
        return this.markingLanguageMapPk;
    }
}
