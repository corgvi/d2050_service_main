package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_material_char")
@SequenceGenerator(schema = "glossary", name = "material_char_seq_gen", sequenceName = "material_char_id_seq")
public class MaterialChar implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "material_char_seq_gen")
    private Integer id;

    @Column(name = "m_2_ml")
    private Double m2Ml;

    @Column(name = "inert_anteil")
    private Double inertAnteil;

    @Column(name = "gruppennummer_intern")
    private Long gruppennummerIntern;

    @Column(name = "identifiziert")
    private Boolean identifiziert;

    @Length(max = 250)
    @Column(name = "anmerkung", length = 250)
    private String anmerlung;

    @Column(name="dichte")
    private Double dichte;

    @Column(name="elv")
    private Boolean elv;

    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private MaterialMain materialMainEntity;
}
