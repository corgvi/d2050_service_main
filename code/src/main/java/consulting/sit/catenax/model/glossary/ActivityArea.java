package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_activity_area")
@SequenceGenerator(schema = "glossary", name = "activity_area_seq_gen", sequenceName = "activity_area_id_seq")
public class ActivityArea implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "activity_area_seq_gen")
    @Setter(AccessLevel.NONE)
    private Integer id;

    @Column(name = "is_dirty")
    private Boolean isDirty;

    @OneToMany(mappedBy = "activityAreaEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "activityAreaLanguageMapPk.language")
    private Map<String, ActivityAreaLanguageMap> activityAreaLanguageMaps;

    public void setId(Integer id) {
        this.id = id;
    }
}