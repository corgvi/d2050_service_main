package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_fixing_char")
@SequenceGenerator(schema = "glossary", name = "fixing_char_seq_gen", sequenceName = "fixing_char_id_seq")
public class FixingChar implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fixing_char_seq_gen")
    private Integer id;

    @Length(max = 15)
    @Column(name = "loesbarkeit", length = 15)
    private String loesbarkeit;

    @Length(max = 30)
    @Column(name = "schlussart", length = 30)
    private String schlussart;

    @Length(max = 26)
    @Column(name = "verbindungsart", length = 26)
    private String verbindungsart;

    @Length(max = 75)
    @Column(name = "erlaeuterung", length = 75)
    private String erlaeuterung;
}
