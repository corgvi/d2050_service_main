package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_material_main")
@SequenceGenerator(schema = "glossary", name = "material_main_seq_gen", sequenceName = "material_main_id_seq")
public class MaterialMain implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "material_main_seq_gen")
    private Integer id;

    @OneToMany(mappedBy = "materialMainEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "materialMainLanguageMapPk.language")
    private Map<String, MaterialMainLanguageMap> materialMainLanguageMaps;

    @Column(name = "g_material_group_id")
    private Integer materialGroupId;

    @OneToOne(mappedBy = "materialMainEntity", cascade = CascadeType.REMOVE)
    private MaterialGroup materialGroupEntity;

    @OneToOne(mappedBy = "materialMainEntity", cascade = CascadeType.REMOVE)
    private MaterialValue materialValueEntity;

    @OneToOne(mappedBy = "materialMainEntity", cascade = CascadeType.REMOVE)
    private MaterialFamily materialFamilyEntity;

    @OneToOne(mappedBy = "materialMainEntity", cascade = CascadeType.REMOVE)
    private MaterialChar materialCharEntity;
}
