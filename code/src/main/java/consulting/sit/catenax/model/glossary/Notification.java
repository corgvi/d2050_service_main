package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_notification")
@SequenceGenerator(schema = "glossary", name = "notification_seq_gen", sequenceName = "notification_id_seq")
public class Notification implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notification_seq_gen")
    private Integer id;

    @Column(name = "active", nullable = false)
    private boolean active;

    @OneToMany(mappedBy = "notificationEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "notificationLanguageMapPk.language")
    private Map<String, NotificationLanguageMap> notificationLanguageMaps;
}
