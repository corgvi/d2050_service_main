package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_fixing_location")
public class FixingLocation implements ModelBaseInterface<String> {
    @Length(min = 0,max = 5)
    @Id
    @Column(name = "x_lage_ve", length = 5)
    private String xLageVe;

    @OneToMany(mappedBy = "fixingLocationEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "fixingLocationLanguageMapPk.language")
    private Map<String, FixingLocationLanguageMap> fixingLocationLanguageMaps;

    @Override
    public void setId(String id) {
        this.xLageVe = id;
    }

    @Override
    public String getId() {
        return this.xLageVe;
    }
}
