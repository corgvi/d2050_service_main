package consulting.sit.catenax.model.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import consulting.sit.catenax.model.glossary.AirbagPosition;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "project", name = "p_variant_airbag")
@SequenceGenerator(schema = "project", name = "variant_airbag_seq_gen", sequenceName = "variant_airbag_id_seq")
public class VariantAirbag implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "variant_airbag_seq_gen")
    private Integer id;

    @Column(name = "variant_id")
    private Integer variantId;

    @ManyToOne
    @JoinColumn(name = "variant_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private Variant variantEntity;

    @Column(name = "airbag_position_id")
    private Integer airbagPositionId;

    @ManyToOne
    @JoinColumn(name = "airbag_position_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private AirbagPosition airbagPosition;

    @Column(name = "existing", nullable = false)
    private Boolean existing;

    @Length(max = 20)
    @Column(name = "typ", length = 20)
    private String typ;
}
