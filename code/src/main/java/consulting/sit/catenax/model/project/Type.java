package consulting.sit.catenax.model.project;

public enum Type {
    COMPONENT,
    MATERIAL,
    SEMI_COMPONENT,
    SUBSTANCE
}
