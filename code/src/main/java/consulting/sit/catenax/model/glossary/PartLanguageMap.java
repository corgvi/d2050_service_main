package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_part_language_map")
public class PartLanguageMap implements ModelBaseInterface<PartLanguageMapPk> {
    @EmbeddedId
    private PartLanguageMapPk partLanguageMapPk;

    @Column(name = "value", columnDefinition = "TEXT")
    private String value;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private Part partEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(PartLanguageMapPk id) {
        this.partLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public PartLanguageMapPk getId() {
        return this.partLanguageMapPk;
    }
}
