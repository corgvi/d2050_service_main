package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_language")
public class Language implements ModelBaseInterface<String> {
    @Length(min = 2, max = 2)
    @Id
    @Column(name = "language", length = 2)
    private String language;

    @Column(name = "active", nullable = false)
    private boolean active;

    @Column(name = "flag")
    private byte[] flag;

    @OneToMany(mappedBy = "languageEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "languageLanguageMapPk.language")
    private Map<String, LanguageLanguageMap> languageLanguageMaps;

    @Override
    public void setId(String id) {
        this.language = id;
    }

    @Override
    public String getId() {
        return this.language;
    }
}
