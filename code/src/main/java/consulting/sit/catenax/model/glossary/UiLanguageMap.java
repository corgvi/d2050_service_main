package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_ui_language_map")
public class UiLanguageMap implements ModelBaseInterface<UiLanguageMapPk> {
    @EmbeddedId
    private UiLanguageMapPk uiLanguageMapPk;

    @Column(name = "text", columnDefinition = "TEXT")
    private String text;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Ui uiEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(UiLanguageMapPk id) {
        this.uiLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public UiLanguageMapPk getId() {
        return this.uiLanguageMapPk;
    }
}
