package consulting.sit.catenax.model.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import consulting.sit.catenax.model.glossary.FixingLocationLanguageMapPk;
import consulting.sit.catenax.model.glossary.MaterialGroup;
import lombok.Data;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(schema = "project", name = "p_vda_balance")
@EntityListeners(AuditingEntityListener.class)
public class VdaBalance implements ModelBaseInterface<VdaBalancePk> {
    @EmbeddedId
    private VdaBalancePk vdaBalancePk;

    @ManyToOne
    @JoinColumn(name = "component_id", referencedColumnName = "id", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private ComponentMain componentMainEntity;

    @ManyToOne
    @JoinColumn(name = "g_material_group_id", referencedColumnName = "id", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private MaterialGroup materialGroupEntity;

    @Column(name = "component_id", insertable = false, updatable = false)
    private Integer componentId;

    @Column(name = "g_material_group_id", insertable = false, updatable = false)
    private Integer materialGroupId;

    @Column(name = "value")
    @Comment("kg")
    private Double value;

    @CreatedBy
    @Column(name = "created_by")
    private String createdBy;

    @CreatedDate
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @LastModifiedBy
    @Column(name = "modified_by")
    private String modifiedBy;

    @LastModifiedDate
    @Column(name = "modified_at")
    private LocalDateTime modifiedAt;

    @Override
    public void setId(VdaBalancePk id) {
        this.vdaBalancePk = id;
    }

    @Override
    @JsonIgnore
    public VdaBalancePk getId() {
        return this.vdaBalancePk;
    }
}
