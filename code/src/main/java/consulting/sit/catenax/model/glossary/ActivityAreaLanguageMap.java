package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_activity_area_language_map")
public class ActivityAreaLanguageMap implements ModelBaseInterface<ActivityAreaLanguageMapPk> {
    @EmbeddedId
    private ActivityAreaLanguageMapPk activityAreaLanguageMapPk;

    @Length(max = 30)
    @Column(name = "value", length = 30)
    private String value;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Language languageEntity;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private ActivityArea activityAreaEntity;

    @Override
    public void setId(ActivityAreaLanguageMapPk id) {
        this.activityAreaLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public ActivityAreaLanguageMapPk getId() {
        return this.activityAreaLanguageMapPk;
    }
}
