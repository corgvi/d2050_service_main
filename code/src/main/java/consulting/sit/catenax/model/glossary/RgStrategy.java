package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_rg_strategy")
@SequenceGenerator(schema = "glossary", name = "rg_strategy_seq_gen", sequenceName = "rg_strategy_id_seq")
public class RgStrategy implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rg_strategy_seq_gen")
    private Integer id;

    @Column(name = "treeview", nullable = false)
    private boolean treeview;

    @Column(name = "mandatory_removal", nullable = false)
    private boolean mandatoryRemoval;

    @Column(name = "standard", nullable = false)
    private boolean standard;

    @Column(name = "treatment_map")
    private Integer treatmentMap;

    @ManyToOne
    @JoinColumn(name = "treatment_map", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private RqTreatment rqTreatmentEntity;

    @Column(name = "aktiv", nullable = false)
    private boolean aktive;

    @Column(name = "share", nullable = false)
    private boolean share;

    @OneToMany(mappedBy = "rgStrategyEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "rgStrategyLanguageMapPk.language")
    private Map<String, RgStrategyLanguageMap> rgStrategyLanguageMaps;
}
