package consulting.sit.catenax.model.project;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.Comment;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "project", name = "p_component_notification")
@SequenceGenerator(schema = "project", name = "component_notification_seq_gen", sequenceName = "component_notification_id_seq")
public class ComponentNotification implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "component_notification_seq_gen")
    private Integer id;

    @Length(max = 255)
    @Column(name = "bemerkung", length = 255)
    private String bemerkung;

    @Column(name = "info1")
    @Comment("Die formschlüssige Verbindung ist in das Bauteil integriert")
    private Boolean info1;

    @Column(name = "info2")
    @Comment("Die Steckverbindung ist in das Bauteil integriert")
    private Boolean info2;

    @Column(name = "info3")
    @Comment("Die Klammer ist in das Bauteil integriert")
    private Boolean info3;

    @Column(name = "info4")
    @Comment("Das Bauteil ist nach dem Prinzip Einhängen und Sichern befestigt")
    private Boolean info4;

    @Column(name = "info5")
    @Comment("Das Bauteil ist nach dem Prinzip Einhängen und Sichern befestigt")
    private Boolean info5;

    @Column(name = "info6")
    @Comment("Das Bauteil wird durch ein anderes Bauteil gehalten")
    private Boolean info6;

    @Column(name = "info7")
    @Comment("Die formschlüssige Verbindung ist zusätzlich geklebt")
    private Boolean info7;

    @Column(name = "info8")
    @Comment("Es verbleibt eine Restmenge einer Betriebsflüssigkeit im Bauteil")
    private Boolean info8;
}
