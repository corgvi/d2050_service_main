package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_position")
@SequenceGenerator(schema = "glossary", name = "position_seq_gen", sequenceName = "position_id_seq")
public class Position implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "position_seq_gen")
    private Integer id;

    @Column(name = "sortorder")
    private Integer sortorder;

    @OneToMany(mappedBy = "positionEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "positionLanguageMapPk.language")
    private Map<String, PositionLanguageMap> positionLanguageMaps;
}