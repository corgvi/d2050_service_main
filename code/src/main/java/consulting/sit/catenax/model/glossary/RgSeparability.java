package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_rg_separability")
@SequenceGenerator(schema = "glossary", name = "rg_separability_seq_gen", sequenceName = "rg_separability_id_seq")
public class RgSeparability implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rg_separability_seq_gen")
    private Integer id;

    @Column(name = "id_combination")
    private Integer idCombination;

    @Column(name = "value", nullable = false)
    private boolean value;

    @OneToMany(mappedBy = "rgSeparabilityEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "rgSeparabilityLanguageMapPk.language")
    private Map<String, RgSeparabilityLanguageMap> rgSeparabilityLanguageMaps;
}
