package consulting.sit.catenax.model.project;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class ComponentConnectPk implements Serializable {
    @Column(name = "fixing_id")
    private int fixingId;

    @Column(name = "component_id")
    private int componentId;
}
