package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_marking")
@SequenceGenerator(schema = "glossary", name = "marking_seq_gen", sequenceName = "marking_id_seq")
public class Marking implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "marking_seq_gen")
    private Integer id;

    @OneToMany(mappedBy = "markingEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "markingLanguageMapPk.language")
    private Map<String, MarkingLanguageMap> markingLanguageMaps;
}
