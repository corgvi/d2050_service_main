package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_fixing_drop_code")
@SequenceGenerator(schema = "glossary", name = "fixing_drop_code_seq_gen", sequenceName = "fixing_drop_code_id_seq")
public class FixingDropCode implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fixing_code_seq_gen")
    private Integer id;

    @Column(name = "fixing_drop_id")
    private Integer fixingDropId;

    @OneToOne
    @JoinColumn(name = "fixing_drop_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private FixingDrop fixingDropEntity;

    @OneToMany(mappedBy = "fixingDropCodeEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "fixingDropCodeLanguageMapPk.language")
    private Map<String, FixingDropCodeLanguageMap> fixingDropCodeLanguageMaps;
}
