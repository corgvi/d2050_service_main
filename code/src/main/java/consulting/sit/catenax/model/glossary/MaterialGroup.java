
package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_material_group")
public class MaterialGroup implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "vda_hg")
    private Integer vdaHg;

    @Column(name = "vda_ug")
    private String vdaUg;

    @Length(max = 255)
    @Column(name="standard", length = 255)
    private String standard;

    @Length(max = 255)
    @Column(name="standard_nummer", length = 255)
    private String standardNummer;

    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private MaterialMain materialMainEntity;
}
