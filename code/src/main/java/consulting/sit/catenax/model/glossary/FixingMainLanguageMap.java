package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(schema = "glossary", name = "g_fixing_main_language_map")
public class FixingMainLanguageMap implements ModelBaseInterface<FixingMainLanguageMapPk> {
    @EmbeddedId
    private FixingMainLanguageMapPk fixingMainLanguageMapPk;

    @NotNull
    @Column(name = "value", columnDefinition = "TEXT")
    private String value;

    @Column(name = "code", columnDefinition = "TEXT")
    private String code;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private FixingMain fixingMainEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(FixingMainLanguageMapPk id) {
        this.fixingMainLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public FixingMainLanguageMapPk getId() {
        return this.fixingMainLanguageMapPk;
    }
}
