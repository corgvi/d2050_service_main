package consulting.sit.catenax.model.project;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class VdaBalancePk implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1549941086700382475L;

	@Column(name = "component_id")
    private int componentId;

    @Column(name = "g_material_group_id")
    private int materialGroupId;
}
