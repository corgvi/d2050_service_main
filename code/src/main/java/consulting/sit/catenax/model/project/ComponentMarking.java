package consulting.sit.catenax.model.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import consulting.sit.catenax.model.glossary.MarkIdent;
import lombok.Data;
import org.hibernate.annotations.Comment;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(schema = "project", name = "p_component_marking")
@SequenceGenerator(schema = "project", name = "component_marking_seq_gen", sequenceName = "component_marking_id_seq")
public class ComponentMarking implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "component_marking_seq_gen")
    private Integer id;

    @Column(name = "component_id")
    private Integer componentId;

    @OneToOne
    @JoinColumn(name = "component_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private ComponentMain componentMainEntity;

    @Column(name = "ws_marked")
    @Comment("Werkstoffkennzeichnung vorhanden (Anzahl)")
    private Integer wsMarked;

    @Column(name = "ws_sichtbar")
    @Comment("Werkstoffkennzeichnung sichtbar (Begründung)")
    private Integer wsSichtbar;

    @Column(name = "ws_marked_ok")
    @Comment("wenn Werkstoffkennzeichnung vorhanden, ist diese korrekt ????")
    private Boolean wsMarkedOk;

    @Length(max = 50)
    @Column(name = "ws_hersteller", length = 50)
    @Comment("Werkstoff laut Herstellerangabe (Kennzeichnung auf Bauteil)")
    private String wsHersteller;

    @Column(name = "identified", nullable = false)
    private Boolean identified;

    @Column(name = "identified_type")
    private Integer identifiedType;

    @ManyToOne
    @JoinColumn(name = "identified_type", referencedColumnName = "id", insertable = false, updatable = false)
    private MarkIdent markIdentEntity;

    @CreatedBy
    @Column(name = "created_by")
    private String createdBy;

    @CreatedDate
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @LastModifiedBy
    @Column(name = "modified_by")
    private String modifiedBy;

    @LastModifiedDate
    @Column(name = "modified_at")
    private LocalDateTime modifiedAt;
}
