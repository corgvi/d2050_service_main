package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_rg_separability_language_map")
public class RgSeparabilityLanguageMap implements ModelBaseInterface<RgSeparabilityLanguageMapPk> {
    @EmbeddedId
    private RgSeparabilityLanguageMapPk rgSeparabilityLanguageMapPk;

    @Column(name = "value", columnDefinition = "TEXT")
    private String value;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private RgSeparability rgSeparabilityEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(RgSeparabilityLanguageMapPk id) {
        this.rgSeparabilityLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public RgSeparabilityLanguageMapPk getId() {
        return this.rgSeparabilityLanguageMapPk;
    }
}
