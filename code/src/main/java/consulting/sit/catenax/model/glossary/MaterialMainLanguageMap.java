package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_material_main_language_map")
public class MaterialMainLanguageMap implements ModelBaseInterface<MaterialMainLanguageMapPk> {
    @EmbeddedId
    private MaterialMainLanguageMapPk materialMainLanguageMapPk;

    @Length(max = 50)
    @Column(name = "value", length = 50)
    private String value;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private MaterialMain materialMainEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(MaterialMainLanguageMapPk id) {
        this.materialMainLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public MaterialMainLanguageMapPk getId() {
        return this.materialMainLanguageMapPk;
    }
}
