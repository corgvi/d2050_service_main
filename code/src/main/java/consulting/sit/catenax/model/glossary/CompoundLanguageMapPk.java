package consulting.sit.catenax.model.glossary;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class CompoundLanguageMapPk implements Serializable {
    @Length(min = 1, max = 1)
    @Column(name = "verbundanteil", length = 1)
    private String verbundanteil;

    @Length(min = 2, max = 2)
    @Column(name = "language", length = 2)
    private String language;
}
