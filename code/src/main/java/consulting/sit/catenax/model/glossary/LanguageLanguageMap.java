package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_language_language_map")
public class LanguageLanguageMap implements ModelBaseInterface<LanguageLanguageMapPk> {
    @EmbeddedId
    private LanguageLanguageMapPk languageLanguageMapPk;

    @Column(name = "value", columnDefinition = "TEXT")
    private String value;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Language languageEntity;

    @ManyToOne
    @JoinColumn(name = "translation_language", referencedColumnName = "language", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Language translationLanguageEntity;

    @Override
    public void setId(LanguageLanguageMapPk id) {
        this.languageLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public LanguageLanguageMapPk getId() {
        return this.languageLanguageMapPk;
    }
}
