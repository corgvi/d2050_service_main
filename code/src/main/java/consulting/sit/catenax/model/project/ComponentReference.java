package consulting.sit.catenax.model.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(schema = "project", name = "p_component_reference")
@SequenceGenerator(schema = "project", name = "component_reference_seq_gen", sequenceName = "component_reference_id_seq")
public class ComponentReference implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "component_reference_seq_gen")
    private Integer id;

    @Column(name = "component_id")
    private Integer componentId;

    @OneToOne
    @JoinColumn(name = "component_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private ComponentMain componentMainEntity;

    @Length(max = 25)
    @Column(name = "teile_nummer", length = 25)
    @JsonProperty(value = "partNo")
    private String teileNummer;

    @Length(max = 50)
    @Column(name = "katalog_baugruppe", length = 50)
    @JsonProperty(value = "catalogueModule")
    private String katalogBaugruppe;


    @Length(max = 50)
    @Column(name = "katalog_ref", length = 50)
    @JsonProperty(value = "catalogueReference")
    private String katalogRef;

    @CreatedBy
    @Column(name = "created_by")
    private String createdBy;

    @CreatedDate
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @LastModifiedBy
    @Column(name = "modified_by")
    private String modifiedBy;

    @LastModifiedDate
    @Column(name = "modified_at")
    private LocalDateTime modifiedAt;
}
