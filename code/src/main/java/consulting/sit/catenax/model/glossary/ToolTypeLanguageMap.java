package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_tool_type_language_map")
public class ToolTypeLanguageMap implements ModelBaseInterface<ToolTypeLanguageMapPk> {
    @EmbeddedId
    private ToolTypeLanguageMapPk toolTypeLanguageMapPk;

    @Length(max = 255)
    @Column(name = "value", length = 255)
    private String value;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private ToolType toolTypeEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(ToolTypeLanguageMapPk id) {
        this.toolTypeLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public ToolTypeLanguageMapPk getId() {
        return this.toolTypeLanguageMapPk;
    }
}
