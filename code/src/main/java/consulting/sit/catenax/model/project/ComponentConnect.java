package consulting.sit.catenax.model.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "project", name = "p_component_connect")
public class ComponentConnect implements ModelBaseInterface<ComponentConnectPk> {
    @EmbeddedId
    private ComponentConnectPk componentConnectPk;

    @ManyToOne
    @JoinColumn(name = "component_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private ComponentMain componentMainEntity;


    @ManyToOne
    @JoinColumn(name = "fixing_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private Fixing fixingEntity;

    @Override
    public void setId(ComponentConnectPk id) {
        this.componentConnectPk = id;
    }

    @Override
    @JsonIgnore
    public ComponentConnectPk getId() {
        return this.componentConnectPk;
    }
}
