package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_rq_treatment")
@SequenceGenerator(schema = "glossary", name = "rg_treatment_seq_gen", sequenceName = "rg_treatment_id_seq")
public class RqTreatment implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rg_treatment_seq_gen")
    private Integer id;

    @Column(name = "real", nullable = false)
    private boolean real;

    @Column(name = "treatment_cost")
    private Double treatmentCost;

    @Column(name = "disposal_cost")
    private Double disposalCost;

    @Column(name = "logistic_cost")
    private Double logisticCost;

    @OneToMany(mappedBy = "rgTreatmentEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "rqTreatmentLanguageMapPk.language")
    private Map<String, RqTreatmentLanguageMap> rqTreatmentLanguageMaps;
}
