package consulting.sit.catenax.model.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import consulting.sit.catenax.model.glossary.Language;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "project", name = "p_addinfo_language_map")
public class AddInfoLanguageMap implements ModelBaseInterface<AddInfoLanguageMapPk> {
    @EmbeddedId
    private AddInfoLanguageMapPk addInfoLanguageMapPk;

    @Column(name = "checksum")
    private String checksum;

    @Column(name = "original_filename")
    private String originalFilename;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Language languageEntity;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private AddInfo addInfoEntity;

    @Override
    public void setId(AddInfoLanguageMapPk id) {
        this.addInfoLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public AddInfoLanguageMapPk getId() {
        return this.addInfoLanguageMapPk;
    }
}
