package consulting.sit.catenax.model.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.Comment;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(schema = "project", name = "p_component_picture")
public class ComponentPicture implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private ComponentMain componentMainEntity;

    @Column(name = "bemerkung_bild")
    private String bemerkungBild;

    @Column(name = "bild")
    private byte[] bild;

    @Length(max = 255)
    @Column(name = "dateiname", length = 255)
    private String dateiname;

    @Column(name = "bewertung")
    @Comment("0 = neg, 1 = pos, 2 = indifferent (nur Typ-0-Bilder)")
    private Integer bewertung;

    @Column(name = "l")
    private Double l;

    @Column(name = "b")
    private Double b;

    @NotNull
    @Column(name = "double_layer", nullable = false)
    private boolean doubleLayer;

    @NotNull
    @Column(name = "bil", nullable = false)
    private boolean bil;

    @Column(name = "platinen_nummer")
    private Integer platinenNummer;

    @Column(name = "image_url")
    private String imageUrl;
}
