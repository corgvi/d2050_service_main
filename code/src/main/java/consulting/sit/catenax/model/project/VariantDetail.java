package consulting.sit.catenax.model.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Entity
@Table(schema = "project", name = "p_variant_detail")
public class VariantDetail implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id")
    @JsonIgnore
    private Variant variantEntity;

    // TODO: Hersteller in Manufacturer-Tabelle auslagern?
    @Length(max = 20)
    @Column(name = "hersteller", length = 20)
    private String hersteller;

    @Length(max = 20)
    @Column(name = "fahrzeugbezeichnung", length = 20)
    private String fahrzeugbezeichnung;

    @Length(max = 20)
    @Column(name = "karosserie", length = 20)
    private String karosserie;

    @Length(max = 50)
    @Column(name = "fahrzeug_typ", length = 50)
    private String fahrzeugTyp;

    @Length(max = 20)
    @Column(name = "fahrgestell_nummer", length = 20)
    private String fahrgestellNr;

    @Length(max = 30)
    @Column(name = "motor_typ", length = 30)
    private String motorTyp;

    @Length(max = 20)
    @Column(name = "motor_nummer", length = 20)
    private String motorNummer;

    @Length(max = 30)
    @Column(name = "getriebe_typ", length = 30)
    private String getriebeTyp;

    @Length(max = 20)
    @Column(name = "getriebe_nummer", length = 20)
    private String getriebeNummer;

    @Length(max = 20)
    @Column(name = "herstellung", length = 20)
    private String herstellung;

    @Column(name = "leergewicht_praktisch")
    private Integer leergewichtPraktisch;

    @Column(name = "leergewicht_theoretisch")
    private Integer leergewichtTheoretisch;

    @Column(name = "radlast_vl")
    private Double radlastVl;

    @Column(name = "radlast_vr")
    private Double radlastVr;

    @Column(name = "radlast_hl")
    private Double radlastHl;

    @Column(name = "radlast_hr")
    private Double radlastHr;

    @Column(name = "zulaessiges_gesamtgewicht")
    private Integer zulaessigesGesamtgewicht;

    @Length(max = 15)
    @Column(name = "farbe", length = 15)
    private String farbe;

    @Length(max = 10)
    @Column(name = "farbcode", length = 10)
    private String farbcode;

    @Length(max = 15)
    @Column(name = "farbcode_innen", length = 15)
    private String farbcodeInnen;

    @Column(name = "km_stand")
    private Integer kmStand;

    @OneToMany(mappedBy = "variantEntity", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private List<VariantAirbag> variantAirbagList;

    @Length(max = 50)
    @Column(name = "herkunft", length = 50)
    private String herkunft;

    @NotNull
    @Column(name = "batterie_typ_12v", nullable = false)
    private Boolean batterieTyp12v;

    @NotNull
    @Column(name = "batterie_typ_24v", nullable = false)
    private Boolean batterieTyp24v;

    @NotNull
    @Column(name = "batterie_typ_48v", nullable = false)
    private Boolean batterieTyp48v;

    @NotNull
    @Column(name = "batterie_typ_hv", nullable = false)
    private Boolean batterieTypHv;

    @Length(max = 20)
    @Column(name = "reifen", length = 20)
    private String reifen;

    @Length(max = 50)
    @Column(name = "modell_nummer", length = 50)
    private String modellNummer;

    @Length(max = 20)
    @Column(name = "besonderheiten", length = 20)
    private String besonderheiten;
}
