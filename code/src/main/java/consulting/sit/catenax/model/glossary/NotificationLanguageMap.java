package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_notification_language_map")
public class NotificationLanguageMap implements ModelBaseInterface<NotificationLanguageMapPk> {
    @EmbeddedId
    private NotificationLanguageMapPk notificationLanguageMapPk;

    @Length(max = 30)
    @Column(name = "value", length = 30)
    private String value;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private Notification notificationEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(NotificationLanguageMapPk id) {
        this.notificationLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public NotificationLanguageMapPk getId() {
        return this.notificationLanguageMapPk;
    }
}
