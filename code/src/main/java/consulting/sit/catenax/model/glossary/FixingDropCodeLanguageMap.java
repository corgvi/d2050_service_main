package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_fixing_drop_code_language_map")
public class FixingDropCodeLanguageMap implements ModelBaseInterface<FixingDropCodeLanguageMapPk> {
    @EmbeddedId
    private FixingDropCodeLanguageMapPk fixingDropCodeLanguageMapPk;

    @Length(max = 5)
    @Column(name = "value", length = 5)
    private String value;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Language languageEntity;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private FixingDropCode fixingDropCodeEntity;

    @Override
    public void setId(FixingDropCodeLanguageMapPk id) {
        this.fixingDropCodeLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public FixingDropCodeLanguageMapPk getId() {
        return this.fixingDropCodeLanguageMapPk;
    }
}
