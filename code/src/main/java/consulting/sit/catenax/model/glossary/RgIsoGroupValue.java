package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_rg_iso_group_value")
public class RgIsoGroupValue implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private RgIsoGroup rgIsoGroupEntity;

    @Column(name = "group_value")
    private Integer groupValue;

    @Column(name = "calc_group")
    private Integer calcGroup;

    @Column(name = "treeview", nullable = false)
    private boolean treeview;

    @Column(name = "mandatory_removal", nullable = false)
    private boolean mandatoryRemoval;
}
