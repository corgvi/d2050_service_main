package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_pt_treatment_map")
public class PtTreatmentMap implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private PtListing ptListingEntity;

    @Column(name = "treatment")
    private Integer treatment;

    @ManyToOne
    @JoinColumn(name = "treatment", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private RqTreatment rqTreatmentEntity;
}
