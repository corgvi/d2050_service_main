package consulting.sit.catenax.model.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(schema = "project", name = "p_component_description")
public class ComponentDescription implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "component_desc_seq_gen")
    private Integer id;

    @Column(name = "component_id")
    private Integer componentId;

    @OneToOne
    @JoinColumn(name = "component_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private ComponentMain componentMainEntity;

    @Column(name = "anzahl_ws")
    private Integer anzahlWs;


    @Column(name = "loesbar_leicht")
    private Integer loesbarLeicht;

    @Column(name = "loesbar_schwer")
    private Integer loesbarSchwer;

    @Column(name = "loesbar_nicht")
    private Integer loesbarNicht;

    @Column(name = "beschichtet")
    private Boolean beschichtet;

    @Column(name = "lackiert")
    private Boolean lackiert;

    @Column(name = "wkz")
    private Integer wkz;

    @Column(name = "elektronik", nullable = false)
    private Boolean elektronik;

    @Column(name = "anzahl_werkzeugwechsel")
    private Integer anzahlWerkzeugwechsel;

    @Column(name = "anzahl_ve")
    private Integer anzahlVe;

    @Column(name = "anzahl_lose")
    private Integer anzahlLose;

    @Column(name = "gut_auffindbar")
    private Integer gutAuffindbar;

    @Column(name = "gut_zugaenglich")
    private Integer gutZugaenglich;

    @Column(name = "gut_loesbar")
    private Integer gutLoesbar;

    @Column(name = "punkte")
    private Integer punkte;

    @Column(name = "gewichtet")
    private Integer gewichtet;

    @CreatedBy
    @Column(name = "created_by")
    private String createdBy;

    @CreatedDate
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @LastModifiedBy
    @Column(name = "modified_by")
    private String modifiedBy;

    @LastModifiedDate
    @Column(name = "modified_at")
    private LocalDateTime modifiedAt;
}
