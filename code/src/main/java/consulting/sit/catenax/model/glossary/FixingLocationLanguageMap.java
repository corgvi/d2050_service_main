package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_fixing_location_language_map")
public class FixingLocationLanguageMap implements ModelBaseInterface<FixingLocationLanguageMapPk> {

    @EmbeddedId
    private FixingLocationLanguageMapPk fixingLocationLanguageMapPk;

    @Length(max = 5)
    @Column(name = "value", length = 5)
    private String value;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Language languageEntity;

    @ManyToOne
    @JoinColumn(name = "x_lage_ve", referencedColumnName = "x_lage_ve", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private FixingLocation fixingLocationEntity;

    @Override
    public void setId(FixingLocationLanguageMapPk id) {
        this.fixingLocationLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public FixingLocationLanguageMapPk getId() {
        return this.fixingLocationLanguageMapPk;
    }
}
