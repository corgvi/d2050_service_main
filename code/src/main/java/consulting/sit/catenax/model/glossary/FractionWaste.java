package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_fraction_waste")
@SequenceGenerator(schema = "glossary", name = "fraction_waste_seq_gen", sequenceName = "fraction_waste_id_seq")
public class FractionWaste implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fraction_waste_seq_gen")
    private Integer id;

    @Length(max = 8)
    @Column(name = "abfall_schluessel", length = 8)
    private String abfallSchluessel;


    @Length(max = 30)
    @Column(name = "schadstoff_inhalt", length = 30)
    private String schadstoffInhalt;

    @Column(name = "bes_ueb")
    private Boolean besUeb;

    @Column(name = "ueb", nullable = false)
    private boolean ueb;

    @OneToMany(mappedBy = "fractionWasteEntity", cascade = CascadeType.REMOVE)
    @MapKey(name="fractionWasteLanguageMapPk.language")
    private Map<String, FractionWasteLanguageMap> fractionWasteMaps;
}
