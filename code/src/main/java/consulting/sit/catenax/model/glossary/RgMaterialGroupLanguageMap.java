package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_rg_material_group_language_map")
public class RgMaterialGroupLanguageMap implements ModelBaseInterface<RgMaterialGroupLanguageMapPk> {
    @EmbeddedId
    private RgMaterialGroupLanguageMapPk rgMaterialGroupLanguageMapPk;

    @Length(max = 50)
    @Column(name = "value", length = 50)
    private String value;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private RgMaterialGroup rgMaterialGroupEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(RgMaterialGroupLanguageMapPk id) {
        this.rgMaterialGroupLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public RgMaterialGroupLanguageMapPk getId() {
        return this.rgMaterialGroupLanguageMapPk;
    }
}
