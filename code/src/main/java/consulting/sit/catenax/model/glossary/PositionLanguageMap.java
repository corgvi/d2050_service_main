package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_position_language_map")
public class PositionLanguageMap implements ModelBaseInterface<PositionLanguageMapPk> {
    @EmbeddedId
    private PositionLanguageMapPk positionLanguageMapPk;

    @Column(name = "value", columnDefinition = "TEXT")
    private String value;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Language languageEntity;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Position positionEntity;

    @Override
    public void setId(PositionLanguageMapPk id) {
        this.positionLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public PositionLanguageMapPk getId() {
        return this.positionLanguageMapPk;
    }
}
