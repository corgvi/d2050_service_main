package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_rq_treatment_language_map")
public class RqTreatmentLanguageMap implements ModelBaseInterface<RqTreatmentLanguageMapPk> {
    @EmbeddedId
    private RqTreatmentLanguageMapPk rqTreatmentLanguageMapPk;

    @Column(name = "value", columnDefinition = "TEXT")
    private String value;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private RqTreatment rgTreatmentEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(RqTreatmentLanguageMapPk id) {
        this.rqTreatmentLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public RqTreatmentLanguageMapPk getId() {
        return this.rqTreatmentLanguageMapPk;
    }
}
