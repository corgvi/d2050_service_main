package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_rg_material_class")
@SequenceGenerator(schema = "glossary", name = "rg_material_class_seq_gen", sequenceName = "rg_material_class_id_seq")
public class RgMaterialClass implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rg_material_class_seq_gen")
    private Integer id;

    @Column(name = "interne_gruppennummer")
    private Integer interneGruppennummer;

    @Length(max = 255)
    @Column(name = "gruppennummer", length = 255)
    private String gruppennummer;

    @Column(name = "rq_gruppe")
    private Integer rQGruppe;

    @Column(name = "ss")
    private Double ss;

    @Column(name = "ssf")
    private Double ssf;

    @Column(name = "slf")
    private Double slf;

    @Column(name = "ss_stofflich")
    private Double ss_stofflich;

    @Column(name = "ss_thermisch")
    private Double ss_thermisch;

    @Column(name = "ss_deponiert")
    private Double ss_deponiert;

    @Column(name = "ssf_stofflich")
    private Double ssf_stofflich;

    @Column(name = "ssf_thermisch")
    private Double ssf_thermisch;

    @Column(name = "ssf_deponiert")
    private Double ssf_deponiert;

    @Column(name = "slf_pyr_stofflich")
    private Double slf_pyr_stofflich;

    @Column(name = "slf_pyr_thermisch")
    private Double slf_pyr_thermisch;

    @Column(name = "slf_pyr_deponiert")
    private Double slf_pyr_deponiert;

    @Column(name = "slf_agg_stofflich")
    private Double slf_agg_stofflich;

    @Column(name = "slf_agg_thermisch")
    private Double slf_agg_thermisch;

    @Column(name = "slf_agg_deponiert")
    private Double slf_agg_deponiert;

    @Column(name = "obergruppe")
    private Integer obergruppe;

    @Length(max = 255)
    @Column(name = "anmerkung", length = 255)
    private String anmerkung;
}
