package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_fixing_drop_language_map")
public class FixingDropLanguageMap implements ModelBaseInterface<FixingDropLanguageMapPk> {
    @EmbeddedId
    private FixingDropLanguageMapPk fixingDropLanguageMapPk;

    @Length(max = 70)
    @Column(name = "beschreibung", length = 70)
    private String beschreibung;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Language languageEntity;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private FixingDrop fixingDropEntity;

    @Override
    public void setId(FixingDropLanguageMapPk id) {
        this.fixingDropLanguageMapPk = id;
    }

    @Override
    public FixingDropLanguageMapPk getId() {
        return this.fixingDropLanguageMapPk;
    }
}
