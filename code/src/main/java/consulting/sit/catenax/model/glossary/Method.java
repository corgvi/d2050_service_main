package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_method")
@SequenceGenerator(schema = "glossary", name = "method_seq_gen", sequenceName = "method_id_seq")
public class Method implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "method_seq_gen")
    private Integer id;

    @Column(name = "sortorder")
    private Integer sortorder;

    @OneToMany(mappedBy = "methodEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "methodLanguageMapPk.language")
    private Map<String, MethodLanguageMap> methodLanguageMaps;
}