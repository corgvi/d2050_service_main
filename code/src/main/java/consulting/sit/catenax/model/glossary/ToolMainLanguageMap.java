package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(schema = "glossary", name = "g_tool_main_language_map")
public class ToolMainLanguageMap implements ModelBaseInterface<ToolMainLanguageMapPk> {
    @EmbeddedId
    private ToolMainLanguageMapPk toolMainLanguageMapPk;

    @NotNull
    @Column(name = "value", columnDefinition = "TEXT")
    private String value;

    @NotNull
    @Column(name = "code", columnDefinition = "TEXT")
    private String code;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private ToolMain toolMainEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(ToolMainLanguageMapPk id) {
        this.toolMainLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public ToolMainLanguageMapPk getId() {
        return this.toolMainLanguageMapPk;
    }
}
