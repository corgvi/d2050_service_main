package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_fixing_main")
@SequenceGenerator(schema = "glossary", name = "fixing_main_seq_gen", sequenceName = "fixing_main_id_seq")
public class FixingMain implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fixing_main_seq_gen")
    private Integer id;

    @Column(name = "sortorder")
    private int sortorder;

    @Column(name = "fixing_char_id")
    private Integer fixingCharId;

    @ManyToOne
    @JoinColumn(name = "fixing_char_id", referencedColumnName = "id", insertable = false, updatable = false)
    private FixingChar fixingCharEntity;

    @OneToMany(mappedBy = "fixingMainEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "fixingMainLanguageMapPk.language")
    private Map<String, FixingMainLanguageMap> fixingMainLanguageMaps;
}
