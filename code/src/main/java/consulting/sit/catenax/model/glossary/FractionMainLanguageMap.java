package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_fraction_main_language_map")
public class FractionMainLanguageMap implements ModelBaseInterface<FractionMainLanguageMapPk> {
    @EmbeddedId
    private FractionMainLanguageMapPk fractionMainLanguageMapPk;

    @Length(max = 25)
    @Column(name = "value", length = 25)
    private String value;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private FractionMain fractionMainEntity;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @JsonIgnore
    private Language languageEntity;

    @Override
    public void setId(FractionMainLanguageMapPk id) {
        this.fractionMainLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public FractionMainLanguageMapPk getId() {
        return this.fractionMainLanguageMapPk;
    }
}
