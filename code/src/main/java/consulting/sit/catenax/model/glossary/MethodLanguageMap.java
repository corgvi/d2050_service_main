package consulting.sit.catenax.model.glossary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Data
@Entity
@Table(schema = "glossary", name = "g_method_language_map")
public class MethodLanguageMap implements ModelBaseInterface<MethodLanguageMapPk> {
    @EmbeddedId
    private MethodLanguageMapPk methodLanguageMapPk;

    @Column(name = "value", columnDefinition = "TEXT")
    private String value;

    @ManyToOne
    @JoinColumn(name = "language", referencedColumnName = "language", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Language languageEntity;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Method methodEntity;

    @Override
    public void setId(MethodLanguageMapPk id) {
        this.methodLanguageMapPk = id;
    }

    @Override
    @JsonIgnore
    public MethodLanguageMapPk getId() {
        return this.methodLanguageMapPk;
    }
}
