package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_rg_material_group")
@SequenceGenerator(schema = "glossary", name = "rg_material_group_seq_gen", sequenceName = "rg_material_group_id_seq")
public class RgMaterialGroup implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rg_material_group_seq_gen")
    private Integer id;

    @OneToMany(mappedBy = "rgMaterialGroupEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "rgMaterialGroupLanguageMapPk.language")
    private Map<String, RgMaterialGroupLanguageMap> rgMaterialGroupLanguageMaps;
}
