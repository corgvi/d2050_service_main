package consulting.sit.catenax.model.glossary;

import consulting.sit.catenax.model.ModelBaseInterface;
import lombok.Data;

import javax.persistence.*;
import java.util.Map;

@Data
@Entity
@Table(schema = "glossary", name = "g_fixing_drop")
@SequenceGenerator(schema = "glossary", name = "fixing_drop_seq_gen", sequenceName = "fixing_drop_id_seq")
public class FixingDrop implements ModelBaseInterface<Integer> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fixing_drop_seq_gen")
    private Integer id;

    @OneToOne(mappedBy = "fixingDropEntity", cascade = CascadeType.REMOVE)
    private FixingDropCode fixingDropCodeEntity;

    @OneToMany(mappedBy = "fixingDropEntity", cascade = CascadeType.REMOVE)
    @MapKey(name = "fixingDropLanguageMapPk.language")
    private Map<String, FixingDropLanguageMap> fixingDropLanguageMaps;
}