package consulting.sit.catenax.projection.project;

public interface ComponentDependPlain {
    Integer getId();

    Long getComponentId();

    Long getAbhaengigkeit();

    boolean isKonstruktiv();

    boolean isStrategisch();
}
