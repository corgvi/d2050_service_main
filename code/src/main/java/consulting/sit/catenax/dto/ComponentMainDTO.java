package consulting.sit.catenax.dto;

import consulting.sit.catenax.model.glossary.Part;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
public class ComponentMainDTO {
    private long id;
}
