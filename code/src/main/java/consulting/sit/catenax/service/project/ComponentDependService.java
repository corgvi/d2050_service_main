package consulting.sit.catenax.service.project;

import consulting.sit.catenax.model.project.ComponentDepend;
import consulting.sit.catenax.projection.project.ComponentDependPlain;
import consulting.sit.catenax.repository.project.ComponentDependRepository;
import consulting.sit.catenax.service.GenericService;

import java.util.List;

public class ComponentDependService extends GenericService<ComponentDepend, Integer> {
    public ComponentDependRepository repository;

    public List<ComponentDependPlain> findByComponentId(long componentId) {
        return repository.findByComponentId(componentId);
    }

    public List<ComponentDepend> findAllByComponentId(long componentId) {
        return repository.findAllByComponentId(componentId);
    }

    public ComponentDepend updateByFieldOfComponentDepend(int id, ComponentDepend updateComponentDepend) {
        ComponentDepend componentDepend = repository.findById(id).orElseThrow(() -> new NullPointerException());

        if (updateComponentDepend.getAbhaengigkeit() != null) {
            componentDepend.setAbhaengigkeit(updateComponentDepend.getAbhaengigkeit());
        }
        if (updateComponentDepend.getKonstruktiv() != null) {
            componentDepend.setKonstruktiv(updateComponentDepend.getKonstruktiv());
        }
        if (updateComponentDepend.getComponentId() != null) {
            componentDepend.setComponentId(updateComponentDepend.getComponentId());
        }
        if (updateComponentDepend.getStrategisch() != null) {
            componentDepend.setStrategisch(updateComponentDepend.getStrategisch());
        }
        if (updateComponentDepend.getModifiedBy() != null) {
            componentDepend.setModifiedBy(updateComponentDepend.getModifiedBy());
        }
        if (updateComponentDepend.getCreatedBy() != null) {
            componentDepend.setCreatedBy(updateComponentDepend.getCreatedBy());
        }
        if (updateComponentDepend.getZbPart() != null){
            componentDepend.setZbPart(updateComponentDepend.getZbPart());
        }
        return repository.save(componentDepend);
    }

    public Boolean checkExistAbhengigkeitInComponent(long componentId, long abhengigkeit) {
        return repository.existsByComponentIdAndAbhaengigkeit(componentId, abhengigkeit);
    }
}
