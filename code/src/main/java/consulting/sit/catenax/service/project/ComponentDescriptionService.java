package consulting.sit.catenax.service.project;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import consulting.sit.catenax.model.project.ComponentCe;
import consulting.sit.catenax.model.project.ComponentDescription;
import consulting.sit.catenax.repository.project.ComponentDescriptionRepository;
import consulting.sit.catenax.service.GenericService;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;

@Service
public class ComponentDescriptionService extends GenericService<ComponentDescription, Integer> {

    private final ComponentDescriptionRepository repository;

    public ComponentDescriptionService(ComponentDescriptionRepository repository) {
        this.repository = repository;
    }

    public ComponentDescription findByComponentId(int componentId){
        return repository.findByComponentId(componentId);
    }


    public ComponentDescription updateComponentDescription(ComponentDescription updateComponentDescription, Integer id) {
        ComponentDescription componentDescription = repository.findById(id).orElseThrow(() -> new NullPointerException("Component description not found"));
        if (updateComponentDescription.getAnzahlWs() != null) {
            componentDescription.setAnzahlWs(updateComponentDescription.getAnzahlWs());
        }
        if (updateComponentDescription.getLoesbarLeicht() != null) {
            componentDescription.setLoesbarLeicht(updateComponentDescription.getLoesbarLeicht());
        }
        if (updateComponentDescription.getLoesbarSchwer() != null) {
            componentDescription.setLoesbarSchwer(updateComponentDescription.getLoesbarSchwer());
        }
        if (updateComponentDescription.getLoesbarNicht() != null) {
            componentDescription.setLoesbarNicht(updateComponentDescription.getLoesbarNicht());
        }
        if (updateComponentDescription.getBeschichtet() != null) {
            componentDescription.setBeschichtet(updateComponentDescription.getBeschichtet());
        }
        if (updateComponentDescription.getLackiert() != null) {
            componentDescription.setLackiert(updateComponentDescription.getLackiert());
        }
        if (updateComponentDescription.getWkz() != null) {
            componentDescription.setWkz(updateComponentDescription.getWkz());
        }
        if(updateComponentDescription.getElektronik() != null){
            componentDescription.setElektronik(updateComponentDescription.getElektronik());
        }
        if (updateComponentDescription.getAnzahlWerkzeugwechsel() != null){
            componentDescription.setAnzahlWerkzeugwechsel(updateComponentDescription.getAnzahlWerkzeugwechsel());
        }
        if (updateComponentDescription.getAnzahlVe() != null){
            componentDescription.setAnzahlVe(updateComponentDescription.getAnzahlVe());
        }
        if (updateComponentDescription.getAnzahlLose() != null){
            componentDescription.setAnzahlLose(updateComponentDescription.getAnzahlLose());
        }
        if (updateComponentDescription.getGutAuffindbar() != null){
            componentDescription.setGutAuffindbar(updateComponentDescription.getGutAuffindbar());
        }
        if (updateComponentDescription.getGutZugaenglich() != null){
            componentDescription.setGutZugaenglich(updateComponentDescription.getGutZugaenglich());
        }
        if (updateComponentDescription.getGutLoesbar() != null) {
            componentDescription.setGutLoesbar(updateComponentDescription.getGutLoesbar());
        }
        if(updateComponentDescription.getPunkte() != null){
            componentDescription.setPunkte(updateComponentDescription.getPunkte());
        }
        if (updateComponentDescription.getGewichtet() != null) {
            componentDescription.setGewichtet(updateComponentDescription.getGewichtet());
        }
        if (updateComponentDescription.getModifiedBy() != null) {
            componentDescription.setModifiedBy(updateComponentDescription.getModifiedBy());
        }
        if (updateComponentDescription.getCreatedBy() != null) {
            componentDescription.setCreatedBy(updateComponentDescription.getCreatedBy());
        }
        if (updateComponentDescription.getComponentId() != null){
            componentDescription.setComponentId(updateComponentDescription.getComponentId());
        }

        return repository.save(componentDescription);
    }

}


