package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.model.glossary.RgIsoGroup;
import consulting.sit.catenax.repository.glossary.RgIsoGroupRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RgIsoGroupService {
    private final RgIsoGroupRepository rgIsoGroupRepository;

    public RgIsoGroupService(RgIsoGroupRepository rgIsoGroupRepository) {
        this.rgIsoGroupRepository = rgIsoGroupRepository;
    }

    public List<RgIsoGroup> findRgIsoGroupWithoutRgIsoGroupValue() {
        return rgIsoGroupRepository.findRgIsoGroupWithoutRgIsoGroupValue();
    }
}