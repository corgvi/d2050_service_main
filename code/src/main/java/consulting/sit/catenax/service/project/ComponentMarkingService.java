package consulting.sit.catenax.service.project;

import consulting.sit.catenax.model.project.ComponentCe;
import consulting.sit.catenax.model.project.ComponentMarking;
import consulting.sit.catenax.model.project.ComponentReference;
import consulting.sit.catenax.repository.project.ComponentMarkingRepository;
import consulting.sit.catenax.service.GenericService;
import org.springframework.stereotype.Service;

@Service
public class ComponentMarkingService extends GenericService<ComponentReference, Integer> {

    private final ComponentMarkingRepository componentMarkingRepository;

    public ComponentMarkingService(final ComponentMarkingRepository componentMarkingRepository) {
        this.componentMarkingRepository = componentMarkingRepository;
    }

    public ComponentMarking findByComponentId(int componentId){
        return componentMarkingRepository.findByComponentId(componentId);
    }

    public ComponentMarking updateByFieldsOfComponentMarking(int id, ComponentMarking updateComponentMarking) {
        ComponentMarking componentMarking = componentMarkingRepository.findById(id).orElseThrow(() -> new NullPointerException());

        if (updateComponentMarking.getWsMarked() != null) {
            componentMarking.setWsMarked(updateComponentMarking.getWsMarked());
        }

        if (updateComponentMarking.getWsMarkedOk() != null) {
            componentMarking.setWsMarkedOk(updateComponentMarking.getWsMarkedOk());
        }

        if (updateComponentMarking.getWsSichtbar() != null) {
            componentMarking.setWsSichtbar(updateComponentMarking.getWsSichtbar());
        }

        if (updateComponentMarking.getWsHersteller() != null) {
            componentMarking.setWsHersteller(updateComponentMarking.getWsHersteller());
        }

        if (updateComponentMarking.getIdentified() != null) {
            componentMarking.setIdentified(updateComponentMarking.getIdentified());
        }

        if (updateComponentMarking.getIdentified() != null) {
            componentMarking.setIdentified(updateComponentMarking.getIdentified());
        }

        if (updateComponentMarking.getIdentifiedType() != null) {
            componentMarking.setIdentifiedType(updateComponentMarking.getIdentifiedType());
        }

        if (updateComponentMarking.getModifiedBy() != null) {
            componentMarking.setModifiedBy(updateComponentMarking.getModifiedBy());
        }

        if (updateComponentMarking.getCreatedBy() != null) {
            componentMarking.setCreatedBy(updateComponentMarking.getCreatedBy());
        }

        if (updateComponentMarking.getComponentId() != null){
            componentMarking.setComponentId(updateComponentMarking.getComponentId());
        }

        return componentMarkingRepository.save(componentMarking);
    }
}
