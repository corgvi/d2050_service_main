package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.model.glossary.FixingDropCodeLanguageMap;
import consulting.sit.catenax.model.glossary.FixingLocationLanguageMap;
import consulting.sit.catenax.repository.glossary.FixingDropCodeLanguageMapRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FixingDropCodeLanguageMapService {

    private final FixingDropCodeLanguageMapRepository fixingDropCodeLanguageMapRepository;

    public FixingDropCodeLanguageMapService(final FixingDropCodeLanguageMapRepository fixingDropCodeLanguageMapRepository) {
        this.fixingDropCodeLanguageMapRepository = fixingDropCodeLanguageMapRepository;
    }

    public FixingDropCodeLanguageMap findByLanguageAndFixingDropId(int id, String language) {
        return fixingDropCodeLanguageMapRepository.findByFixingDropCodeEntity_FixingDropIdAndFixingDropCodeLanguageMapPk_Language(id, language);
    }
}
