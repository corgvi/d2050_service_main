package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.model.glossary.FixingMainLanguageMap;
import consulting.sit.catenax.model.glossary.ToolMainLanguageMap;
import consulting.sit.catenax.repository.glossary.ToolMainLanguageMapRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ToolMainLanguageMapService {

    private final ToolMainLanguageMapRepository toolMainLanguageMapRepository;

    public ToolMainLanguageMapService(final ToolMainLanguageMapRepository toolMainLanguageMapRepository) {
        this.toolMainLanguageMapRepository = toolMainLanguageMapRepository;
    }

    public List<ToolMainLanguageMap> findAllByLanguage(String language) {
        return toolMainLanguageMapRepository.findAllByToolMainLanguageMapPk_Language(language);
    }
}
