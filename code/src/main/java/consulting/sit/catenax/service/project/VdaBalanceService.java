package consulting.sit.catenax.service.project;

import consulting.sit.catenax.model.glossary.MaterialMainLanguageMap;
import consulting.sit.catenax.model.project.ComponentComp;
import consulting.sit.catenax.model.project.VdaBalance;
import consulting.sit.catenax.repository.project.VdaBalanceRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

@Service
public class VdaBalanceService {

    private final VdaBalanceRepository vdaBalanceRepository;

    public VdaBalanceService(final VdaBalanceRepository vdaBalanceRepository) {
        this.vdaBalanceRepository = vdaBalanceRepository;
    }

    public List<VdaBalance> findAllByComponentId(Integer componentId) {
        return vdaBalanceRepository.findAllByVdaBalancePk_ComponentId(componentId);
    }

    public List<VdaBalance> findByComponentId(int componentId){
        return vdaBalanceRepository.findByComponentId(componentId);
    }
//
//    public VdaBalance updateByFieldsOfVdaBalance(Integer id, VdaBalance updateVdaBalance) {
//        VdaBalance vdaBalance = vdaBalanceRepository.findByComponentId(id);
//        if (vdaBalance == null) {
//            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
//        }
////        VdaBalance vdaBalance = vdaBalanceRepository.findByComponentId(id).get(0).orElseThrow(() -> new NullPointerException());
//
//        if (updateVdaBalance.getValue() != null) {
//            vdaBalance.setValue(updateVdaBalance.getValue());
//        }
//
//        if (updateVdaBalance.getModifiedBy() != null) {
//            vdaBalance.setModifiedBy(updateVdaBalance.getModifiedBy());
//        }
//
//        if (updateVdaBalance.getCreatedBy() != null) {
//            vdaBalance.setCreatedBy(updateVdaBalance.getCreatedBy());
//        }
//        return vdaBalanceRepository.save(vdaBalance);
//    }
}
