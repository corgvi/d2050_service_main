package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.model.glossary.FractionMainLanguageMap;
import consulting.sit.catenax.repository.glossary.FractionMainLanguageMapRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FractionMainLanguageMapService {

    private final FractionMainLanguageMapRepository fractionMainLanguageMapRepository;

    public FractionMainLanguageMapService(final FractionMainLanguageMapRepository fractionMainLanguageMapRepository) {
        this.fractionMainLanguageMapRepository = fractionMainLanguageMapRepository;
    }


    public List<FractionMainLanguageMap> findAllByLanguage(String language) {
        return fractionMainLanguageMapRepository.findAllByFractionMainLanguageMapPk_Language(language);
    }
}
