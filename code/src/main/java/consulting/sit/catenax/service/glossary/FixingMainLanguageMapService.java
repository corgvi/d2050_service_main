package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.model.glossary.FixingMainLanguageMap;
import consulting.sit.catenax.model.glossary.PartLanguageMap;
import consulting.sit.catenax.repository.glossary.FixingMainLanguageMapRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FixingMainLanguageMapService {

    private final FixingMainLanguageMapRepository fixingMainLanguageMapRepository;

    public FixingMainLanguageMapService(final FixingMainLanguageMapRepository fixingMainLanguageMapRepository) {
        this.fixingMainLanguageMapRepository = fixingMainLanguageMapRepository;
    }


    public List<FixingMainLanguageMap> findAllByLanguage(String language) {
        return fixingMainLanguageMapRepository.findAllByFixingMainLanguageMapPk_Language(language);
    }
}
