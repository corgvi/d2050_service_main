package consulting.sit.catenax.service.project;

import consulting.sit.catenax.model.project.ComponentCe;
import consulting.sit.catenax.model.project.ComponentReference;
import consulting.sit.catenax.repository.project.ComponentReferenceRepository;
import consulting.sit.catenax.service.GenericService;
import org.springframework.stereotype.Service;

@Service
public class ComponentReferenceService extends GenericService<ComponentReference, Integer> {

    private final ComponentReferenceRepository componentReferenceRepository;

    public ComponentReferenceService(final ComponentReferenceRepository componentReferenceRepository) {
        this.componentReferenceRepository = componentReferenceRepository;
    }

    public ComponentReference findByComponentId(int componentId){
        return componentReferenceRepository.findByComponentId(componentId);
    }

    public ComponentReference updateByFieldsOfComponentReference(int id, ComponentReference updateComponentReference) {
        ComponentReference componentReference = componentReferenceRepository.findById(id).orElseThrow(() -> new NullPointerException());

        if (updateComponentReference.getTeileNummer() != null) {
            componentReference.setTeileNummer(updateComponentReference.getTeileNummer());
        }

        if (updateComponentReference.getKatalogBaugruppe() != null) {
            componentReference.setKatalogBaugruppe(updateComponentReference.getKatalogBaugruppe());
        }

        if (updateComponentReference.getKatalogRef() != null) {
            componentReference.setKatalogRef(updateComponentReference.getKatalogRef());
        }

        if (updateComponentReference.getComponentId() != null){
            componentReference.setComponentId(updateComponentReference.getComponentId());
        }

        return componentReferenceRepository.save(componentReference);
    }
}
