package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.model.glossary.FixingDrop;
import consulting.sit.catenax.model.glossary.FixingDropCodeLanguageMap;
import consulting.sit.catenax.model.glossary.FixingDropLanguageMap;
import consulting.sit.catenax.model.glossary.FixingLocationLanguageMap;
import consulting.sit.catenax.repository.glossary.FixingDropLanguageMapRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FixingDropLanguageMapService {

    private final FixingDropLanguageMapRepository fixingDropLanguageMapRepository;

    public FixingDropLanguageMapService(final FixingDropLanguageMapRepository fixingDropLanguageMapRepository) {
        this.fixingDropLanguageMapRepository = fixingDropLanguageMapRepository;
    }

    public List<FixingDropLanguageMap> findAllByLanguage(String language) {
        return fixingDropLanguageMapRepository.findAllByFixingDropLanguageMapPk_Language(language);
    }
}
