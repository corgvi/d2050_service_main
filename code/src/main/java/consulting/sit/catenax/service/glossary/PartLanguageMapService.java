package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.model.glossary.MaterialMainLanguageMap;
import consulting.sit.catenax.model.glossary.PartLanguageMap;
import consulting.sit.catenax.repository.glossary.PartLanguageMapRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PartLanguageMapService {

    private final PartLanguageMapRepository partLanguageMapRepository;

    public PartLanguageMapService(final PartLanguageMapRepository partLanguageMapRepository) {
        this.partLanguageMapRepository = partLanguageMapRepository;
    }


    public List<PartLanguageMap> findAllByLanguage(String language) {
        return partLanguageMapRepository.findAllByPartLanguageMapPk_Language(language);
    }
}
