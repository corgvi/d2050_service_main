package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.model.glossary.FixingDrop;
import consulting.sit.catenax.repository.glossary.FixingDropRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FixingDropService {
    private final FixingDropRepository fixingDropRepository;

    public FixingDropService(FixingDropRepository fixingDropRepository) {
        this.fixingDropRepository = fixingDropRepository;
    }

    public List<FixingDrop> findFixingDropWithoutFixingDropCode (){
        return fixingDropRepository.findFixingDropWithoutFixingDropCode();
    }
}
