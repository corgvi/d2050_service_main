package consulting.sit.catenax.service.project;

import consulting.sit.catenax.model.project.ComponentComp;
import consulting.sit.catenax.model.project.ComponentMain;
import consulting.sit.catenax.repository.project.ComponentCompRepository;
import consulting.sit.catenax.service.GenericService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComponentCompService extends GenericService<ComponentComp, Integer> {

    private final ComponentCompRepository componentCompRepository;

    public ComponentCompService(final ComponentCompRepository componentCompRepository) {
        this.componentCompRepository = componentCompRepository;
    }

    public List<ComponentComp> findByComponentMainId(int componentMainId) {
        return componentCompRepository.findByComponentMainId(componentMainId);
    }

    public ComponentComp updateByFieldsOfComponentComp(int id, ComponentComp updateComponentComp) {
        ComponentComp componentComp = componentCompRepository.findById(id).orElseThrow(() -> new NullPointerException());

        if (updateComponentComp.getVerbundAnteil() != null) {
            componentComp.setVerbundAnteil(updateComponentComp.getVerbundAnteil());
        }

        if (updateComponentComp.getVerbundMaterial() != null) {
            componentComp.setVerbundMaterial(updateComponentComp.getVerbundMaterial());
        }

        if (updateComponentComp.getVMasse() != null) {
            componentComp.setVMasse(updateComponentComp.getVMasse());
        }

        if (updateComponentComp.getAnzahlWerkstoffmarkierungen() != null) {
            componentComp.setAnzahlWerkstoffmarkierungen(updateComponentComp.getAnzahlWerkstoffmarkierungen());
        }

        if (updateComponentComp.getPrefix() != null) {
            componentComp.setPrefix(updateComponentComp.getPrefix());
        }

        if (updateComponentComp.getMain() != null) {
            componentComp.setMain(updateComponentComp.getMain());
        }

        if (updateComponentComp.getRecContent() != null) {
            componentComp.setRecContent(updateComponentComp.getRecContent());
        }

        if (updateComponentComp.getRecCategory() != null) {
            componentComp.setRecCategory(updateComponentComp.getRecCategory());
        }

        if (updateComponentComp.getRecPirPcr() != null) {
            componentComp.setRecPirPcr(updateComponentComp.getRecPirPcr());
        }

        if (updateComponentComp.getWsMark() != null) {
            componentComp.setWsMark(updateComponentComp.getWsMark());
        }

        if (updateComponentComp.getType() != null) {
            componentComp.setType(updateComponentComp.getType());
        }

        if (updateComponentComp.getUnit() != null) {
            componentComp.setUnit(updateComponentComp.getUnit());
        }

        if (updateComponentComp.getModifiedBy() != null) {
            componentComp.setModifiedBy(updateComponentComp.getModifiedBy());
        }

        if (updateComponentComp.getCreatedBy() != null) {
            componentComp.setCreatedBy(updateComponentComp.getCreatedBy());
        }

        return componentCompRepository.save(componentComp);
    }
}
