package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.model.glossary.FixingLocationLanguageMap;
import consulting.sit.catenax.repository.glossary.FixingLocationLanguageMapRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FixingLocationLanguageMapService {

    private final FixingLocationLanguageMapRepository fixingLocationLanguageMapRepository;

    public FixingLocationLanguageMapService(final FixingLocationLanguageMapRepository fixingLocationLanguageMapRepository) {
        this.fixingLocationLanguageMapRepository = fixingLocationLanguageMapRepository;
    }

    public List<FixingLocationLanguageMap> findAllByLanguage(String language) {
        return fixingLocationLanguageMapRepository.findAllByFixingLocationLanguageMapPk_Language(language);
    }
}
