package consulting.sit.catenax.service.project;

import consulting.sit.catenax.model.project.Project;
import consulting.sit.catenax.repository.project.ProjectRepository;
import consulting.sit.catenax.service.GenericService;
import org.springframework.stereotype.Service;

@Service
public class ProjectService extends GenericService<Project, Integer> {
    private final ProjectRepository repository;

    public ProjectService(final ProjectRepository repository) {
        this.repository = repository;
    }

    public Project updateProject(Project updateProject, Integer id) {
        Project project = repository.findById(id).orElseThrow(() -> new NullPointerException("Project not found"));
        if (updateProject.getName() != null) {
            project.setName(updateProject.getName());
        }
        if (updateProject.getDescription() != null) {
            project.setDescription(updateProject.getDescription());
        }
//        if (updateProject.getCreatedBy() != null) {
//            project.setCreatedBy(updateProject.getCreatedBy());
//        }
        if (updateProject.getModifiedBy() != null) {
            project.setModifiedBy(updateProject.getModifiedBy());
        }
        if (updateProject.getCxProject() != null) {
        project.setCxProject(updateProject.getCxProject());
        }
        return repository.save(project);
    }
}
