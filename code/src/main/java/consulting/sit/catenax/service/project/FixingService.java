package consulting.sit.catenax.service.project;

import consulting.sit.catenax.model.project.ComponentMarking;
import consulting.sit.catenax.model.project.Fixing;
import consulting.sit.catenax.repository.project.FixingRepository;
import consulting.sit.catenax.service.GenericService;
import org.springframework.stereotype.Service;

@Service
public class FixingService extends GenericService<Fixing, Integer> {

    private final FixingRepository fixingRepository;

    public FixingService(final FixingRepository fixingRepository) {
        this.fixingRepository = fixingRepository;
    }

    public Fixing updateByFieldsOfFixing(int id, Fixing updateFixing) {
        Fixing fixing = fixingRepository.findById(id).orElseThrow(() -> new NullPointerException());

        if (updateFixing.getVerbindungselement() != null) {
            fixing.setVerbindungselement(updateFixing.getVerbindungselement());
        }

        if (updateFixing.getVe() != null) {
            fixing.setVe(updateFixing.getVe());
        }

        if (updateFixing.getGroesse() != null) {
            fixing.setGroesse(updateFixing.getGroesse());
        }

        if (updateFixing.getVeAnzahl() != null) {
            fixing.setVeAnzahl(updateFixing.getVeAnzahl());
        }

        if (updateFixing.getVeMasse() != null) {
            fixing.setVeMasse(updateFixing.getVeMasse());
        }

        if (updateFixing.getDemonteurPosition() != null) {
            fixing.setDemonteurPosition(updateFixing.getDemonteurPosition());
        }

        if (updateFixing.getVertraeglichkeit() != null) {
            fixing.setVertraeglichkeit(updateFixing.getVertraeglichkeit());
        }

        if (updateFixing.getVertraeglichkeitBegruendung() != null) {
            fixing.setVertraeglichkeitBegruendung(updateFixing.getVertraeglichkeitBegruendung());
        }

        if (updateFixing.getAuffindbarkeit() != null) {
            fixing.setAuffindbarkeit(updateFixing.getAuffindbarkeit());
        }

        if (updateFixing.getAuffindbarkeitBegruendung() != null) {
            fixing.setAuffindbarkeitBegruendung(updateFixing.getAuffindbarkeitBegruendung());
        }

        if (updateFixing.getZugaenglichkeit() != null) {
            fixing.setZugaenglichkeit(updateFixing.getZugaenglichkeit());
        }

        if (updateFixing.getZugaenglichkeitBegruendung() != null) {
            fixing.setZugaenglichkeitBegruendung(updateFixing.getZugaenglichkeitBegruendung());
        }

        if (updateFixing.getLoesbarkeit() != null) {
            fixing.setLoesbarkeit(updateFixing.getLoesbarkeit());
        }

        if (updateFixing.getLoesbarkeitBegruendung() != null) {
            fixing.setLoesbarkeitBegruendung(updateFixing.getLoesbarkeitBegruendung());
        }

        if (updateFixing.getGrobdemontage() != null) {
            fixing.setGrobdemontage(updateFixing.getGrobdemontage());
        }

        if (updateFixing.getGrobdemontageBegruendung() != null) {
            fixing.setGrobdemontageBegruendung(updateFixing.getGrobdemontageBegruendung());
        }

        if (updateFixing.getNacharbeitszeit() != null) {
            fixing.setNacharbeitszeit(updateFixing.getNacharbeitszeit());
        }

        if (updateFixing.getHilfsmittel() != null) {
            fixing.setHilfsmittel(updateFixing.getHilfsmittel());
        }

        if (updateFixing.getEinsatz() != null) {
            fixing.setEinsatz(updateFixing.getEinsatz());
        }

        if (updateFixing.getVeWerkstoff() != null) {
            fixing.setVeWerkstoff(updateFixing.getVeWerkstoff());
        }

        if (updateFixing.getVerbleib() != null) {
            fixing.setVerbleib(updateFixing.getVerbleib());
        }
        if(updateFixing.getVeLage() != null ){
            fixing.setVeLage(updateFixing.getVeLage());
        }
        if(updateFixing.getWerkzeug() != null ){
            fixing.setWerkzeug(updateFixing.getWerkzeug());
        }
        return fixingRepository.save(fixing);
    }
}
