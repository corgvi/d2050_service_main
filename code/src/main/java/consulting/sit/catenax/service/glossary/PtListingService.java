package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.model.glossary.PtListing;
import consulting.sit.catenax.repository.glossary.PtListingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PtListingService {

    private final PtListingRepository ptListingRepository;

    public PtListingService(final PtListingRepository ptListingRepository) {
        this.ptListingRepository = ptListingRepository;
    }

    public List<PtListing> findPtListingWithoutPtMaterialMap() {
        return ptListingRepository.findPtListingWithoutPtMaterialMap();
    }

}
