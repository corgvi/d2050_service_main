package consulting.sit.catenax.service.project;

import consulting.sit.catenax.model.project.ComponentCe;
import consulting.sit.catenax.repository.project.ComponentCeRepository;
import consulting.sit.catenax.service.GenericService;
import org.springframework.stereotype.Service;

@Service
public class ComponentCeService extends GenericService<ComponentCe, Integer> {

    private final ComponentCeRepository componentCeRepository;

    public ComponentCeService(final ComponentCeRepository componentCeRepository) {
        this.componentCeRepository = componentCeRepository;
    }

    public ComponentCe findByComponentId(int componentId){
        return componentCeRepository.findByComponentId(componentId);
    }

    public ComponentCe updateComponentCe(ComponentCe updateComponentCe, Integer id) {
        ComponentCe componentCe = componentCeRepository.findById(id).orElseThrow(() -> new NullPointerException("Component Ce not found"));

        if (updateComponentCe.getKe1() != null) {
            componentCe.setKe1(updateComponentCe.getKe1());
        }
        if (updateComponentCe.getKe1Masse() != null) {
            componentCe.setKe1Masse(updateComponentCe.getKe1Masse());
        }
        if (updateComponentCe.getKe1Mat() != null) {
            componentCe.setKe1Mat(updateComponentCe.getKe1Mat());
        }
        if (updateComponentCe.getKe1Zeit() != null) {
            componentCe.setKe1Zeit(updateComponentCe.getKe1Zeit());
        }
        if (updateComponentCe.getKe2() != null) {
            componentCe.setKe2(updateComponentCe.getKe2());
        }
        if (updateComponentCe.getKe2Zeit() != null) {
            componentCe.setKe2Zeit(updateComponentCe.getKe2Zeit());
        }
        if (updateComponentCe.getSTime() != null) {
            componentCe.setSTime(updateComponentCe.getSTime());
        }
        if(updateComponentCe.getMasseZeit() != null){
            componentCe.setMasseZeit(updateComponentCe.getMasseZeit());
        }
        if (updateComponentCe.getZeit() != null){
            componentCe.setZeit(updateComponentCe.getZeit());
        }
        if (updateComponentCe.getZeitIst() != null){
            componentCe.setZeitIst(updateComponentCe.getZeitIst());
        }
        if (updateComponentCe.getZeitSoll() != null){
            componentCe.setZeitSoll(updateComponentCe.getZeitSoll());
        }
        if (updateComponentCe.getComponentId() != null){
            componentCe.setComponentId(updateComponentCe.getComponentId());
        }

        return componentCeRepository.save(componentCe);
    }

}


