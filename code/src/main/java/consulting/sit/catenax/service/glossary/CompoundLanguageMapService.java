package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.model.glossary.CompoundLanguageMap;
import consulting.sit.catenax.model.glossary.PartLanguageMap;
import consulting.sit.catenax.repository.glossary.CompoundLanguageMapRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompoundLanguageMapService {

    private final CompoundLanguageMapRepository compoundLanguageMapRepository;

    public CompoundLanguageMapService(final CompoundLanguageMapRepository compoundLanguageMapRepository) {
        this.compoundLanguageMapRepository = compoundLanguageMapRepository;
    }

    public List<CompoundLanguageMap> findAllByLanguage(String language) {
        return compoundLanguageMapRepository.findAllByCompoundLanguageMapPk_Language(language);
    }
}
