package consulting.sit.catenax.service.project;

import consulting.sit.catenax.model.project.Variant;
import consulting.sit.catenax.repository.project.VariantAirbagRepository;
import consulting.sit.catenax.repository.project.VariantDetailRepository;
import consulting.sit.catenax.repository.project.VariantRepository;
import consulting.sit.catenax.service.GenericService;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VariantService extends GenericService<Variant, Integer> {

    private final VariantRepository repository;

    private final VariantDetailRepository variantDetailRepository;
    private final VariantAirbagRepository variantAirbagRepository;

    public VariantService(final VariantRepository repository, final VariantDetailRepository variantDetailRepository, final VariantAirbagRepository variantAirbagRepository) {
        this.repository = repository;
        this.variantDetailRepository = variantDetailRepository;
        this.variantAirbagRepository = variantAirbagRepository;
    }

    public List<Variant> findByProjectId(int projectId) {
        List<Variant> variants = repository.findByProjectId(projectId);
        return variants;
    }

    public Variant updateVariant(Variant updateVariant, Integer id) {
        Variant variant = repository.findById(id).orElseThrow(() -> new NullPointerException("Variant not found"));
        if (updateVariant.getVariantStatus() != null) {
            variant.setVariantStatus(updateVariant.getVariantStatus());
        }
        if (updateVariant.getVersion() != null) {
            variant.setVersion(updateVariant.getVersion());
        }
        if (updateVariant.getVariantType() != null) {
            variant.setVariantType(updateVariant.getVariantType());
        }
//        if (updateVariant.getCreatedAt() != null) {
//            variant.setCreatedAt(updateVariant.getCreatedAt());
//        }
        if (updateVariant.getCreatedBy() != null) {
            variant.setCreatedBy(updateVariant.getCreatedBy());
        }
//        if (updateVariant.getModifiedAt() != null) {
//            variant.setModifiedAt(updateVariant.getModifiedAt());
//        }
        if (updateVariant.getModifiedBy() != null) {
            variant.setModifiedBy(updateVariant.getModifiedBy());
        }
        if (updateVariant.getUntersuchungEnde() != null) {
            variant.setUntersuchungEnde(updateVariant.getUntersuchungEnde());
        }
        if (updateVariant.getUntersuchungStart() != null) {
            variant.setUntersuchungStart(updateVariant.getUntersuchungStart());
        }
        if (updateVariant.getDescription() != null) {
            variant.setDescription(updateVariant.getDescription());
        }
        if (updateVariant.getDismantlingStudy() != null) {
            variant.setDismantlingStudy(updateVariant.getDismantlingStudy());
        }
        if (updateVariant.getName() != null) {
            variant.setName(updateVariant.getName());
        }
        return repository.save(variant);

    }
}
