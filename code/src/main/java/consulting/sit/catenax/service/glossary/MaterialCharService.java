package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.repository.glossary.MaterialCharRepository;
import org.springframework.stereotype.Service;

@Service
public class MaterialCharService {
    private final MaterialCharRepository materialCharRepository;

    public MaterialCharService(MaterialCharRepository materialCharRepository) {
        this.materialCharRepository = materialCharRepository;
    }

    public Boolean existsByMaterialMain(int id) {
        return materialCharRepository.existsById(id);
    }
}
