package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.model.glossary.TableName;
import consulting.sit.catenax.repository.glossary.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class GlossaryService {

    @Autowired
    private FractionMainRepository fractionMainRepository;
    @Autowired
    private DocumentRepository documentRepository;
    @Autowired
    private ActivityAreaRepository activityAreaRepository;
    @Autowired
    private LanguageRepository languageRepository;
    @Autowired
    private FixingDropRepository fixingDropRepository;
    @Autowired
    private FixingDropCodeRepository fixingDropCodeRepository;
    @Autowired
    private MarkIdentRepository markIdentRepository;
    @Autowired
    private FixingLocationRepository fixingLocationRepository;
    @Autowired
    private FixingMainRepository fixingMainRepository;
    @Autowired
    private FixingCharRepository fixingCharRepository;
    @Autowired
    private CompoundRepository compoundRepository;
    @Autowired
    private AirbagPositionRepository airbagPositionRepository;
    @Autowired
    private MaterialMainRepository materialMainRepository;
    @Autowired
    private MaterialCharRepository materialCharRepository;
    @Autowired
    private MaterialFamilyRepository materialFamilyRepository;
    @Autowired
    private MaterialFamilyValueRepository materialFamilyValueRepository;
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private MaterialValueRepository materialValueRepository;
    @Autowired
    private MethodRepository methodRepository;
    @Autowired
    private MaterialGroupRepository materialGroupRepository;
    @Autowired
    private PositionRepository positionRepository;
    @Autowired
    private PartRepository partRepository;
    @Autowired
    private PtListingRepository ptListingRepository;
    @Autowired
    private RqTreatmentRepository rqTreatmentRepository;
    @Autowired
    private RgIsoGroupRepository rgIsoGroupRepository;
    @Autowired
    private RgIsoGroupValueRepository rgIsoGroupValueRepository;
    @Autowired
    private RgMaterialClassRepository rgMaterialClassRepository;
    @Autowired
    private PtMaterialMapRepository ptMaterialMapRepository;
    @Autowired
    private RegionRepository regionRepository;
    @Autowired
    private RgSeparabilityRepository rgSeparabilityRepository;
    @Autowired
    private RgStrategyRepository rgStrategyRepository;
    @Autowired
    private RgIsoCalcRepository rgIsoCalcRepository;
    @Autowired
    private RgMaterialGroupRepository rgMaterialGroupRepository;
    @Autowired
    private PtTreatmentMapRepository ptTreatmentMapRepository;
    @Autowired
    private UiRepository uiRepository;
    @Autowired
    private VariantSpecificationRepository variantSpecificationRepository;
    @Autowired
    private VariantTypeRepository variantTypeRepository;
    @Autowired
    private FractionWasteRepository fractionWasteRepository;

    @Autowired
    private ToolTypeRepository toolTypeRepository;

    @Autowired
    private ToolMainRepository toolMainRepository;

    @Autowired
    private MarkingRepository markingRepository;

    public Object getDataModel(final String tableName) {
        String tableNameUpper = tableName.toUpperCase();
        TableName name = TableName.valueOf(tableNameUpper);
        switch (name) {
            case G_DOCUMENT:
                return documentRepository.findAll();
            case G_ACTIVITY_AREA:
                return activityAreaRepository.findAll();
            case G_LANGUAGE:
                return languageRepository.findAll();
            case G_FIXING_DROP:
                return fixingDropRepository.findAll();
            case G_FIXING_DROP_CODE:
                return fixingDropCodeRepository.findAll();
            case G_MARK_IDENT:
                return markIdentRepository.findAll();
            case G_FRACTION_MAIN:
                return fractionMainRepository.findAll();
            case G_FIXING_LOCATION:
                return fixingLocationRepository.findAll();
            case G_FIXING_MAIN:
                return fixingMainRepository.findAll();
            case G_FIXING_CHAR:
                return fixingCharRepository.findAll();
            case G_COMPOUND:
                return compoundRepository.findAll();
            case G_FRACTION_WASTE:
                return fractionWasteRepository.findAll();
            case G_AIRBAG_POSITION:
                return airbagPositionRepository.findAll();
            case G_MATERIAL_CHAR:
                return materialCharRepository.findAll();
            case G_MATERIAL_FAMILY:
                return materialFamilyRepository.findAll();
            case G_MATERIAL_FAMILY_VALUE:
                return materialFamilyValueRepository.findAll();
            case G_NOTIFICATION:
                return notificationRepository.findAll();
            case G_MATERIAL_VALUE:
                return materialValueRepository.findAll();
            case G_METHOD:
                return methodRepository.findAll();
            case G_MATERIAL_GROUP:
                return materialGroupRepository.findAll();
            case G_POSITION:
                return positionRepository.findAll();
            case G_PART:
                return partRepository.findAll();
            case G_PT_LISTING:
                return ptListingRepository.findAll();
            case G_RQ_TREATMENT:
                return rqTreatmentRepository.findAll();
            case G_RG_ISO_GROUP:
                return rgIsoGroupRepository.findAll();
            case G_RG_ISO_GROUP_VALUE:
                return rgIsoGroupValueRepository.findAll();
            case G_RG_MATERIAL_CLASS:
                return rgMaterialClassRepository.findAll();
            case G_PT_MATERIAL_MAP:
                return ptMaterialMapRepository.findAll();
            case G_REGION:
                return regionRepository.findAll();
            case G_RG_SEPARABILITY:
                return rgSeparabilityRepository.findAll();
            case G_RG_STRATEGY:
                return rgStrategyRepository.findAll();
            case G_RG_ISO_CALC:
                return rgIsoCalcRepository.findAll();
            case G_RG_MATERIAL_GROUP:
                return rgMaterialGroupRepository.findAll();
            case G_PT_TREATMENT_MAP:
                return ptTreatmentMapRepository.findAll();
            case G_UI:
                return uiRepository.findAll();
            case G_VARIANT_SPECIFICATION:
                return variantSpecificationRepository.findAll();
            case G_VARIANT_TYPE:
                return variantTypeRepository.findAll();
            case G_MATERIAL_MAIN:
                return materialMainRepository.findAll();
            case G_MARKING:
                return markingRepository.findAll();
            case G_TOOL_MAIN:
                return toolMainRepository.findAll();
            case G_TOOL_TYPE:
                return toolTypeRepository.findAll();
            default:
                return ResponseEntity.notFound().build();
        }
    }
}
