package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.repository.glossary.MaterialValueRepository;
import org.springframework.stereotype.Service;

@Service
public class MaterialValueService {
    private final MaterialValueRepository materialValueRepository;

    public MaterialValueService(MaterialValueRepository materialValueRepository) {
        this.materialValueRepository = materialValueRepository;
    }

    public Boolean exitsByMaterialMain(int id) {
        return materialValueRepository.existsById(id);
    }
}
