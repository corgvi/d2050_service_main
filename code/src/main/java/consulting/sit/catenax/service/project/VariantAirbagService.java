package consulting.sit.catenax.service.project;

import consulting.sit.catenax.model.project.VariantAirbag;
import consulting.sit.catenax.repository.project.VariantAirbagRepository;
import consulting.sit.catenax.service.GenericService;
import org.springframework.stereotype.Service;

@Service
public class VariantAirbagService extends GenericService<VariantAirbag, Integer> {

    private final VariantAirbagRepository variantAirbagRepository;

    public VariantAirbagService(final VariantAirbagRepository variantAirbagRepository) {
        this.variantAirbagRepository = variantAirbagRepository;
    }

    public VariantAirbag updateVariantAirBag(final VariantAirbag updateVariantAirbag, final Integer id) {
        VariantAirbag variantAirbag = variantAirbagRepository.findById(id).orElseThrow(() -> new NullPointerException("variantAirbag not found"));

        if (updateVariantAirbag != null) {
            if (updateVariantAirbag.getVariantId() != null) {
                variantAirbag.setVariantId(updateVariantAirbag.getVariantId());
            }
            if (updateVariantAirbag.getAirbagPositionId() != null) {
                variantAirbag.setAirbagPositionId(updateVariantAirbag.getAirbagPositionId());
            }
            if (updateVariantAirbag.getExisting() != null) {
                variantAirbag.setExisting(updateVariantAirbag.getExisting());
            }
            if (updateVariantAirbag.getTyp() != null) {
                variantAirbag.setTyp(updateVariantAirbag.getTyp());
            }
        }
        return variantAirbagRepository.save(variantAirbag);
    }
}
