package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.model.glossary.MaterialMain;
import consulting.sit.catenax.repository.glossary.MaterialMainRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaterialMainSerivce {
    private final MaterialMainRepository materialMainRepository;

    public MaterialMainSerivce(MaterialMainRepository materialMainRepository) {
        this.materialMainRepository = materialMainRepository;
    }

    public List<MaterialMain> findMaterialMainWithoutMaterialChar() {
        return materialMainRepository.findMaterialMainWithoutMaterialChar();
    }
    public List<MaterialMain> findMaterialMainWithoutMaterialValue() {
        return materialMainRepository.findMaterialMainWithoutMaterialValue();
    }
    public List<MaterialMain> findMaterialMainWithoutMaterialGroup() {
        return materialMainRepository.findMaterialMainWithoutMaterialGroup();
    }
    public List<MaterialMain> findMaterialMainWithoutMaterialFamily() {
        return materialMainRepository.findMaterialMainWithoutMaterialFamily();
    }
}
