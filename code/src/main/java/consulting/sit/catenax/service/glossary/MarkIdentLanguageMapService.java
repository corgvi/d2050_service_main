package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.model.glossary.MarkIdentLanguageMap;
import consulting.sit.catenax.model.glossary.PartLanguageMap;
import consulting.sit.catenax.repository.glossary.MarkIdentLanguageMapRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MarkIdentLanguageMapService {

    private final MarkIdentLanguageMapRepository markIdentLanguageMapRepository;

    public MarkIdentLanguageMapService(final MarkIdentLanguageMapRepository markIdentLanguageMapRepository) {
        this.markIdentLanguageMapRepository = markIdentLanguageMapRepository;
    }


    public List<MarkIdentLanguageMap> findAllByLanguage(String language) {
        return markIdentLanguageMapRepository.findAllByMarkIdentLanguageMapPk_Language(language);
    }
}
