package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.repository.glossary.FixingDropCodeRepository;
import org.springframework.stereotype.Service;

@Service
public class FixingDropCodeService {
    private final FixingDropCodeRepository fixingDropCodeRepository;

    public FixingDropCodeService(FixingDropCodeRepository fixingDropCodeRepository) {
        this.fixingDropCodeRepository = fixingDropCodeRepository;
    }

    public Boolean existFixingDropId(int fixingDropId){
        return fixingDropCodeRepository.existsByFixingDropId(fixingDropId);
    }
}
