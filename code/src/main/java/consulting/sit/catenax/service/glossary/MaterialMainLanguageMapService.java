package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.model.glossary.MaterialMainLanguageMap;
import consulting.sit.catenax.repository.glossary.MaterialMainLanguageMapRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaterialMainLanguageMapService {

    private final MaterialMainLanguageMapRepository materialMainLanguageMapRepository;

    public MaterialMainLanguageMapService(final MaterialMainLanguageMapRepository materialMainLanguageMapRepository) {
        this.materialMainLanguageMapRepository = materialMainLanguageMapRepository;
    }

    public List<MaterialMainLanguageMap> findAllByLanguage(String language) {
        return materialMainLanguageMapRepository.findAllByMaterialMainLanguageMapPk_Language(language);
    }
}
