package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.repository.glossary.MaterialGroupRepository;
import org.springframework.stereotype.Service;

@Service
public class MaterialGroupService {
    private final MaterialGroupRepository materialGroupRepository;

    public MaterialGroupService(MaterialGroupRepository materialGroupRepository) {
        this.materialGroupRepository = materialGroupRepository;
    }

    public Boolean exitsByMaterialMain(int id) {
        return materialGroupRepository.existsById(id);
    }
}
