package consulting.sit.catenax.service.project;

import com.fasterxml.jackson.databind.JsonNode;
import consulting.sit.catenax.dto.ComponentMainDTO;
import consulting.sit.catenax.model.glossary.Part;
import consulting.sit.catenax.model.project.ComponentMain;
import consulting.sit.catenax.repository.project.ComponentMainRepository;
import consulting.sit.catenax.service.GenericService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ComponentMainService extends GenericService<ComponentMain, Integer> {
    private final ComponentMainRepository repository;

    public ComponentMainService(final ComponentMainRepository repository) {
        this.repository = repository;
    }

    public List<ComponentMain> findByVariantId(int variantId) {
        return repository.findByVariantId(variantId);
    }

//    public List<ComponentMainDTO> findComponentFirstByVariantId(int variantId) {
//        int levelInBom = 1;
//        List<ComponentMainDTO> componentMainDTOs = new ArrayList<>();
//        List<Object[]> objects = repository.findAllComponentIdByVariantIdAndLevelInBom(variantId, levelInBom);
//        for(Object[] results: objects){
//            ComponentMainDTO componentMainDTO = new ComponentMainDTO();
//            componentMainDTO.setId((Long) results[0]);
//            componentMainDTOs.add(componentMainDTO);
//        }
//        return componentMainDTOs;
//    }

    public List<ComponentMainDTO> findAllComponentIdByTop(long top) {
        List<ComponentMainDTO> componentMainDTOs = new ArrayList<>();
        List<Object[]> objects = repository.findAllComponentIdByTop(top);
        for(Object[] results: objects){
            ComponentMainDTO componentMainDTO = new ComponentMainDTO();
            componentMainDTO.setId((Integer) results[0]);
            componentMainDTOs.add(componentMainDTO);
        }
        return componentMainDTOs;
    }

    public List<ComponentMainDTO> findAllComponentIdByVariantIdAndTopIsNull(int variantId) {
        List<ComponentMainDTO> componentMainDTOs = new ArrayList<>();
        List<Object[]> objects = repository.findAllComponentIdByVariantIdAndTopIsNull(variantId);
        for(Object[] results: objects){
            ComponentMainDTO componentMainDTO = new ComponentMainDTO();
            componentMainDTO.setId((Integer) results[0]);
            componentMainDTOs.add(componentMainDTO);
        }
        return componentMainDTOs;
    }

    public Boolean checkExistByIdAndVariantId(int componentId, int variantId) {
        return repository.existsByIdAndVariantId(componentId, variantId);
    }

    public ComponentMain updateByFieldsOfComponentMain(int id, ComponentMain updateComponentMain) {
        ComponentMain componentMain = repository.findById(id).orElseThrow(() -> new NullPointerException());

        if (updateComponentMain.getAnzahl() != null) {
            componentMain.setAnzahl(updateComponentMain.getAnzahl());
        }

        if (updateComponentMain.getZbMasse() != null) {
            componentMain.setZbMasse(updateComponentMain.getZbMasse());
        }

        if (updateComponentMain.getZbTeil() != null) {
            componentMain.setZbTeil(updateComponentMain.getZbTeil());
        }

        if (updateComponentMain.getMasse() != null) {
            componentMain.setMasse(updateComponentMain.getMasse());
        }

        if (updateComponentMain.getMLiter() != null) {
            componentMain.setMLiter(updateComponentMain.getMLiter());
        }

        if (updateComponentMain.getBauGruppe() != null) {
            componentMain.setBauGruppe(updateComponentMain.getBauGruppe());
        }

        if (updateComponentMain.getVr1() != null) {
            componentMain.setVr1(updateComponentMain.getVr1());
        }

        if (updateComponentMain.getAusbaupflicht() != null) {
            componentMain.setAusbaupflicht(updateComponentMain.getAusbaupflicht());
        }

        if (updateComponentMain.getAnmerkung() != null) {
            componentMain.setAnmerkung(updateComponentMain.getAnmerkung());
        }

        if (updateComponentMain.getVariante() != null) {
            componentMain.setVariante(updateComponentMain.getVariante());
        }

        if (updateComponentMain.getReused() != null) {
            componentMain.setReused(updateComponentMain.getReused());
        }

        if (updateComponentMain.getSchadstoff() != null) {
            componentMain.setSchadstoff(updateComponentMain.getSchadstoff());
        }

        if (updateComponentMain.getSsAnteil() != null) {
            componentMain.setSsAnteil(updateComponentMain.getSsAnteil());
        }

        if (updateComponentMain.getSsComment() != null) {
            componentMain.setSsComment(updateComponentMain.getSsComment());
        }

        if (updateComponentMain.getBenennungOrginal() != null) {
            componentMain.setBenennungOrginal(updateComponentMain.getBenennungOrginal());
        }

        if (updateComponentMain.getVergleichsteil() != null) {
            componentMain.setVergleichsteil(updateComponentMain.getVergleichsteil());
        }

        if (updateComponentMain.getCpsc() != null) {
            componentMain.setCpsc(updateComponentMain.getCpsc());
        }

        if (updateComponentMain.getPosition() != null) {
            componentMain.setPosition(updateComponentMain.getPosition());
        }

        if (updateComponentMain.getPrefix() != null) {
            componentMain.setPrefix(updateComponentMain.getPrefix());
        }

        if (updateComponentMain.getMain() != null) {
            componentMain.setMain(updateComponentMain.getMain());
        }

        if (updateComponentMain.getSuffix() != null) {
            componentMain.setSuffix(updateComponentMain.getSuffix());
        }

        if (updateComponentMain.getWsKombinationen() != null) {
            componentMain.setWsKombinationen(updateComponentMain.getWsKombinationen());
        }

        if (updateComponentMain.getRecContent() != null) {
            componentMain.setRecContent(updateComponentMain.getRecContent());
        }

        if (updateComponentMain.getRecCategory() != null) {
            componentMain.setRecCategory(updateComponentMain.getRecCategory());
        }

        if (updateComponentMain.getRecPirPcr() != null) {
            componentMain.setRecPirPcr(updateComponentMain.getRecPirPcr());
        }

        if (updateComponentMain.getLaenge() != null) {
            componentMain.setLaenge(updateComponentMain.getLaenge());
        }

        if (updateComponentMain.getBreite() != null) {
            componentMain.setBreite(updateComponentMain.getBreite());
        }

        if (updateComponentMain.getHoehe() != null) {
            componentMain.setHoehe(updateComponentMain.getHoehe());
        }

        if (updateComponentMain.getTop() != null) {
            componentMain.setTop(updateComponentMain.getTop());
        }

        if (updateComponentMain.getLeft() != null) {
            componentMain.setLeft(updateComponentMain.getLeft());
        }

        if(updateComponentMain.getPVerwTxt() != null){
            componentMain.setPVerwTxt(updateComponentMain.getPVerwTxt());
        }

        if (updateComponentMain.getFuGr() != null) {
            componentMain.setFuGr(updateComponentMain.getFuGr());
        }

        if (updateComponentMain.getBauteilBesonderheit() != null) {
            componentMain.setBauteilBesonderheit(updateComponentMain.getBauteilBesonderheit());
        }

        if (updateComponentMain.getModulHersteller() != null) {
            componentMain.setModulHersteller(updateComponentMain.getModulHersteller());
        }

        if (updateComponentMain.getVerunreinigung() != null) {
            componentMain.setVerunreinigung(updateComponentMain.getVerunreinigung());
        }

        if (updateComponentMain.getRezyklatAnteil() != null) {
            componentMain.setRezyklatAnteil(updateComponentMain.getRezyklatAnteil());
        }

        if (updateComponentMain.getXPartRef() != null) {
            componentMain.setXPartRef(updateComponentMain.getXPartRef());
        }

        if (updateComponentMain.getSa() != null) {
            componentMain.setSa(updateComponentMain.getSa());
        }

        if (updateComponentMain.getEbene() != null) {
            componentMain.setEbene(updateComponentMain.getEbene());
        }
        if (updateComponentMain.getDirekt() != null) {
            componentMain.setDirekt(updateComponentMain.getDirekt());
        }

        if (updateComponentMain.getXcpsciiRef() != null) {
            componentMain.setXcpsciiRef(updateComponentMain.getXcpsciiRef());
        }

        if (updateComponentMain.getLevelInBom() != null) {
            componentMain.setLevelInBom(updateComponentMain.getLevelInBom());
        }

        if (updateComponentMain.getIdemNumberBom() != null) {
            componentMain.setIdemNumberBom(updateComponentMain.getIdemNumberBom());
        }

        if (updateComponentMain.getBagCoated() != null) {
            componentMain.setBagCoated(updateComponentMain.getBagCoated());
        }

        if (updateComponentMain.getBagMarked() != null) {
            componentMain.setBagMarked(updateComponentMain.getBagMarked());
        }

        if (updateComponentMain.getActivityAreaId() != null) {
            componentMain.setActivityAreaId(updateComponentMain.getActivityAreaId());
        }

        if (updateComponentMain.getComponentMainBenennungId() != null) {
            componentMain.setComponentMainBenennungId(updateComponentMain.getComponentMainBenennungId());
        }

        if (updateComponentMain.getMaterialMainId() != null) {
            componentMain.setMaterialMainId(updateComponentMain.getMaterialMainId());
        }

        if (updateComponentMain.getFractionMainId() != null) {
            componentMain.setFractionMainId(updateComponentMain.getFractionMainId());
        }

        if (updateComponentMain.getCreatedBy() != null) {
            componentMain.setCreatedBy(updateComponentMain.getCreatedBy());
        }

        if (updateComponentMain.getModifiedBy() != null) {
            componentMain.setModifiedBy(updateComponentMain.getModifiedBy());
        }
        if (updateComponentMain.getType() != null) {
            componentMain.setType(updateComponentMain.getType());
        }

        return repository.save(componentMain);
    }

    public List<ComponentMain> findAllByZbTeilIsTrue(){
        return repository.findAllByZbTeilIsTrue();
    }
}
