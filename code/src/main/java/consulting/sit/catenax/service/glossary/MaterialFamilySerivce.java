package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.repository.glossary.MaterialFamilyRepository;
import org.springframework.stereotype.Service;

@Service
public class MaterialFamilySerivce {
    private final MaterialFamilyRepository materialFamilyRepository;

    public MaterialFamilySerivce(MaterialFamilyRepository materialFamilyRepository) {
        this.materialFamilyRepository = materialFamilyRepository;
    }

    public Boolean existsByMaterialMain(int id) {
        return materialFamilyRepository.existsById(id);
    }
}
