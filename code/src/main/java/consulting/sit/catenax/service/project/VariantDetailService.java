package consulting.sit.catenax.service.project;

import consulting.sit.catenax.model.project.VariantDetail;
import consulting.sit.catenax.repository.project.VariantDetailRepository;
import consulting.sit.catenax.service.GenericService;
import org.springframework.stereotype.Service;

@Service
public class VariantDetailService extends GenericService<VariantDetail, Integer> {

    private final VariantDetailRepository variantDetailRepository;

    public VariantDetailService(final VariantDetailRepository variantDetailRepository) {
        this.variantDetailRepository = variantDetailRepository;
    }

    public VariantDetail updateVariantDetail(VariantDetail updateVariantDetail, Integer id) {
        VariantDetail variantDetail = variantDetailRepository.findById(id).orElseThrow(() -> new NullPointerException("VariantDetail not found"));

        if (updateVariantDetail.getHersteller() != null) {
            variantDetail.setHersteller(updateVariantDetail.getHersteller());
        }
        if (updateVariantDetail.getFahrzeugbezeichnung() != null) {
            variantDetail.setFahrzeugbezeichnung(updateVariantDetail.getFahrzeugbezeichnung());
        }
        if (updateVariantDetail.getKarosserie() != null) {
            variantDetail.setKarosserie(updateVariantDetail.getKarosserie());
        }
        if (updateVariantDetail.getFahrzeugTyp() != null) {
            variantDetail.setFahrzeugTyp(updateVariantDetail.getFahrzeugTyp());
        }
        if (updateVariantDetail.getFahrgestellNr() != null) {
            variantDetail.setFahrgestellNr(updateVariantDetail.getFahrgestellNr());
        }
        if (updateVariantDetail.getMotorTyp() != null) {
            variantDetail.setMotorTyp(updateVariantDetail.getMotorTyp());
        }
        if (updateVariantDetail.getMotorNummer() != null) {
            variantDetail.setMotorNummer(updateVariantDetail.getMotorNummer());
        }
        if (updateVariantDetail.getGetriebeTyp() != null) {
            variantDetail.setGetriebeTyp(updateVariantDetail.getGetriebeTyp());
        }
        if (updateVariantDetail.getGetriebeNummer() != null) {
            variantDetail.setGetriebeNummer(updateVariantDetail.getGetriebeNummer());
        }
        if (updateVariantDetail.getHerstellung() != null) {
            variantDetail.setHerstellung(updateVariantDetail.getHerstellung());
        }
        if (updateVariantDetail.getLeergewichtPraktisch() != null) {
            variantDetail.setLeergewichtPraktisch(updateVariantDetail.getLeergewichtPraktisch());
        }
        if (updateVariantDetail.getLeergewichtTheoretisch() != null) {
            variantDetail.setLeergewichtTheoretisch(updateVariantDetail.getLeergewichtTheoretisch());
        }
        if (updateVariantDetail.getReifen() != null) {
            variantDetail.setReifen(updateVariantDetail.getReifen());
        }
        if (updateVariantDetail.getRadlastHl() != null) {
            variantDetail.setRadlastHl(updateVariantDetail.getRadlastHl());
        }
        if (updateVariantDetail.getRadlastHr() != null) {
            variantDetail.setRadlastHr(updateVariantDetail.getRadlastHr());
        }
        if (updateVariantDetail.getRadlastVl() != null) {
            variantDetail.setRadlastVl(updateVariantDetail.getRadlastVl());
        }
        if (updateVariantDetail.getRadlastVr() != null) {
            variantDetail.setRadlastVr(updateVariantDetail.getRadlastVr());
        }
        if (updateVariantDetail.getZulaessigesGesamtgewicht() != null) {
            variantDetail.setZulaessigesGesamtgewicht(updateVariantDetail.getZulaessigesGesamtgewicht());
        }
        if (updateVariantDetail.getFarbe() != null) {
            variantDetail.setFarbe(updateVariantDetail.getFarbe());
        }
        if (updateVariantDetail.getFarbcode() != null) {
            variantDetail.setFarbcode(updateVariantDetail.getFarbcode());
        }
        if (updateVariantDetail.getFarbcodeInnen() != null) {
            variantDetail.setFarbcodeInnen(updateVariantDetail.getFarbcodeInnen());
        }
        if (updateVariantDetail.getKmStand() != null) {
            variantDetail.setKmStand(updateVariantDetail.getKmStand());
        }
        if (updateVariantDetail.getHerkunft() != null) {
            variantDetail.setHerkunft(updateVariantDetail.getHerkunft());
        }
        if (updateVariantDetail.getBatterieTyp12v() != null) {
            variantDetail.setBatterieTyp12v(updateVariantDetail.getBatterieTyp12v());
        }
        if (updateVariantDetail.getBatterieTyp24v() != null) {
            variantDetail.setBatterieTyp24v(updateVariantDetail.getBatterieTyp24v());
        }
        if (updateVariantDetail.getBatterieTyp48v() != null) {
            variantDetail.setBatterieTyp48v(updateVariantDetail.getBatterieTyp48v());
        }
        if (updateVariantDetail.getBatterieTypHv() != null) {
            variantDetail.setBatterieTypHv(updateVariantDetail.getBatterieTypHv());
        }
        if (updateVariantDetail.getReifen() != null) {
            variantDetail.setReifen(updateVariantDetail.getReifen());
        }
        if (updateVariantDetail.getModellNummer() != null) {
            variantDetail.setModellNummer(updateVariantDetail.getModellNummer());
        }
        if (updateVariantDetail.getBesonderheiten() != null) {
            variantDetail.setBesonderheiten(updateVariantDetail.getBesonderheiten());
        }

        return variantDetailRepository.save(variantDetail);

    }
}
