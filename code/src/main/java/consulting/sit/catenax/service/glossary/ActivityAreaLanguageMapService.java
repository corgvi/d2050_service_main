package consulting.sit.catenax.service.glossary;

import consulting.sit.catenax.model.glossary.ActivityAreaLanguageMap;
import consulting.sit.catenax.repository.glossary.ActivityAreaLanguageMapRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityAreaLanguageMapService {

    private final ActivityAreaLanguageMapRepository activityAreaLanguageMapRepository;

    public ActivityAreaLanguageMapService(final ActivityAreaLanguageMapRepository activityAreaLanguageMapRepository) {
        this.activityAreaLanguageMapRepository = activityAreaLanguageMapRepository;
    }

    public List<ActivityAreaLanguageMap> findAllActivityAreaLanguageMapByLanguage(String language) {
        return activityAreaLanguageMapRepository.findAllByActivityAreaLanguageMapPk_Language(language);
    }
}
